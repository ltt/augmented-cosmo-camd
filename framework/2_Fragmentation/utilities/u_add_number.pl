#!/usr/bin/perl

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#       
#    This file is part of the augmented COSMO-CAMD project which is released under the GNU v3.0 license. 
#         See file LICENSE for full license details
#
#    - AUTHORS: Yifan Wang, Lorenz Fleitmann
#      Institute of Technical Thermodynamics, RWTH Aachen University, 52062 Aachen, Germany
#
#
#
#   Utility:
#     - to number each fragments/molecules in one sdf file
#        The number will be added in the first line of each connetion table and starts from 1.
#        E.g. -------Nr.1
#     - input is a sdf-file
#     - call the function: ./u_add_number.pl file.name	
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


local($file) = @ARGV;

die "\nErr: This is no file called $file. Please check your input file name.\n\n" if !(-e $file);

my $filenew = $file.'_new';


open(FILEIN, "<$file");
open(FILEOUT, ">$filenew");

local $addnu = 1, $inu = 0;

while(<FILEIN>){
    
    my $conv = $_;
    
    if($addnu){
        
        $addnu = 0;
        $inu++;

        chop($conv);
        
        $conv = $conv.'----Nr.'.$inu."\n";
   
    }
    
    print FILEOUT $conv;
    
    if($conv =~/\$\$\$\$/){
        $addnu = 1;
    }
}

close FILEIN;
close FILEOUT;

rename $filenew, $file;

print "\n\t\t\t\tDone!\n\n";
