#!/usr/bin/perl

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#       
#    This file is part of the augmented COSMO-CAMD project which is released under the GNU v3.0 license. 
#         See file LICENSE for full license details
#
#    - AUTHORS: Yifan Wang, Lorenz Fleitmann
#      Institute of Technical Thermodynamics, RWTH Aachen University, 52062 Aachen, Germany
#
#
#
#   Utility:
#     - to compare USMILES from two files ( one txt and COSMO-CAND log file)
#        
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


local ($file2) = @ARGV;
local *IN, $file1 = 'molecule.txt';
my $i = 0, $j = 0, $if_read = 0, $line = '', @mol_USMILES = '', @mol_USMILES_2 = '', @get = ''; @nr = '';

# print header
my $text="
    ########################################################################\n\n
    \t\t\tcompare USMILES from two files\n\n
    ########################################################################\n\n";

system("echo \"$text\""); 

# read USMILES from txt
open (IN, $file1);

  while(<IN>){
  
    my @get = split(' ', $_);
    
    $nr[$i] = $get[0];
    $mol_USMILES[$i] = $get[@get-1];
    $mol_USMILES[$i] =~ s/\#/\+/g;

    $i++;
  }

close IN;

print "\tread $i USMILES from $file1\n\n";


#read USMILES from log file of COSMO-CAMD 
$i = 0;
open (IN, $file2);

  while(<IN>){
    
    $line = $_;
    
    if($if_read){
      
      @get = split(' ', $_);
    
      $mol_USMILES_2[$i] = $get[@get-1];
      $if_read = 0;

      $i++;
    }
   
    if ($line =~ /^Molecule No./){
      $if_read = 1;
    }
    
  }

close IN;

print "\tread $i USMILES from $file2\n\n\n";

@different = '';

foreach $i(0..@mol_USMILES){
  
  print "$nr[$i]\t$mol_USMILES[$i]\t\t\t$mol_USMILES_2[$i]\n";
  
  if ($mol_USMILES[$i] ne $mol_USMILES_2[$i]){
    print "\n-------------------------------------------------\n\n";
    $j++;
  }
}

$i = @mol_USMILES;

print "\tThere are $j different USMILES-Paar of $i\n\n\n";
