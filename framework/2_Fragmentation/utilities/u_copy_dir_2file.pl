#!/usr/bin/perl

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#       
#    This file is part of the augmented COSMO-CAMD project which is released under the GNU v3.0 license. 
#         See file LICENSE for full license details
#
#    - AUTHORS: Yifan Wang, Lorenz Fleitmann
#      Institute of Technical Thermodynamics, RWTH Aachen University, 52062 Aachen, Germany
#
#
#
#   Utility:
#     - copy all SDF-files in one file according to the ranking in TXT-file.
#     - at first: define the file-names and directory
#     - call ./u_copy_dir_2file.pl
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


# define the file name: $in_file, $out_file
my $in_file = 'Case2Top40.txt', $i = 0, $out_file = 'Case2.sdf';;

my @mol_name = '', @mol_USMILES = '';

local *IN, *OUT;

open(IN, $in_file);

while(<IN>){
    my $line = $_;
    
    my @get = split(' ', $line);
    
    $mol_name[$i] = $get[1];
    $mol_USMILES[$i] = $get[@get-1];

    $i++;
}

close IN;


# define the directory name.
chop(my $tmp = `ls ./sdf_Case2_delete/*.sdf`);

my @getbase = split(' ', $tmp);


open (OUT, ">$out_file");

foreach $i(0..@mol_name){
    
    foreach $j(0..@getbase-1){
        my $string = './sdf_Case2_delete/'.$mol_name[$i].'.sdf';
        
        print "$getbase[$j]        $string\n";
        if($getbase[$j] eq $string){
           
            $i_line = 0;
            
            open (IN, "<$getbase[$j]");
                while(<IN>){
                    if ($i_line eq 0){
                        $line_1 =  $mol_USMILES[$i]."--------------------$i\n";
                        print OUT $line_1;
                    }
                    else{
                        print OUT $_;
                    }
                    $i_line++;
                }
            close IN
        }
    }
}

close OUT;


print "\n\t\tDone!\n\n";


