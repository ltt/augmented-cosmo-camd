#!/usr/bin/perl

print "SPLIT OK\n";

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#       
#    This file is part of the augmented COSMO-CAMD project which is released under the GNU v3.0 license. 
#         See file LICENSE for full license details
#
#    - AUTHORS: Yifan Wang, Lorenz Fleitmann
#      Institute of Technical Thermodynamics, RWTH Aachen University, 52062 Aachen, Germany
#	 
#    - Info: all subs about split things
#
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


sub split_reactant{

# ----------
#
#   delete the useless atoms, like Y and the atoms, which combine with atom Y
#
# ----------


    ($file, $who_call_me, $ifrag_change_anchor) = @_; #$iFragAnchorChange
    
    &print_debug('split_reactant', $file, $who_call_me, $ifrag_change_anchor);

    local *INSP;
  
    if(($file eq '' )||(!(-f $file))){
        print "usage: \n nosel <file.sdf>\n";
        exit;
    };

	unlink $ft_diss;

	local $new_frag=1; #$flagnew
	$flagNum = 0;
    
#     local $molpingFindStart = 0;

    @frag_change_before = split('-', $ifrag_change_anchor); #FragAnchorChangeBefore
    
    @frag_change_after = ''; # @FragAnchorChangeAfter

	open(INSP,"<$file");
	
	while(<INSP>){
		
		if($new_frag){
			
            $masse=0;
			$i_moli++;
			$compt=0;
			$ig=1;
			$jg=0;
			@strx='';
			@stry='';
			@strz='';
			@atom='';
			@coval='';
			@fonc='';
			@ifonc='';
			@covfonc='';
			@bond='';
			@listb='';
			@typeb='';
			$blanc=' ';	
			@radius='';
			@lignebond='';		
			$atomlourd=0;
			$new_frag=0;
			$keepX="";
            $readtempXatom = 0;
			@sdf='';
			$flagNum++;
# 			$molpingFindStart++;
		};
		
		@getstr = split(' ',$_);
	
		$sdf[$compt]=$_;
		$compt++;
        
        
        # let defined atom retain
        if ($readtempXatom){
            @keepXatom = split(" ", $_);

            $keepX = $keepXatom[0];
            $readtempXatom = 0;
        }
        
		if (($compt > 4) && ($ig <= $istratom)){
			$strx[$ig]=$getstr[0];
			$stry[$ig]=$getstr[1];
			$strz[$ig]=$getstr[2];
			$atom[$ig]=$getstr[3];

			$atomlourd ++ if($getstr[3] ne 'H' && $getstr[3] ne 'X');
			$radius[$ig]=$tabR{$getstr[3]};
			$masse=$masse+$tabmm{$getstr[3]};
			$ig++;
		};
		if (($compt > 4) && ($ig > $istratom) && ($jg <=$istrbond)){
			if ($jg == 0){
				$jg++;
			}
			else{

                @coller=split(' *',$getstr[0]);
                @coller2=split(' *',$getstr[1]);
                if(@coller==6 && $getstr[1] ne ""){
                    $getstr[0]=$coller[0].$coller[1].$coller[2];
                    $getstr[2]=$getstr[1];
                    $getstr[1]=$coller[3].$coller[4].$coller[5];
                }
                elsif(@coller==6 && $getstr[1] eq ""){
                    $getstr[0]=$coller[0].$coller[1];
                    $getstr[1]=$coller[2].$coller[3].$coller[4];
                    $getstr[2]=$coller[5];
                }
                elsif(@coller==5){
                    if($_=~/^\s/){
                        $getstr[0]=$coller[0].$coller[1];
                        $getstr[2]=$getstr[1];
                        $getstr[1]=$coller[2].$coller[3].$coller[4];
                    }
                    else{
                        $getstr[0]=$coller[0].$coller[1].$coller[2];
                        $getstr[2]=$getstr[1];
                        $getstr[1]=$coller[3].$coller[4];
                    };
                }
                elsif(@coller==4){
                    if($_=~/^\s/){
                        $getstr[0]=$coller[0];
                        $getstr[2]=$getstr[1];
                        $getstr[1]=$coller[1].$coller[2].$coller[3];
                    }
                    else{
                        $getstr[0]=$coller[0].$coller[1].$coller[2];
                        $getstr[2]=$getstr[1];
                        $getstr[1]=$coller[3];
                    };					
                }
                elsif(@coller2==4){
                    $getstr[1]=$coller2[0].$coller2[1].$coller2[2];
                    $getstr[2]=$coller2[3];
                }
                elsif(@coller==7){
                    $getstr[0]=$coller[0].$coller[1].$coller[2];
                    $getstr[1]=$coller[3].$coller[4].$coller[5];
                    $getstr[2]=$coller[6];
                };
                
                die "\nErr: There is something wrong in the file $file. Please check if the sdf format is right.\n\n\n" 
                if ((!(looks_like_number($getstr[0]))) || (!(looks_like_number($getstr[1]))) || (!(looks_like_number($getstr[2]))) );
                
				$bond[$getstr[0]]=$bond[$getstr[0]].$blanc.$getstr[1].$blanc.$getstr[2];
				$listb[$getstr[0]]=$listb[$getstr[0]].$blanc.$getstr[1];
				$typeb[$getstr[0]]=$typeb[$getstr[0]].$blanc.$getstr[2];

				$bond[$getstr[1]]=$bond[$getstr[1]].$blanc.$getstr[0].$blanc.$getstr[2];
				$listb[$getstr[1]]=$listb[$getstr[1]].$blanc.$getstr[0];
				$typeb[$getstr[1]]=$typeb[$getstr[1]].$blanc.$getstr[2];


				$fonc[$getstr[0]]=$fonc[$getstr[0]].$blanc.$getstr[2].'-'.$atom[$getstr[1]].$blanc;
				$ifonc[$getstr[0]]=$ifonc[$getstr[0]].$blanc.$getstr[1].$blanc;
				$covfonc[$getstr[0]]=$covfonc[$getstr[0]].$blanc.$getstr[2];
				$coval[$getstr[0]]=$coval[$getstr[0]]+$getstr[2];

				$fonc[$getstr[1]]=$fonc[$getstr[1]].$blanc.$getstr[2].'-'.$atom[$getstr[0]].$blanc;
				$ifonc[$getstr[1]]=$ifonc[$getstr[1]].$blanc.$getstr[0].$blanc;
				$covfonc[$getstr[1]]=$covfonc[$getstr[1]].$blanc.$getstr[2];
				$coval[$getstr[1]]=$coval[$getstr[1]]+$getstr[2];
				$lignebond[$jg]=$_;
				$jg++;
			};
		};
		

		if ($compt == 4){
			$istratom=$getstr[0];
			$istrbond=$getstr[1];
			
			@coller=split(' *',$istratom);
			
			if(@coller>3 && @coller==6){
				$istratom=$coller[0].$coller[1].$coller[2];
				$istrbond=$coller[3].$coller[4].$coller[5];
			}
			elsif(@coller>3 && @coller==5){
				if($_=~/^\s/){
					$istratom=$coller[0].$coller[1];
					$istrbond=$coller[2].$coller[3].$coller[4];
				}
				else{
					$istratom=$coller[0].$coller[1].$coller[2];
					$istrbond=$coller[3].$coller[4];
				};
			};

			die "\nErr: There is something wrong in your inputfile. Please check if the sdf format is right.\n\n\n" if ((!(looks_like_number($istratom))) || (!(looks_like_number($istrbond))));
		};
		
		if ($_=~/<tempXatom>/){
            $readtempXatom = 1;
        }
		  
		if ($_=~/\$\$\$\$/){
			$new_frag=1;
			
			if($istratom > 0){
             
                
                &molping_find if ($who_call_me ne 'check_last');

                &convert;

                if ($who_call_me ne 'check_last'){
                    
                    &molping_change;
                    
                    $mol_main_tmp = ' '.$mol_main_tmp.' ';
                    
                    @moltemp = split(' ',$mol_main_tmp);
                    
                    $mol_main_tmp = join(' ', @moltemp);
                    
                    if($one){

                        open(OUT,">>$ft_diss");
                        
                        foreach $p (0..@sdf-1){
                            print OUT "$sdf[$p]";
                        };
                        close(OUT);
                    }
                    else{
                    };
                };
			};
			
			
		};
	
	
	};
	
	close(INSP);
	
}

sub convert{

#
#    copied from LEA3D
#
	&print_debug('convert');
	
	$one=0;
	$many=0;
	$suite=" 1 ";
	$continue=1;
	$bi=1;
	$pos=0;
	$findAnchoral = 0; 
	
	while($continue){

        @get=split(' ',$ifonc[$bi]);
                
                
        foreach $k (0..@get-1){	
            $suite=$suite."$get[$k] " if ($suite!~/ $get[$k] /); 	
        };
        
        @get2=split(' ',$suite);
                
        $pos++;

                
        if($pos == @get2){
            $continue=0;
        }
        else{
            $bi=$get2[$pos];
        };

	};

    @get2=split(' ',$suite);
    $nbacc= @get2;
    $acc=$suite;

    if(@get2 == $istratom){
        $one=1;
    }
    else{
        	#SPLIT LES FICHIERS
        @geto=split(' ',$suite);
		$atl=0;
		
		foreach $m (0..@geto-1){
			$atl++ if($atom[$geto[$m]] ne 'H' && $atom[$geto[$m]] ne 'X');
		};       	
                              
		&print($suite, $ifrag_change_anchor) if($suite=~/ $keepX /);
		
		$many++ if($suite=~/ $keepX /);

		$continue=1;
		
		while($continue){
            
            $suite=" ";
            $bi='';
		        
            foreach $k (1..$istratom){	
                $bi=$k if($acc!~/ $k /);
            };
            
            $suite=$suite."$bi ";
           
            $cont=1;
            $pos=0;
            
            while($cont){
                @get=split(' ',$ifonc[$bi]);
                
                foreach $k (0..@get-1){	
                        $suite=$suite."$get[$k] " if ($suite!~/ $get[$k] /); 	
                };
                @get2=split(' ',$suite);
                $pos++;
                if($pos == @get2){
                    $cont=0;
                }
                else{
                    $bi=$get2[$pos];
                };		        	
            };
           
			$acc=$acc.' '.$suite;
			@getd=split(' ',$acc);
		  	$continue=0 if(@getd >= $istratom);
		  	$continue=0 if($bi eq '');
		  	
		  	@geto=split(' ',$suite);
		  	$atl=0;
		  	foreach $m (0..@geto-1){
		  		$atl++ if($atom[$geto[$m]] ne 'H' && $atom[$geto[$m]] ne 'X');
		  	};
			#&print($suite) if($atl > 4);
			&print($suite, $ifrag_change_anchor) if($suite=~/ $keepX /);
			$many++ if($suite=~/ $keepX /);
        };
    };
    $one;
};

sub print{

#
# copied from LEA3D
#
    &print_debug('print');
    
    (local $atomes)=@_;

    $atomes=' '.$atomes.' ';
    
	@getp=split(' ',$atomes);

# 	print "$n_atom++@getp\n";
	
	foreach $iatom(0.. @getp-1){
        
        if ($getp[$iatom] > $n_atom){
          
#             $is_last = 1;

#         print "is_last is111: $is_last\n";
        }
	}
    
    if ($who_call_me ne 'check_last'){
        
        $longa = @getp;
        
        $longb=0;
        $gh=1;
        @ligneb='';
        $iFragAnchor = 1;
    #     $findAnchorNu = 0;
    # 	print("yuavor @findAnchor\n");
        
        my @findAnchorTemp = @findAnchor;
        
        foreach $f (($istratom+5-1)..@sdf-1){ # 5-1 to begin index 0 see $compt++
            #print "$f $sdf[$f]\n";
            
                @getb=split(' ',$sdf[$f]);

                @coller=split(' *',$getb[0]);
                @coller2=split(' *',$getb[1]);
                
                if(@coller==6 && $getb[1] ne ""){
                        $getb[0]=$coller[0].$coller[1].$coller[2];
                        $getb[2]=$getb[1];
                        $getb[1]=$coller[3].$coller[4].$coller[5];
                }
                elsif(@coller==6 && $getb[1] eq ""){
                        $getb[0]=$coller[0].$coller[1];
                        $getb[1]=$coller[2].$coller[3].$coller[4];
                        $getb[2]=$coller[5];
                }
                elsif(@coller==5){
                        if($sdf[$f]=~/^\s/){
                                $getb[0]=$coller[0].$coller[1];
                                $getb[2]=$getb[1];
                                $getb[1]=$coller[2].$coller[3].$coller[4];
                        }
                        else{
                                $getb[0]=$coller[0].$coller[1].$coller[2];
                                $getb[2]=$getb[1];
                                $getb[1]=$coller[3].$coller[4];
                        };
                }
                elsif(@coller==4){
                        if($sdf[$f]=~/^\s/){
                                $getb[0]=$coller[0];
                                $getb[2]=$getb[1];
                                $getb[1]=$coller[1].$coller[2].$coller[3];
                        }
                        else{
                                $getb[0]=$coller[0].$coller[1].$coller[2];
                                $getb[2]=$getb[1];
                                $getb[1]=$coller[3];
                        };					
                }
                elsif(@coller2==4){
                        $getb[1]=$coller2[0].$coller2[1].$coller2[2];
                        $getb[2]=$coller2[3];
                }
                elsif(@coller==7){
                        $getb[0]=$coller[0].$coller[1].$coller[2];
                        $getb[1]=$coller[3].$coller[4].$coller[5];
                        $getb[2]=$coller[6];
                };

            
                $gh=0 if($sdf[$f]=~/^>/ || $getb[0] eq '' || $sdf[$f]=~/^M/ || $sdf[$f]=~/^\$\$\$\$/);
                
                
                if($gh){
                    
                    if($atomes=~/ $getb[0] /){

                        $a1='';
                        $a2='';

                        foreach $p (0..@getp-1){ # INFO @getp=split(' ',$atomes);

                        #  $findAnchoral = 0;            
                            
                            if($getb[0] eq $getp[$p]){
                                
                                $a1=($p+1);
                            
                                foreach $ifindAnchor(0..@findAnchor-1){
                                    
    #                                 print("yuaTest+++$getp[$p]+$findAnchor[$ifindAnchor]) && ($findAnchoral)\n");
                                    
                                    if ($getp[$p] eq $findAnchor[$ifindAnchor]){
    #                                     print("find this 1\n");
                                        $findAnchor[$ifindAnchor] = '';
                                        $findAnchorTemp[$ifindAnchor] = $a1;
                                        $ifindAnchorTemp++;
                                    }
                                }
                                
                                foreach $iFragAnchor(0..@frag_change_before-1){
                                    
                                    if (($getp[$p] eq $frag_change_before[$iFragAnchor]) && ($keepX ne $frag_change_before[$iFragAnchor])){
                                        $frag_change_before[$iFragAnchor]= '';
                                        $frag_change_after[$iFragAnchor]= $flagNum.'*'.$a1;
                                    }
                                }
                            }
                            
                            if($getb[1] eq $getp[$p]){
                                
                                $a2=($p+1);
                                
                                foreach $ifindAnchor(0..@findAnchor-1){
                                    
    #                                 print("yuaTest+++$getp[$p]+$findAnchor[$ifindAnchor]) && ($findAnchoral)\n");
                                    
                                    if( $getp[$p]==$findAnchor[$ifindAnchor] ){
    #                                       print("find this 2\n");
                                        $findAnchor[$ifindAnchor]  = '';
                                        $findAnchorTemp[$ifindAnchor] = $a2;
                                        $findAnchoral = 1;
                                        $findAnchorNu++;
                                    }
                                }
                                
                                foreach $iFragAnchor(0..@frag_change_before-1){
                                    if (($getp[$p] eq $frag_change_before[$iFragAnchor]) && ($keepX ne $frag_change_before[$iFragAnchor])){
                                        
                                        $frag_change_before[$iFragAnchor]= '';
                                        $frag_change_after[$iFragAnchor]=$flagNum.'*'.$a2;
                                    }
                                }
                                
                            }
                        };
                        
                        $z="  0  0  0  0";
                        $ligneb[$longb]=sprintf"%3s%3s%3s  0  0  0  0",$a1,$a2,$getb[2];
            
                        $longb++;
    
                    };
                };
        };
        
        @findAnchor = @findAnchorTemp;

        open(OUT,">>dissociated.sdf");
        
        foreach $p (0..2){
            print OUT "$sdf[$p]";
        };
        
        printf OUT "%3s%3s  0  0  0  0  0  0  0  0999 V2000\n",$longa,$longb;
        
        
        foreach $f (0..@getp-1){
            print OUT "$sdf[$getp[$f]+3]";
        };

        foreach $f (0..@ligneb-1){
            print OUT "$ligneb[$f]\n";
        };	
                
        print OUT "M  END\n";
        
        $ecritfin=0;
        
        foreach $mp (0..@sdf-1){
            $ecritfin=1 if($sdf[$mp]=~/^>/);	
            print OUT "$sdf[$mp]" if($ecritfin);
        };
        
        print OUT "\$\$\$\$\n" if($ecritfin==0);	
        close(OUT);		
    };
};

sub molping_find{

# ----------
#
#   find the atoms in LEA-string need to be changed
#
# ----------

    &print_debug('molping_find');
    
    my $nanchor = 0, $tmp_mp = '', @fragment_mp = '';
    
    @findAnchor = '';

    foreach my $i_mol_frag($mol_start..$mol_end){

        $tmp_mp = ' '.$molping[$i_mol_frag].' ' if ($who_call_me eq 'main');
        $tmp_mp = ' '.$mp_main_tmp.' ' if ($who_call_me eq 'section');

        if($tmp_mp =~ / $i_moli /){

            @fragment_mp = split(' ', $molping[$i_mol_frag]) if ($who_call_me eq 'main');
            @fragment_mp = split(' ', $mp_main_tmp) if ($who_call_me eq 'section');
            
            foreach my $i(0..@fragment_mp-1){
                
                my $ele = $fragment_mp[$i];
                
                foreach my $j($i+1..@fragment_mp-1){
                    
                    if($fragment_mp[$j] == $ele){
                        $fragment_mp[$j] = '';
                    }
                }
            }
    
            foreach $i_molping(0..@fragment_mp-1){
                
                if ($fragment_mp[$i_molping] == $i_moli){ 
                       
                    @fragment_mol = split(' ',$mol[$i_mol_frag]) if ($who_call_me eq 'main');
                    @fragment_mol = split(' ',$mol_main_tmp) if ($who_call_me eq 'section');

                    foreach $ifrag_mol(0..@fragment_mol-1){
                    
                        @fragment_mol1 = split('-',$fragment_mol[$ifrag_mol]);
                        
                        foreach $ifrag_mol1(0..@fragment_mol1-1){

                            if ($fragment_mol1[$ifrag_mol1] =~/$i_moli\*/){
                                
                                @fragment_mol2=split('\*', $fragment_mol1[$ifrag_mol1]);
                                
                                $findAnchor[$nanchor] = $fragment_mol2[1];
#                               
                                $nanchor++;
                                
                                die "\nErr: defined in SPLIT.pl\n\n\n" if ($nachor > 1);

                            }
                            $fragment_mol[$ifrag_mol]=$fragment_mol2[0].'*'.$fragment_mol2[1];
                        }
                    }
                }
            }
        }
    }

}


sub molping_change{

# ----------
#
#   change the atoms in LEA-string need to be changed
#
# ----------

    &print_debug('molping_change');

    my $tmp_mp = '';
    $nanchor = 0;
    
    foreach my $i_mol_frag($mol_start..$mol_end){
        
        $tmp_mp = ' '.$molping[$i_mol_frag].' ' if ($who_call_me eq 'main');
        $tmp_mp = ' '.$mp_main_tmp.' ' if ($who_call_me eq 'section');
        
        if($tmp_mp =~ / $i_moli /){
            
            @fragment_mp = split(' ', $molping[$i_mol_frag]) if ($who_call_me eq 'main');
            @fragment_mp = split(' ', $mp_main_tmp) if ($who_call_me eq 'section');
            
            foreach $i(0..@fragment_mp-1){
                my $ele = $fragment_mp[$i];
                
                foreach $j($i+1..@fragment_mp-1){
                    if($fragment_mp[$j] eq $ele){
                        $fragment_mp[$j] = '';
                    }
                }
            }
        
            foreach $i_molping(0..@fragment_mp-1){
            
                if ($fragment_mp[$i_molping] eq $i_moli){
                    
                    @fragment_mol = split(' ',$mol[$i_mol_frag]) if ($who_call_me eq 'main');
                    @fragment_mol = split(' ',$mol_main_tmp) if ($who_call_me eq 'section');
                    
                    foreach $ifrag_mol(0..@fragment_mol-1){
                    
                        @fragment_mol1 = split('-',$fragment_mol[$ifrag_mol]);
                        
                        foreach $ifrag_mol1(0..@fragment_mol1-1){

                            if ($fragment_mol1[$ifrag_mol1] =~/$i_moli\*/){
                            
                                @fragment_mol2=split('\*', $fragment_mol1[$ifrag_mol1]);
                                
                                $fragment_mol2[1] = $findAnchor[$nanchor];
                                
                                $fragment_mol1[$ifrag_mol1]=join('*', @fragment_mol2);
                                
                                $nanchor++;
                                
                                die if ($nachor > 1);
                            }
                        }
                      
                        $fragment_mol[$ifrag_mol] = join('-', @fragment_mol1); 
                    }
                    
                    $mol[$i_mol_frag] = join(' ', @fragment_mol) if ($who_call_me eq 'main');
                    $mol_main_tmp = join(' ', @fragment_mol) if ($who_call_me eq 'section');
                }
            }
        }
    }
    
    
}
