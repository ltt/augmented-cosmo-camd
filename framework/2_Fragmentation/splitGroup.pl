#!/usr/bin/perl

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#       
#    This file is part of the augmented COSMO-CAMD project which is released under the GNU v3.0 license. 
#         See file LICENSE for full license details
#
#    - AUTHORS: Yifan Wang, Lorenz Fleitmann
#      Institute of Technical Thermodynamics, RWTH Aachen University, 52062 Aachen, Germany
#
#    - Info:
#        1) Subroutines are also found in READ.pl, FIND.pl, WRITE.pl, SPLIT.pl, SORT.pl.
#        2) Some Code are original from LEA3D.
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


#---------- header ----------
use File::Copy;
use List::Util qw( min max );
use Scalar::Util qw(looks_like_number);

local($operation, $inputfile) = @ARGV;

# # # to control
# ==========================================  
# to debug
$if_debug = 0; # only for debug
$if_delete = 1; # if delete the redundant fragments
$if_XH = 'withH';
#$if_XH = 'noH';
# ==========================================


# call subroutines
require "./READ.pl";
require "./WRITE.pl";
require "./FIND.pl";
require "./SPLIT.pl";
require "./SORT.pl";

# define global files name
&edit_file('define');

# delete all old files
&edit_file('delete_a');

# print header
&print_header; 

#---------- if the input is to read a directory ----------
if (($operation !~ /-/) && $operation ne ''){
    $inputfile = $operation;
    $operation = '';
}
elsif($operation eq '-r'){

    # if input is a directory, call the sub
    &read_directory; 
}
elsif($operation eq '-i'){
    
    &print_i;
}
else{
    die "\nErr: Cannot understand your input, please try ./splitGroup.pl -i\n\n\n";
}

# if inputfile is not correctly defined
if (!(-e $inputfile)||(-z $inputfile)){
    die "\nErr: Cannot find the inputfile!\n\n\n";
}
if (!(-f $inputfile)){
    die "\nErr: Not a file name, please check your inputfile\n\n\n";
}

# define and copy Hash -> the group names are important and muss be identical in the code
%funcGroup = (
        'CH3',      0,
        'OH',       0,
        'COOH',     0,
        'NHH',      0,
        'F',        0,
        'Cl',       0,
        'Br',       0,
        'I',        0,
        'CO2',      0,
        'CO2CO2',   0,
        'CO2N1',    0,
        'CO2O1',    0,
        'O',        0,
        'NH',       0,
        'C',        0,
        'N',        0,
        'CC2',      0,
        'CC3',      0,
        'CNO',      0,
);

# save initial HASH
%iniGroup0 = %{funcGroup};

# to write
%nrGroup = {%iniGroup0};
%combineGroup = {%iniGroup0};

# not use jet
%cornGroup = {%iniGroup0};
%cornbondGroup = {%iniGroup0};
%combineNoGroup = {%iniGroup0};
%combineKeepGroup =  {%iniGroup0};
%combineCorn = {%iniGroup0};

# read setting_group.in file
&read_settingin('1');

#---------- find cycle ----------
# find if there is a cycle every molecule
# if yes, change the bond information

# to remember the combination of cycle
@ncycle = '';
@cycle_atom = '';

&find_cycle($inputfile, 1);


## global variables
# for code
$i_mol = 0;   # $iMOl
$i_lego = 0;  # $i_lego
$i_moli = 0;  # $i_moli

$split_one_anchor = 0;

$fragDelete = '';
$failed_mol = '';

@mol='';
@molping='';
@lego ='';
@legono ='';
@typelego ='';
@points ='';

#---------- read inputfile ----------
#(big deal in it :D)

# $inputfile = 'new_'.$inputfile; # need to be changed later

local *DOC;

open(DOC, ">$ft_lib");

    &read_origin_mol($inputfile);

close DOC;


#---------- post-processing ----------

$i_moli = 0;
$mol_start = 1;
$mol_end = @molping;

# &split_reactant($fo_lib, 'main', '');

# delete new_input file
unlink $inputfile;


# file collection.sdf: change atoms H to X, write ><POINT> info
&change_H2X($fo_lib, $if_XH);

# write summary.out
&write_split_result($fo_str, 'all');

# delete temporary files
&edit_file('delete_e');


# delete redundant fragments
&sort_redundant if($if_delete);

#&change_H2X($fo_lib, 'withH'); # earlier the operator is withH


# change combination of cycle in collection.sdf

&find_cycle($fo_lib, 0);

# file new_collection.sdf: change atoms H to X, write ><POINT> info
$fo_lib_new = 'new_'.$fo_lib;
&change_H2X($fo_lib_new, $if_XH);

&find_cycle($fo_lib_new, 0);


# print footer
&print_footer;


# ------------------------------------
#
#  below are subs 
#
# ------------------------------------
sub control_cut{

# ----------
#
#    differentiate between single- and multi-anchor-groups 
#    if multi-anchor-groups: go to the split-loop (a big deal)
#
# ----------

    ($which_read_this_sub, local $ifrag_change) = @_; 
    
    &print_debug("controlCut", $which_read_this_sub, $ifrag_change);
    
    # define variable
    local $ifrag_change_anchor = '-';
    local $goto_next = 1; # to control the split of multi-anchor-molecule, every time only one group
    
    @ligneTemp = '';
    
    # copy the structure information of each molecule
    foreach my $li (0..@ligne-1){
        $ligneTemp[$li] = $ligne[$li];
    }
    
    if(($which_read_this_sub eq 'is_temp_frag') || ($which_read_this_sub eq 'is_origin')){ 
    
        if ($ngroup_multi == 0){
            
            # after split the molecule because of multi-anchor-groups, copy all fragments in one file diss_all.sdf -> to split one anchor groups. This sub is found in WRITE.pl
            # or there is no multi-anchor-groups, only single-anchor-groups
           
            &copy_all_in_one($ft_copy_all);
            
        }
        else{
            #---------- if there are more than one multi-anchor-fragment ----------
            
            # define variable

            my $ft_lib_1 = "outputTemp_1.sdf";
            local  $mol_sub_tmp = '', $mp_sub_tmp = '';
        
            #---------- 
            
            # mol and molping (anchor) musst be always changed, if a new decomposition is happened.
            # to record which molping need to be splited. 
            
            if($which_read_this_sub eq 'is_temp_frag'){

                die "\nErr: $mol and $molping of fragment $i_frag is leer.\n\n\n" if (($mol_main_tmp eq '') || ($mp_main_tmp eq ''));
                
                $mol_sub_tmp = $mol_main_tmp;
                $mp_sub_tmp = $mp_main_tmp;
                $mol_main_tmp = '';
                $mp_main_tmp = '';
               
                my @split_mp = split(' ', $mp_sub_tmp);  #@tempmolpingTempSub
                
#               print "split_mp is:@split_mp\n$ifrag_change++$nr_add_frag\n";
                
                $ifrag_change = $split_mp[$nr_add_frag]; ##$ifrag_change-1

                my @frag1 = split(' ', $mol_sub_tmp);
                
                foreach my $ifrag1(0..@frag1-1){
                    
                    my @frag2= split('-',$frag1[$ifrag1]);
                    
                    foreach my $ifrag2(0..@frag2-1){
                        
                        if ($frag2[$ifrag2] =~ /$ifrag_change\*/){
                            
                            my @temp = split('\*',$frag2[$ifrag2]);
                            $ifrag_change_anchor = $ifrag_change_anchor.$temp[1].'-';
                        }
                    }
                }
            }

            
            # to split the groups and write the intermediate data in the file outputTemp_1.sdf
            
            #---------- 
            

            open(TEMPDOC, ">$ft_lib_1");
        
                &fonction_multi;
         
            close TEMPDOC;

            local $mol_start = $i_mol, $mol_end = $i_mol;
        
            &split_reactant($ft_lib_1, 'section', $ifrag_change_anchor);
     
            if($which_read_this_sub eq 'is_temp_frag'){
            
                &change_mol_mp;   
                
            }
          
            $dis_file_new = 'dissociated_'.$new_dissociated.'.sdf'; #$disFilenameNew

            copy($ft_diss, $dis_file_new);
            
            $new_dissociated++;  
            
            my $OPENTMP;

            open ($OPENTMP, "<$dis_file_new");
        
                &read_temp_frag(*$OPENTMP);
           
            close $OPENTMP;

            unlink $ft_lib_1;

        }
    }
    elsif(($which_read_this_sub eq 'is_one_anchor')){
        
        if(($ngroup_single == 0)){
            
            &copy_all_in_one($ft_single);
        }
        else{
            
            my $ft_lib_1 = 'outputTemp_1.sdf';
            
            $mol_sub_tmp = '';
            $mp_sub_tmp = '';

            $mol_sub_tmp = $mol_main_tmp;
            $mp_sub_tmp = $mp_main_tmp;
            $mol_main_tmp = '';
            $mp_main_tmp = '';
                
            my @split_mp = split(' ', $mp_sub_tmp);

            $ifrag_change = $split_mp[$ifrag_change-1];

            my @frag1 = split(' ', $mol_sub_tmp);
                
            foreach my $ifrag1(0..@frag1-1){
                
                my @frag2= split('-',$frag1[$ifrag1]);
                
                foreach my $ifrag2(0..@frag2-1){
                    
                    if ($frag2[$ifrag2] =~ /$ifrag_change\*/){
                        my @temp = split('\*',$frag2[$ifrag2]);
                        $ifrag_change_anchor = $ifrag_change_anchor.$temp[1].'-';
                    }
                }
            }
            

            open(TEMPDOC, ">$ft_lib_1");
        
                &fonction_single;
        
            close TEMPDOC;
                
            $mol_start = $i_mol;
            $mol_end = $i_mol;
            
            &split_reactant($ft_lib_1, 'section', $ifrag_change_anchor);

            unlink $ft_lib_1;

                
            if (($mol_sub_tmp ne '') && ($mp_sub_tmp ne '')){
                
                &change_mol_mp;
            }

            local (*COLL, *READ);

            open (COLL, ">>$ft_single");
            open (READ, "<$ft_diss");
            
            while(<READ>){
                print COLL $_;
            }
            
            close COLL;
            close READ;
            
            unlink $ft_diss;

        }
    }

}



sub fonction_multi{

# ----------
# 
# give the important information to split the muti-anchor-groups. Attention: the rank hier is important
# 
# ----------
    
    &print_debug('fonction_multi');
    
    @ligneplus = '';
    
    local $lp = 0, $tempXatom = '';

    local $finbond = 5 + $nbatom + $nbbond;
    
    my $record_a, $record_g; 
    
    if (($funcGroup{CO2CO2}) && ($ngroupCO2CO2 > 0) && ($goto_next)){
        
        print "\t\tSplitting C(=O)C=O...\n" if($if_debug == 1);
        
        $record_a = '';
        $record_g = '';
        
        foreach $i(0..0){
            
            if($groupCO2CO2anchor[$i] =~/\+/){
                
                @tempgroupAnchor = split('\+', $groupCO2CO2anchor[$i]);
                @tempgroupAnchorG= split('\+', $groupCO2CO2anchorG[$i]);
                
                foreach $itemp(0..@tempgroupAnchor-1){
                    
                    @getanchor = split('-', $tempgroupAnchor[$itemp]);
                    $record_a = $record_a.'-'.$getanchor[0];
                    $tempXatom = $getanchor[0];

                    &updateLigne;
                    &write_ligne;
                    
                    if ($mp_main_tmp eq ''){
                        $mp_main_tmp = $i_lego;
                    }
                    else{
                        $mp_main_tmp =$mp_main_tmp.' '.$i_lego;
                    }
                }
                
                foreach $itemp(0..@tempgroupAnchor-1){
                
                        @getanchor = split('-', $tempgroupAnchorG[$itemp]); 
                        $record_g = $record_g.'-'.$getanchor[0];
                        $tempXatom = $getanchor[0];
                        
                        &updateLigne;
                }
                
                &write_ligne;
                $mp_main_tmp = $mp_main_tmp.' '.$i_lego;

            }
            else{
                my $recordtempXatom = '';
                my $checkcombine = '#';
                
                for $j(0..$ngroupCO2CO2-1){
                    
                    @getanchor = split('-', $groupCO2CO2anchor[$j]);
                    $tempXatom = $getanchor[0];
                    
                    if($checkcombine !~ /#$groupCO2CO2anchor[$j]#/){
                        if ($j == 0){
                            $recordtempXatom = $getanchor[0];
                            $record_a = $record_a.'-'.$getanchor[0];
                        }
                        else{
                            $record_a = $record_a.'-'.$recordtempXatom;
                        }
                        &updateLigne;
                        $checkcombine = $checkcombine.$groupCO2CO2anchorG[$j].'#';
                    }
                }
                
                if ($mp_main_tmp eq ''){
                    $mp_main_tmp = ($i_lego+1);
                }
                else{
                    $mp_main_tmp = $mp_main_tmp.' '.($i_lego+1);
                }
                
                &write_ligne;
                
                my $checkcombine = '#';
                
                foreach $j(0..$ngroupCO2CO2-1){
                
                    @getanchor = split('-', $groupCO2CO2anchorG[$j]);
                    
                    $tempXatom = $getanchor[0];
                    $record_g = $record_g.'-'.$tempXatom;
                    
                    if($checkcombine !~ /#$groupCO2CO2anchor[$j]#/){
                        $checkcombine = $checkcombine.$groupCO2CO2anchorG[$j].'#';
                        
                        &updateLigne;
                        &write_ligne;
                        
                        $mp_main_tmp = $mp_main_tmp.' '.$i_lego;
                    }
                }
            }
            
            @record_a1 = '';
            @record_g1 = '';
            @record_mp = '';
        
            @record_a1 = split('-', $record_a);
            @record_g1 = split('-', $record_g);
            @record_mp = split(' ', $mp_main_tmp);
            
            if($groupCO2CO2anchor[$i] =~ /\+/){
                
                my $j = @record_a1-1;
                
                foreach $i(1..@record_a1-1){
                    
                    if($mol_main_tmp eq ''){
                        $mol_main_tmp = $record_mp[$i-1].'*'.$record_g1[$i].'-'.$record_mp[@record_mp-1].'*'.$record_a1[$i];
                    }
                    else{
                        $mol_main_tmp = $mol_main_tmp.' '.$record_mp[$i-1].'*'.$record_g1[$i].'-'.$record_mp[@record_mp-1].'*'.$record_a1[$i];
                    }
                    $j--;
                }
                }
                else{
                
                my $j = 1;
            
                foreach $i(1..@record_a1-1){
                    
                    if($mol_main_tmp eq ''){
                        $mol_main_tmp = $record_mp[0].'*'.$record_g1[$i].'-'.$record_mp[$j].'*'.$record_a1[$i];
                    }
                    else{
                    
                        $mol_main_tmp = $mol_main_tmp.' '.$record_mp[0].'*'.$record_g1[$i].'-'.$record_mp[$j].'*'.$record_a1[$i];
                    }
                    $j++;
                }
                last;
                }
        }
        
        $goto_next = 0;
        $funcGroup{CO2CO2} = 0;
    
    }
    if (($funcGroup{CO2}) && ($ngroupCO2 > 0) && ($goto_next)){
        
        print "\t\tSplitting C=O...\n" if($if_debug == 1);
        
        $record_a = '';
        $record_g = '';
        
        foreach $i(0..0){
            if($groupCO2anchor[$i] =~ /\+/){
                
                @tempgroupAnchor = split('\+', $groupCO2anchor[$i]);
                @tempgroupAnchorG= split('\+', $groupCO2anchorG[$i]);
                
                foreach $itemp(0..@tempgroupAnchor-1){
                    
                    @getanchor = split('-', $tempgroupAnchor[$itemp]);
                    $record_a = $record_a.'-'.$getanchor[0];
                    $tempXatom = $getanchor[0];

                    &updateLigne;
                    &write_ligne;
                    
                    if ($mp_main_tmp eq ''){
                        $mp_main_tmp = $i_lego;
                    }
                    else{
                        $mp_main_tmp =$mp_main_tmp.' '.$i_lego;
                    }
                }
                
                foreach $itemp(0..@tempgroupAnchor-1){
                
                        @getanchor = split('-', $tempgroupAnchorG[$itemp]); 
                        $record_g = $record_g.'-'.$getanchor[0];
                        $tempXatom = $getanchor[0];
                        
                        &updateLigne;
                }
                
                &write_ligne;
                $mp_main_tmp = $mp_main_tmp.' '.$i_lego;

            }
            else{
                my $recordtempXatom = '';
                my $checkcombine = '#';
                
                for $j(0..$ngroupCO2-1){
                    
                    @getanchor = split('-', $groupCO2anchor[$j]);
                    $tempXatom = $getanchor[0];
                    
                    if($checkcombine !~ /#$groupCO2anchor[$j]#/){
                        if ($j == 0){
                            $recordtempXatom = $getanchor[0];
                            $record_a = $record_a.'-'.$getanchor[0];
                        }
                        else{
                            $record_a = $record_a.'-'.$recordtempXatom;
                        }
                        
                        &updateLigne;
                        $checkcombine = $checkcombine.$groupCO2anchorG[$j].'#';
                    }
                }
                
                if ($mp_main_tmp eq ''){
                    $mp_main_tmp = ($i_lego+1);
                }
                else{
                    $mp_main_tmp = $mp_main_tmp.' '.($i_lego+1);
                }
                
                &write_ligne;
                
                my $checkcombine = '#';
                
                for $j(0..$ngroupCO2-1){
                
                    @getanchor = split('-', $groupCO2anchorG[$j]);
                    
                    $tempXatom = $getanchor[0];
                    $record_g = $record_g.'-'.$tempXatom;
                    
                    if($checkcombine !~ /#$groupCO2anchor[$j]#/){
                        $checkcombine = $checkcombine.$groupCO2anchorG[$j].'#';
                        &updateLigne;
                        &write_ligne;
                        $mp_main_tmp = $mp_main_tmp.' '.$i_lego;
                    }
                }
            }
            
            @record_a1 = '';
            @record_g1 = '';
            @record_mp = '';
        
            @record_a1 = split('-', $record_a);
            @record_g1 = split('-', $record_g);
            @record_mp = split(' ', $mp_main_tmp);
            
            if($groupCO2anchor[$i] =~ /\+/){
                
                my $j = @record_a1-1;
                
                foreach $i(1..@record_a1-1){
                    
                    if($mol_main_tmp eq ''){
                        $mol_main_tmp = $record_mp[$i-1].'*'.$record_g1[$i].'-'.$record_mp[@record_mp-1].'*'.$record_a1[$i];
                    }
                    else{
                        $mol_main_tmp = $mol_main_tmp.' '.$record_mp[$i-1].'*'.$record_g1[$i].'-'.$record_mp[@record_mp-1].'*'.$record_a1[$i];
                    }
                    $j--;
                }
                }
                else{
                
                my $j = 1;
            
                foreach $i(1..@record_a1-1){
                    
                    if($mol_main_tmp eq ''){
                        $mol_main_tmp = $record_mp[0].'*'.$record_g1[$i].'-'.$record_mp[$j].'*'.$record_a1[$i];
                    }
                    else{
                    
                        $mol_main_tmp = $mol_main_tmp.' '.$record_mp[0].'*'.$record_g1[$i].'-'.$record_mp[$j].'*'.$record_a1[$i];
                    }
                    $j++;
                }
                last;
            }
        }
        
        $goto_next = 0;
        $funcGroup{CO2} = 0;
    }
    if (($funcGroup{CO2O1}) && ($ngroupCO2O1 > 0) && ($goto_next)){
        
        print "\t\tSplitting C(=O)O...\n" if($if_debug == 1);
        
        $record_a = '';
        $record_g = '';
        

        foreach $i(0..0){
        
            
            if($groupCO2O1anchor[$i] =~/\+/){
                
            
                @tempgroupAnchor = split('\+', $groupCO2O1anchor[$i]);
                @tempgroupAnchorG = split('\+', $groupCO2O1anchorG[$i]);
                
                
                foreach $itemp(0..@tempgroupAnchor-1){
                    
                    @getanchor = split('-', $tempgroupAnchor[$itemp]);
                    $record_a = $record_a.'-'.$getanchor[0];
                    $tempXatom = $getanchor[0];

                    &updateLigne;
                    &write_ligne;
                    
                    if ($mp_main_tmp eq ''){
                        $mp_main_tmp = $i_lego;
                    }
                    else{
                        $mp_main_tmp = $mp_main_tmp.' '.$i_lego;
                    }
                }
                
                foreach $itemp(0..@tempgroupAnchor-1){
                
                        @getanchor = split('-', $tempgroupAnchorG[$itemp]); 
                        $record_g = $record_g.'-'.$getanchor[0];
                        $tempXatom = $getanchor[0];
                        
                        &updateLigne;
                }
                &write_ligne;
                
                $mp_main_tmp = $mp_main_tmp.' '.$i_lego;
            
            }
            else{
                
                my $recordtempXatom = '';
                my $checkcombine = '#';
                
                for $j(0..$ngroupCO2O1-1){
                    
                    if ($groupCO2O1anchor[$j] =~ /\+/){
                        
                        @temp = split('\+', $groupCO2O1anchor[$j]);
                        $i_match = 0;
                            
                        foreach $i_temp(0..@temp-1){
                            
                            if($checkcombine !~ /#$temp[$i_temp]#/){
                                $i_match++;
                            }
                                
                            @getanchor = split('-', $temp[$i_temp]);
                            $tempXatom = $getanchor[0];
                        }
                            
                        if($i_match == @temp){
                            
                            $checkcombine = $checkcombine.$groupCO2O1anchorG[$j].'#';
                            &updateLigne;
                            $record_a = $record_a.'-'.$getanchor[0];
                        }

                    }
                    else{
                    
                        @getanchor = split('-', $groupCO2O1anchor[$j]);
                        $tempXatom = $getanchor[0];
                        
                        if($checkcombine !~ /#$groupCO2O1anchor[$j]#/){
#                             if ($j eq 0){
#                                 $recordtempXatom = $getanchor[0];
                            $record_a = $record_a.'-'.$getanchor[0];
#                             }
#                             else{
#                                 $record_a = $record_a.'-'.$recordtempXatom;
#                             }
                            &updateLigne;
                            $checkcombine = $checkcombine.$groupCO2O1anchorG[$j].'#';
                        }
                    }
                }
                if ($mp_main_tmp eq ''){
                    $mp_main_tmp = ($i_lego+1);
                }
                else{
                    $mp_main_tmp = $mp_main_tmp.' '.($i_lego+1);
                }
                &write_ligne;
                
                my $checkcombine = '#';
                
                for $j(0..$ngroupCO2O1-1){
                
                    @getanchor = split('-', $groupCO2O1anchorG[$j]);
                    
                    $tempXatom = $getanchor[0];
                    $record_g = $record_g.'-'.$tempXatom;
                    
                    if($checkcombine !~ /#$groupCO2O1anchor[$j]#/){
                        $checkcombine = $checkcombine.$groupCO2O1anchorG[$j].'#';
                        
                        &updateLigne;
                        &write_ligne;
                        
                        $mp_main_tmp = $mp_main_tmp.' '.$i_lego;
                    }
                }
            }

            @record_a1 = '';
            @record_g1 = '';
            @record_mp = '';
        
            @record_a1 = split('-', $record_a);
            @record_g1 = split('-', $record_g);
            @record_mp = split(' ', $mp_main_tmp);
            
            if($groupCO2O1anchor[$i] =~/\+/){
                
                my $j = @record_a1-1;
                
                foreach $i(1..@record_a1-1){
                    
                    if($mol_main_tmp eq ''){
                        $mol_main_tmp = $record_mp[$i-1].'*'.$record_g1[$i].'-'.$record_mp[@record_mp-1].'*'.$record_a1[$i];
                    }
                    else{
                        $mol_main_tmp = $mol_main_tmp.' '.$record_mp[$i-1].'*'.$record_g1[$i].'-'.$record_mp[@record_mp-1].'*'.$record_a1[$i];
                    }
                    $j--;
                }
                }
                else{
                
                my $j = 1;
            
                foreach $i(1..@record_a1-1){
                    
                    if($mol_main_tmp eq ''){
                        $mol_main_tmp = $record_mp[0].'*'.$record_g1[$i].'-'.$record_mp[$j].'*'.$record_a1[$i];
                    }
                    else{
                    
                        $mol_main_tmp = $mol_main_tmp.' '.$record_mp[0].'*'.$record_g1[$i].'-'.$record_mp[$j].'*'.$record_a1[$i];
                    }
                    $j++;
                }
                
                last;
                }
        }
        
        $goto_next = 0;
        $funcGroup{CO2O1} = 0;
    }
    if (($funcGroup{O}) && ($ngroupO > 0) && ($goto_next)){
        
        print "\t\tSplitting O...\n" if($if_debug == 1);

        $record_a = '';
        $record_g = '';
        
        foreach $i(0..0){
            if($groupOanchor[$i] =~/\+/){

                @tempgroupAnchor = split('\+', $groupOanchor[$i]);
                @tempgroupAnchorG= split('\+', $groupOanchorG[$i]);
                
                foreach $itemp(0..@tempgroupAnchor-1){
                    
                    @getanchor = split('-', $tempgroupAnchor[$itemp]);
                    $record_a = $record_a.'-'.$getanchor[0];
                    $tempXatom = $getanchor[0];
                    
                    &updateLigne;
                    &write_ligne;
                    
                    if ($mp_main_tmp eq ''){
                        $mp_main_tmp = $i_lego;
                    }
                    else{
                        $mp_main_tmp =$mp_main_tmp.' '.$i_lego;
                    }
                }
                
                foreach $itemp(0..@tempgroupAnchor-1){
                
                        @getanchor = split('-', $tempgroupAnchorG[$itemp]); 
                        $record_g = $record_g.'-'.$getanchor[0];
                        $tempXatom = $getanchor[0];
                        
                        &updateLigne;
                }
                &write_ligne;
                $mp_main_tmp = $mp_main_tmp.' '.$i_lego;
            }
            else{
                die "Err: problem of atom $atom[$bi]\n";
            }
        }

        $goto_next = 0;
        $funcGroup{O} = 0;
        
        @record_a1 = '';
        @record_g1 = '';
        @record_mp = '';
        
        @record_a1 = split('-', $record_a);
        @record_g1 = split('-', $record_g);
        @record_mp = split(' ', $mp_main_tmp);

        my $j = @record_a1-1;
        
        foreach $i(1..@record_a1-1){
            if($mol_main_tmp eq ''){
            
                $mol_main_tmp = $record_mp[$i-1].'*'.$record_g1[$j].'-'.$record_mp[@record_mp-1].'*'.$record_a1[$j];
                
            }
            else{
            
                $mol_main_tmp = $mol_main_tmp.' '.$record_mp[$i-1].'*'.$record_g1[$j].'-'.$record_mp[@record_mp-1].'*'.$record_a1[$j];
            }
            $j--;
        }

    }
    if (($funcGroup{CC3}) && ($ngroupCC3 > 0) && ($goto_next)){
    
        print "\t\tSplitting C+C...\n" if($if_debug == 1);
#         print "@groupCC3anchor\n\n@groupCC3anchorG\n";
        
        for $i(0..0){
         
            $record_a = '';
            $record_g = '';
            
            if($groupCC3anchor[$i] =~ /\+/){
                
                @tempgroupAnchor = split('\+', $groupCC3anchor[$i]);
                @tempgroupAnchorG= split('\+', $groupCC3anchorG[$i]);
                
                foreach $itemp(0..@tempgroupAnchor-1){
                    
                    @getanchor = split('-', $tempgroupAnchor[$itemp]);
                    $record_a = $record_a.'-'.$getanchor[0];
                    $tempXatom = $getanchor[0];
                    
                    &updateLigne;
                
                    &write_ligne;
                
                
                    if ($mp_main_tmp eq ''){
                        $mp_main_tmp = $i_lego;
                    }
                    else{
                        $mp_main_tmp = $mp_main_tmp.' '.$i_lego;
                    }
                
                }
                
                foreach $itemp(0..@tempgroupAnchor-1){
                
                        @getanchor = split('-', $tempgroupAnchorG[$itemp]); 
                        $record_g = $record_g.'-'.$getanchor[0];
                        $tempXatom = $getanchor[0];
                        &updateLigne;
                }
                    &write_ligne;
                    $mp_main_tmp = $mp_main_tmp.' '.$i_lego;
            }
            else{
                
                my $recordtempXatom = '';
                my $checkcombine = '#';
                
                for $j(0..$ngroupCC3-1){
                    
                    @getanchor = split('-', $groupCC3anchor[$j]);

                    $tempXatom = $getanchor[0];
                    
#                     print "$checkcombine==$groupCC3anchor[$j]\n";
                    
                    if($groupCC3anchor[$j] =~ /\+/){
                        
                        @temp = split('\+', $groupCC3anchor[$j]);
                        $i_match = 0;
                        
                        foreach $i_temp(0..@temp-1){
                            if($checkcombine !~ /#$temp[$i_temp]#/){
                                $i_match++;
                            }
                        }
                        
                        if($i_match == @temp){
                            $record_a = $record_a.'-'.$getanchor[0];                      
                            &updateLigne;
                            $checkcombine = $checkcombine.$groupCC3anchorG[$j].'#';
                        }
                    }
                    else{
                        if($checkcombine !~ /#$groupCC3anchor[$j]#/){
                
                            $record_a = $record_a.'-'.$getanchor[0];                      
                            &updateLigne;
                            $checkcombine = $checkcombine.$groupCC3anchorG[$j].'#';

                        }
                    }
                }
                
#                 print "++++++:$mp_main_tmp\n";
                
                if ($mp_main_tmp eq ''){
                    $mp_main_tmp = ($i_lego+1);
                }
                else{
                    $mp_main_tmp = $mp_main_tmp.' '.($i_lego+1);
                }

                &write_ligne;
                

                 
                my $checkcombine = '#';
            
                for $j(0..$ngroupCC3-1){

                    if($groupCC3anchor[$j] =~ /\+/){
                        
                        @temp = split('\+', $groupCC3anchor[$j]);
                        $i_match = 0;
                        
                        foreach $i_temp(0..@temp-1){
                            if($checkcombine !~ /#$temp[$i_temp]#/){
                                $i_match++;
                            }
                            
                            @getanchor = split('-', $temp[$i_temp]);
                    
                            $tempXatom = $getanchor[0];

                            $record_g = $record_g.'-'.$tempXatom;
                        
                        }
                        
                        if($i_match == @temp){
                            
                            $checkcombine = $checkcombine.$groupCC3anchorG[$j].'#';
                            &updateLigne;
                            $mp_main_tmp = $mp_main_tmp.' '.$i_lego;
                        }
                        else{
                        
                            @get = split('-', $record_g);
                            $tem = pop @get;
                            $record_g = join('-', @get),
                        }
                    }
                    else{
                        
                        @getanchor = split('-', $groupCC3anchorG[$j]);
                    
                        $tempXatom = $getanchor[0];

                        $record_g = $record_g.'-'.$tempXatom;
                        
                        if($checkcombine !~ /#$groupCC3anchor[$j]#/){
                            $checkcombine = $checkcombine.$groupCC3anchorG[$j].'#';
                            &updateLigne;
                            
                            &write_ligne;
                            
                            $mp_main_tmp = $mp_main_tmp.' '.$i_lego;

                        }
                    }
                }
            }
            
            @record_a1 = '';
            @record_g1 = '';
            @record_mp = '';
        
            
            @record_a1 = split('-', $record_a);
            @record_g1 = split('-', $record_g);
            @record_mp = split(' ', $mp_main_tmp);

            if($groupCC3anchor[$i] =~/\+/){
                
                my $j = @record_a1-1;
                
                foreach $i(1..@record_a1-1){
                    
                    if($mol_main_tmp eq ''){
                        $mol_main_tmp = $record_mp[$i-1].'*'.$record_g1[$i].'-'.$record_mp[@record_mp-1].'*'.$record_a1[$i];
                    }
                    else{
                        $mol_main_tmp = $mol_main_tmp.' '.$record_mp[$i-1].'*'.$record_g1[$i].'-'.$record_mp[@record_mp-1].'*'.$record_a1[$i];
                    }
                    
                    $j--;
                }
                }
                else{
                
                my $j = 1;
            
                foreach $i(1..@record_a1-1){
                    
                    if($mol_main_tmp eq ''){
                        $mol_main_tmp = $record_mp[0].'*'.$record_g1[$i].'-'.$record_mp[$j].'*'.$record_a1[$i];
                    }
                    else{
                    
                        $mol_main_tmp = $mol_main_tmp.' '.$record_mp[0].'*'.$record_g1[$i].'-'.$record_mp[$j].'*'.$record_a1[$i];
                    }
                    $j++;
                }
                
                last;
                }
        }
        
        $goto_next = 0;
        $funcGroup{CC3} = 0;

    }

    if (($funcGroup{CC2}) && ($ngroupCC2 > 0) && ($goto_next)){
        
        print "\t\tSplitting C=C...\n" if($if_debug == 1);

        for $i(0..0){
            
            $record_a = '';
            $record_g = '';
            
            if($groupCC2anchor[$i] =~/\+/){
                
                my $checkcombine = '#';
                
                @tempgroupAnchor = split('\+', $groupCC2anchor[$i]);
                @tempgroupAnchorG= split('\+', $groupCC2anchorG[$i]);
                
                $i_match = 0;
                @temp = split('\+', $groupCC2anchor[$i]);
                
                foreach $i_temp(0..@temp-1){
                    if($checkcombine !~ /#$temp[$i_temp]#/){
                        $i_match++;
                    }
                }
                
                if($i_match == @temp){
                
                    foreach $itemp(0..@tempgroupAnchor-1){
                        
                        @getanchor = split('-', $tempgroupAnchor[$itemp]);
                        $record_a = $record_a.'-'.$getanchor[0];
                        $tempXatom = $getanchor[0];
            
                        &updateLigne;
                        &write_ligne;
                        
                        if ($mp_main_tmp eq ''){
                            $mp_main_tmp = $i_lego;
                        }
                        else{
                            $mp_main_tmp = $mp_main_tmp.' '.$i_lego;
                        }

                    }
                    
                    foreach $itemp(0..@tempgroupAnchor-1){
                    
                            @getanchor = split('-', $tempgroupAnchorG[$itemp]); 
                            $record_g = $record_g.'-'.$getanchor[0];
                            $tempXatom = $getanchor[0];
                            &updateLigne;
                    }
                    &write_ligne;
                    $mp_main_tmp = $mp_main_tmp.' '.$i_lego;
                }
            }
            else{
            
                my $recordtempXatom = '';
                my $checkcombine = '#';
                
                for $j(0..0){ #$ngroupCC2-1

                    if($groupCC2anchor[$j] =~ /\+/){
                    
                        @temp = split('\+', $groupCC2anchor[$j]);
                        $i_match = 0;
                        
                        
                        foreach $i_temp(0..@temp-1){
                            if($checkcombine !~ /#$temp[$i_temp]#/){
                                $i_match++;
                            }
                            
                            @getanchor = split('-', $temp[$i_temp]);
                            $tempXatom = $getanchor[0];
                        }
                        
                        if($i_match == @temp){
                        
                            $checkcombine = $checkcombine.$groupCC2anchorG[$j].'#';
                            &updateLigne;
                            $record_a = $record_a.'-'.$getanchor[0];
                        }
                    }
                    else{
                        if($checkcombine !~ /#$groupCC2anchor[$j]#/){
                            @getanchor = split('-', $groupCC2anchor[$j]);
                            $tempXatom = $getanchor[0];
                            
                            $record_a = $record_a.'-'.$getanchor[0];

                            &updateLigne;
                            $checkcombine = $checkcombine.$groupCC2anchorG[$j].'#';
                        }
                    }
                }
                
                
                if ($mp_main_tmp eq ''){
                    $mp_main_tmp = ($i_lego+1);
                }
                else{
                    $mp_main_tmp = $mp_main_tmp.' '.($i_lego+1);
                }
                &write_ligne;

                my $checkcombine = '#';
                
                for $j(0..0){   #$ngroupCC2-1
        
                    @getanchor = split('-', $groupCC2anchorG[$j]);
                    $record_g = $record_g.'-'.$getanchor[0];
                    $tempXatom = $getanchor[0];
                    
                    if($groupCC2anchor[$j] =~ /\+/){
                        
                        @temp = split('\+', $groupCC2anchor[$j]);
                        $i_match = 0;
                        
                        foreach $i_temp(0..@temp-1){
                            
                            if($checkcombine !~ /#$temp[$i_temp]#/){
                                $i_match++;
                            }
                        }
                        
                        if($i_match == @temp){
                        
                            $checkcombine = $checkcombine.$groupCC2anchorG[$j].'#';
                            &updateLigne;
                            &write_ligne;
                            $mp_main_tmp = $mp_main_tmp.' '.$i_lego;
                        }
                        else{
                            @get = split('-', $record_g);
                            $tem = pop @get;
                            $record_g = join('-', @get);
                        
                        }
                    }
                    else{
                        if($checkcombine !~ /#$groupCC2anchor[$j]#/){
                            $checkcombine = $checkcombine.$groupCC2anchorG[$j].'#';
                            &updateLigne;
                            &write_ligne;
                            $mp_main_tmp = $mp_main_tmp.' '.$i_lego;
                        }
                    }
                }
            }
    
            @record_a1 = '';
            @record_g1 = '';
            @record_mp = '';
        
            @record_a1 = split('-', $record_a);
            @record_g1 = split('-', $record_g);
            @record_mp = split(' ', $mp_main_tmp);
            
            
            if ($groupCC2anchor[$i] =~/\+/){
                
                my $j = @record_a1-1;
                
                foreach $i(1..@record_a1-1){
                    if($mol_main_tmp eq ''){
                        $mol_main_tmp = $record_mp[$i-1].'*'.$record_g1[$i].'-'.$record_mp[@record_mp-1].'*'.$record_a1[$i];
                    }
                    else{
                        $mol_main_tmp = $mol_main_tmp.' '.$record_mp[$i-1].'*'.$record_g1[$i].'-'.$record_mp[@record_mp-1].'*'.$record_a1[$i];
                    }
                    $j--;
                }
            }
            else{
                my $j = 1;
                foreach $i(1..@record_a1-1){
                    if($mol_main_tmp eq ''){
                        $mol_main_tmp = $record_mp[0].'*'.$record_g1[$i].'-'.$record_mp[$j].'*'.$record_a1[$i];
                    }
                    else{
                    
                        $mol_main_tmp = $mol_main_tmp.' '.$record_mp[0].'*'.$record_g1[$i].'-'.$record_mp[$j].'*'.$record_a1[$i];
                    }
                    $j++;
                }
                
                last;
            }
        }
        $goto_next = 0;
        $funcGroup{CC2} = 0; 
        
    }

}

sub fonction_single{
# 
# give the important information to split the single-anchor-groups. Attention: the rank hier is not important
# 
    &print_debug('fonction_single');
    
    @ligneplus='';
    local $lp=0;
    local $tempXatom = '';

    $finbond=5+$nbatom+$nbbond;

    $record_a = '';
    $record_g = '';
    
    if (($funcGroup{OH}) && ($ngroupOH > 0)){
        
        print "\t\tSplitting OH...\n" if($if_debug == 1);
        
        for $i(0..$ngroupOH-1){
            @getanchor = split('-', $groupOHanchor[$i]);
            $tempXatom = $getanchor[0];

            $record_a = $record_a.'-'.$tempXatom;
            &updateLigne;
        } 
    }

    if (($funcGroup{CH3}) && ($ngroupCH3 > 0)){
        
        print "\t\tSplitting CH3...\n" if($if_debug == 1);
        
        for $i(0..$ngroupCH3-1){
            
            @getanchor = split('-', $groupCH3anchor[$i]);
            $tempXatom = $getanchor[0];
            
            $record_a = $record_a.'-'.$tempXatom;

            &updateLigne;

        } 
    }
    
    &write_ligne;
    
  
    if ($mp_main_tmp eq ''){
        $mp_main_tmp = $i_lego;
    }
    else{
        $mp_main_tmp = $mp_main_tmp.' '.$i_lego;
    }
    
    
    if (($funcGroup{OH}) && ($ngroupOH > 0)){

        for $i(0..$ngroupOH-1){
            
            @getanchor = split('-', $groupOHanchorG[$i]);
            
            $record_g = $record_g.'-'.$getanchor[0];

            $tempXatom = $getanchor[0];

            &updateLigne;
            &write_ligne;
            $i_addfrag++; 
           
            $mp_main_tmp = $mp_main_tmp.' '.$i_lego;
            
        } 
    }
    
    if (($funcGroup{CH3}) && ($ngroupCH3 > 0)){
        
        
        for $i(0..$ngroupCH3-1){
            
            @getanchor = split('-', $groupCH3anchorG[$i]);
            
            $record_g = $record_g.'-'.$getanchor[0];
        
            $tempXatom = $getanchor[0];

            &updateLigne;
            &write_ligne;
            $i_addfrag++; 

            $mp_main_tmp = $mp_main_tmp.' '.$i_lego;

        }
            
    }

    @record_a1 = '';
    @record_g1 = '';
    @record_mp = '';
            
                
    @record_a1 = split('-', $record_a);
    @record_g1 = split('-', $record_g);
    @record_mp = split(' ', $mp_main_tmp);
    
    my $j = 1;

    foreach $i(1..@record_a1-1){
                    
        if($mol_main_tmp eq ''){
            $mol_main_tmp = $record_mp[0].'*'.$record_g1[$i].'-'.$record_mp[$j].'*'.$record_a1[$i];
        }
        else{
            $mol_main_tmp = $mol_main_tmp.' '.$record_mp[0].'*'.$record_g1[$i].'-'.$record_mp[$j].'*'.$record_a1[$i];
        }
        $j++;
    }
}


sub updateLigne{
    
# ----------
#
#    find the relative atom combination, break the combination with atom X and Y
#
# ----------
    
    &print_debug('updateLigne');
    
    my $iatom, $ei, $ianchor, $atomx = 'X', $newatom, @getstr = '', @get1 = '';
    
    $getstr[4] ="0";
    $getstr[5] ="0";
    $getstr[6] ="0";
    $getstr[7] ="0";
    $getstr[8] ="0";
    $getstr[9]= "0";
    $getstr[10]="0";
    $getstr[11]="0";
    $getstr[12]="0";
    $getstr[13]="0";
    $getstr[14]="0";
    $getstr[15]="0";

    $saveXatom = $saveXatom.'-'.$getanchor[0]; # global variable

    for $iatom(1..$nbatom){
       
        if($iatom == $getanchor[0]){
            
            @getstr=split(' ', $ligne[4+$iatom]);
            
            $newatom=sprintf"%10s%10s%10s%1s%1s  %2s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s\n",$corx[$iatom],$cory[$iatom],$corz[$iatom],$blanc,$atomx,$getstr[4],$getstr[5],$getstr[6],$getstr[7],$getstr[8],$getstr[9],$getstr[10],$getstr[11],$getstr[12],$getstr[13],$getstr[14],$getstr[15];
            
            $ligne[$iatom+4]=$newatom;


            foreach $ei (5+$nbatom..$finbond){
                
                @get=split(' ',$ligne[$ei]);
            
                if($get[0]==$iatom || $get[1]==$iatom){
                    
                    foreach $ianchor(1..@getanchor-1){

                        if(($get[0]==$getanchor[$ianchor] )|| ($get[1]==$getanchor[$ianchor])){
                        
                            $atomH="Y";
                            $ligneplus[$lp]=sprintf"%10s%10s%10s%1s%1s  %2s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s\n",$corx[$iatom],$cory[$iatom],$corz[$iatom],$blanc,$atomH,$getstr[4],$getstr[5],$getstr[6],$getstr[7],$getstr[8],$getstr[9],$getstr[10],$getstr[11],$getstr[12],$getstr[13],$getstr[14],$getstr[15];
                            
                            @get1=split(' ',$ligne[4]);
                            $get1[0]++;
                            $ligne[4]=sprintf"%3s%3s  0  0  0  0  0  0  0  0999 V2000\n",$nbatom+1+$lp,$get1[1];
                            $newAtomY = 0;

                            if($get[0]==$iatom){
                                $ligne[$ei]=sprintf"%3s%3s%3s  0  0  0  0\n",$nbatom+1+$lp,$get[1],$get[2];
                            }
                            else{
                                $ligne[$ei]=sprintf"%3s%3s%3s  0  0  0  0\n",$nbatom+1+$lp,$get[0],$get[2];
                            };
                        
                            $lp++;
                        } 
                    }
                }
            }
        }
    }
   
}
