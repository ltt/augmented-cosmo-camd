#!/usr/bin/perl

print "SORT OK \n";

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#       
#    This file is part of the augmented COSMO-CAMD project which is released under the GNU v3.0 license. 
#         See file LICENSE for full license details
#
#    - AUTHORS: Yifan Wang, Lorenz Fleitmann
#      Institute of Technical Thermodynamics, RWTH Aachen University, 52062 Aachen, Germany
#	 
#    - Info: all subs about sort things
#
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# ------------------------------------
sub sort_molping{

# ----------
#
# Sort the lea-string, so they can be read by LEA3D.
#
# ----------

    &print_line('Sorting molping') if($if_debug);
    
    local $i, $j, $not_change, $car, @each_mp, @count_used = '';
    
    my $m_start1, $m_start2, $mol_end, $mp_save;

    # foreach molecule
    foreach $i_mol(1..@mol-1){

        print "this is $i_mol\n" if($if_debug == 10);
        print "in sort_molping:$mol[$i_mol]==$molping[$i_mol]\n" if($if_debug == 1);

        @each_mp = split(' ', $molping[$i_mol]);
        
        $mp_save = $molping[$i_mol];
        $molping[$i_mol] = '';
        $mp_search = '';
        
        $not_change = 0;
        @count_used = '';

         # if the molecule consists of more than one fragment
        
        if(@each_mp < 3){
            $not_change = 1;
        }
        elsif(@each_mp > 2){
        
            $m_start1 = 0;
            $m_start2 = 1;
            
            $molping[$i_mol] = '';
            $mol_end = '';
            
            $car = $mol[$i_mol];
            
            @get_mol = split(' ', $mol[$i_mol]);

            foreach $j(0..@each_mp-1){

                # count how many the fragment $each_mp[$j] is used
                my $ncc1 = $car =~s/^$each_mp[$j]\*/^$each_mp[$j]\*/g;
                my $ncc2 = $car =~s/ $each_mp[$j]\*/ $each_mp[$j]\*/g;
                my $ncc3 = $car =~s/-$each_mp[$j]\*/-$each_mp[$j]\*/g;

                my $ncc = $ncc1+$ncc2+$ncc3;
                
#                 print "$each_mp[$j]\t$ncc\n";
                
                $count_used[$j] = $ncc;
            
                if ($ncc eq @get_mol){
                    
                    if ($j eq 0){
                        $not_change = 1;
                    }
                    else{
                        $mp_search = $each_mp[$j];
                    }
                    last; 
                }
                
            }
            
            @get_mol = '';
            
            @get_mol = split(' ', $mol[$i_mol]);
            
            $count_1 = 0;
            $count_2 = 0;
            
            if ($mp_search eq ''){

                foreach $j(0..@count_used-1){
                    $count_1++ if ($count_used[$j] == 1);
                    $count_2++ if ($count_used[$j] == 2);
                }
                
                if(($count_1 == 2) && (@count_used == ($count_1 + $count_2))){
                    
                    foreach $j(0..@count_used-1){
                        $mp_search = $each_mp[$j] if ($count_used[$j] == 1);
                    }
                
                }
                
                if ($mp_search eq ''){
                    $max_used = max @count_used;
                
                
                    foreach $j(0..@count_used-1){
                        if ($count_used[$j] eq $max_used){
                            $mp_search = $each_mp[$j];
                            last;
                        }
                    }       
#                     print "@count_used++$max_used\n";
                }
            }
            
            $molping[$i_mol] = $mp_search;
        }

        $is_all_de = 0;
        
        @get_mol='';
        @get_mol = split(' ', $mol[$i_mol]);
        
        $count_gm = @get_mol;
        $max_used++ if ($max_used > 1);
        

        my $i_while = 0;
        my $used_mp_start = '';
        my $atom_save = '';
        my $j_while = 0;
        
        $used_mp_start = ' '.$mp_search.' ';
        $atom_save = ' '.$mp_search.' ';
            
            while(($is_all_de < $count_gm) && (!$not_change)){
                
                foreach $iget_mol(0..@get_mol-1){
                   
                    if ($max_used > 1){
                    
                        @get = split(' ', $atom_save);
                        
                        $mp_search = $get[0];
                        $max_used--,

                    }
                    
                    if($get_mol[$iget_mol] ne ''){
                    
                        print "vor:$get_mol[$iget_mol]\n" if($if_debug == 10);
                    
                        $molping_tmp = ' '.$molping[$i_mol].' ';
                    
                        $used_mp_start = $used_mp_start.' '.$mp_search.' ' if ($used_mp_start !~ / $mp_search /);
                    
                        $changed = 0;
                    
                        if($get_mol[$iget_mol] =~ /^$mp_search\*/){
                        
                            my @mol2 = split('-', $get_mol[$iget_mol]);
                            my @mol3 = split('\*', $mol2[1]);

                            
                            @get = split(' ', $atom_save);
                            
                            if ($atom_save !~ / $mp_search /){
                                $atom_save = $atom_save.' '.$mp_search.' ';
                                $m_start1 = @get + 1;
                            }
                            else{
                            
                                foreach $j(0..@get-1){
                                    $m_start1 = $j + 1 if ($mp_search eq $get[$j]);
                                }
                            }
                            
                            $mp_search = $mol3[0];
                            
                            @get = split(' ', $atom_save);
                            
                            if ($atom_save !~ / $mp_search /){
                                $atom_save = $atom_save.' '.$mp_search.' ';
                                $m_start2 = @get + 1;
                            }
                            else{
                                
#                                 @get = split(' ', $atom_save);
                        
                                foreach $j(0..@get-1){
                                    $m_start2 = $j + 1 if ($mp_search eq $get[$j]);
                                }
                            }
                        
                            if($molping_tmp=~/ $mol3[0] /){
                                
                                $mol2[1] = $m_start2.'*'.$mol3[1];
                                my @mol3 = split('\*', $mol2[0]);
                                $mol2[0] = $m_start1.'*'.$mol3[1];
                                
                                
                            }
                            else{
                                
                                $molping[$i_mol] = $molping[$i_mol].' '.$mol3[0];
                                $mol2[1] = $m_start2.'*'.$mol3[1];
                            
                                my @mol3 = split('\*', $mol2[0]);
                            
                                $mol2[0] = $m_start1.'*'.$mol3[1];
                            
                            }
                        
                            $changed = 1;
                            $get_mol[$iget_mol] = join('-',@mol2);
                            
                            if ($mol_end eq ''){
                                $mol_end = $get_mol[$iget_mol];
                            }
                            else{
                                $mol_end = $mol_end.'_'.$get_mol[$iget_mol];
                            }
                            
                            $get_mol[$iget_mol] = '';
                            $is_all_de++;

                        }
                        elsif($get_mol[$iget_mol]=~/-$mp_search\*/){
                        
                            my @mol2 = split('-', $get_mol[$iget_mol]);
                            my @mol3 = split('\*', $mol2[0]);
                            
                            @get = split(' ', $atom_save);
                            
                            if ($atom_save !~ / $mp_search /){
                                $atom_save = $atom_save.' '.$mp_search.' ';
                                $m_start1 = @get + 1;
                            }
                            else{
#                                 @get = split(' ', $atom_save);
                        
                                foreach $j(0..@get-1){
                                    $m_start1 = $j + 1 if ($mp_search eq $get[$j]);
#                                   print "\nm_start1 is $m_start1\n";
                                }
                            }
                            
                            $mp_search = $mol3[0];
                            
                            @get = split(' ', $atom_save);

                            if ($atom_save !~ / $mp_search /){
                                $atom_save = $atom_save.' '.$mp_search.' ';
                                $m_start2 = @get +1 ;
                            }
                            else{
                                
                                @get = split(' ', $atom_save);
                        
                                foreach $j(0..@get-1){
                                    $m_start2 = $j +1 if ($mp_search eq $get[$j]);
                                }
                            }
                            
#                             print "m_star is: $m_start1+++$m_start1";
                            
                            if($molping_tmp=~/ $mol3[0] /){

                                $mol2[0] = $m_start1.'*'.$mol3[1];
                                my @mol3 = split('\*', $mol2[1]);
                                
                                $mol2[1] = $m_start2.'*'.$mol3[1];
                                
                                if ($m_start2 < $m_start1 ){
                            
                                $get_mol[$iget_mol] = $mol2[1].'-'.$mol2[0];
                                }
                                else{
                                
                                    $get_mol[$iget_mol] = join('-',@mol2);
                                }
                                
                            }
                            else{
                                
                                $molping[$i_mol] = $molping[$i_mol].' '.$mol3[0];
                                
                                $mol2[0]=$m_start2.'*'.$mol3[1];
                                my @mol3 = split('\*', $mol2[1]);
                                $mol2[1]=$m_start1.'*'.$mol3[1];
                                $get_mol[$iget_mol] = $mol2[1].'-'.$mol2[0];
                            }
                            
                            $changed = 1;
                            
                            if ($mol_end eq ''){
                                $mol_end = $get_mol[$iget_mol];
                            }
                            else{
                                $mol_end = $mol_end.'_'.$get_mol[$iget_mol];
                            }
                            
                            $get_mol[$iget_mol] = '';
                            $is_all_de++;
                        }

#                         print "nach:$mol_end\n\n" if($if_debug == 10);
                        }
                    }
                
                    if (~$changed){
                        
                        my @get = split(' ', $atom_save);

                        $mp_search = $get[$j_while];

                        $j_while++;
                        
                        $j_while = 0 if ($j_while == @get);
                        
                    
                    }
                    
                    $i_while++;
                    
                    die if($i_while > 100); # not stark!!!!!!!!
                }
                
            if ($not_change == 1){
                 
                $mp_start = 1;
            
                foreach $j(0..@each_mp-1){
                                                    
                    $mol[$i_mol]=~s/^$each_mp[$j]\*/$mp_start\*/g;
                    $mol[$i_mol]=~s/ $each_mp[$j]\*/ $mp_start\*/g;
                    $mol[$i_mol]=~s/-$each_mp[$j]\*/-$mp_start\*/g;
                    $mp_start++;
                }
            
                $mol[$i_mol] =~ s/ /_/g;
                $molping[$i_mol] = $mp_save;    
            }
            else{
                $mol[$i_mol] = $mol_end;
            }
            
#             print "nach:mol[$i_mol]==$molping[$i_mol]\n";
        }
}

sub sort_redundant{

# ----------
#
# delete the redundant fragments
# -> the mol and molping also need to be changed
#
# ----------

    &print_line('Deleting redundant fragments');
    
    # define file name
    local $in_su = $fo_str, $in_co = $fo_lib, $out_su = 'new_'.$in_su, $out_co = 'new_'.$in_co;
    
    # define local variables
    local *OUT_CO, $no_repeat = ' ', $nr_mol = 0, @c_point = '', @FONC = '', @IFONC = '', @NBATOM = '', @NBBOND = '', @ATOM ='', @XPOINT='', @LIGEND = '';

    # read all fragments
    &read_all_frag($in_co, 'sort_redundant');
    
    # read mol and molping from summary.out
    &read_str($in_su);

    # for debug
    if($debug == 10){
        
        print "mol and molping\n";
        foreach $i(0..@mol){
            print"\t\t$mol[$i]\t\t$molping[$i]\n";
        }
    }
    
    
    open(OUT_CO, ">>$out_co");

    foreach $i(1..@FONC-1){

        $a_w = 0;
        my $re_j = '';
         
        foreach $j($i+1..@FONC){

            if(($NBATOM[$i] ne 0) && ($NBATOM[$i] eq $NBATOM[$j])&&($NBBOND[$i] eq $NBBOND[$j])){

                my $find_same_combine = 0;
                my $find_same_atom = 0;
                
                my @atomi = split('\+\+\+', $ATOM[$i]);
                my @atomj = split('\+\+\+', $ATOM[$j]);
                
                my @fonci = split('\+\+\+', $FONC[$i]);
                my @foncj = split('\+\+\+', $FONC[$j]);

                
                foreach my $iatomi(0..@atomi){
                    
                    
                    my @fonc_i_temp = split(' ', $fonci[$iatomi]);
                    my @fonc_i_tempsort = sort(@fonc_i_temp);
                    
                    foreach my $jatomj(0..@atomj){
                        
                        $find_same_combine = 0;
                        
                        my @fonc_j_temp = split(' ', $foncj[$jatomj]);
                        my @fonc_j_tempsort = sort(@fonc_j_temp);
                        
                        #print "$iatomi\t $jatomj\t$atomi[$iatomi]\t$atomj[$jatomj]\n@fonc_i_tempsort\n@fonc_j_tempsort\n\n";
                        
                        if(($atomi[$iatomi] eq $atomj[$jatomj]) && ($atomj[$jatomj] ne '')){

                            foreach my $eachTerm(0..@fonc_i_tempsort-1){
                                
                                my @get_i = split('-', $fonc_i_tempsort[$eachTerm]);
                                my @get_j = split('-', $fonc_j_tempsort[$eachTerm]);
                                
                                #print "$get_i[0] == $get_j[0]) && ($get_i[1] eq $get_j[1] 66 $find_same_combine\n";
                                
                                if (( $get_i[0] == $get_j[0]) && ($get_i[1] eq $get_j[1])){
                                    
                                    $find_same_combine++;
                                   
                                }
                            }
                        }
                        
                       # print "($find_same_combine == @fonc_i_tempsort)\n";
                        
                        if (($find_same_combine == @fonc_i_tempsort) && ($find_same_combine ne 0)){
                            $find_same_atom++;
                            $find_same_combine = 0;
                             $atomj[$jatomj] = '';
                            last;
                        }
                    }
                    
                    $find_same_combine = 0;
                }
                
                if ($find_same_atom eq $NBATOM[$i]){
                    
                    if (!($a_w)){
                        print OUT_CO $LIGEND[$i];
                        $nr_fragment++;
                        $a_w = 1;
                    }

                    $FONC[$j] = ''; 
                    $NBATOM[$j] = 0;
                    @XPOINTTemp = '';
                    @XPOINT1 = '';
                    @XPOINT2 = '';
                    
                    $no_repeat = $no_repeat.$i.' ' if($no_repeat !~ / $i /);

                    @XPOINTTemp = split('-', $XPOINT[$i]);
                    
                    @XPOINT1 = sort(@XPOINTTemp);

                    @XPOINTTemp = split('-', $XPOINT[$j]);
                    
                    @XPOINT2 = sort(@XPOINTTemp);
                    
#                     print "@XPOINT1\n@XPOINT2\n" if($debug == 10);
                    
                    $nXPOINT1 = @XPOINT1;
                    $nXPOINT2 = @XPOINT2;
                    
                    die "\nErr: There is something wrong by fragment $i and $j.\n\n\n" if($nXPOINT1 ne $nXPOINT2);

                    foreach my $iatomi(0..@XPOINT1-1){
                        
                        foreach my $jatomj(0..@XPOINT2-1){

                            my @fonc_i_temp = split(' ', $fonci[$XPOINT1[$iatomi]]);
                            my @fonc_j_temp = split(' ', $foncj[$XPOINT2[$jatomj]]);

                            my @fonc_i_tempsort = sort(@fonc_i_temp);
                            my @fonc_j_tempsort = sort(@fonc_j_temp);
                            
                            my $count_same = 0;
                            
                            foreach my $eachTerm(0..@fonc_i_tempsort-1){
                                
                                if((($fonc_i_tempsort[$eachTerm] == $fonc_j_tempsort[$eachTerm])) && ($fonc_i_tempsort[$eachTerm] eq $fonc_j_tempsort[$eachTerm])){
                                    $count_same++;
                                }
                            }
                            
                            if (($count_same ne 0) && ($count_same eq @fonc_j_tempsort) && (@fonc_i_temp ne '') && (@fonc_j_temp ne '')){

                                if ($c_point[$i] eq ''){
                                    if(($re_j eq '') || ($re_j ne $j)){
                                        $re_j = $j;
                                        $nr_same[$i-1]++;
                                    }
                                    
                                    $c_point[$i]=$j.'*'.$XPOINT2[$jatomj].'-TO-'.$i.'*'.$XPOINT1[$iatomi];
                                }
                                else{
                                    if(($re_j eq '') || ($re_j ne $j)){
                                        $re_j = $j;
                                        $nr_same[$i-1]++;
                                    }
                                    
                                    $c_point[$i] = $c_point[$i].' '.$j.'*'.$XPOINT2[$jatomj].'-TO-'.$i.'*'.$XPOINT1[$iatomi];  
                                }
                            
                                $XPOINT1[$iatomi]='';
                                $XPOINT2[$jatomj]='';
                            
                            }
                        }
                    }
                }
                else{
                
                    if (!($a_w)){
                        print OUT_CO $LIGEND[$i];
                        $a_w = 1;
                        $nr_fragment++;
                    }
                    
                    $no_repeat = $no_repeat.$i.' ' if($no_repeat !~ / $i /);
                }
                
            }
            elsif(($NBATOM[$i] ne 0)){
                
                if (!($a_w)){
                    print OUT_CO $LIGEND[$i];
                    $a_w = 1;
                    $nr_fragment++;
                }
                
                $no_repeat = $no_repeat.$i.' ' if($no_repeat !~ / $i /);
            }
        }
#         print "$c_point[$i]\n" if($debug == 10);
    }
    
    close OUT_CO;
    
    foreach $i(1..@nr_same){
        if($nr_same[$i-1] ne ''){
            print("\t\tfind $nr_same[$i-1] fragment(s) the same as fragment $i\n");
        }
    }
    
    print("\n\t\tThere are $nr_fragment individual fragments from all $nr_mol framgments\n\n");
    
    local @same_frag, @change_p2p, @change_before, @change_after;
    
    foreach $i_str(0..@molping-1){
                    
        @molping_tmp = split(' ', $molping[$i_str]);
        
        @mol_tmp = split("_|-", $mol[$i_str]);
        @mol_tmp_cp = split("_|-", $mol[$i_str]);
        
        foreach $i_mp_each(0..@molping_tmp-1){
            
            $oldmolping = $molping_tmp[$i_mp_each];
            
            foreach $ic_point(0..@c_point){
                
                if($c_point[$ic_point] ne ''){
                    
                    @same_frag = split(' ', $c_point[$ic_point]);
                    
                    foreach $i_same(0..@same_frag-1){

                        if($same_frag[$i_same] =~ /^$oldmolping\*/){
                        
                            @change_p2p = split('-TO-', $same_frag[$i_same]);
                            @change_before = split('\*', $change_p2p[0]);
                            @change_after = split('\*', $change_p2p[1]);

                            if($oldmolping eq $change_before[0]){
                                
                                $mol_ch_before = ($i_mp_each+1).'\*'.$change_before[1];
                                $mol_ch_after = ($i_mp_each+1).'*'.$change_after[1];
                                
                                if($mol[$i_str] =~ /$mol_ch_before/){

                                    foreach $k (0..@mol_tmp_cp-1){
                                        
                                        if($mol_tmp_cp[$k] =~ /$mol_ch_before/){

                                            $mol_tmp[$k] =~ s/$mol_ch_before/$mol_ch_after/g;
                                            $mol_tmp_cp[$k] = '';
                                            $newmolping = $change_after[0];
                                            $molping_tmp[$i_mp_each] = $change_after[0];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        
        $molping[$i_str] = join(' ', @molping_tmp);

        if (($mol_tmp[0] ne '') && ($mol_tmp[1] ne '')){
            $mol[$i_str] = $mol_tmp[0].'-'.$mol_tmp[1];
        }
        elsif(($mol_tmp[0] ne '') && ($mol_tmp[1] eq '')){
            $mol[$i_str] = $mol_tmp[0];
        }
        elsif(($mol_tmp[0] eq '') && ($mol_tmp[1] ne '')){
            $mol[$i_str] = $mol_tmp[1];
        }

        for(my $i = 2; $i < @mol_tmp; $i += 2){
        
            $mol[$i_str] = $mol[$i_str].'_'.$mol_tmp[$i].'-'.$mol_tmp[$i+1];
          
        }
    }
    
    &renew_mp;
    
    &write_split_result($out_su, 'sort_redundant');

}

sub renew_mp{

# ----------
#
# renew molping
#
# ----------
    
    &print_debug('rewrite_lib');
    
    my $i_frag = 0, $i, @new_frag = '', @split_mp = '';
    
    @new_frag = split(' ', $no_repeat);
    
    foreach $i_mp(0..@molping-1){
        
        @split_mp = split(' ', $molping[$i_mp]);
        
        foreach $i_split_mp(0..@split_mp-1){
            
            foreach $i(0..@new_frag-1){
                
                if($split_mp[$i_split_mp] == $new_frag[$i]){
                    
                    $split_mp[$i_split_mp] = $i+1;
                    
                }
            }
        
        }
        
        $molping[$i_mp] = join(' ', @split_mp);
    }
    
    $i_frag = @new_frag;
}
