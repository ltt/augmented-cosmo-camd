#!/usr/bin/perl

print "WRITE OK \n";

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#       
#    This file is part of the augmented COSMO-CAMD project which is released under the GNU v3.0 license. 
#         See file LICENSE for full license details
#
#    - AUTHORS: Yifan Wang, Lorenz Fleitmann
#      Institute of Technical Thermodynamics, RWTH Aachen University, 52062 Aachen, Germany
#	 
#    - Info: all subs about writing things, all subs about screen output
#
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


sub write_mol_info{

# ----------
#
#    to write the functional groups information of each molecule
#
# ----------
    
    &print_debug("write_mol_info");
    
    local *INFO;
   
    
    open(INFO, ">>$fo_mol");
    
    # file header
    if ($i_mol eq 1){
        print INFO ("# # # # # # # # # # # #\n\n");
        print INFO "\# molecule information\n\n";
        print INFO "\#\t-Summary of the functional groups of each molecule\n\n";

        print INFO ("# # # # # # # # # # # #\n\n");
    }
    
    # each group
    print INFO "Nr.$i_mol: $ligne[1]\n";
    
    print INFO "There are $ngroupAll functional groups: $ngroup_single single groups, $ngroup_multi multi groups and $ncycle[$i_mol] cycles.\n";
    
    print INFO ("\ngroup\tNr.\trelevant atoms\n\n");

    # write cycle
    print INFO "-cycle\t$ncycle[$i_mol]\tatoms [ $cycle_atom[$i_mol] ]\t\n" if ($ncycle[$i_mol] > 0);
    
    # write functional groups
    foreach $key(keys %nrGroup){
    
        if($nrGroup{$key} > 0){
            print INFO "-$key\t$nrGroup{$key}\t[ $combineGroup{$key} ]\n"
        }
    }
    
    print INFO "\n\$\$\$\$\n\n\n";

    close INFO;
    
};

sub mol_2_frag{

# ----------
#
#    this molecule is too complex to split
#       -> all atom H is changed to atom X
#       -> this molecule is used as one fragment
#
# ----------
    my $atomH = 0, $if_change = 0;
    local *IN;

    unlink $ft_copy_all;
    
    # change atom H to X
    
    foreach my $i(0..@ligne){
        $ligne[$i] =~ s/ H / X /g;
    }
    
    # write the structure info in diss_all.sdf
    open (IN, ">$ft_copy_all");
        print IN @ligne;
    close IN;

    $i_lego++;
    $mp_main_tmp = $i_lego;
    $mol_main_tmp = '1*'.$combine_atomH;
    $i_moli++;
}

sub copy_all_in_one{

# ----------
#
#   write/copy all fragments(before split one-anchor-groups) in one file.
#
# ----------
    
    my ($write_file) = @_;
    
    &print_debug("copy_all_in_one", $write_file);
    
    local *IN;
    my @line, $nr_atom, @atomTemp = '', $nr_atomX = 0;
    
    # read the number of atoms of this molecule
    @line  = split(' ', $ligneTemp[4]);
    $nr_atom  = $line[0];

    # count the number of atom X
    foreach my $m(5..5 + $nr_atom){
        
        @line = split(' ', $ligneTemp[$m]);
        
        if($line[3] eq 'X'){
            $nr_atomX++;
        }
        
        die "\nErr: One fragment has atom Y.\n\n" if($line[3] eq 'Y');
    }
    
    # check, if a fragment is made up of only atom X
    if($nr_atomX != $nr_atom){
        
        # if none of defined functional groups is found in one molecule, this molecule is defined as one fragment.
        if(($mol_main_tmp eq '') && ($mp_main_tmp eq '')){
            
            print "\t\t-> This molecule is defined as 1 fragment.\n" if($ngroup_single == 0);

            if (($ngroup_single == 0) && ($ngroup_multi == 0)){
                
                foreach my $i(0..@ligneTemp){
                    $ligneTemp[$i] =~ s / H / X /g;
                }
            
                $i_lego++;
                $mp_main_tmp = $i_lego;
                $mol_main_tmp = '1*'.$combine_atomH;
                $i_moli++;
            }
        }
        
        # write the structure info in file
        open(IN, ">>$write_file");
        
            print IN @ligneTemp;
            
        close IN;
        $nr_add_frag++;
    }
    else{
        die "\nErr: The fragment $i_lego is made up of only atom X. Please have a check of the $i_mol. molecule. \n\n";
    }
    
}


sub write_ligne{

# ----------
# 
#    write the decomposed fragment inclusive Y atom(s)
#
# ----------

    &print_debug('writeLigne');
    
    my $li;

    # add information <tempXatom> for sub split_reactant.
    # the anchor atom X muss not be retained.
    
    $ligne[$line_write+2] = $ligne[$line_write];
    $ligne[$line_write] = "> <tempXatom>\n";
    $ligne[$line_write+1]= "$tempXatom\n";

    
#     if ($is_last == 0){
    # written in different files, if the call-me-subs are different
    foreach $li (1..@ligne-1){
        
        if(($ngroup_multi eq 0) && ($split_one_anchor eq 0)){
            print DOC $ligne[$li] if($ligne[$li] ne "");
        }
        else{
            print TEMPDOC $ligne[$li] if($ligne[$li] ne "");
        }
        
        if($li == 4 + $nbatom){
                
            foreach $li2(0..@ligneplus-1){
                if(($ngroup_multi eq 0) && ($split_one_anchor eq 0)){
                    print DOC $ligneplus[$li2];
                }
                else{
                    print TEMPDOC $ligneplus[$li2];
                }
            };
        };	
    };
    
    # the number of lego/fragment is always added, as lang as the fragment info is written.
    $i_lego++;

    # copy the origin structure information in @ligne
    foreach $li (0..@ligne-1){
        $ligne[$li] = $ligneTemp[$li];
    }
    
}

sub move_all2diss{

# ----------
#
# if all multi-anchor-groups are splited, copy all fragments to dissociated.sdf 
# -> next step is to split one-anchor-groups
#
# ----------

    local (*COLL, *READ);
    
    unlink $ft_diss;
    
    open (COLL, ">>$fo_lib");
    open (READ, "<$ft_copy_all");
    
    while(<READ>){
        print COLL $_;
    }
    close COLL;
    close READ;
    
    unlink $ft_copy_all;

}


sub write_split_result{

# ----------
# 
#    write the split result in summary.OUT
#    before write, the order of molping of each molecule muss be sorted, so that LEA3D could read and rebuild the molecule
#
# ----------
    
    local ($sumfile, $oper) = @_;
    
    &print_debug('write_split_result', $sumfile, $oper);
    
    
    local (*OUT, $i, $j = 0);
    
    # sort the order of molping -> for LEA3D to read 
    &sort_molping if ($oper eq 'all');
    
    # screen output
    &print_line("Writing lea-strings in $sumfile");

    # write the combination's information in summary.out || new_summary.out
    open(OUT, ">$sumfile");
    
    print OUT "\n\t\tresult from screening:\n\n";
    print OUT "|generation\t|Rank\t|sdf file no\t|Score\t|Fragments\t|\n";
    
    for $i(0..@mol){
        if($mol[$i] ne ''){
            print OUT "|\t|\t|\t|\t|\t$mol[$i] \/\t$molping[$i]|\n" ;
            $j++;
        }
    }
    close OUT;
    
    # screen output
    print "\t\t$j strings are written!\n\n";

}

sub change_H2X{
    
# ----------
#
# change all atom H of each fragment to atom X, as a free anchor
#
# ----------
    
    local($f_in, $operator) = @_;
     
    if ($operator eq 'noH'){
        
        &print_line ("Add <POINT> info");
    }
    elsif ($operator eq 'withH'){
    
        &print_line ("Changing all H atom to anchor X atom and Add <POINT> info");
    }
    
    &print_debug ('change_H2X', $f_in, $nb = 0);
    
    local ($ecrit = 1, $pasvu = 1, $point = "", $point2 = "");
    local $f_out = "H_".$f_in;	
    
    local (*DOC);
    
    # read library file, find auch atom H and change them to atom X.
    open(DOC, ">$f_out"); 
    
        &read_all_frag($f_in, 'change_H2X', $operator);
    
    close(DOC);
 
    rename $f_out, $fo_lib;

    print "\t\t$nb fragments are writing $fo_lib\n\n";	

}


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#       
#               all subs about screen output
#
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

sub print_header{

# ----------
#
# just as sub name said
#
# ----------

    my $text="
    ########################################################################\n\n
    \t\t\tsplit functional groups\n\n
    ########################################################################\n\n";

    system("echo \"$text\""); 

}

sub print_line{

# ----------
#
# screen output for information
#
# ----------
    
    my ($in) = @_;
    
    print "\n------------------------------------\n";
    print "\t$in...\n\n";
}

sub print_debug{

# ----------
#
# screen output for debug
#
# ----------

    if ($if_debug > 0){
    
        print "\ndebug: IN SUB <@_[0]>";
        
        foreach my $i(1..@_){
            if(@_[$i] ne ''){
                print "  with Input:" if($i == 1) ;
                print "\t@_[$i]";
            }
        }
        
        print "\n\n";

    }
}

sub print_findgroup{

# ----------
#
# screen output for information: which groups of a molecule are found
#
# ----------

    my($which_group) = @_;
    
    print("\t\tfind group: $which_group\n");
    
}

sub print_i{

# ----------
#
# sreen output: information of this algorithmus 
#
# ----------
    
    # attention: it is four blanks not a tab
    my $text = "General Command list:
    ./splitGroup.pl -i help information
    ./splitGroup.pl -r <directory>
    ./splitGroup.pl <file.sdf>\n";	       

    system("echo \"$text\""); 

    die "\n";
}

sub print_footer{

# ----------
#
# sreen output: footer
#
# ----------

    &print_line('Finished');
    
    my $i = 3;
    
    $i = 5 if($if_delete);
    
    $i++ if($operation eq '-r');
    
    print "\t$i output file:\n";
    
    print "\t\t $fo_mol:\tmolecule group information\n";
    print "\t\t $fo_str:\t\tlea-string before delete\n";
    print "\t\t $fo_lib:\tfragment library before delete\n";
    
    if ($if_delete){
        print "\t\t new_$fo_str:\tlea-string after delete\n";
        print "\t\t new_$fo_lib:\tfragment library after delete\n";
    }
    if ($operation eq '-r'){
        print "\t\t $fo_allmol:\tall molecule from directory\n";
    }
    
    my @failed = split(' ', $failed_mol);
    
    $i = @failed;
    
    print "\n\t$i molecules are not splited because of too many atoms: \n\t\t Nr.@failed\n" if($i > 0);
    
    
    # attention: it is four blanks not a tab
    my $text="
    ########################################################################\n\n
    \t\t\t\tAll DONE!\n\n";

    system("echo \"$text\""); 

}


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#       
#               all subs about file things
#
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
sub edit_file{

# ----------
#
# define and delete the gobal files 
#
# ----------

    my ($operator) = @_;
    
    &print_debug('edit_file', $operator);
    
    if ($operator eq 'define'){
        
        # define
        
        # fo: acronym of file output
        # ft: acronym of file temp
        # fi: acronym of file in
        
        $fi_setin = 'setting_group.in';
        
        $fo_str = 'summary.out';
        $fo_lib = 'collection.sdf';
        $fo_mol = 'mol_group_info.txt';
        $fo_allmol = 'all_mol_fromDir.sdf';
        
        $ft_lib = 'outputTemp.sdf';
        $ft_copy_all = 'diss_all.sdf';
        $ft_single = 'dissociated_ONE.sdf';
        $ft_diss = 'dissociated.sdf';
        
    
    }
    elsif ($operator eq 'delete_a'){
        
        # delete all files before the algorithmus starts 
        
        unlink $ft_diss;
        unlink $fo_str;
        unlink $ft_lib;
        unlink $fo_mol;
        unlink $ft_copy_all;

        unlink $fo_lib;
        unlink 'new_collection.sdf';
        unlink $fo_allmol;
        
        unlink 'dissociated_1.sdf';
        unlink 'dissociated_2.sdf';
        unlink 'dissociated_3.sdf';
        unlink 'dissociated_4.sdf';
    }
    elsif($operator eq 'delete_e'){
        
        # delete all temporary files before the algorithmus stops
        
        unlink $ft_lib;
        unlink $ft_diss;

        unlink 'dissociated_1.sdf';
        unlink 'dissociated_2.sdf';
#         unlink 'dissociated_3.sdf';
        unlink 'dissociated_4.sdf';
        
    }
    else{
        
        die "\nErr: This operator $operator isn't in sub edit_file defined.\n\n";
    }
}



