#!/usr/bin/perl

print "FIND OK \n";

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#       
#    This file is part of the augmented COSMO-CAMD project which is released under the GNU v3.0 license. 
#         See file LICENSE for full license details
#
#    - AUTHORS: Yifan Wang, Lorenz Fleitmann
#      Institute of Technical Thermodynamics, RWTH Aachen University, 52062 Aachen, Germany
#	 
#    - Info: all subs about finding things
#            - groups
#            - cycles
#
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


sub find_group{

# ----------
#
#    find which groups does a molecule/fragment have
#
# ----------
    
    &print_debug("find_group");
    
    # initialisation of variables
    local ($if_print) = @_;
    my $anotherC = '';    
    
    %nrGroup = {%iniGroup0};
    %combineGroup = {%iniGroup0};
    
    # counter single-anchor-groups
    $ngroupOH = 0;
    $ngroupCH3 = 0;
    $ngroupF = 0;
    $ngroupCl = 0;
    $ngroupBr = 0;
    $ngroupI = 0;
    $ngroupNHH = 0;
    
    
    # counter multi-anchor-groups
    $ngroupCC3 = 0;
    $ngroupCC2 = 0;
    $ngroupO = 0;
    $ngroupCO2O1 = 0;
    $ngroupCO2N1 = 0;
    $ngroupCO2 = 0;
    $ngroupCO2CO2= 0;
    $ngroupNH = 0;
    $ngroupN = 0;
 
    $ngroup_single = 0;
    $ngroup_multi = 0;
    $ngroupAll = 0;
    
    # to remember the combination information of each group
    @groupOHanchor = '';
    @groupOHanchorG = '';
    
    @groupCH3Anchor = '';
    @groupCH3AnchorG = '';

    @groupOanchor = '';
    @groupOanchorG = '';
    
    @groupCC3anchor = '';
    @groupCC3anchorG = '';
    
    @groupCC2anchor = '';
    @groupCC2anchorG = '';
    
    @groupCO2O1anchor = '';
    @groupCO2O1anchorG = '';
    
    @groupCO2anchor = '';
    @groupCO2anchorG = '';
    
    @groupCO2CO2anchor = '';
    @groupCO2CO2anchorG = '';

    
    my $used = 0;
    my $used_atoms = '-';
    
    # read each atom to find groups
    foreach $bi(1..$nbatom){
        
        if ($atom[$bi] eq 'C'){
            
            # find C(=O)O
            if(($fonc[$bi] =~ / 2-O /) && ($fonc[$bi] =~ / 1-O /) && (($fonc[$bi] =~ / 1-C /) || ($fonc[$bi] =~ / 1-H /) )){
                
                $used = 0;
                
                @det  = split(' ', $fonc[$bi]);
                @detF = split(' ',$ifonc[$bi]);
                
                $pO2 = '';
                $pO1 = '';
                $pO2O1not = '';
                
                # find (=O) and (O)
                foreach $k(0..@det-1){
                    
                    if($det[$k] eq '2-O'){
                        $pO2 = $detF[$k];
                        $used = 1 if ($atom[$pO2] eq '');
                        $atom[$pO2]='';
                    }
                    elsif($det[$k] eq '1-O'){
                        $pO1 = $detF[$k];
                        $used = 1 if ($atom[$pO1] eq '');
                        $atom[$pO1]='';
                    }
                    else{
                        if ($pO2O1not eq ''){
                            $pO2O1not = $detF[$k];
                        }
                        else{
                            $pO2O1not = $pO2O1not.'-'.$detF[$k];
                        }
                    }
                }
                
                
                @det1=split(' ', $fonc[$pO1]);
                $k_c = 0;
                
                foreach $k(0..@det1-1){
                    $k_c++ if (($det1[$k] eq '1-C') || ($det1[$k] eq '1-H') || ($det1[$k] eq '1-X'));
                }
                
                if (($k_c eq @det1) && ($used == 0)){
                
                $recordGroupA = $pO2.'-'.$pO1;
                $recordGroupG = '';    
                
                $endj = 1;
                $bi1 = $bi;
                    
                # if the atom (O) combines with atom (H) or atom (X)
                if(($fonc[$pO1] =~ / 1-H /) || ($fonc[$pO1] =~ / 1-X /)){
                    $endj = 0;
                }
                
                # if the molecule/fragment is HC(=O)OH, HC(=O)OX, XC(=O)OH,  XC(=O)OX
                if(($endj == 0) && (($fonc[$bi] =~ / 1-H /) || ($fonc[$bi] =~ / 1-X /))){
                    $endj = -1; 
                }
                
                # if the molecule/fragment is HC(=O)O..., XC(=O)O... (... means not H or X atom)
                if(($endj == 1) && (($fonc[$bi] =~ / 1-H /) || ($fonc[$bi] =~ / 1-X /))){
                    
                    $endj = 0;
                    $bi1 = $pO1;

                    @det1=split(' ', $fonc[$pO1]);
                    @detF1=split(' ',$ifonc[$pO1]);
                    
                    foreach my $k(0..@det1-1){
                        $pO2O1not = $detF1[$k] if ($detF1[$k] ne $bi); 
                    }
                    
                    $recordGroupA = $bi;
                }
                
                # remember the information of combination
                foreach my $j(0..$endj){
                    
                    
                    
                    if($j>0){
                        
                        $bi1 = $pO1;
                        
                        @det1=split(' ', $fonc[$pO1]);
                        @detF1=split(' ',$ifonc[$pO1]);
                    
                        foreach my $k(0..@det1-1){
                            $pO2O1not = $detF1[$k] if ($detF1[$k] ne $bi); 
                        }
                        
                        $recordGroupG = '';
                        $recordGroupA = $bi;
                    }

                    @det1=split(' ', $fonc[$pO2O1not]);
                    @detF1=split(' ',$ifonc[$pO2O1not]);
                
                
                    foreach $k(0..@det1-1){
                
                        if($detF1[$k] ne $bi1){
                    
                            if ($recordGroupG eq ''){
                                $recordGroupG = $detF1[$k];
                            }
                            else{
                                $recordGroupG= $recordGroupG.'-'.$detF1[$k];
                            }
                        }
                    }
                    
                    $used_atoms = $used_atoms.'-'.$bi1.'-'.$recordGroupA;
                    
                    if ($groupCO2O1anchor[$ngroupCO2O1] eq ''){
                    
                        $groupCO2O1anchor[$ngroupCO2O1]=$bi1."-".$recordGroupA;
                        $groupCO2O1anchorG[$ngroupCO2O1]=$pO2O1not."-".$recordGroupG;
                    
                    }
                    else{
                    
                        $groupCO2O1anchor[$ngroupCO2O1]=$groupCO2O1anchor[$ngroupCO2O1].'+'.$bi1."-".$recordGroupA;
                        $groupCO2O1anchorG[$ngroupCO2O1]=$groupCO2O1anchorG[$ngroupCO2O1].'+'.$pO2O1not."-".$recordGroupG;
                    }
                    
                    $record_atoms = $record_atoms.' '.$groupCO2O1anchor[$ngroupCO2O1].' ';
                }

                if ($endj ne -1){
                        
                        
                        $ngroupCO2O1++;
                        $nrGroup{CO2O1}++;
                        $combineGroup{CO2O1} = join(' ', @groupCO2O1anchor);
                        $ngroup_multi++;
                        $ngroupAll++;
                        &print_findgroup("C(=O)O")if ($if_print);
                }
            }
            $used = 0;
            }
            # find C(=O)N
            elsif(($fonc[$bi]=~/ 2-O /)&&($fonc[$bi]=~/ 1-N /)){
                die "Err: Find group C(=O)N, which is not jet defined. Please close it in file setting_group.in\n\n\n";
            }
            
            # find C(=O)C(=O) or C=O 
            elsif($fonc[$bi]=~/ 2-O /){
                
                # how many C and H/X does atom bi combine with
                $car = $fonc[$bi];
                $nccC = $car =~ s/ 1-C / 1-C /g;
                $nccH = $car =~ s/ 1-H / 1-H /g;
                $nccX = $car =~ s/ 1-X / 1-X /g;
                $nccH = $nccH + $nccX;
                
                @det=split(' ', $fonc[$bi]);
                @detF=split(' ',$ifonc[$bi]);
                
                $findCO2CO2 = 0;
                $noread = 1;
                
                # if atom $bi combines more than one 1-C...
                if($nccC > 1){
                    foreach $k(0..@det-1){
                        if(($det[$k] eq '1-C') && ($fonc[$detF[$k]] =~ / 2-O /)){
                            
                            # find another C=O
                            $car = $fonc[$detF[$k]];
                            $nccC = $car =~ s/ 1-C / 1-C /g;
                            $findCO2CO2++ if ($nccC > 1);
                            $noread = 0 if ($nccC == 1);
                        }
                    }
                }
                elsif(($nccC == 1) && ($nccH == 1)){
                    foreach $k(0..@det-1){
                        if(($det[$k] eq '1-C') && ($fonc[$detF[$k]] =~ / 2-O /)){
                            $car = $fonc[$detF[$k]];
                            $nccC = $car =~ s/ 1-C / 1-C /g;
                            $noread = 0 if ($nccC = 1);
                        }
                    }
                }
                
#                 print "i am hier\n";
#                 print "$findCO2CO2\t$funcGroup{CO2}\t$funcGroup{CO2CO2}\t$noread\n";
                
                $findCO2CO2 = 0 if (($funcGroup{CO2CO2} == 0) && ($findCO2CO2 == 1));
                
                if($findCO2CO2 > 1 ){
                    die "Err: There is a C(=O)C(=O)C(=O)\n" if($findCO2CO2 > 1);
                }
                elsif(($findCO2CO2 == 1) && ($nccH < 1)){
                    # find C(=O)C(=O) 
                    
                    # remember combination information
                    
                    
                    $pO2 = '';
                    $pO2H = '';
                    $pO2not = '';
                    
                    $pC = '';
                    $pCO2 = '';
                    $pCO2H = '';
                    $pCO2not = '';
                    
                    foreach $k(0..@det-1){
                        
                        if($det[$k] eq '2-O'){  
                            $pO2 = $detF[$k];
                        }
                        elsif(($det[$k] eq '1-C') && ($fonc[$detF[$k]] =~ / 2-O /)){
                            
                            $pC = $detF[$k];
                            $atom[$bi]='';
                            $atom[$pC]='';
                            
                            @det1=split(' ', $fonc[$detF[$k]]);
                            @detF1=split(' ',$ifonc[$detF[$k]]);

                            foreach $k1(0..@det1-1){
                                
                                if($det1[$k1] eq '2-O'){ 
                                    $pCO2 = $detF1[$k1];
                        
                                }
                                elsif(($detF1[$k1] ne $bi) && ($det1[$k1] eq '1-C' )){
                                    $pCO2not = $detF1[$k1];
                                }
                                elsif(($detF1[$k1] ne $bi) && (($det1[$k1] eq '1-H')||($det1[$k1] eq '1-X'))){
                                    $pCO2H = $detF1[$k1];
                                }
                            }
                        }
                        else{
                            if($pO2not eq ''){
                                $pO2not = $detF[$k];
                            }
                            else{
                                $pO2not = $pO2not.'-'.$detF[$k];
                            }
                        }
                    }
                    
                    $recordGroupA = '';
                    $recordGroupG = '';
                    
                    $endj = 1;
                    $endj = 0 if ($pCO2H ne '');
                    
                    $recordGroupA =  $pO2.'-'.$pC;
                    
                    $bi1 = $bi;
                    
                    foreach my $j(0..$endj){
                        
                        if($j > 0){
                            $pO2not = $pCO2not;
                            $bi1 = $pC;
                            $recordGroupA = $pCO2.'-'.$bi;
                        }
                        
                        @det1=split(' ', $fonc[$pO2not]);
                        @detF1=split(' ',$ifonc[$pO2not]);
                                    
                        $recordGroupG = '';
                                
                        foreach $k(0..@det1-1){
                    
                            if($detF1[$k] ne $bi1){
                    
                                if ($recordGroupG eq ''){
                                    $recordGroupG = $detF1[$k];
                                }
                                else{
                                    $recordGroupG= $recordGroupG.'-'.$detF1[$k];
                                }
                            }
                        }
                            
                        if ($groupCO2CO2anchor[$ngroupCO2CO2] eq ''){
            
                            $groupCO2CO2anchor[$ngroupCO2CO2]=$bi1."-".$recordGroupA;
                            $groupCO2CO2anchorG[$ngroupCO2CO2]=$pO2not."-".$recordGroupG;
            
                        }
                        else{
            
                            $groupCO2CO2anchor[$ngroupCO2CO2]=$groupCO2CO2anchor[$ngroupCO2CO2].'+'.$bi1."-".$recordGroupA;
                            $groupCO2CO2anchorG[$ngroupCO2CO2]=$groupCO2CO2anchorG[$ngroupCO2CO2].'+'.$pO2not."-".$recordGroupG;
                        }
                        
                       
                    }
                    

                    $ngroupCO2CO2++;
                    $nrGroup{CO2CO2}++;
                    $combineGroup{CO2CO2} = join(' ', @groupCO2CO2anchor);
                    $ngroup_multi++;
                    $ngroupAll++;
                    
                    &print_findgroup("C(=O)C(=O)")if ($if_print);
                   
                }
                elsif(($findCO2CO2 == 0) && ($funcGroup{CO2}) && ($noread == 1)){
                    
                    # find C=O
                    
#                     print "i am hier\n";
                    
                    $pO2 = '';
                    $pO2H = '';
                    $pO2not = '';

                    if($nccH == 2){
                        $ngroupCO2++;
                        &print_findgroup("C=O")if ($if_print);
                    }
                    else{
                        if(($nccC == 2)||(($nccH == 1) && ($nccC == 1))){

                            foreach $k(0..@det-1){
                                if($det[$k] eq '2-O'){  
                                    $pO2 = $detF[$k];
                                }
                                elsif(($det[$k] eq '1-H') || ($det[$k] eq '1-X')){
                                    $pO2H= $detF[$k];
                                }
                                else{
                                    if($pO2not eq ''){
                                        $pO2not = $detF[$k];
                                    }
                                    else{
                                        $pO2not = $pO2not.'-'.$detF[$k];
                                    }
                                }
                            }

                            $recordGroupA = '';
                            $recordGroupG = '';
                        
                            $endj = 1;
                            @pO2not1 = split('-',$pO2not);
                            
                            if ($nccH == 1){
                                $endj = 0;
                                $recordGroupA = $pO2.'-'.$pO2H;
                            }
                            else{
                            
                                $recordGroupA = $pO2.'-'.$pO2not1[@pO2not1-1];
                            }
                            $bi1 = $bi;

                            foreach my $j(0..$endj){
                                
                                if($j > 0){
                                    $recordGroupA = $pO2.'-'.$pO2not1[0];
                                }
                                
                                @det1=split(' ', $fonc[$pO2not1[$j]]);
                                @detF1=split(' ',$ifonc[$pO2not1[$j]]);
                                    
                                $recordGroupG = '';
                                
                                foreach $k(0..@det1-1){

                                    if($detF1[$k] != $bi1){
                    
                                        if ($recordGroupG eq ''){
                                            $recordGroupG = $detF1[$k];
                                        }
                                        else{
                                            $recordGroupG= $recordGroupG.'-'.$detF1[$k];
                                        }
                                    }
                                }
                            
                                if ($groupCO2anchor[$ngroupCO2] eq ''){
                    
                                    $groupCO2anchor[$ngroupCO2]=$bi1."-".$recordGroupA;
                                    $groupCO2anchorG[$ngroupCO2]=$pO2not1[$j]."-".$recordGroupG;
                    
                                }
                                else{
                    
                                    $groupCO2anchor[$ngroupCO2]=$groupCO2anchor[$ngroupCO2].'+'.$bi1."-".$recordGroupA;
                                    $groupCO2anchorG[$ngroupCO2]=$groupCO2anchorG[$ngroupCO2].'+'.$pO2not1[$j]."-".$recordGroupG;
                                }
                            }
                            
                            $ngroupCO2++;
                            $nrGroup{CO2}++;
                            $combineGroup{CO2} = join(' ', @groupCO2anchor);
                            $ngroup_multi++;
                            $ngroupAll++;
                            &print_findgroup("C=O")if ($if_print);
                            
                        }
                    }       
                
                
                }
            }
            
            # find C+C
            elsif(($fonc[$bi]=~/ 3-C /) && ($fonc[$bi] !~ / 1-H /)&& ($fonc[$bi] !~ / 1-X /)){
            
                @det = split(' ', $fonc[$bi]);
                @detF = split(' ', $ifonc[$bi]);
 
                $pCC3 = '';
                $pCC3Not = '';

                foreach $k(0..@det-1){

                    if ($det[$k] eq '3-C'){
                       
                        if ($pCC3 eq ''){
                            $pCC3 = $detF[$k];
                        }
                        else{
                            $pCC3 =$pCC3.'-'.$detF[$k];
                        }
                    }
                    else{

                        if ($pCC3Not eq ''){
                            $pCC3Not = $detF[$k];
                        }
                        else{
                            $pCC3Not =$pCC3Not.'-'.$detF[$k];
                        }
                    }
                }
           
                $recordGroup = '';

                my $findSameAtom = 0;
                my @tempFindSame = '';
                
                for $i(0..@groupCC3anchor-1){
                
                    @tempFindSame = split('\-|\+', $groupCC3anchor[$i]);

                    if($tempFindSame[1] == $bi){
                        $findSameAtom = 1;
                        last;
                    }
                }

                $endj = 1;
                
                $endj = 0 if(($fonc[$pCC3] =~ / 1-H /) || ($fonc[$pCC3] =~ / 1-X /));

                if(!$findSameAtom){
                    my $bi1 = $bi;
                
                    foreach my $j(0..$endj){
                    
                        if($j>0){
                        
                            $pCC3 = '';
                            $pCC3Not = '';
                        
                            foreach $k(0..@det-1){
                    
                                if ($det[$k] eq '3-C'){
                                    $pCC3 =$pCC3.'-'.$k;
                                }
                                else{
                                    $pCC3Not =$pCC3Not.'-'.$k;
                                }
                            }
                    
                            $recordGroup = '';
                            $pCC3 = $detF[$pCC3];
                            $pCC3Not = $detF[$pCC3Not];
                        }
                    
                        @det1 = split(' ', $fonc[$pCC3Not]);
                        @detF1 = split(' ', $ifonc[$pCC3Not]);
                
                        foreach $k(0..@det1-1){
                    # print ("bi is $bi\n");
                            if($detF1[$k] ne $bi1){
                                if ($recordGroup eq ''){
                                    $recordGroup=$detF1[$k];
                                }
                                else{
                                    $recordGroup= $recordGroup.'-'.$detF1[$k];
                                }

                            }
                        }
                        if ($groupCC3anchor[$ngroupCC3] eq ''){
                            $groupCC3anchor[$ngroupCC3]=$bi1."-".$pCC3;
                            $groupCC3anchorG[$ngroupCC3]=$pCC3Not."-".$recordGroup;
                        }
                        else{
                            $groupCC3anchor[$ngroupCC3]=$groupCC3anchor[$ngroupCC3].'+'.$bi1."-".$pCC3;
                            $groupCC3anchorG[$ngroupCC3]=$groupCC3anchorG[$ngroupCC3].'+'.$pCC3Not."-".$recordGroup;
                        }
                        
                        @det = split(' ', $fonc[$pCC3]);
                        @detF = split(' ', $ifonc[$pCC3]);
                        $bi1 = $pCC3;

                    }

                    $ngroupCC3++;
                    $nrGroup{CC3}++;
                    $combineGroup{CC3} = join(' ', @groupCC3anchor);
                    $ngroup_multi++;
                    $ngroupAll++;
                    &print_findgroup("C+C")if ($if_print);
                }            
            }
            
            # find C=C
            elsif($fonc[$bi]=~/ 2-C / && ($bi ne $anotherC)){
                
                $car=$fonc[$bi];
                $nocc=$car=~s/ 2-C / 2-C /g;
                
                my $noccH=$car=~s/ 1-H | 1-X / 1-H /g;
                
                if(($nocc == 1) && ($noccH != 2)){
                    
                    @det=split(' ', $fonc[$bi]);
                    @detF=split(' ',$ifonc[$bi]);
    
                    $pCC2 = '';
                    $pCC2H = '';
                    $pCC2Not = '';
                 
                    foreach $k(0..@det-1){
                            
                        if ($det[$k] eq '2-C'){
                                #print("hierhier is $pCC2 \n");
                            $pCC2 =$detF[$k];
                            
                                #print("hierhier is $pCC2 \n");
                        }
                        elsif(($det[$k] eq '1-H') || ($det[$k] eq '1-X') ){
                            $pCC2H = $detF[$k];
                            
                        }
                        elsif(($det[$k] ne '1-H') && ($det[$k] ne '2-C') && ($det[$k] ne '1-X')){
                            if ($pCC2Not eq ''){
                                $pCC2Not = $detF[$k];
                            }
                            else{
                                $pCC2Not =$pCC2Not.'-'.$detF[$k];
                            }
                        }
                    }
                    
                    $car = $fonc[$pCC2];
                    $nocc = $car =~ s/ 2-C / 2-C /g;
                    
                    
                    if ($nocc == 1){    
                        
                        $anotherC = $detF[$k];
                    
                        $recordGroup = '';

                        my $findSameAtom = 0;
                        my $findCombineAtom = 0;
                        my @tempFindSameAnchor = '';
                        my @tempFindSameAnchor1 = '';

                        for $i(0..@groupCC2anchor-1){
                        
                            @tempFindSameAnchor = split('\+', $groupCC2anchor[$i]);

                            
                            foreach $j(0..@tempFindSameAnchor-1){
                                
                                @tempFindSameAnchor1 = split('\-', $tempFindSameAnchor[$i]);
                                
                                if(($bi == $tempFindSameAnchor1[1])){
                                    $findSameAtom = 1;
                                    last;
                                }
                                
                            }
    #                         print("hierhier @groupCC2anchor##@groupCC2anchoG\n");
                        }

                        $car = $fonc[$pCC2];
                        $nocc1 = $car =~ s/ 1-H | 1-X / 1-H /g;

                        $car = $fonc[$bi];
                        $nocc2 = $car =~ s/ 1-H | 1-X / 1-H /g;
                        
        #               print("hierhier $nocc1\t$nocc11\t\n");

                        $nocc = $nocc1  + $nocc2;
    #                     print("nocc is $nocc\n");
                        $endj = 0 if ($nocc1 == 2);
                        $endj = 1 if ($nocc1 < 2);;
    #                     print("pCC2H is $pCC2+$pCC2H+$pCC2Not\n");
                        
                        if(!$findSameAtom){
                            my $bi1 = $bi;
                            
                            foreach my $j(0..$endj){
                            
                                if($j>0){
                                
                                    $pCC2 = '';
                                    $pCC2H = '';
                                    $pCC2Not = '';
                                
                                    foreach $k(0..@det-1){
                            
                                        if ($det[$k] eq '2-C'){
                                            $pCC2 =$detF[$k];
                                        }
                                        elsif(($det[$k] eq '1-H') || ($det[$k] eq '1-X')) {
                                                $pCC2H = $detF[$k];
                                        }
                                        elsif($det[$k] =~ /1-/){
                                            
                                            if ($pCC2Not eq ''){
                                                $pCC2Not = $detF[$k];
                                            }
                                            else{
                                                $pCC2Not =$pCC2Not.'-'.$detF[$k];
                                            }
                                        }
                                        else{
                                            die "\nErr: The combination of atom $atom[$bi] is not defined!\n\n\n";
                                        }
                                    }
                                }
                                
                                my @pCC2Not1 = split('-', $pCC2Not);
                                
                                
                                foreach my $ipCC2Not1(0..@pCC2Not1-1){
                                    
                                    $recordGroup = '';
                                    $recordGroup1 = '';
                                    
                                    @det1 = split(' ', $fonc[$bi1]);
                                    @detF1 = split(' ', $ifonc[$bi1]);

                                    foreach $k(0..@det1-1){
                                # print ("bi is $bi\n");
                                        if($detF1[$k] ne $pCC2Not1[$ipCC2Not1]){
                                            
                                            if ($recordGroup1 eq ''){
                                                $recordGroup1=$detF1[$k];
                                            }
                                            else{
                                                $recordGroup1= $recordGroup1.'-'.$detF1[$k];
                                            }
                                        #print("$k\t$detNot1[$k]\n");
                                        }
                                    }
                                    
                                    @det1 = split(' ', $fonc[$pCC2Not1[$ipCC2Not1]]);
                                    @detF1 = split(' ', $ifonc[$pCC2Not1[$ipCC2Not1]]);

                                    foreach $k(0..@det1-1){
                                # print ("bi is $bi\n");
                                        if($detF1[$k] ne $bi1){
                                            if ($recordGroup eq ''){
                                                $recordGroup=$detF1[$k];
                                            }
                                            else{
                                                $recordGroup= $recordGroup.'-'.$detF1[$k];
                                            }
                                        #print("$k\t$detNot1[$k]\n");
                                        }
                                    }
                                    if ($groupCC2anchor[$ngroupCC2] eq ''){
                                        
                                        $groupCC2anchor[$ngroupCC2]=$bi1."-".$recordGroup1;

                                        $groupCC2anchorG[$ngroupCC2]=$pCC2Not1[$ipCC2Not1]."-".$recordGroup;
                                        
                                    }
                                    else{
                                        
                                        $groupCC2anchor[$ngroupCC2]=$groupCC2anchor[$ngroupCC2].'+'.$bi1."-".$recordGroup1;
                                    
                                        $groupCC2anchorG[$ngroupCC2]=$groupCC2anchorG[$ngroupCC2].'+'.$pCC2Not1[$ipCC2Not1]."-".$recordGroup;
                                    }
                                
    #                                 print("CC2 $groupCC2anchor[$ngroupCC2] $groupCC2anchorG[$ngroupCC2]\n");
                                }
                                
                                @det = split(' ', $fonc[$pCC2]);
                                @detF = split(' ', $ifonc[$pCC2]);
                                $bi1 = $pCC2;
                            }   
                        }
                        $ngroup_multi++;
                        $nrGroup{CC2}++;
                        $combineGroup{CC2} = join(' ', @groupCC2anchor);
                        $ngroupCC2++;
                        $ngroupAll++;
                        &print_findgroup("C=C")if ($if_print);
                    }
                }
                elsif($nocc eq 2){
#                     die "Err: C=C=C is not defined!\n"; 
                }
            } 
        
            # find CH3
            elsif($fonc[$bi]=~/ 1-H /){
            
                
                $car = $fonc[$bi];
                $nccH = $car =~ s/ 1-H / 1-H /g;
                $nccX = $car =~ s/ 1-X / 1-X /g;

                
                if (($nccH == 3) && ($nccX != 1)){

                    @det=split(' ', $fonc[$bi]);
                    @detF=split(' ',$ifonc[$bi]);
                    
                    $pH = '';
                    $pHnot = '';
                    
                    $recordGroupG = '';
                    $recordGroupA = '';
                    
                    foreach $k(0..@det-1){
                        if(($det[$k] eq '1-H') || ($det[$k] eq '1-X')){
                            
                            if($pH eq ''){
                                $pH = $detF[$k];
                            }
                            else{
                                $pH = $pH.'-'.$detF[$k];
                            }
                        }
                        else{
                            $pHnot = $detF[$k];
                        }
                    }
                    
                    $recordGroupG = $pHnot;
                    $recordGroupA = $bi.'-'.$pH;
                    
                   
                    @det=split(' ', $fonc[$pHnot]);
                    @detF=split(' ',$ifonc[$pHnot]);
                    
                    foreach $k(0..@det-1){
                        if ($detF[$k] ne $bi){
                            $recordGroupG = $recordGroupG.'-'.$detF[$k];
                        }
                    }
                    
                   
                    $groupCH3anchor[$ngroupCH3] = $recordGroupA;
                    $groupCH3anchorG[$ngroupCH3] = $recordGroupG;

                    $ngroupCH3++;
                    $nrGroup{CH3}++;
                    $combineGroup{CH3} = join(' ', @groupCH3anchor);
                    $ngroup_single++;
                    $ngroupAll++;
                    &print_findgroup("CH3")if ($if_print);
                }
            }
        }
        
        elsif ($atom[$bi] eq 'O'){
            
            $recordGroup = '';
            
            @det=split(' ', $fonc[$bi]);
            @det1=split(' ',$ifonc[$bi]);
            
            $car=$fonc[$bi];
            $nocc=$car=~s/ 1-C / 1-C /g;
            
            # find OH
            if(($fonc[$bi] =~ / 1-H /) && ($fonc[$bi] !~ / 1-X /)){
        
                $pH = 0;
                $pHnot = 0;    
                
                foreach $k(0..@det-1){
                    $pH = $det1[$k] if($det[$k]=~/1-H/);
                    $pHnot = $det1[$k] if($det[$k]!~/1-H/);
                }
                
                @detNot=split(' ', $fonc[$pHnot]);
                @detNot1=split(' ',$ifonc[$pHnot]);
                
                foreach $k(0..@detNot-1){
                   # print ("bi is $bi\n");
                    if($detNot1[$k] ne $bi){
                        if ($recordGroup eq ''){
                            $recordGroup=$detNot1[$k];
                        }
                        else{
                            $recordGroup= $recordGroup.'-'.$detNot1[$k];
                        }
                        #print("$k\t$detNot1[$k]\n");
                    }
                }
                if(($fonc[$bi]=~/ 1-C /)){
                    
                    #@det=split(' ', $fonc[$bi]);
                    $p = 0;
                    
                    foreach $k(0..@det-1){
                        $iatom=$det1[$k] if($det[$k]=~/1-C/);
                    }

                    if($fonc[$iatom]!~/ 2-O /){
                        
                       
                        $groupOHanchor[$ngroupOH]=$bi.'-'.$pH;
                        $groupOHanchorG[ $ngroupOH]=$pHnot.'-'.$recordGroup;
                        
#                         print("OH:$groupOHanchor[$ngroupOH]++$groupOHanchorG[ $ngroupOH]\n");
                        $ngroupOH++;
                        $nrGroup{OH}++;
                        $combineGroup{OH} = join(' ', @groupOHanchor);
                        $ngroup_single++;
                        $ngroupAll++;
                        &print_findgroup("OH")if ($if_print);
                
                    }
                }
                else{
                
                    $noccH=$car=~s/ 1-H / 1-H /g;
                    if ( $noccH ne 2){ 

                        $groupOHanchor[$ngroupOH]=$bi.'-'.$pH;
                        $groupOHanchorG[ $ngroupOH]=$pHnot.'-'.$recordGroup;
                        
#                         print("OH:$groupOHanchor[$ngroupOH]++$groupOHanchorG[ $ngroupOH]\n");
                        
                        $ngroupOH++;
                        $nrGroup{OH}++;
                        $combineGroup{OH} = join(' ', @groupOHanchor);
                        $ngroup_single++;
                        $ngroupAll++;
                        &print_findgroup("OH")if ($if_print);
                    }
            
                }
           }
            
            # find C-O-C
            elsif($nocc == 2){
            
                @det=split(' ', $fonc[$bi]);
                @detF=split(' ',$ifonc[$bi]);
                
                $pO = '';
                $pOnot = '';
                
                $pO = $bi;
                
                foreach $k(0..@detF-1){
                    
                    if ($pOnot eq ''){
                        $pOnot =$detF[$k];
                    }
                    else{
                        $pOnot =$pOnot.'-'.$detF[$k];
                    }
                }    

                @pOnot1 = split('-', $pOnot);
                
                my $isO = 1;
                
                foreach $ipnot1(0..@pOnot1-1){
                    $isO = 0 if ($fonc[$pOnot1[$ipnot1]] =~ / 2-O /);
                }
                
                if($isO){
                foreach $ipnot1(0..@pOnot1-1){
                    
                    $recordGroup = '';
                    @detNot=split(' ', $fonc[$pOnot1[$ipnot1]]);
                    @detNot1=split(' ',$ifonc[$pOnot1[$ipnot1]]);
                    
                    foreach $k(0..@detNot-1){
                        
                        if($detNot1[$k] ne $bi){
                            if ($recordGroup eq ''){
                                $recordGroup=$detNot1[$k];
                            }
                            else{
                                $recordGroup= $recordGroup.'-'.$detNot1[$k];
                            }
                        }
                    
                    }
                    
                    if($groupOanchor[$ngroupO] eq ''){
                    
                        @groupOanchor[$ngroupO]=$bi.'-'.$pOnot1[$ipnot1];
                        $groupOanchorG[$ngroupO]=$pOnot1[$ipOnot1].'-'.$recordGroup;
                    }
                    else{
                        $groupOanchor[$ngroupO]=$groupOanchor[$ngroupO].'+'.$bi.'-'.$pOnot1[$ipnot1];
                        $groupOanchorG[$ngroupO]=$groupOanchorG[$ngroupO].'+'.$pOnot1[$ipnot1].'-'.$recordGroup;
 
                    }
                }
                
                    $ngroupO++;
                    $nrGroup{O}++;
                    $combineGroup{O} = join(' ', @groupOanchor);
                    $ngroup_multi++;
                    $ngroupAll++;
                    &print_findgroup("O")if ($if_print);
           }
           }
        }
        
        elsif ($atom[$bi] eq 'N'){
            
            $pH = '';
            $pHnot = '';
            @det=split(' ', $fonc[$bi]);
            @det1=split(' ',$ifonc[$bi]);
 
            $car=$fonc[$bi];
            $noNH=$car=~s/ 1-H / 1-H /g;
               
            if($noNH == 2){

                $recordGroup = '';
                foreach $k(0..@det-1){
                    
                    if($det[$k]=~/1-H/){
                        if ($pH eq ''){
                            $pH =$det1[$k];
                        }
                        else{
                            $pH =$pH.'-'.$det1[$k];
                        }
                    }    
                    else{
                        $pHnot = $det1[$k];
                    }
                }
                
                @detNot=split(' ', $fonc[$pHnot]);
                @detNot1=split(' ',$ifonc[$pHnot]);
                
                foreach $k(0..@detNot-1){
                  
                    if($detNot1[$k] ne $bi){
                        if ($recordGroup eq ''){
                            $recordGroup=$detNot1[$k];
                        }
                        else{
                            $recordGroup= $recordGroup.'-'.$detNot1[$k];
                        }
                    }
                }

                $groupNHHanchor[$ngroupNHH]=$bi.'-'.$pH;
                $groupNHHanchorG[$ngroupNHH]=$pHnot.'-'.$recordGroup;
                $nfragmentONE=$nfragmentONE+2;
                        
                $ngroupNHH++;
                $ngroupAll++;
            }

            elsif(($noNH == 1) &&($fonc[$bi]!~/ 2-/)){
        
                foreach $k(0..@det-1){
                
                    if($det[$k]=~/1-H/){
                        if ($pH eq ''){
                            $pH =$det1[$k];
                        }
                        else{
                            $pH =$pH.'-'.$det1[$k];
                        }
                    }    
                    else{
                        if ($pHnot eq ''){
                            $pHnot = $det1[$k];
                        }
                        else{
                            $pHnot =$pHnot.'-'.$det1[$k];
                        }
                    }
                }
        
                @pHnot1 = split('-', $pHnot);
            
                foreach $ipHnot1(0..@pHnot1-1){
                    $recordGroup = '';
                    @detNot=split(' ', $fonc[$pHnot1[$ipHnot1]]);
                    @detNot1=split(' ',$ifonc[$pHnot1[$ipHnot1]]);
                    
#                     print("pHnot1 is $pHnot1[$ipHnot1] @detNot1\n");
                    
                    foreach $k(0..@detNot-1){
                        
                        if($detNot1[$k] ne $bi){
                            if ($recordGroup eq ''){
                                $recordGroup=$detNot1[$k];
                            }
                            else{
                                $recordGroup= $recordGroup.'-'.$detNot1[$k];
                            }
                        }
                    
                    }
                }
            }
            elsif(($noNH == 0) && ($fonc[$bi]!~/ 2-/) && ($fonc[$bi]!~/ 3-/)){
                
                foreach $k(0..@det-1){
                    if ($pHnot eq ''){
                        $pHnot =$det1[$k];
                    }
                    else{
                        $pHnot =$pHnot.'-'.$det1[$k];
                    }
                }
                
                @pHnot1 = split('-', $pHnot);
                
#                 print("pHnot1 is @pHnot1\n");
                
                foreach $ipHnot1(0..@pHnot1-1){
                    
                    $recordGroup = '';
                    
                    @detNot=split(' ', $fonc[$pHnot1[$ipHnot1]]);
                    @detNot1=split(' ',$ifonc[$pHnot1[$ipHnot1]]);
                    
                    $groupNanchor[$ngroupN]=$bi;
                    
                    foreach $k(0..@pHnot1-1){
                        $groupNanchor[$ngroupN]=$groupNanchor[$ngroupN].'-'.$pHnot1[$k] if ($k ne $ipHnot1);
                    }

                    foreach $k(0..@detNot-1){
                        
                        if($detNot1[$k] ne $bi){
                            if ($recordGroup eq ''){
                                $recordGroup=$detNot1[$k];
                            }
                            else{
                                $recordGroup= $recordGroup.'-'.$detNot1[$k];
                            }
                        }
                    
                    }
                }
            }
       
        }
        
        elsif ($atom[$bi] eq 'F'){
            
            $recordGroup = '';
            
            @det=split(' ', $fonc[$bi]);
            @det1=split(' ',$ifonc[$bi]);
            
            foreach $k(0..@det-1){
                $pFnot=$det1[0];
                die if($k > 0);
            }
            
            @detNot=split(' ', $fonc[$pFnot]);
            @detNot1=split(' ',$ifonc[$pFnot]);
           # print ("1111 @detNot1\n");
            foreach $k(0..@detNot-1){
                if ($detNot1[$k] ne $bi){
                    if ($recordGroup eq ''){
                        $recordGroup=$detNot1[$k];
                    }
                    else{
                        $recordGroup= $recordGroup.'-'.$detNot1[$k];
                    }
                }
            }
            
            $groupFanchor[$ngroupF]=$bi;
            $groupFanchorG[$ngroupF]=$pFnot.'-'.$recordGroup;
            #$molTempMain=$molTempMain.' '.$nfragmentONE.'*'.$pFnot.'-'.($nfragmentONE+1).'*'.$bi.' ';
            $nfragmentONE=$nfragmentONE+2;
                        
           # print("hahaha $groupFanchor[$ngroupF]\t $groupFanchorG[$ngroupF]\n");
            $nrGroup{F}++;
            $ngroupAll++;
            $ngroupF++;
            
        }
        elsif ($atom[$bi] eq 'Cl'){
            
            $recordGroup = '';
            
            @det=split(' ', $fonc[$bi]);
            @det1=split(' ',$ifonc[$bi]);
            
            foreach $k(0..@det-1){
                $pClnot=$det1[0];
                die if($k > 0);
            }
            
            @detNot=split(' ', $fonc[$pClnot]);
            @detNot1=split(' ',$ifonc[$pClnot]);
           # print ("1111 @detNot1\n");
            foreach $k(0..@detNot-1){
                if ($detNot1[$k] ne $bi){
                    if ($recordGroup eq ''){
                        $recordGroup=$detNot1[$k];
                    }
                    else{
                        $recordGroup= $recordGroup.'-'.$detNot1[$k];
                    }
                }
            }
            
            $groupClanchor[$ngroupCl]=$bi;
            $groupClanchorG[$ngroupCl]=$pClnot.'-'.$recordGroup;
            #$molTempMain=$molTempMain.' '.$nfragmentONE.'*'.$pClnot.'-'.($nfragmentONE+1).'*'.$bi.' ';
            $nfragmentONE=$nfragmentONE+2;
                        
            #print("hahaha $groupClanchor[$ngroupCl]\t $groupClanchorG[$ngroupCl]\n");
            $nrGroup{Cl}++;
            $ngroupAll++;
            $ngroupCl++;
        }
        elsif ($atom[$bi] eq 'Br'){
            
            $recordGroup = '';
            
            @det=split(' ', $fonc[$bi]);
            @det1=split(' ',$ifonc[$bi]);
            
            foreach $k(0..@det-1){
                $pBrnot=$det1[0];
                die if($k > 0);
            }
            
            @detNot=split(' ', $fonc[$pBrnot]);
            @detNot1=split(' ',$ifonc[$pBrnot]);
           # print ("1111 @detNot1\n");
            foreach $k(0..@detNot-1){
                if ($detNot1[$k] ne $bi){
                    if ($recordGroup eq ''){
                        $recordGroup=$detNot1[$k];
                    }
                    else{
                        $recordGroup= $recordGroup.'-'.$detNot1[$k];
                    }
                }
            }
            
            $groupBranchor[$ngroupBr]=$bi;
            $groupBranchorG[$ngroupBr]=$pBrnot.'-'.$recordGroup;
            #$molTempMain=$molTempMain.' '.$nfragmentONE.'*'.$pBrnot.'-'.($nfragmentONE+1).'*'.$bi.' ';
            $nfragmentONE=$nfragmentONE+2;
                        
           # print("hahaha $groupClanchor[$ngroupCl]\t $groupClanchorG[$ngroupCl]\n");
            $nrGroup{Cl}++;
            $ngroupAll++;
            $ngroupBr++;
        }
        elsif ($atom[$bi] eq 'I'){
            $recordGroup = '';
            
            @det=split(' ', $fonc[$bi]);
            @det1=split(' ',$ifonc[$bi]);
            
            foreach $k(0..@det-1){
                $pInot=$det1[0];
                die if($k > 0);
            }
            
            @detNot=split(' ', $fonc[$pInot]);
            @detNot1=split(' ',$ifonc[$pInot]);
           # print ("1111 @detNot1\n");
            foreach $k(0..@detNot-1){
                if ($detNot1[$k] ne $bi){
                    if ($recordGroup eq ''){
                        $recordGroup=$detNot1[$k];
                    }
                    else{
                        $recordGroup= $recordGroup.'-'.$detNot1[$k];
                    }
                }
            }
            
            $groupIanchor[$ngroupI]=$bi;
            $groupIanchorG[$ngroupI]=$pInot.'-'.$recordGroup;

            $nrGroup{I}++;
            $ngroupAll++;
            $ngroupI++;
        }
    }
    
    &print_findgroup("cycle")if (($if_print) && ($ncycle[$i_mol] > 0));
    print("\n\t\tFound $ngroupAll groups: $ngroup_single single groups, $ngroup_multi multi groups, $ncycle[$i_mol] cycles.\n\n") if ($if_print);
    
    &check_organic if ($if_print);
    

}

sub find_halogen{
    
    local ($elem) = @_;
    
    $recordGroup = '';
    $pNot = '';

    @det=split(' ', $fonc[$bi]);
    @det1=split(' ',$ifonc[$bi]);
            
    foreach $k(0..@det-1){
        $pNot=$det1[0];
        die if($k > 0);
    }
            
    @detNot=split(' ', $fonc[$pNot]);
    @detNot1=split(' ',$ifonc[$pNot]);
    
    foreach $k(0..@detNot-1){
        if ($detNot1[$k] ne $bi){
            if ($recordGroup eq ''){
                $recordGroup=$detNot1[$k];
            }
            else{
                $recordGroup= $recordGroup.'-'.$detNot1[$k];
            }
        }
    }
    
    if($elem eq 'F'){
    
        $groupFanchor[$ngroupF]=$bi;
        $groupFanchorG[$ngroupF]=$pNot.'-'.$recordGroup;
        $ngroupF++;
    }
    elsif($elem eq 'Cl'){
        $groupClanchor[$ngroupCl]=$bi;
        $groupClanchorG[$ngroupCl]=$pNot.'-'.$recordGroup;
        $ngroupCl++;
    }
    elsif($elem eq 'Br'){
        $groupBranchor[$ngroupBr]=$bi;
        $groupBranchorG[$ngroupBr]=$pNot.'-'.$recordGroup;
        $ngroupBr++;
    }
    elsif($elem eq 'I'){
        $groupIanchor[$ngroupI]=$bi;
        $groupIanchorG[$ngroupI]=$pNot.'-'.$recordGroup;
        $ngroupI++;
    }
        
    $nfragmentONE=$nfragmentONE+2;
    
    $ngroup_single++;
    $ngroupAll++;
    &print_findgroup("$elem") if (if_print); 
}

sub check_organic{

# ----------
#
#    only a easy check, if the molecule might be an inorganic compound
#
# ----------
    
    my $all_atoms = '';
    
    $all_atoms = join('-', @atom);
    
    $all_atoms = '-'.$all_atoms.'-';
    
    print "\t\t-> This molecule might be an inorganic compound.\n" if ($all_atoms !~ /-C-/);
    
}

sub find_cycle{

# ----------
#
#    find if a molecule do have a cycle structure. 
#    find yes, change the bond information + 5
#
# ----------
    
    &print_debug("find_cycle");
    
    local($fileread, $if_plus) = @_;
    
    local $filewrite = 'cyc_'.$fileread;

    local $nr_mol = 0;
    
    unlink $filewrite;
    
    &read_all_frag($fileread, 'find_cycle');
    
    $inputfile = $filewrite if ($if_plus == 1);
    rename $filewrite, $fileread if ($if_plus == 0);
}    

sub cyclesdf{

# ----------
#
#    comes originally from LEA3D
#    
#
# ----------
    
    &print_debug("cyclesdf");
    
    local $nbcycle=0, @cycle='', $debug=0;
    local $listpassage, $listedirecte, $contact1, $suite, $suitei,$suitefinale;
    local @gsuite4;
    
    foreach $cyc (1..$istratom){

        foreach $cyc3 (1..$istratom){

            $listpassage="";
            $listedirecte="";
            $contact1=-1;
            $suite="";
            $suitei="";
            $suitefinale="";
            @gsuite4=split(' ',$ifonc[$cyc3]);
            
            if(@gsuite4 > 1){
                foreach $cyc2 (0..@gsuite4-1){
                    $contact1=$cyc3 if($gsuite4[$cyc2] == $cyc);
                };
            };
            
            if($contact1 > -1){
                print "depart $cyc puis via $contact1\n" if($debug);
                
                @gsuite=split(' ',$ifonc[$contact1]); #cyc3 --Yifan
                
                if(@gsuite > 1){
                
                    foreach $cyc2 (0..@gsuite-1){
                    
                        if($gsuite[$cyc2] != $cyc){
                        
                            $listpassage=$listpassage." $gsuite[$cyc2] ";
                            $suite=$suite." $gsuite[$cyc2] ";
                            $suitei=$suitei." $contact1 ";
                        };
                    };
                    
                    @gsuite2=split(' ',$suite);
                    @gsuite3=split(' ',$suitei);
                
                    $qs=@gsuite2-1;
                    
                    while($gsuite2[0] ne ""){
                        
                        print "\t suite $suite\n" if($debug);
                        
                        @fget=split(' ',$ifonc[$gsuite2[$qs]]);
                        $endsub=0;
                        
                        foreach $cyc2 (0..@fget-1){
                        
                            if($listedirecte!~/ $gsuite2[$qs]-$fget[$cyc2] /){
                                
                                if($fget[$cyc2]==$cyc && $fget[$cyc2]!=$gsuite3[$qs] && $listpassage!~/ $fget[$cyc2] /){
                                    
                                    $endsub=1;
                                    
                                    @gsuite5=split(' ',$suitei);
                                    
                                    foreach $qs3 (0..@gsuite5-1){
                                        if($suitefinale!~/ $gsuite5[@gsuite5-1-$qs3] /){
                                            $suitefinale=$suitefinale." $gsuite5[@gsuite5-1-$qs3] ";
                                        };
                                    };	
                                    
                                    $suitefinale=" $gsuite2[$qs] ".$suitefinale if($suitefinale!~/ $gsuite2[$qs] /);
                                    $suitefinale=$suitefinale." $cyc " if($suitefinale!~/ $cyc /);
                                    print "suite finale $suitefinale\n" if($debug);
                                    
                                    $cycle[$nbcycle]=$suitefinale;
                                    
                                    $nbcycle++;
                                    
                                    $suitefinale="";
                                    
                                    $listedirecte=$listedirecte." $gsuite2[$qs]-$fget[$cyc2] ";
                                }	
                                elsif($fget[$cyc2]!=$gsuite3[$qs] && $listpassage!~/ $fget[$cyc2] /){
                                    
                                    $endsub=1;
                                    print "\t add $fget[$cyc2]\n" if($debug);
                                    $listpassage=$listpassage." $fget[$cyc2] ";
                                    $suite=$suite." $fget[$cyc2] ";
                                    $suitei=$suitei." $gsuite2[$qs] ";
                                };
                            };
                        };
                        
                        if($endsub==0){
                            $gsuite2[$qs]="";
                            $suite=join(' ',@gsuite2);
                            $suite=" ".$suite." ";
                            $gsuite3[$qs]="";
                            $suitei=join(' ',@gsuite3);
                            $suitei=" ".$suitei." ";
                        };
                        @gsuite2=split(' ',$suite);
                        @gsuite3=split(' ',$suitei);
                        $qs=@gsuite2-1;
                    };
                };
            };
        };
    };

###################################################################################
############### ELIMINATION DES DOUBLES

    $longc=$nbcycle-1;
    
    foreach $lc (0..$longc-2){
        foreach $lc2 (($lc+1)..$longc-1){
            if ($cycle[$lc2] ne ''){
                
                $nbzero=0;
                @get1= split(' ',$cycle[$lc]);
                @get2= split(' ',$cycle[$lc2]);
                
                if (@get1 == @get2){
                    foreach $lc3 (0..@get1-1){
                        foreach $lc4 (0..@get2-1){
                            if ($get1[$lc3] == $get2[$lc4]){
                                $get2[$lc4] = 0;
                                $nbzero ++;
                            };
                        };
                    };
                    $cycle[$lc2]='' if ($nbzero == @get2);
                };
            };
        };
    };

    $nbc = 0;
    $nbc56 = 0;
    @cyclef='';

    foreach $lc (0..$longc-1){
        if ($cycle[$lc] ne ''){
            print "unique $cycle[$lc]\n" if($debug);
            $cyclef[$nbc]=$cycle[$lc];
            $nbc++;
            @get56=split(' ',$cycle[$lc]);
            $long56=@get56;
            $nbc56++ if($long56<=6);	
        };
    };
    
    $ncycle[$nr_mol] = $nbc;
    
    foreach $i(0..@cyclef){
        
        my $tmp = $cyclef[$i];
        $tmp =~ s/  /-/g;
        
        $cycle_atom[$nr_mol] = $cycle_atom[$nr_mol].' '.$tmp;
    }
   
    
    print "$nbc56 cycles a 3, 4, 5 ou 6 atomes\n"  if($debug);
    print "$nbc cycles\n" if($debug);
    print "\n" if($debug);
};

sub changebond{
    
    
    &print_debug("changebond");

    local $ccyc = @cyclef;
    
    my $i, $j, $k, @c_atom, @c_ifonc, @c_fonc, $add_atom = '', $count = 0, $all_mol = '';
    
    $all_mol = $cyclef[0];
    
    if ($ccyc > 1){
        
        foreach $k(1..@cyclef-1){
        
            @c_atom = split(' ', $cyclef[$k]);
            
            foreach $i(0..@c_atom-1){
            
                if ($all_mol !~ / $c_atom[$i] /){
                
                    $all_mol = $all_mol.' '.$c_atom[$i].' ';
                }
            }
        }

    }

    foreach $i(1..@fonc-1){
        
        if ($all_mol  =~/ $i /){
            
            if (($fonc[$i] =~ / 2-/) || ($fonc[$i] =~ / 7-/)){
                
                @c_ifonc = split(' ', $ifonc[$i]);
                @c_fonc = split(' ', $fonc[$i]);
                
                foreach $j(0..@c_fonc-1){
                    
                    if(($c_fonc[$j] =~ /^2-/) || ($c_fonc[$j] =~ /^7-/)){

                        $add_atom = $add_atom.' '.$c_ifonc[$j];
                        $all_mol = $all_mol.' '.$c_ifonc[$j];
                    }
                }
            }
            
        }
    }
    
    @add = split(' ', $add_atom);
    
    foreach $i(0..@add-1){
        if (($fonc[$add[$i]] =~ / 2-/) || ($fonc[$add[$i]] =~ / 7-/)){
            $count++;
            die "Err: There is something wrong in FIND.pl.(found cycle==)\n" if ($count > @add); 
        }
    
    }
    
    $all_mol = $all_mol.' ' if ~($add_atom);
    
    open(WRITE, ">>$filewrite");
    
    foreach $iligne(0..@ligne-1){
    
        if($iligne > 4){
        
            @getligne = '';
            @getligne = split(' ', $ligne[$iligne]);

            if(($all_mol =~ / $getligne[0] /) && ($all_mol =~ / $getligne[1] /) && ($getligne[0] ne '') && ($getligne[0] ne ' ')){
                
                if($if_plus){
                    $getligne[2] = $getligne[2] + 5;
                }
                else{
                    $getligne[2] = $getligne[2] - 5;
                }
               
                if(($getligne[0] < 100)  && ($getligne[0] > 9) && ($getligne[1] > 9) && ($getligne[1] < 100) ){
                    $ligne[$iligne] = ' '.$getligne[0].' '.$getligne[1];
                }
                elsif(($getligne[0] < 10) && ($getligne[1] > 9) && ($getligne[1] < 100)){
                    $ligne[$iligne] = '  '.$getligne[0].' '.$getligne[1];
                }
                elsif(($getligne[0] < 100) && ($getligne[0] > 9) && ($getligne[1] < 10)){
                    $ligne[$iligne] = ' '.$getligne[0].'  '.$getligne[1];
                }
                elsif(($getligne[0] < 10) && ($getligne[1] < 10)){
                    $ligne[$iligne] = '  '.$getligne[0].'  '.$getligne[1];
                }
                else{
                    die "Err: Not defined!\n";
                }
                
                foreach my $i(2..@getligne-1){
                    
                    $ligne[$iligne] = $ligne[$iligne].'  '.$getligne[$i];
                }
                
                
                $ligne[$iligne] = $ligne[$iligne]."\n";
            }
        }
        print WRITE $ligne[$iligne];
    }

    close WRITE; 

}


sub change_mol_mp{

# ----------
#
#    change the mol and molping, wenn the split happens
#
# ----------

    my $new_mol = '', $new_mp = '', @new_split_mol_sub = '', $i, $j, $j1; #newMolTempMain
    
    my @split_mp_sub = split(' ', $mp_sub_tmp); #splitMolpingTempSub
    my @split_mol_sub = split(/[' ',-]/, $mol_sub_tmp); #splitMolTempSub
    
    my @split_mp_main = split(' ', $mp_main_tmp); #splitMolpingTempMain
    my @split_mol_main = split(' ', $mol_main_tmp); #splitMolTempMain
    
#     print "\n\nvor:$mp_sub_tmp\t$mol_sub_tmp\n$mp_main_tmp\t$mol_main_tmp\n";
            
#     print "ifrag_change: $ifrag_change\n";

    foreach $i(0..@split_mp_sub-1){
                
        if ($split_mp_sub[$i] < $ifrag_change){
            
            if ($new_mp eq ''){
                $new_mp = $split_mp_sub[$i];
            }
            else{
                $new_mp = $new_mp.' '.$split_mp_sub[$i];
            }
            
            foreach my $isplit_mol_sub(0..@split_mol_sub-1){
            
                if ($split_mol_sub[$isplit_mol_sub] =~ /^$split_mp_sub[$i]\*/){
                    $new_split_mol_sub[$isplit_mol_sub] = $split_mol_sub[$isplit_mol_sub];
                    $split_mol_sub[$isplit_mol_sub] = '';
                }
            }
        }
        
        elsif($split_mp_sub[$i] == $ifrag_change){

            my @frag_after_tmp = @frag_change_after; #frag_after_tmp
            
            foreach $j(0..@split_mp_main-1){
                
                my $add_one_ele =  $ifrag_change+$j; #$addOneMolping

                if ($new_mp eq ''){
                
                    $new_mp = $add_one_ele;
                
                }
                else{
                    $new_mp = $new_mp.' '.$add_one_ele;
                
                }
                
                foreach $j1(0..@frag_change_after-1){
                    
                    my @change_mp = split('\*', $frag_change_after[$j1]);
                    
                    if($change_mp[0] == ($j+1)){
                    
                        $change_mp[0] = $add_one_ele;
                        $frag_after_tmp[$j1] = join('*', @change_mp);
                        $frag_change_after[$j1]  = '';
                    }
                }
            }
            
            my @frag_change_after = @frag_after_tmp;


            foreach $j(0..@split_mol_main-1){
                
                $split_mol_main[$j] = '-'.$split_mol_main[$j];
                
                foreach $j1(0..@split_mp_main-1){
                    
                    my $add_one_ele = $ifrag_change+$j1;
                    
                    $split_mol_main[$j]=~s/$split_mp_main[$j1]\*/$add_one_ele\*/g;
                    
                    $split_mol_main[$j]=~s/ $split_mp_main[$j1]\*/ $add_one_ele\*/g;
                    
                    $split_mol_main[$j]=~s/-$split_mp_main[$j1]\*/-$add_one_ele\*/g;

                }
                
                $split_mol_main[$j] =~ s/^.//;
                
                if ($new_mol eq ''){
                    $new_mol = $split_mol_main[$j];
                }
                else{
                    $new_mol = $new_mol.' '.$split_mol_main[$j];
                }
            }
        
            foreach my $k(0..@split_mol_sub-1){
                    
                my @frag1 = split('\*', $split_mol_sub[$k]);
                
                if (($frag1[0] eq $ifrag_change) && ($ifrag_change_anchor =~/-$frag1[1]-/)){
                    
                    my @frag_change_before = split('-', $ifrag_change_anchor);
                
                    foreach my $i_change_before(0..@frag_change_before-1){
                            
                        if ($frag_change_before[$i_change_before] eq $frag1[1]){
                            
                            $new_split_mol_sub[$k] = $frag_change_after[$i_change_before];
                            
                            $split_mol_sub[$k]='';
                        }
                    }
                }
            }
        }
        
        elsif ($split_mp_sub[$i] > $ifrag_change){

            my $add_one_ele = $split_mp_sub[$i] + @split_mp_main - 1;
            
            my $new_mp_tmp = ' '.$new_mp.' '; 

            if($new_mp_tmp !~ / $add_one_ele /){
            
                $new_mp = $new_mp.' '.$add_one_ele;
                
                
                foreach my $k(0..@split_mol_sub-1){
                    
                    if($split_mol_sub[$k] =~ /$split_mp_sub[$i]\*/){
                        
                        my @temp = split('\*', $split_mol_sub[$k]);
                        
                        $new_split_mol_sub[$k]= $add_one_ele.'*'.$temp[1];
                        $split_mol_sub[$k] = ''; 
                                            
                    }
                }
            }
        }
    }
    
    
    # neu mol string
    for ($i = 0; $i < @new_split_mol_sub; $i += 2) {
    
        $new_mol = $new_mol.' '.$new_split_mol_sub[$i].'-'.$new_split_mol_sub[$i+1];
    
    }
    
    $mol_main_tmp = $new_mol;
    $mp_main_tmp = $new_mp;
    
    $mol_sub_tmp = '';
    $mp_sub_tmp = '';
    
    $i_lego--;
    $i_moli--;
                
#     print "nach $mol_main_tmp\t$mp_main_tmp\n";

}
