#!/bin/bash

while :
do
RESULT=`pgrep MAIN`
if [ "${RESULT:-null}" = null ]; then
   break
else
    killall -q  --older-than 60m  MATLAB ;
    sleep 1m;
fi

done

