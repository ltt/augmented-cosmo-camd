#!/usr/bin/perl

###################################################################################################
#----------------------------------------LEA Run Options -----------------------------------------#
#                                                                                                 #
# Set options for LEA3D (COSMO-CAMD approach) 	                                                  #
#                                                                                                 #
# Written by Lorenz Fleitmann, supervised by Jan Scheffcyzk    			                  #
# Lehrstuhl für Technische Thermodynamik (LTT), RWTH Aachen University,      Juni, 2016           #
#                                                                                                 #
###################################################################################################

# Directories for COSMO-Programs
$generalFolder  = "/opt/est/";
$cosmoconfdir   = "COSMOconfX15/COSMOconf/cosmoconf.pl";
$rdkitdir       = "COSMOquick15/extapps/rdkit/bin/confcreate_linux";
$cosmoCFDBdir   = "CFDB_3.6";
$cosmofragdir   = "COSMOquick15/COSMOfrag/cf_linux";
$licensefragdir = "COSMOquick15/licensefiles";
$databasedir    = "/../Central_COSMO_Database/"; # --Yifan change
#$databasedir    = "/daten/Central_COSMO_Database/"; # this is origin, change from Yifan
$COSMOFileGen   = "COSMOFileGen/";

# Directories for Gate2LEA:
$Gatedir	= "Gate/";
$gatecosmo	= "$Gatedir/cosmoFiles";
$gatemolstruct  = "$Gatedir/molStruct_temp";
$gateout2lea	= "$Gatedir/output2LEA";

# Choose COSMO-RS parametrization:
$cosmomethod = "TZVP-MF";
#$cosmomethod = "TZVPD-FINE"; 
#$cosmomethod = "COSMOfrag";
#$cosmomethod = 'TZVP';
#$cosmomethod = 'SVP-AM1';

# Specify limits for nbatom or set "no_nbatom_cutoff":
$nb_max = 16;
#$nb_max = "no_nbatom_cutoff";

# For COSMOfrag precalculation set following variable to 1
$cosmofrag_precalc = 0;
#$cosmofrag_precalc = 1;
$lb_cosmofrag = 0.001;

# Specify limits for any atom:
#$atom_to_limit = "O";
#$atom_limit = 2;

# Specify undesired types of molecules:
$peroxide  = "OO";
$oxalate   = "C(=O)C(=O)";
$anhydride = "C(=O)OC(=O)";
$oxygenfluoride1 = "OF";
$oxygenfluoride2 = "FO";

# If result (=last generation) of previous run should be used as new first population, choose $restart_run = 1
#$restart_run = 1;
$restart_run = 0;

# Maximum number of CPU-cores (nproc returns number of cores in this machine) 
#$numberofCPU = `nproc`;      
#chomp($numberofCPU);
$numberofCPU = 8;
#$numberofCPU = $numberofCPU -1;

# Specify email for update on LEA3D progress
#$recipient = 'lorenz.fleitmann@rwth-aachen.de, jan.scheffczyk@ltt.rwth-aachen.de';
#$recipient = 'benedikt.winter@rwth-aachen.de';
#$recipient = 'christoph.gertig@ltt.rwth-aachen.de, lorenz.fleitmann@rwth-aachen.de'; --Yifan changed
$recipient = 'yifan.wang236@gmail.com'; --Yifan changed
#-----------------------------------------------------------------------------------------

@undesired_mols = ($peroxide, $oxalate, $anhydride, $oxygenfluoride1, $oxygenfluoride2);
#@undesired_mols = ();

if ($cosmomethod eq "TZVP-MF"){
        $COSMOdatabasename = "TZVP-COSMO";
        $ctd = "BP_TZVP_C30_1501.ctd";
        $resultsdir = "Results_of_job_BP-TZVP-MF-COSMO";}
elsif ($cosmomethod eq "TZVPD-FINE"){
        $COSMOdatabasename = "TZVPD-FINE";
        $ctd = "BP_TZVPD_FINE_C30_1501.ctd";
        $resultsdir = "Results_of_BP-TZVPD-FINE-COSMO";}
elsif (($cosmomethod eq "COSMOfrag") or ($cosmomethod eq 'SVP-AM1')){
        $COSMOdatabasename = "SVP-AM1";
        $ctd = "BP_SVP_AM1_C30_1501.ctd";
	$resultsdir = "Results_of_job_BP-SVP-AM1-COSMO";}
elsif ($cosmomethod eq "TZVP"){
        $COSMOdatabasename = "TZVP-COSMO";
        $ctd = "BP_TZVP_C30_1501.ctd";
        $resultsdir = "Results_of_job_BP-TZVP-COSMO";}

$databasedir    = $databasedir.$cosmomethod."/";


die "\n\nLimit imposed on atomtype $atom_to_limit, but no integer limit was specified. Please specify limit in \$atom_limit. Code stopped" if (defined($atom_to_limit) and (!defined($atom_limit)));

if ($restart_run){print "\n\nInitialization of this run with last generation of previous run selected. If correct, please press enter\n";
			<STDIN>;
}
if (!(($cosmomethod eq 'TZVP-MF') or ($cosmomethod eq 'COSMOfrag'))){
	print "\n\nCOSMO-RS parametrization was chosen to $cosmomethod, but so far there is only a good database on TZVP-MF level. Do you really want to continue?";
	<STDIN>;
}
1;
