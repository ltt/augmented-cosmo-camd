function molStruct = job_T_boil_con(molStruct,compounds,specs)
% Property prediction to check T_boil constraint
%  Calculates Antoine Parameter and compares them to desired range of T_boil

%% General
%%
% Preallocate fields
n = length(specs.compounds);

for k=1:length(molStruct);
    molStruct(k).AntoineParam = repmat({NaN},n,1);
    molStruct(k).T_boil = NaN;
    molStruct(k).T_boil_ok = NaN;
end


% Initialize parfor_progressbar
if strcmp(specs.Mode, 'screening')
    parfor_progress(length(molStruct));
    display_text = ['\nCalculating Antoine Parameter for ', num2str(length(molStruct)), ' solvents\nProgress:\n'];
    fprintf(display_text);
end

%% Job procedure
%%
if (~strcmp(specs.T_boil_min, 'no_T_min') && ~strcmp(specs.T_boil_max, 'no_T_max'))
    T = [specs.T_boil_min+273.15, specs.T_boil_max+273.15]; %Conversion ot T/C to T/K 
elseif ~strcmp(specs.T_boil_min, 'no_T_min')
    T = [specs.T_boil_min+273.15, specs.T_boil_min+273.15+100]; 
else
    T = [specs.T_boil_max+273.15-100, specs.T_boil_max+273.15]; 
end

for k = 1:length(molStruct)
    molStruct(k).AntoineParam{n,1} = sub_Antoine(k,compounds(k,end), T, specs);
    [molStruct(k).T_boil_ok, molStruct(k).T_boil] = func_check_T_boil( molStruct(k).AntoineParam{n,1}, specs);
    
    % Update parfor-progressbar
    if strcmp(specs.Mode, 'screening')
        parfor_progress;
    end
    
end
end

