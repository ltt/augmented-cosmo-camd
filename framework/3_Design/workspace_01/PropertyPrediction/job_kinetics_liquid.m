function [ molStruct ] = job_kinetics_liquid( molStruct, compounds,specs )
%Procedure for calculation of reaction kinetics in liquid phase

%% General
%%
% Preallocate fields
n = length(specs.compounds); %number of compounds per system



for k=1:length(molStruct);
    molStruct(k).MW_solvent = NaN;
    molStruct(k).deltaG_solv_reactants = NaN;
    molStruct(k).deltaG_solv_TS = NaN;
    molStruct(k).x_LLE_NIPAM = NaN;
%    molStruct(k).x_LLE_VCL = NaN;
%     molStruct(k).x_LLE_BIS = NaN;
    molStruct(k).x_LLE_H2O = NaN;
   
end

%Set reaction temperature
T = specs.T_reaction; %in K

%
temp_compounds = cell(length(molStruct),5);
for k=1:length(molStruct)
temp_compounds{k,1}='NIPAM';
temp_compounds{k,2}='VCL';
temp_compounds{k,3}='BIS';
temp_compounds{k,4}='h2o';
temp_compounds{k,5}=compounds{k,size(compounds,2)};
end 


% Initialize parfor_progressbar
if strcmp(specs.Mode, 'screening')
    display_text = ['\nCalculating reaction kinetics in liquid phase with ', num2str(length(molStruct)), ' solvents\nProgress:\n'];
    fprintf (display_text);
    parfor_progress(length(molStruct));
end


%% Job procedure
%%
for k=1:length(molStruct)
%for k=1:length(molStruct); %optional: parfor loop
 
 
%% deltaG_solv calculation 
[deltaG_solv] = sub_deltaG_solv(k,compounds(k,:), T, specs);


molStruct(k).deltaG_solv_reactants = deltaG_solv(1:specs.n_reactants,1);
molStruct(k).deltaG_solv_TS = deltaG_solv((specs.n_reactants+1):(specs.n_reactants+specs.n_TS),1);


% Molecular weights of solvents
    
    compound = compounds{k,n};
    molStruct(k).MW_solvent = sub_molweight(compound, k, specs);


    % Get LLE Properties
        

    
    %LLE calculations
    active_mixture = '1 5'; %info for input
    molStruct(k).x_LLE_NIPAM = sub_LLE(active_mixture, k,temp_compounds(k,:), T, specs);
%     active_mixture = '2 5'; %info for input
%     molStruct(k).x_LLE_VCL = sub_LLE(active_mixture, k,temp_compounds(k,:), T, specs);
%     active_mixture = '3 5'; %info for input
%     molStruct(k).x_LLE_BIS = sub_LLE(active_mixture, k,temp_compounds(k,:), T, specs);
    active_mixture = '4 5'; %info for input
    molStruct(k).x_LLE_H2O = sub_LLE(active_mixture, k,temp_compounds(k,:), T, specs);


%Update parfor progressbar
if strcmp(specs.Mode, 'screening')
    parfor_progress;
end


end
end
