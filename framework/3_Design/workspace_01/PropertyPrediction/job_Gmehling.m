function [ molStruct ] = job_Gmehling( molStruct,compounds,specs )
% Property Prediction procedure for Gmehling-Score
%  LLEs and corresponding activity coefficients are calculated

%% General
%%
% Preallocate fields
n = length(specs.compounds);

for k=1:length(molStruct);
    molStruct(k).x_LLE = NaN;
    molStruct(k).gammaI = NaN;
    molStruct(k).gammaII = NaN;
end


% Initialize parfor_progressbar
if strcmp(specs.Mode, 'screening')
    parfor_progress(length(molStruct));
    display_text = ['\nCalculating LLE and activity coefficients of ', num2str(length(molStruct)), ' solvents\nProgress:\n'];
    fprintf (display_text);
end

%% Job procedure
%%

T = specs.T+273.15; %Conversion ot T/C to T/K 

parfor k = 1:length(molStruct)
 
%% LLE calculations
	 active_mixture = specs.LLEcomp;
     molStruct(k).x_LLE = sub_LLE(active_mixture, k,compounds(k,:), T, specs);

%% Activity coefficients 

	if ~isnan(molStruct(k).x_LLE)
                % Phase I
                compositionI = [0, molStruct(k).x_LLE(1:2)];
                molStruct(k).gammaI = sub_gamma(compositionI, k,compounds(k,:), T, specs);
                % Phase II
                compositionII = [0, molStruct(k).x_LLE(3:4)];
                molStruct(k).gammaII = sub_gamma(compositionII, k,compounds(k,:), T, specs);
     end
        
    % Update parfor-progressbar
    if strcmp(specs.Mode, 'screening')
        parfor_progress;
    end 
    
end

if strcmp(specs.Mode, 'screening')
   fprintf 'Property prediction for Gmehling-Score completed\n';
end


end

