function molStruct = job_SigmaProfile(molStruct,compounds, specs )
    
    for k = 1:length(molStruct)
        disp(k)
        [molStruct(k).ConfNum, molStruct(k).wconf, molStruct(k).sigmaProfiles] = sub_wconf(compounds(k,end), k,specs);
         molStruct(k).weightedProfile = weightProfile(molStruct(k));
    end
end

function [ weightedProfile ] = weightProfile( molStruct)

ConfNum = molStruct.ConfNum;
weightedProfile = zeros(61, 2);
    for i = 1 : 61
        weightedProfile(i, 1) = molStruct.sigmaProfiles(i, 1);
        % weight by averaging with conformer weights
        for j = 1 : ConfNum        
            weightedProfile(i, 2) = weightedProfile(i, 2) + molStruct.wconf(j) * molStruct.sigmaProfiles(i, j+1);
        end
    end
end
