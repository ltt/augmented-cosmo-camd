function [result,ok] = sub_gamma(composition, k,compounds, T, specs)
%% Calculates the binary activity coevicents for a given mixture and conzentration
%
%   Input:    compunds 
%           k: unique number k used for identiying files 
%           T: temperature vector 
%           composition: the composition of the mixture
%           specs 
%
% Output:   ln gamma1, ln gamma2 
%
%%


 %% Generate jobs "jobPrint"
    jobPrint = ''; % Initialize jobPrint               
    tk = num2str(T(1));
    jobPrint = strcat(jobPrint,'gamma xg={ ');   
    
    for i = 1:length(compounds)  
        jobPrint = [jobPrint,num2str(composition(i)),' '];
    end
    
    jobPrint = [jobPrint, '} tk= ' tk ' ignore_charge # Automatic Activity Coefficient Calculation \n' ];  
 
 %% Call COSMOtherm
    
    [tempFile,~] = cosmoSingleCall( k, compounds, specs,jobPrint);
   
 %% ReadOutputFile 
 
    [result,ok] = extractGamma(tempFile,compounds);
    
 %% Delete temporary files after calculation     

if exist([tempFile, '.inp'])>0
    delete([tempFile, '.inp'])
end
if exist([tempFile, '.tab'])>0
    delete([tempFile, '.tab'])
end
if exist([tempFile, '.out'])>0
    delete([tempFile, '.out'])
end

end

function [result,ok] = extractGamma(tempFile,compounds)
% RETURNS: [lngam1 lngam2 lngam3, wconf_liq..., wconf_gas]
% Check if .out file is empty
s = dir([tempFile,'.tab']);
if s.bytes == 0
    result = NaN([1,2]); % Empty file
    ok = 0;
    disp('ERROR: tab file empty');
else
    ok = 1;
    fileID = fopen([tempFile,'.tab'],'r');
    line = fgetl(fileID);
    formatSpec = '%*s %*s %f';
    i = 0;
    while ~numel(strfind(line,'Nr Compound')) && i < 20 
         line = fgetl(fileID); % MATLAB: moves one line (= get next line)
         i = i + 1; % Prevent eternal loop
    end        
    line = fgetl(fileID); % For gamma: information below "Nr Compound"
    
    for i = 1:length(compounds)
        result(i) = textscan(line,formatSpec); % Read line 
        line = fgetl(fileID);
    end
    
    result = cell2mat(result);    
    fclose(fileID);        
end
end