function [dbFile, tempFile, inputPath, dbPath, exePath, CTD, CTDIR, LDIR, CFDB, LDIRfrag, exeFrag] = main_header(specs)
    %% Implemented by A. Demuth, bachelor thesis supervised by Jan Scheffczyk @LTT, RWTH Aachen University.	
    %% Header file
    %% MODIFY for other computers:
    % ------------------------------------------------------------
    % ($ denominates variables which have to be changed)
    % 1. Change path where temporary files (inp, tab, out) are written ($tempFile)
    % 2. Change path to COSMO folder ($cosmoPath)
    % ------------------------------------------------------------
   
    % MODIFY working directory, the subfolders are: input (.txt files with molecule
    % names), temp (temporary .inp, .tab, .out files), output (structure
    % arrays)
 	tempFolder  = [pwd, '/PropertyPrediction/']; % Folder for temporary files   
    
    %% Database for different COSMO-RS parametrizations:

    if strcmp(specs.COSMOpara, 'TZVPD-FINE')
            dbName      = 'BP-TZVPD-FINE';  % Database name
            CTD         = 'BP_TZVPD_FINE_C30_1501.ctd';        
    elseif strcmp(specs.COSMOpara, 'TZVP-MF') || strcmp(specs.COSMOpara, 'TZVP')
            dbName      = 'BP-TZVP-COSMO'; % Database name
            CTD         = 'BP_TZVP_C30_1501.ctd';  
    elseif strcmp(specs.COSMOpara, 'SVP') || strcmp(specs.COSMOpara, 'COSMOfrag')
            dbName      = 'BP-SVP-AM1';  % Database name
            CTD         = 'BP_SVP_AM1_C30_1501.ctd';
    end
   
    
    % COSMOfrag fragment Database (CFDB)
    CFDB = specs.paths.cfdb;
    
    %% Other directories

    if ispc 
        dbPath      = [specs.paths.dbpath, '\\', dbName, '\\'];    % Database path
        dbFile      = [dbPath, '\\', dbName,'.csv'];   % Input file
        CTDIR       = specs.paths.ctdir;
        LDIR        = specs.paths.ldir;                            	% License files path
        exePath     = specs.paths.exepath;    % Path to COSMOtherm.exe          
        tempFile    = [tempFolder, 'temp\\temp_'];                                  % Name for temporary files
        inputPath   = [tempFolder, 'input\\']; 
        % COSMOfrag specific
        LDIRfrag    = specs.paths.ldirfrag;                            	% License files path
        exeFrag     = specs.paths.exefrag;
    elseif isunix
        dbPath      = [specs.paths.dbpath, '/', dbName, '/'];    % Database paths
        dbFile      = [dbPath, '/', dbName,'.csv'];   % Input file
        CTDIR       = specs.paths.ctdir;
        LDIR        = specs.paths.ldir;                            	% License files path
        exePath     = specs.paths.exepath;    % Path to COSMOtherm.exe    
        tempFile    = [tempFolder, 'temp/temp_'];  
        inputPath   = [tempFolder, 'input/']; 
        % COSMOfrag specific
        LDIRfrag    = specs.paths.ldirfrag;                            	% License files path
        exeFrag     = specs.paths.exefrag;
    end
  
end