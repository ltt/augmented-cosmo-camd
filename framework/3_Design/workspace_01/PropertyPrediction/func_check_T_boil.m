function [T_boil_ok, T_boil] = func_check_T_boil( AntoineParam, specs)
% Checking boiling point temperature of solvent for given Antoine-parameter
% at 1 atmosphere (1.013 bar)
% The lower and upper bound of acceptable boiling temperature are given in
% the "specs"-struct, fields T_boil_max and T_boil_min

warning('off', 'all');
options=optimset('Display','off');

%% Check T_boil of given compound

% Get Antoine Parameter from molStruct
A = AntoineParam(1);
B = AntoineParam(2);
C = AntoineParam(3);

if strcmp(specs.T_boil_max, 'no_T_max')
    T_max = 1e99;
else
    T_max = str2double(specs.T_boil_max);
    T_max = T_max +273.15;
end
if strcmp(specs.T_boil_min, 'no_T_min')
    T_min = 0;
else
    T_min = specs.T_boil_min;
    T_min = T_min +273.15;
end

p = 1013; %[mbar]

T_boil = fsolve(@(T) -log(p) + A - B/(T+C), 373, options);

if(T_boil > T_max)
        T_boil_ok = 0;
elseif(T_boil < T_min)
        T_boil_ok = 0;
else    T_boil_ok = 1;
end

end