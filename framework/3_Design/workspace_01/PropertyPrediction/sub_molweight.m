function [ MW_solvent ] = sub_molweight(compound, k, specs )
% Get molecular weights from COSMOfrag

%% General: Get path definitions

[~, tempFile, ~, ~, ~, ~, ~, ~, CFDB, LDIRfrag, exeFrag] = main_header(specs);


%% Execute job

MW_solvent = cell(0);

% Use COSMOfrag for optimization mode
if strcmp(specs.Mode, 'optimization')
    tempFileMW = [tempFile,'frag_', specs.Number];
    fileID = fopen([tempFileMW,'.inp'],'wt');
    
    fprintf(fileID,['ACTION=7 cfdbdir=', CFDB, ' strdir=.', '\n']);
    fprintf(fileID,['licensedir=', LDIRfrag, ' USMILES FULLTAB \n\n']);
    fprintf(fileID,['smi:',compound]);

    % Execute COSMOfrag
    %chdir('temp');
    if ispc
        status = dos([exeFrag,' ' tempFileMW, '.inp"']);
    elseif isunix
        status = system([exeFrag,' ' tempFileMW, '.inp']);
    end

    %% open .out datei and get MOLWEIGHT
    fileID = fopen([tempFileMW,'.out'],'rt');
    line = fgetl(fileID);
    i = 0;
    while ~numel(strfind(line,'MOLWEIGHT')) && i < 100 % Move file pointer to line of interest
         line = fgetl(fileID);
         i = i + 1; % Prevent eternal loop
    end
    fclose(fileID);
    formatSpec = '%*s %*s %n';
    MW_solvent = textscan(line,formatSpec); % Read line
    
    % Read mcos name
    fileID = fopen([tempFileMW,'.out'],'rt');
    line = fgetl(fileID);
    i = 0;
    while ~numel(strfind(line,'MOLTAB')) && i < 100 % Move file pointer to line of interest
         line = fgetl(fileID);
         i = i + 1; % Prevent eternal loop
    end
    fclose(fileID);
    formatSpec = '%*s %*s %s';
    mcos_name = textscan(line,formatSpec); % Read line
    mcos_name = mcos_name{1,1}{1,1};

% Use COSMOtherm for screening mode
elseif strcmp(specs.Mode, 'screening')

    jobPrint = 'Gamma=1 tc=25.0  # Automatic Activity Coefficient Calculation';
    
    [tempFileMW,~] = cosmoSingleCall(k, {compound}, specs, jobPrint);

    %% open .out datei and get MOLWEIGHT
    fileID = fopen([tempFileMW,'.out'],'rt');
    line = fgetl(fileID);
    i = 0;
    while ~numel(strfind(line,'Molecular Weight')) && i < 200 % Move file pointer to line of interest
         line = fgetl(fileID);
         i = i + 1; % Prevent eternal loop
    end
    fclose(fileID);
    formatSpec = '%*s %*s %*s %n %*s';
    MW_solvent = textscan(line,formatSpec); % Read line
end

% Final molecular weight as number
MW_solvent = MW_solvent{1};

% Delete temporary files
if exist([tempFileMW, '.inp'], 'file')>0
    delete([tempFileMW, '.inp'])
end
if exist([tempFileMW, '.tab'], 'file')>0
    delete([tempFileMW, '.tab'])
end
if exist([tempFileMW, '.out'], 'file')>0
    delete([tempFileMW, '.out'])
end


 %% Delete all mcos-Files (only necessary for COSMOfrag)
if strcmp(specs.Mode, 'optimization')
    delete([mcos_name,'.mcos']);
end

end

