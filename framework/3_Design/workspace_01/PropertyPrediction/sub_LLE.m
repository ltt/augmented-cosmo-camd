function [result,ok] = sub_LLE(active_mixture, k,compounds, T, specs)
%% Calculates a binary liquid-liquid-equilibrium
%
% Input:    active_mixture: Number of 2 compunds for the LLE 
%           compunds
%           k: unique number k used for identiying files 
%           T: temperature vector 
%           specs 
%           
%
% Output:   Antoine Parameter
%%

 %% Generate jobs "jobPrint"
    jobPrint = ''; % Initialize jobPrint               
    tk = num2str(T(1));
    jobPrint = strcat(jobPrint, [ 'binary={' active_mixture '} tk= ' tk '  LLE  ignore_charge # Binary VLE computation \n' ]); 
     
 %% Call COSMOtherm
    
    [tempFile,cosmoSpecAlgo] = cosmoSingleCall( k, compounds, specs,jobPrint);
   
 %% ReadOutputFile 
 
    [result,ok] = extractLLE(tempFile,cosmoSpecAlgo);
    
 %% Delete temporary files after calculation     
    
    if exist([tempFile, '.inp'])>0
        delete([tempFile, '.inp'])
    end
    if exist([tempFile, '.tab'])>0
        delete([tempFile, '.tab'])
    end
    if exist([tempFile, '.out'])>0
        delete([tempFile, '.out'])
    end

end

function [result,ok] = extractLLE(tempFile,cosmoSpecAlgo)
% RETURNS [xS{1}, xS{2}, xS{3}, xS{4}] = Phase I; Phase II
% Check if .out file is empty
s = dir([tempFile,'.tab']);
if s.bytes == 0
    result = NaN([1,4]); % Empty file
    ok = 0;
    disp('ERROR: tab file empty');
else
    ok = 1;
    fileID = fopen([tempFile,'.tab'],'r');
    line = fgetl(fileID);
    i = 0;
    while ~numel(strfind(line,'LLE point found at')) && i < cosmoSpecAlgo*2 % Move file pointer to line of interest
         line = fgetl(fileID);
         i = i + 1; % Prevent eternal loop
    end
    fclose(fileID);
    if strfind(line, 'LLE point found at')
        formatSpec = '%*s %*s %*s %*s %*s %*s %n %*s %*s %n %*s %*s %*s %n %*s %*s %n %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s';
        xS = textscan(line,formatSpec); % Read line
        result = [xS{1}, xS{2}, xS{3}, xS{4}];
    else result = nan;
    end
end
end