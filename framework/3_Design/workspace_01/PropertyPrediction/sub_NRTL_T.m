function results = sub_NRTL_T (k,compounds, T, specs) 
%% Calculates the temperature dependend binary NRTL Parameters for given compunds 
% Input:    compunds 
%           k: unique number k used for identiying files 
%           T: temperature vector  
%           specs 
%
% Output    Struct with 3 fields 
%           1) resultsGamma: Gamma1 and Gamma2 calculated by COMSO-RS used for the NRTL
%           fit 
%           2) NRTLParam_T: temperature dependend NRTL Parameters in order 
%               1) alpha 
%               2) tau12
%               3) tau21
%
% The Function contains 7 Sub functions  
%
%   1) getGammaArray: Claculates a (T,x) sized array with gamma values in COSMO-Rs 
%   2) calcNRTL: Calculates temperature dependent NRTL parameters for given
%      gamma array 
%   3) quadSumNRTL: used for the NRTL fit 
%   4) quadSumNRTLLinAlph: used for the NRTL fit 
%   5) calc_Err: Calculates the error of the NRTL fit 
%   6,7) calculate NRTL parameters form NRTL(T) parameters
%%

%% Calculate Gamma Array 

[resultsGamma1,resultsGamma2] = getGammaArray(k,compounds, T, specs);

%% Calculate NRTL Parameters
NRTLParam_T = calcNRTL(resultsGamma1,resultsGamma2,T,specs);

%% Calculate Err
Err = calc_Err(NRTLParam_T,resultsGamma1,resultsGamma2,T,specs);

%% Set Results 
results.resultsGamma{1,1} = resultsGamma1;
results.resultsGamma{1,2} = resultsGamma2;
results.NRTLParam_T = NRTLParam_T;
results.Err = Err;
      
end

function [resultsGamma1,resultsGamma2] = getGammaArray(k,compounds, T, specs) 

    %% Prelocate results and set Composition 
    composition = specs.composition; 
    resultsGamma1 = zeros(length(T),length(composition));
    resultsGamma2 = zeros(length(T),length(composition));

    %% Calculate Gamma for all T,x
    for j=1:length(T)         
        for i=1:length(composition);
            temp = sub_gamma([composition(i),1-composition(i)],k,compounds, T(j), specs);
            resultsGamma1(j,i) = temp(1);
            resultsGamma2(j,i) = temp(2);
        end   
    end
end


function NRTLParam_T = calcNRTL(resultsGamma1,resultsGamma2,T,specs) 

%% Set initial data & prelocate Fields
    
     
    composition = specs.composition;
    resultsNRTLPar = zeros(length(T),3);


%% Fit NRTL 
% The regression is doen twice to increase acurracy. In the first regression
% alpha and tau are determint. In the second regerssion alpha assumed to be 
% a linear function and Tau is fittet     

    for c = 1:2
        
        %% Fit NRTL Para to Gamma(x)
        if c ==1
            for j=1:length(T);
                para0 = [0.3,0,0];   
                resultsNRTLPar(j,:) = fminsearch(@(para)quadSumNRTL(para,composition,resultsGamma1(j,:),resultsGamma2(j,:)),para0,optimset('TolX',1e-6,'MaxIter',1E5,'Display','off'));
                resultsNRTLPar(j,1) = resultsNRTLPar(j,1)^2;
            end          
        else
            for j=1:length(T);
                para0 = [0,0];    
                alph = calcAlphaFromPar(T(j),NRTLParam_T(1,1:2));
                resultsNRTLPar(j,:) = [alph,fminsearch(@(para)quadSumNRTLLinAlph(para,composition,resultsGamma1(j,:),resultsGamma2(j,:),alph),para0,optimset('TolX',1e-6,'MaxIter',1E5,'Display','off'))];
            end
        end
        %% Fit NRTL(T) to NRTL 
        warning ('off','all'); 
        X = [T.^-1, log(T), T];
        NRTLParam_T = zeros(3,4);
        coeffVals = fit(T,resultsNRTLPar(:,1),'poly1'); 
        NRTLParam_T(1,1:2) = [coeffVals.p2,coeffVals.p1];
        coeffVals = robustfit(X, resultsNRTLPar(:,2), 'ols');
        NRTLParam_T(2,:) = coeffVals;
        coeffVals = robustfit(X, resultsNRTLPar(:,3), 'ols');
        NRTLParam_T(3,:) = coeffVals; 
        warning('on','all')
    end
end    


function d = quadSumNRTL(para,X,Gamma1Cosmo,Gamma2Cosmo) 

%% Set inital Data
x1=X;
x2=(1-X);
G12= exp(-para(1)^2*para(2));
G21= exp(-para(1)^2*para(3));
tau12=para(2);
tau21=para(3);

%% Calc Log Gamma 

 gamma1 =   x2.^2.*(tau21.*(G21./(x1+x2.*G21)).^2+tau12*G12./(x2+x1.*G12).^2);
 
 gamma2 =   x1.^2.*(tau12.*(G12./(x2+x1.*G12)).^2+tau21*G21./(x1+x2.*G21).^2);

%% QuadSumm

d=sum(abs(Gamma1Cosmo-gamma1).^2) + sum(abs(Gamma2Cosmo-gamma2).^2); 


%% Set  constrains 
% Function is setting limits to Alpha

    %Alpha has to be greater than 0.05 
    d = d+ 1/((para(1)-0.05)^2*100);
    %Alpha has to be smaler than 1
    d = d+ 1/((para(1)-1)^2*100);
    % in Case alpha manages to jump over the barriere 
    if (para(1)< 0.05 || para(1)>1)
        d=inf;
    end
end

function d = quadSumNRTLLinAlph(para,X,Gamma1Cosmo,Gamma2Cosmo,alph)

%% Set inital Data
x1=X;
x2=(1-X);
G12= exp(-alph*para(1));
G21= exp(-alph*para(2));
tau12=para(1);
tau21=para(2);

%% Calc Log Gamma 

 gamma1 =   x2.^2.*(tau21.*(G21./(x1+x2.*G21)).^2+tau12*G12./(x2+x1.*G12).^2);
 
 gamma2 =   x1.^2.*(tau12.*(G12./(x2+x1.*G12)).^2+tau21*G21./(x1+x2.*G21).^2);

%% QuadSumm

d=sum(abs(Gamma1Cosmo-gamma1).^2) + sum(abs(Gamma2Cosmo-gamma2).^2); 

end

function Err = calc_Err(NRTLParam_T,resultsGamma1,resultsGamma2,T,specs)
      
gammaCalc = zeros(length(T), length(specs.composition) * 2);

%% Calc NRTL Parameter from NRLT(T) Parameter
NRTLPar = zeros(length(T),3);
NRTLPar(:,1)=calcAlphaFromPar(T,NRTLParam_T(1,1:2));
NRTLPar(:,2)=calcTauFromPar(T,NRTLParam_T(2,:));
NRTLPar(:,3)=calcTauFromPar(T,NRTLParam_T(3,:));

%% Calc Gamma from NRLT Parameter         
    for i=1:length(T)
        temp2 =NRTL_function(specs.composition,NRTLPar(i,:)); 
        gammaCalc(i,:)= [temp2(1,:),temp2(2,:)];
    end

%% Calc Error 
Err = (exp(gammaCalc)./exp([resultsGamma1,resultsGamma2]));

minErr = min(abs(Err(:)));
maxErr = max(abs(Err(:)));     

Err = max([1/minErr,maxErr]);
            
end
function gamma = NRTL_function(x,para)

x1=x;
x2=(1-x);
G12= exp(-para(1)*para(2));
G21= exp(-para(1)*para(3));
tau12=para(2);
tau21=para(3);

gamma = zeros(2,length(x));

gamma1 = x2.^2.*(tau21.*(G21./(x1+x2.*G21)).^2+tau12*G12./(x2+x1.*G12).^2);
gamma2 = x1.^2.*(tau12.*(G12./(x2+x1.*G12)).^2+tau21*G21./(x1+x2.*G21).^2);

gamma(1,:)=gamma1;
gamma(2,:)=gamma2;

end
function alpha = calcAlphaFromPar(T,NRTLPar)

alpha = NRTLPar(1) + NRTLPar(2) .*T;

end
function tau= calcTauFromPar(T,NRTLpar)


tau = NRTLpar(1) + NRTLpar(2)./T + NRTLpar(3).*log(T)  + NRTLpar(4) .* T;

end
