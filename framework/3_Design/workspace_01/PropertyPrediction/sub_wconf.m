function [ConfNum, wconf, sigmaProfiles ] = sub_wconf(compounds,k,specs)
% Calculates the distribution of confomers and the Sigma Profiles 
%
% Input:    compunds
%           k: unique number k used for identiying files  
%           specs 
%           
%
% Output:   [number of conformer, weigth of conformer, sigma profiles]
%

 %% Generate jobs "jobPrint"
    jobPrint = ''; % Initialize jobPrint   
    jobPrint = strcat(jobPrint, ['x_pure = 1 tc=25.0 ctab wconf  # Automatic Mixture Calculation', '\n']); 
    adHead = 'autoc sprf'; 
    
 %% Call COSMOtherm
    
    [tempFile,~] = cosmoSingleCall( k, compounds, specs,jobPrint,adHead);
   
 %% ReadOutputFile 
 
    [ConfNum, wconf, sigmaProfiles ] = extractWconf(tempFile);
    
 %% Delete temporary files after calculation     
    
    if exist([tempFile, '.inp'])>0
    delete([tempFile, '.inp'])
    end
    if exist([tempFile, '.tab'])>0
    delete([tempFile, '.tab'])
    end
    if exist([tempFile, '.out'])>0
    delete([tempFile, '.out'])
    end
    if exist([tempFile, '.prf'])>0
    delete([tempFile, '.prf'])
    end

end

function [ConfNum, wconf, sigmaProfiles ] = extractWconf(tempFile)
%% Get Results

%% Confomer weigth

    % Check if .out file is empty
    s = dir([tempFile,'.tab']);
    if s.bytes == 0
        result = NaN; % Empty file
        disp('ERROR: tab file empty');
    else
        fileID = fopen([tempFile,'.tab'],'r');
        line = fgetl(fileID);
        i = 0;
        while ~numel(strfind(line,'Nr Molecule/Conformer')) && i < 3000 % Move file pointer to line of interest
             line = fgetl(fileID);
             i = i + 1; % Prevent eternal loop
        end
        formatSpec = '%f %s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f';
        lastConf = 0;
        ConfNum = 0;
        while ~lastConf
            line = fgetl(fileID); % For wconf: information below "Nr Molecules/Conformer"
            if isempty(line)
                lastConf = 1;
            else
                content = textscan(line,formatSpec); % Read line
                ConfNum = ConfNum + 1;
                wconf(ConfNum) = content{1,13}; % Extract conformer weigth           
            end
        end 

    %% Sigma profiles of conformers

        while ~numel(strfind(line,'sigma')) && i < 3000 % Move file pointer to line of interest
             line = fgetl(fileID);
             i = i + 1; % Prevent eternal loop
        end
        formatSpec = '%f';
        for i = 1 : ConfNum
            formatSpec = [formatSpec, ' %f'];
        end
        for i = 1 : 61
            line = fgetl(fileID); % For sigma profile: information below "sigma"
            content = textscan(line,formatSpec); % Read line
            for j = 0 : ConfNum % Extract respective sigma profiles for all conformers
                sigmaProfiles(i, j+1) = content{1, j+1};
            end        
        end    
    end  
    fclose('all');
end