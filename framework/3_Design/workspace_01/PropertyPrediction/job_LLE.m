function [ molStruct ] = job_LLE( molStruct,compounds,specs )

for k=1:length(molStruct);
    molStruct(k).x_LLE = NaN;
end
%% Job procedure
%%

T = specs.T+273.15; %Conversion ot T/C to T/K 

for k=1:length(molStruct);
 
%% LLE calculations
         active_mixture = specs.LLEcomp;
         molStruct(k).x_LLE = sub_LLE(active_mixture, k,compounds(k,:), T, specs);

end

end

