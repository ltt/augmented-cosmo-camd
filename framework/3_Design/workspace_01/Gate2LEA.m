function Gate2LEA( job, gate, solvent, parametrization, noofcalc, precalc )
% Handling input and output arguments of LEA3D optimization and MATLAB
% property prediction and process model
% Two modes: (i) Scoring of molecules and 
%            (ii) collecting scored molecule structs
%            (iii) write specs-struct


%%  only job hat a value--Yifan changed?????

    %% Load Inputs from corresponding job input file
    addpath('JobInput');
    
    % Heuristic partitition coefficient
    % loadInputs_logP;
    
    % Heuristic partitition coefficient from Gmehling et al.
    loadInputs_Gmehling; % --Yifan changed
    
    % Hybrid Extraction-Distillation process
    % loadInputs_Mex_RBM;
    
    % Superstructure optimization of formic acid derivatives production
    % loadInputs_Exergy_Loss;
    
    % Reaction rates optimization in liquid phase
    %loadInputs_kinetics_liquid; % --Yifan changed
    
    
    rmpath('JobInput');
    
   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Mode (I): Scoring of molecule
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(job, 'Scoring')
    %% Turn off parpool-autocreate
    TurnOffParPool;
    
try
    %% Call property prediction and process model evaluation routine 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
    % Check if molecule was already scored before
    % If yes, do not score again, if not, execute procedure
    if precalc
        nameofstruct = 'molStruct_precalculation.mat';
    else
        nameofstruct = 'molStruct_of_all_molecules.mat'; % where are you from?? --Yifan
    end
    
    if exist(nameofstruct, 'file')
        load(nameofstruct);
        index = find(strcmp({molStruct_all.name}, solvent));
        if isempty(index)
            already_scored = 0;
        else
            molStruct = molStruct_all(index(1));
            already_scored = 1;
        end
    else already_scored = 0;
    end
    
    if (~already_scored) % is 0. so molstruct(****) not exist --Yifan
        % Create a molstruct with the corresponding molecule
        name = ['LEA_', noofcalc]; % means generation--Yifan, i think it is num2str() or??
        molStruct = struct('name', solvent);
        addpath([specs.maindir,'/PropertyPrediction/']);

        %% Constraints
        % Check T_boil
        if (~strcmp(specs.T_boil_min, 'no_T_min') || ~strcmp(specs.T_boil_max, 'no_T_max'))
            molStruct = main_cosmoData(molStruct ,'T_boil_con', specs);
        else molStruct.T_boil_ok = 1;
        end

        % Constraints fulfilled
        if molStruct.T_boil_ok % && molStruct.COSMOfrag_Score_ok
            molStruct.ConCheck = 1;
        else
            molStruct.ConCheck = 0;
        end
    
    end
    
    % Output results of constraints check into comments section
    comments = cell(0);
    comments{length(comments)+1} = '__________________________________________________\n\n';
    
    % Constraints on T_boil
    if (~strcmp(specs.T_boil_min, 'no_T_min') || ~strcmp(specs.T_boil_max, 'no_T_max'))
        
        comments{length(comments)+1} = ['T_boiling by Antoine equation = ', num2str(molStruct.T_boil), ' K \n'];
        
        if (~strcmp(specs.T_boil_min, 'no_T_min') && molStruct.ConCheck)
            comments{length(comments)+1} = ['T_boil is ok (',num2str(molStruct.T_boil), ' K > ' num2str(specs.T_boil_min+273.15), ' K )\n'];
        elseif (~strcmp(specs.T_boil_min, 'no_T_min') && (~molStruct.ConCheck))
            comments{length(comments)+1} = ['T_boil is too low (',num2str(molStruct.T_boil), ' K < ' num2str(specs.T_boil_min+273.15), ' K )\n'];
        end
        if (~strcmp(specs.T_boil_max, 'no_T_max') && molStruct.ConCheck)
                comments{length(comments)+1} = ['T_boil is ok (',num2str(molStruct.T_boil), ' K < ' num2str(specs.T_boil_max+273.15), ' K )\n'];
        elseif (~strcmp(specs.T_boil_max, 'no_T_max') && (~molStruct.ConCheck))
                comments{length(comments)+1} = ['T_boil is too high (',num2str(molStruct.T_boil), ' K > ' num2str(specs.T_boil_max+273.15), ' K )\n'];
        end
        comments{length(comments)+1} = '\n';
    end

    
    %% 
    if (~already_scored)
        % Mextraction and RBM routine
        if (strcmp(specs.Objfunc, 'Mex-RBM') && molStruct.ConCheck)
                molStruct = ScoreWithMexRBM(molStruct, specs);

        % logP-Score
        elseif (strcmp(specs.Objfunc, 'logP') && molStruct.ConCheck)
                molStruct = calculatelogPScore(molStruct, specs);

        % Gmehling-Score
        elseif (strcmp(specs.Objfunc, 'Gmehling') && molStruct.ConCheck)
                molStruct = calculateGmehlingScore( molStruct, specs);
                
        % Exergy-Loss-Score
        elseif (strcmp(specs.Objfunc, 'Exergy-Loss') && molStruct.ConCheck)
                molStruct = ScoreExergyLoss( molStruct, specs);
        
        
        % liquid_kinetics-Score
        elseif (strcmp(specs.Objfunc, 'kinetics_liquid') && molStruct.ConCheck)
                molStruct = ScoreKineticsLiquid( molStruct, specs);
        
        
        end      
        
    end

%% Print all results and comments to out2LEA-File

    [molStruct, comments] = printComments( molStruct, comments, specs);

    output_LEA = fopen([specs.Gate{4}, '/out_LEA_', specs.Number, '.txt'],'wt');

    % Score
    formatSpec = '%10.6f';
    fprintf(output_LEA, formatSpec, molStruct.Score);
    fprintf(output_LEA, '\n');

    formatSpec = '%1.0f';
    fprintf(output_LEA, formatSpec, molStruct.ConCheck);
    fprintf(output_LEA, '\n');

    % Output further comments and intermediate results of scoring
    for i=1:length(comments)
        fprintf(output_LEA, comments{i});
    end
    
    %% Save molStruct
    if ~already_scored
        save([gate{3}, '/LEA_', noofcalc], 'molStruct');
    end
    
catch
	fprintf ('\n\nWarning! Error in MATLAB during process model evaluation!\n\n');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Mode (II): Collecting results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

elseif strcmp(job, 'Sum_molStructs')
% Add up all molStructs of individually scored molecules
    % Gets molStructs from Gate2LEA and combines them to one cumulative
    % molStruct containing all molecules of LEA optimization run
    try
        fprintf ('\n\nCollecting molStructs...\n\n');

        maindir = pwd;

        if precalc
            nameofstruct = 'molStruct_precalculation.mat';
        else
            nameofstruct = 'molStruct_of_all_molecules.mat';
        end

        % Pick up old molStruct if existing
        if exist(nameofstruct, 'file')
            load(nameofstruct);
            molStruct_exists = 1;
        else molStruct_exists = 0;
        end

        % Move to gate
        chdir(gate{3});

        % Get gate content
        content = dir(pwd); % you get everything in current directly of matlab.

        % add all structs from gate to cumulative molStruct
        for i=1:length(content)
            ismatfile = strfind(content(i).name, '.mat');
            
            if ~isempty(ismatfile)
                
                temp_struct = load(content(i).name);
                name_of_loaded_struct = fieldnames(temp_struct);
                
                if molStruct_exists
                    molStruct_all = [molStruct_all,  temp_struct.(name_of_loaded_struct{1})];
                else
                    molStruct_all = temp_struct.(name_of_loaded_struct{1});
                    molStruct_exists = 1;
                end
            end
        end

        % % Clean gate directory
        system('rm -rf *.mat');

        %% Clean cumulative molStruct from multiple entries of one molecule
        % Step I: Determine number of molecules to delete
        restart = 1;
        while restart
            current_mols = {molStruct_all.name};
            index = cell(length(current_mols),1);
            for k = 1:length(current_mols)
            index{k} = find(strcmp(current_mols, current_mols{k}));
            end

            % Step II: Delete molecules
            for k = 1:length(index)
                if length(index{k}) > 1
                    for i=0:length(index{k})-2
                            molStruct_all(index{k}(length(index{k})-i)) = [];       % delete all double entries	
                    end
                    break;
                end
            end 
            if k==length(index)
                restart = 0;
            end
        end

        %% Save cumulative molStruct in workspace folder
        chdir(maindir);

        save(nameofstruct, 'molStruct_all')
catch
	fprintf ('\n\nWarning! Error in MATLAB during summation of molStructs!\n\n');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Mode (III): Write specifications (specs-struct)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    
elseif strcmp(job, 'Write_specs')
    % Open a text-file
    
    specs_out = fopen('specifications_of_current_run', 'wt');
    
    % Print specifications into a file for LEA
    fprintf(specs_out, ['Objective Function: ', objfunc, '\n\n']);
     
    % Output system used
    if length(components) == 3
        fprintf(specs_out, ['Ternary system: \n']);
    elseif length(components) == 4
        fprintf(specs_out, ['Quaternary system: \n']);
    elseif length(components) == 5
        fprintf(specs_out, ['Quinary system: ']);
    elseif length(components) == 6
        fprintf(specs_out, ['Senary system: \n']);
    elseif length(components) == 7
        fprintf(specs_out, ['Septenary system: \n']);
    elseif length(components) == 8
        fprintf(specs_out, ['Octonary system: \n']);
    elseif length(components) == 9
        fprintf(specs_out, ['Nonary system: \n']);
    else fprintf(specs_out, ['System: \n']);
    end
    
    for s=1:length(components)
        fprintf(specs_out, ['\t\t\t\t -', components{s}, '\n']);
    end
    
    if strcmp(objfunc, 'Mex-RBM')
        % NRTL-Calculation options:
        fprintf(specs_out, ['\nNRTL-parameter calculation options:\n']);
        fprintf(specs_out, ['\t\t\t\t - T_start = ', num2str(T_start), '°C\n']);
        fprintf(specs_out, ['\t\t\t\t - T_end = ', num2str(T_end), '°C\n']);
        fprintf(specs_out, ['\t\t\t\t - T_interval = ', num2str(T_interval), '°C\n\n']);

        % Parameters for extraction column
        fprintf(specs_out, ['Parameters for extraction column:\n']);
        fprintf(specs_out, ['\t\t\t\t - Tc_Extr_max = ', num2str(Tc_Extr_max), '°C\n']);
        fprintf(specs_out, ['\t\t\t\t - Tc_Extr_min = ', num2str(Tc_Extr_min), '°C\n']);
        fprintf(specs_out, ['\t\t\t\t - p_Extr = ', num2str(pExtr), ' bar \n\n']);

        % Parameters for distillation column
        fprintf(specs_out, ['Parameters for distillation column:\n']);
        fprintf(specs_out, ['\t\t\t\t - Tc_RBM = ', num2str(TcRBM), '°C\n']);
        fprintf(specs_out, ['\t\t\t\t - p_RBM = ', num2str(pRBM), ' bar \n']);
        
    elseif strcmp(objfunc, 'logP') || strcmp(objfunc, 'Gmehling')
        fprintf(specs_out, ['\nParameters for LLE- and gamma-calculation:\n']);
        fprintf(specs_out, ['\t\t\t\t - Tc = ', num2str(T), '°C \n']);

    elseif strcmp(objfunc, 'Exergy-Loss')
        % NRTL-Calculation options:
        fprintf(specs_out, ['\nNRTL-parameter calculation options:\n']);
        fprintf(specs_out, ['\t\t\t\t - T_start = ', num2str(T_start), '°C\n']);
        fprintf(specs_out, ['\t\t\t\t - T_end = ', num2str(T_end), '°C\n']);
        fprintf(specs_out, ['\t\t\t\t - T_interval = ', num2str(T_interval), '°C\n\n']);

        % Storage molecule
        fprintf(specs_out, ['CO-Process storage molecule:\n']);
        fprintf(specs_out, ['\t\t\t\t - storage  = ', storage, '\n\n']);
        
    elseif strcmp(objfunc, 'kinetics_liquid')
        fprintf(specs_out, ['specifications\nTemperature for reaction rates calculation:\n']);
        fprintf(specs_out, ['\t\t\t\t - T = ', num2str(T_reaction_value), ' K \n']);
    end
    
    % Constraints on T_boil
    if ~strcmp(T_boil_min, 'no_T_min') || ~strcmp(T_boil_max, 'no_T_max')
        fprintf(specs_out, ['\nConstraint on T_boiling:\n']);
    end
    if ~strcmp(T_boil_min, 'no_T_min')
        fprintf(specs_out, ['\t\t\t\t - T_boil_min = ', num2str(T_boil_min), '\n']);
    end
    if ~strcmp(T_boil_max, 'no_T_max')
        fprintf(specs_out, ['\t\t\t\t - T_boil_max = ', num2str(T_boil_max), '\n']);
    end
    fprintf(specs_out, ['\n']);
    
end

 % without fclose?? ???? --Yifan
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% FUNCTION APPENDIX %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function TurnOffParPool
%% Turning off parpool autocreate function of Matlab
% Required for parallelisation done by LEA3D

    parallelsettings = fopen([prefdir, '/parallel.settings'], 'r');
    line = fgetl(parallelsettings);
    i = 1;
    while ~numel(strfind(line,'<key name="AutoCreate">')) && i < 1000 
           line = fgetl(parallelsettings); % MATLAB: moves one line (= get next line)
           i = i+1; % Prevent eternal loop
           targetline = i+2;
    end    
    fgetl(parallelsettings); %Ignore this one, just necesssary to get the right line
    line = fgetl(parallelsettings); 
    defaultline = '                </bool>';

    % If AutoCreate option is on default, add value = 0 line
    if strcmp(line, defaultline)
        desiredline = '                    <value>0<\/value>';
        defaultline = strrep(defaultline, '/', '\/');
        system(['sed -i ''', num2str(targetline), 's/.*/', desiredline,'\n', defaultline, '/'' ', prefdir, '/parallel.settings']);
    % If Autocreate option is set and not value = 0    
    else
        newline = strrep(line, '1', '0');
        newline = strrep(newline, '/', '\/');
        if ~strcmp(newline, line)
            system(['sed -i ''', num2str(targetline), 's/.*/', newline, '/'' ', prefdir, '/parallel.settings']);
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [molStruct] = calculateGmehlingScore(molStruct, specs)
% Score using the scoring function by Gmehling and Schedemann (2014)
% Required properties are Liquid-Liquid-equilibrium and acitivity coefficients

%% Property prediction and process model procedure 
    molStruct = main_cosmoData(molStruct,'Gmehling',specs);
    rmpath([specs.maindir,'/PropertyPrediction/'])

    % Calculate Score
    addpath([specs.maindir,'/ProcessModel/Gmehling']);        
    molStruct = GmehlingScore(molStruct, specs);
        
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [molStruct] = calculatelogPScore(molStruct, specs)
% Score using the mass-based logP-Score
% Required properties are Liquid-Liquid-equilibrium, acitivity coefficients
% and molecular weight

%% Property prediction and process model procedure 
    molStruct = main_cosmoData(molStruct,'logP',specs);
    rmpath([specs.maindir,'/PropertyPrediction/'])

    % Calculate Score
    addpath([specs.maindir,'/ProcessModel/logP']);	
    molStruct = logPScore(molStruct, specs);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [molStruct] = ScoreWithMexRBM(molStruct, specs)
% Score using the Mextraction and RBM Shortcut models
% Required properties are NRTL-parameter and Antoine coefficients

%% Property prediction and process model procedure 
    % Get NRTL Para
    molStruct = main_cosmoData(molStruct,'Mex-RBM',specs);
    rmpath([specs.maindir,'/PropertyPrediction/'])

    % Call MexRBM routine to evaluate and score
    addpath([specs.maindir,'/ProcessModel/ShortcutModel'])

    % Fixed temperature Tc_Extr
    %molStruct = ScoreMexRBM_tempfix( molStruct, specs );
    
    % Optimize temperature Tc_Extr
    molStruct = ScoreMexRBM_tempopt( molStruct, specs ); 

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [molStruct] = ScoreExergyLoss(molStruct, specs)
% Score Exergy-Loss Superstructure
% Required properties are NRTL-parameter and Antoine coefficients

%% Property prediction and process model procedure 
    % Get NRTL Para
    molStruct = main_cosmoData(molStruct,'Mex-RBM',specs);
    rmpath([specs.maindir,'/PropertyPrediction/'])

    
%% Call optimizeProcess
chdir('ProcessModel/SuperStructureOptimization');
molStruct = optimizeProcess( molStruct, specs );
chdir(specs.maindir);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [molStruct] = ScoreKineticsLiquid( molStruct, specs)
% Score kinetics in liquid phase --> reaction rates
% Required properties are deltaGsolv of all components in reaction mixture

%% Property prediction and process model procedure 
    % Get Properties
    molStruct = main_cosmoData(molStruct,'kinetics_liquid',specs);
    rmpath([specs.maindir,'/PropertyPrediction/'])
    
%% Call optimizeProcess
chdir('ProcessModel/Kinetics');
molStruct = get_kinetics_l( molStruct, specs );
chdir(specs.maindir);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [molStruct, comments] = printComments(molStruct, comments, specs)
%% Print results and set Score for LEA3D
% Print comments which are displayed in LEA to understand the results of
% property prediction and process model evaluation

%% Mextraction and RBM
if (strcmp(specs.Objfunc, 'Mex-RBM') && molStruct.ConCheck)
    comments{length(comments)+1} = '--------------------------------\nMextraction Results:\n';
    if isnan(molStruct.Qreb)
        molStruct.Score = 0;
        if ~isnan(molStruct.SFmin)   
        	comments{length(comments)+1} = ['S_min  =        ', num2str(molStruct.SFmin),'\n'];
            comments{length(comments)+1} = ['T_Extr =        ', num2str(molStruct.Tc_Extr_opt+273.15),'\n'];
            comments{length(comments)+1} = ['x_e    =        ', num2str(molStruct.xExtr), '\n\n'];        
            comments{length(comments)+1} = '--------------------------------------------------\nResults of RBM:\n';
            comments{length(comments)+1} = ['No solution of RBM shortcut for this solvent!\n'];
        else
            comments{length(comments)+1} = ['No solution of Mextraction shortcut for this solvent.\n'];
        end

    else
        molStruct.Score = 1/molStruct.Qreb(2)*10^6;
        comments{length(comments)+1} = ['S_min  =        ', num2str(molStruct.SFmin),'\n'];
        comments{length(comments)+1} = ['T_Extr =        ', num2str(molStruct.Tc_Extr_opt+273.15),'\n'];
        comments{length(comments)+1} = ['x_e    =        ', num2str(molStruct.xExtr), '\n\n'];        
        comments{length(comments)+1} = '--------------------------------------------------\nResults of RBM:\n';
        comments{length(comments)+1} = ['Q_reb =        ', num2str(molStruct.Qreb(2)/10^6),' MJ/mol\n'];
        comments{length(comments)+1} = ['reflux =       ', num2str(molStruct.Rmin),'\n'];
    end

%% logP- and Gmehling--Score
elseif ((strcmp(specs.Objfunc, 'logP') || strcmp(specs.Objfunc, 'Gmehling')) && molStruct.ConCheck)
    if (isnan(molStruct.x_LLE))
        molStruct.Score = 0;
        comments{length(comments)+1} = '--------------------------------------------------\nNo LLE found!\n\n';
    else
        comments{length(comments)+1} = '--------------------------------------------------\nLLE:\n';
        if molStruct.x_LLE(1) >= molStruct.x_LLE(3)
            comments{length(comments)+1} = ['x_LLE_aq  =        ', num2str(molStruct.x_LLE(1:2)),'\n'];
            comments{length(comments)+1} = ['x_LLE_org =        ', num2str(molStruct.x_LLE(3:4)),'\n\n'];
            comments{length(comments)+1} = '--------------------------------------------------\nActivity coefficients:\n';
            comments{length(comments)+1} = ['gamma_aq  =        ', num2str(molStruct.gammaI),'\n'];
            comments{length(comments)+1} = ['gamma_org =        ', num2str(molStruct.gammaII),'\n\n'];
        else
            comments{length(comments)+1} = ['x_LLE_org  =        ', num2str(molStruct.x_LLE(1:2)),'\n'];
            comments{length(comments)+1} = ['x_LLE_aq   =        ', num2str(molStruct.x_LLE(3:4)),'\n\n'];
            comments{length(comments)+1} = '--------------------------------------------------\nActivity coefficients:\n';
            comments{length(comments)+1} = ['gamma_org =        ', num2str(molStruct.gammaI),'\n'];
            comments{length(comments)+1} = ['gamma_aq  =        ', num2str(molStruct.gammaII),'\n\n'];
        end
       
        % logP-Score
        if (strcmp(specs.Objfunc, 'logP'))
            molStruct.Score = molStruct.logP; 
            comments{length(comments)+1} = '--------------------------------------------------\nlogP-Score:\n';
            comments{length(comments)+1} = ['logP =             ', num2str(molStruct.logP),'\n'];

        % Gmehling-Score
        elseif (strcmp(specs.Objfunc, 'Gmehling'))
            molStruct.Score = molStruct.GmehlingScore;
            comments{length(comments)+1} = '--------------------------------------------------\nFactors are:\n';
            comments{length(comments)+1} = ['Score_ws  =   ', num2str(molStruct.Score_ws),'\n'];
            comments{length(comments)+1} = ['Score_wc  =   ', num2str(molStruct.Score_wc),'\n'];
            comments{length(comments)+1} = ['Score_wsl =   ', num2str(molStruct.Score_wsl),'\n\n'];
            comments{length(comments)+1} = '--------------------------------------------------\nGmehling-Score:\n';
            comments{length(comments)+1} = ['Gmehling_Score =   ', num2str(molStruct.GmehlingScore),'\n'];    
        end
    end

%% Exergy-Loss
elseif (strcmp(specs.Objfunc, 'Exergy-Loss') && molStruct.ConCheck)
    if ~isempty(molStruct.Exergyloss)
        molStruct.Score = 1/molStruct.Exergyloss;
	comments{length(comments)+1} = '--------------------------------------------------\nMinimum Exergy Loss:\n';
        comments{length(comments)+1} = ['Exergy Loss =   ', num2str(molStruct.Exergyloss),' kJ/mol\n\n'];
        comments{length(comments)+1} = '--------------------------------------------------\nOptimal Variant:\n';
        comments{length(comments)+1} = ['Optimal Variant =   ', num2str(molStruct.Variant),'\n'];
    else
        molStruct.Score = 0;
	comments{length(comments)+1} = '--------------------------------------------------\nOptimization did not succeed: Score = 0\n';
    end

%% Liquid phase reaction rates
elseif (strcmp(specs.Objfunc, 'kinetics_liquid') && molStruct.ConCheck)
    molStruct.Score = molStruct.rate_constants(2)/molStruct.rate_constants(1); 
    comments{length(comments)+1} = '--------------------------------------------------\nReaction Rate Constants:\n';
    comments{length(comments)+1} = ['NIPAM Polymerization: k =   ', num2str(molStruct.rate_constants(1)),' m3 mol-1 s-1\n\n'];
    comments{length(comments)+1} = ['NIPAM-BIS Crosslinking: k =   ', num2str(molStruct.rate_constants(2)),' m3 mol-1 s-1\n\n'];
    comments{length(comments)+1} = '--------------------------------------------------\nSolubilities in NIPAM and water:\n';
    comments{length(comments)+1} = ['w_NIPAM =  ', num2str(molStruct.w_NIPAM), '\n'];
    comments{length(comments)+1} = ['w_H2O =    ', num2str(molStruct.w_H2O), '\n\n'];
%    if (isnan(molStruct.w_NIPAM)) || (molStruct.w_NIPAM > 0.05)
%	    comments{length(comments)+1} = 'Solubility of NIPAM in water is ok\n\n';
%    else
%	    comments{length(comments)+1} = 'Solubility of NIPAM in water is NOT ok. Scoring terminated with Score = 0\n\n';
%	    molStruct.Score = 0;
%    end
    
    
%% Constraints not fulfilled    
elseif (~molStruct.ConCheck)
    molStruct.Score = 0;
    comments{length(comments)+1} = '--------------------------------------------------\nConstraints are not satisfied! Scoring terminated!\n\n';
    if (strcmp(specs.Objfunc, 'Gmehling'))
        molStruct.x_LLE  = NaN;
        molStruct.gammaI = NaN;
        molStruct.gammaII= NaN;
        molStruct.Score_ws = NaN;
        molStruct.Score_wc = NaN;
        molStruct.Score_wsl = NaN;
        molStruct.GmehlingScore = NaN;
    elseif (strcmp(specs.Objfunc, 'logP'))
        molStruct.x_LLE  = NaN;
        molStruct.gammaI = NaN;
        molStruct.gammaII= NaN;
        molStruct.MW_solvent = NaN;
        molStruct.logP = NaN;
    elseif (strcmp(specs.Objfunc, 'Mex-RBM'))
        molStruct.TauParam = NaN;
        molStruct.AlphaParam = NaN;
        molStruct.resultsGamma = NaN;
        molStruct.err_NRTL = NaN;
        molStruct.Tc_Extr_opt = NaN;
        molStruct.SFmin = NaN;
        molStruct.xExtr = NaN;
        molStruct.extrResult = NaN;
        molStruct.Rmin = NaN;
        molStruct.RBM = NaN;
        molStruct.Qreb = NaN;
    elseif (strcmp(specs.Objfunc, 'Exergy-Loss'))          
        molStruct.TauParam = NaN;
        molStruct.AlphaParam = NaN;
        molStruct.resultsGamma = NaN;
        molStruct.err_NRTL = NaN;
        molStruct.ProcessStructure = [];
        molStruct.Variant = [];
        molStruct.BlockResults = [];
        molStruct.Exergyloss = [];
        molStruct.Topology = [];
    elseif (strcmp(specs.Objfunc, 'kinetics_liquid'))
        molStruct.MW_solvent=NaN;
        molStruct.deltaG_solv_reactants=NaN;
        molStruct.deltaG_solv_TS=NaN;
        molStruct.single_rate_constants=NaN;
        molStruct.rate_constants=NaN;
        molStruct.x_LLE_NIPAM = NaN;
        %molStruct.x_LLE_VCL = NaN;
        molStruct.x_LLE_H2O = NaN;
        molStruct.w_NIPAM = NaN;
	%molStruct.w_VCL = NaN;
        molStruct.w_H2O = NaN;
    end
    
end

comments{length(comments)+1} = '__________________________________________________\n\n\n';

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
