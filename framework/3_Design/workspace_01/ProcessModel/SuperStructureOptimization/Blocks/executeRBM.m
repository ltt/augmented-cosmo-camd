function [n_in, n_out, results] = executeRBM(n_in, specs, properties)
%% main execution of the RBM model
% Input:    n_in (inflow of only relevant species)
%           specs (struct containing specifications (p [bar], compsontop 
%           (those components which are drawn off at the top), tf (Feed
%           temperature [C]))
% Output:   nout (outflow (matrix with two rows, D: row 1, B: row 2))
%           results (results struct (QB [MJ], QC [MJ], TB [K], TC [K], R [-])))

%% Preallocate fields
results = [];

n_out = zeros(2,length(n_in));

%% Check if previous block finished succesful

if (max(n_in) == 0)
    return;
end

%% Definitions

% pressure
p = specs.p; % [bar]

% species considered
NC = length(n_in);

% components drawn off at the top
compsontop = specs.compsontop;

% components drawn off at the bottom
compsonbottom = 1:NC;
compsonbottom(compsontop) = [];

% feed temperature
tf = specs.tf;

%% Thermodynamic Properties

AntoineParam = properties.AntoineParam;
AlphaParam = properties.AlphaParam;
TauParam = properties.TauParam;


%% Initiate mex

system = ['Data/DataRBM.dat']; % Data for mexrbm  
hdl = mexrbm('init',['',system]);

%% Set Antoine parameters
    % Change Antoine equation parameters from ln(p)= A-B/(T+C) in mbar (COSMOtherm 

for i = 1 : NC
    antc1 = AntoineParam{i}(1) + log(100) ;
    antc2 = -AntoineParam{i}(2);
    antc3 = AntoineParam{i}(3);
    antc4 = 0;
    antc5 = 0;
    antc6 = 0;
    antc7 = 0;
    antc8 = 0;
    antc9 = 9000;
    ok = mexrbm('set_antoine',hdl,antc1,antc2,antc3,antc4,antc5,antc6,antc7,antc8,antc9,i);
end

    

    %% Set NRTL parameters
    
for i = 1 : (NC-1)
    for j = (i + 1) : NC
        alpha = AlphaParam{i, j}(1) + 273.15*AlphaParam{i, j}(2); % mexprops expects alpha for C
        alphat = AlphaParam{i, j}(2) ;
        tau12_1 = TauParam{i, j}(1) ;
        tau12_2 = TauParam{i, j}(2) ;
        tau12_3 = TauParam{i, j}(3) ;
        tau12_4 = TauParam{i, j}(4) ;
        tau21_1 = TauParam{j, i}(1) ;
        tau21_2 = TauParam{j, i}(2) ;
        tau21_3 = TauParam{j, i}(3) ;
        tau21_4 = TauParam{j, i}(4) ;
        ok = mexrbm('set_nrtl_alpha',hdl,alpha,alphat,i,j);
        ok = mexrbm('set_nrtl_tau',hdl,tau12_2,tau12_1,tau12_3,tau12_4,i,j);
        ok = mexrbm('set_nrtl_tau',hdl,tau21_2,tau21_1,tau21_3,tau21_4,j,i);
    end
end

%% Molar balances

% Feed
f.flow = sum(n_in) ;
f.z = n_in / f.flow ;

% Distillate
d.flow = sum(n_in(compsontop)) ;
d.z = zeros(1,NC);
for i = 1 : length(compsontop)
    d.z(compsontop(i)) = n_in(compsontop(i))/sum(n_in(compsontop));
end

% Bottom
b.flow = sum(n_in(compsonbottom)) ;
b.z = zeros(1,NC);
for i = 1 : length(compsonbottom)
    b.z(compsonbottom(i)) = n_in(compsonbottom(i))/sum(n_in(compsonbottom));
end

%% Calculate results
    
% check with azeotropic table
azeo = mexrbm('azeo',hdl,[],[],[],[],p);
             
a = size(azeo);
if a(2) == 0
    clear mex;
    return;
end
clear a;

%% Call mex file

%Input: Last entry is vapor fraction of destillate
%Output: (1)qB [J/kmol], (2) qD [J/kmol], (3) TB [K], (4) TD [K], (5)convergence, (6)reflux per Feed flow
mexresult = mexrbm('CALC_RBM',hdl,p,azeo,b.z,d.z,(d.flow/f.flow),tf,0); 
    
%% write results
if (mexresult(5) == 1)
    n_out(1,:) = d.flow * d.z;
    n_out(2,:) = b.flow * b.z;
    results.QB = mexresult(1) * sum(n_in) / 1000000; % [MJ] (flows are assumed to be in [kmol]
    results.QC = mexresult(2) * sum(n_in) / 1000000; % [MJ] (flows are assumed to be in [kmol]
    results.TB = mexresult(3);
    results.TC = mexresult(4);
    results.R = mexresult(6) * f.flow / d.flow;
end

clear mex;

end