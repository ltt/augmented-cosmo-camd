function [molStruct] = runProcess_opt(molStruct, variant, p, specs)
%% Runs the Process model for all molecules in struct name.mat
% variant allows the use of different flowsheets
% p is a parameter vector (variables used for process optimization)
% returns result (value of the objective function with respect to which the
% process is optimized)

addpath([pwd, '/ProcessStructure'])
addpath([pwd, '/Blocks'])
addpath([pwd, '/MexFiles'])
addpath([pwd, '/Evaluation'])

%load(['Properties/',name]); % this struct contains all necessary properties

%% Loop over all molecules

for k = 1 : length(molStruct)
    
    %disp(['Molecule: ', num2str(k), ' / ', num2str(length(molStruct))]);
        
%% Definition of process Structure
% here, the struct containing all process blocks is created
% struct needs fields:
% - number (in calculation sequence)
% - type
% - name
% - species (involved)
% - inflow (contains number of block and number of outflow)
% - block specific specification struct (see execution function of block
%   for details)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% this section needs to be adjusted by the user, e.g. by writing a new sub
% script creating the structure struct

    % choose storage molecule
    storage = specs.storage;

    % For the Co Process, choose from 4 basic variants
    if (variant == 1)
        SBPBStructure; % SBPB Variant from Jens(2016)
    elseif (variant == 2)
        SAPBStructure; % SAPB Variant from Jens(2016)
    elseif (variant == 3)
        SBPAStructure; % SBPA Variant from Jens(2016)
    elseif (variant == 4)
        SAPAStructure; % SAPA Variant from Jens(2016)
    end
    
    % Write azeo table to molStruct
    molStruct(k).Topology = azeo;
    
    if ~possible
        result = 1e100;
        molStruct(k).ProcessStructure = [];
        molStruct(k).Variant = [];
        molStruct(k).BlockResults = [];
        molStruct(k).Exergyloss = [];
        %save(['Output/LEA_', num2str(specs.Number), '.mat'], 'molStruct'); % save procedure
        continue;
    end
    
    % set the optimization variables (for CO Process p is scalar (reactor
    % pressure)
    structureStruct(1).specs.p = p(1);
    n0(4) = p(2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % save a copy of the process structure in molStruct;
    molStruct(k).ProcessStructure = structureStruct;
    molStruct(k).Variant = variant;
    
%% Loop over all blocks

    for l = 1 : length(structureStruct)
        n_in = getInflow(n0, l, structureStruct, molStruct(k));
        % take only relevant species
        n_in = n_in(structureStruct(l).species);
        % read thermodynamic properties from molStruct
        propertiesStruct.AntoineParam = molStruct(k).AntoineParam(structureStruct(l).species);
        propertiesStruct.AlphaParam = molStruct(k).AlphaParam(structureStruct(l).species, structureStruct(l).species);
        propertiesStruct.TauParam = molStruct(k).TauParam(structureStruct(l).species, structureStruct(l).species);
        % execute block
        if strcmp(structureStruct(l).type, 'VLLEReactor')
            [nin, nout, results] = executeVLLEReactor(n_in, structureStruct(l).specs, propertiesStruct);
        elseif strcmp(structureStruct(l).type, 'RBM')
            [nin, nout, results] = executeRBM(n_in, structureStruct(l).specs, propertiesStruct);
        elseif strcmp(structureStruct(l).type, 'HetRBM')
            [nin, nout, results] = executeHetRBM(n_in, structureStruct(l).specs, propertiesStruct);
        elseif strcmp(structureStruct(l).type, 'Mextraction')
            [nin, nout, results] = executeMextraction(n_in, structureStruct(l).specs, propertiesStruct);
        elseif strcmp(structureStruct(l).type, 'RStoic')
            [nin, nout, results] = executeRStoic(n_in, structureStruct(l).specs, propertiesStruct);
        end
        % remove trace elements
        nout = cutTraceElements(nout, 1e-10);
        % write result
        [size_nout, ~] = size(nout);
        molStruct(k).BlockResults.(structureStruct(l).name).Inflow = zeros(1,N);
        molStruct(k).BlockResults.(structureStruct(l).name).Inflow(:,structureStruct(l).species) = nin;
        molStruct(k).BlockResults.(structureStruct(l).name).Outflow = zeros(size_nout,N);
        molStruct(k).BlockResults.(structureStruct(l).name).Outflow(:,structureStruct(l).species) = nout;
        molStruct(k).BlockResults.(structureStruct(l).name).Results = results;
    end

%% Calculation of the results field
% Process evaluation based on user-defined function, e.g. exergy loss etc..    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % this section needs to be adjusted by the user, e.g. by writing a new sub
    % script writing the results field
    
    Eloss = ExergyEvaluation(molStruct(k));
    molStruct(k).Exergyloss = Eloss;
    if ~isempty(Eloss)
        result = Eloss;
    else
        result = 1e100;
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    
    %save(['Output/LEA_', num2str(specs.Number), '.mat'], 'molStruct'); % save procedure
    
end

end

function [ n_in ] = getInflow( n0, number, structureStruct, molStruct )
%% Return the inflow of the number'th block in structureStruct
% if the number is 1: use the flowsheet inflow n0

if (number == 1)
    n_in = n0;
else
    % name of previous block
    name = structureStruct(structureStruct(number).inflow(1)).name;
    % take the correct outlow
    n_in = getfield(molStruct.BlockResults, name, 'Outflow');
    n_in = n_in(structureStruct(number).inflow(2), :);
end


end

function [ n ] = cutTraceElements(n, threshold)
%% Removes trace Elements from streams
% trace elements have flow < threshold

size_n = size(n);

for i = 1 : size_n(1);
    for j = 1 : size_n(2);
        if (n(i,j)<threshold)
            n(i,j) = 0;
        end
    end
end

end

