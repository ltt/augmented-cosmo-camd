%% Writes the process structure struct for CO process variant SBPB
% Change deltaGr for reactors w.r.t. the storage molecule

% general
N = 7; % total number of species
n0 = [1, 1, 0, 1, 0, 1, 1]; % Inflow for flowsheet [kmol/s]
%n0 = zeros(1,N);

% 1st block: storage reactor
structureStruct(1).type = 'VLLEReactor';
structureStruct(1).name = 'StorageReactor';
structureStruct(1).species = 1:7;
structureStruct(1).inflow = [];
structureStruct(1).specs.T = 298.15;
if strcmp(storage, 'DEF')
    structureStruct(1).specs.p= 1;
elseif strcmp(storage, 'DMF')
    structureStruct(1).specs.p = 1;
elseif strcmp(storage, 'MeF')
    structureStruct(1).specs.p = 100;
end
if strcmp(storage, 'DEF')
    structureStruct(1).specs.deltaGR = 5110; % [J/mol] (Values from Jens 2016)
elseif strcmp(storage, 'DMF')
    structureStruct(1).specs.deltaGR = 9920; % [J/mol] (Values from Jens 2016)
elseif strcmp(storage, 'MeF')
    structureStruct(1).specs.deltaGR = 24900; % [J/mol] (Values from Jens 2016)
end
structureStruct(1).specs.v = [-1, -1, 0, 1, 1, -1, 0];
structureStruct(1).specs.phases = 'VLLE';
structureStruct(1).specs.firstliquid = 1; %[1,3];
structureStruct(1).specs.reactingphase = 1; % 1: extraction solvent, 2: catalyst solvent
structureStruct(1).specs.liquids = 4:7;
if strcmp(storage, 'DEF')
    structureStruct(1).specs.xi0 = 0.3;
elseif strcmp(storage, 'DMF')
    structureStruct(1).specs.xi0 = 0.9;
elseif strcmp(storage, 'MeF')
    structureStruct(1).specs.xi0 = 1e-3;
end

% 2nd block: purification
structureStruct(2).type = 'RBM';
structureStruct(2).name = 'DistColumn1';
structureStruct(2).species = 4:7;
structureStruct(2).inflow = [1,3]; %[1,3]: extraction solvent, [1,2]: catalyst solvent
structureStruct(2).specs.p = 1;
structureStruct(2).specs.tf = 0;
structureStruct(2).specs.compsontop = [];

% 3rd block: purification
structureStruct(3).type = [];
structureStruct(3).name = 'DistColumn2';
structureStruct(3).species = 4:7;
structureStruct(3).inflow = [];
structureStruct(3).specs.p = 1;
structureStruct(3).specs.tf = 0;

% 4th block: reforming reactor
structureStruct(4).type = 'RStoic';
structureStruct(4).name = 'ReformingReactor';
structureStruct(4).species = 3:7;
structureStruct(4).inflow = [];
structureStruct(4).specs.v = [1, 0, -1, 1, 0];
structureStruct(4).specs.turnover = 1; % full turnover of storage molecule
structureStruct(4).specs.targetcompound = 3;


%% Fill remaining fields of the structure struct
SBPBTopCheck;
