function molStruct = get_kinetics_l(molStruct,specs)
%compute unimolecular reaction rates in liquid phase 



gas_rates = (specs.gasphase_rate);

for k=1:length(molStruct)
 
 
 Gsolv_reactants = (molStruct(k).deltaG_solv_reactants);
 %Gsolv_reactants = sum(Gsolv_reactants);
 
 Gsolv_TS = (molStruct(k).deltaG_solv_TS);
 
 %Unit conversion
 Gsolv_reactants = Gsolv_reactants.*4.184; %change units to kJ/mol
 Gsolv_TS = Gsolv_TS.*4.184; %kJ/mol
 
 
 
 %get reaction temperature in K
 T_reaction = specs.T_reaction; %K

 reaction_rate_constants = zeros(7,1);
 
 
 %% update reaction rates
 %Reaction 1 NrN 
 for n=1:8
 liquid_rates(n) = gas_rates(n).*exp(-(Gsolv_TS(n)-Gsolv_reactants(1)-Gsolv_reactants(2))/0.008314/T_reaction);
 reaction_rate_constants(1)=reaction_rate_constants(1)+liquid_rates(n);
 end
 
 %Reaction 2 NrB 
 for n=9:15
 liquid_rates(n) = gas_rates(n).*exp(-(Gsolv_TS(n)-Gsolv_reactants(1)-Gsolv_reactants(4))/0.008314/T_reaction);
 reaction_rate_constants(2)=reaction_rate_constants(2)+liquid_rates(n);
 end
 
 %Reaction 3 VrV 
 for n=16:20
 liquid_rates(n) = gas_rates(n).*exp(-(Gsolv_TS(n)-Gsolv_reactants(5)-Gsolv_reactants(6))/0.008314/T_reaction);
 reaction_rate_constants(3)=reaction_rate_constants(3)+liquid_rates(n);
 end
 
 %Reaction 4 VrB
 for n=21:25
 liquid_rates(n) = gas_rates(n).*exp(-(Gsolv_TS(n)-Gsolv_reactants(5)-Gsolv_reactants(4))/0.008314/T_reaction);
 reaction_rate_constants(4)=reaction_rate_constants(4)+liquid_rates(n);
 end
 
 %Reaction 5 BrB
 for n=26:28
 liquid_rates(n) = gas_rates(n).*exp(-(Gsolv_TS(n)-Gsolv_reactants(3)-Gsolv_reactants(4))/0.008314/T_reaction);
 reaction_rate_constants(5)=reaction_rate_constants(5)+liquid_rates(n);
 end
 
 %Reaction 6 BrN
 for n=29:34
 liquid_rates(n) = gas_rates(n).*exp(-(Gsolv_TS(n)-Gsolv_reactants(3)-Gsolv_reactants(2))/0.008314/T_reaction);
 reaction_rate_constants(6)=reaction_rate_constants(6)+liquid_rates(n);
 end
 
 %Reaction 7 BrV
 for n=35:38
 liquid_rates(n) = gas_rates(n).*exp(-(Gsolv_TS(n)-Gsolv_reactants(3)-Gsolv_reactants(6))/0.008314/T_reaction);
 reaction_rate_constants(7)=reaction_rate_constants(7)+liquid_rates(n);
 end
 
%write liquid rates to molStruct
molStruct(k).single_rate_constants = liquid_rates;

%compute overall rate constants for reactions
molStruct(k).rate_constants = reaction_rate_constants;

end


%% Compute Mass Fractions
for k=1:length(molStruct)
%NIPAM
x_LLE_NIPAM=molStruct(k).x_LLE_NIPAM;
if isnan(x_LLE_NIPAM) %full miscibility
    molStruct(k).w_NIPAM = NaN; %no limit on mass fraction
else %limited miscibility
   if x_LLE_NIPAM(1)>x_LLE_NIPAM(3) %second phase is solvent phase
      molStruct(k).w_NIPAM = x_LLE_NIPAM(3)*113.16/(x_LLE_NIPAM(3)*113.16+x_LLE_NIPAM(4)*molStruct(k).MW_solvent);
   else %first phase is solvent phase
      molStruct(k).w_NIPAM = x_LLE_NIPAM(1)*113.16/(x_LLE_NIPAM(1)*113.16+x_LLE_NIPAM(2)*molStruct(k).MW_solvent);
   end
end

% %VCL
% x_LLE_VCL=molStruct(k).x_LLE_VCL;
% if isnan(x_LLE_VCL) %full miscibility
%     molStruct(k).w_VCL = NaN; %no limit on mass fraction
% else %limited miscibility
%    if x_LLE_VCL(1)>x_LLE_VCL(3) %second phase is solvent phase
%       molStruct(k).w_VCL = x_LLE_VCL(3)*139.1/(x_LLE_VCL(3)*139.1+x_LLE_VCL(4)*molStruct(k).MW_solvent);
%    else %first phase is solvent phase
%       molStruct(k).w_VCL = x_LLE_VCL(1)*139.1/(x_LLE_VCL(1)*139.1+x_LLE_VCL(2)*molStruct(k).MW_solvent);
%    end
% end


% %BIS
% x_LLE_BIS=molStruct(k).x_LLE_BIS;
% if isnan(x_LLE_BIS) %full miscibility
%     molStruct(k).w_BIS = NaN; %no limit on mass fraction
% else %limited miscibility
%    if x_LLE_BIS(1)>x_LLE_BIS(3) %second phase is solvent phase
%       molStruct(k).w_BIS = x_LLE_BIS(3)*154.17/(x_LLE_BIS(3)*154.17+x_LLE_BIS(4)*molStruct(k).MW_solvent);
%    else %first phase is solvent phase
%       molStruct(k).w_BIS = x_LLE_BIS(1)*154.17/(x_LLE_BIS(1)*154.17+x_LLE_BIS(2)*molStruct(k).MW_solvent);
%    end
% end

%H2O
x_LLE_H2O=molStruct(k).x_LLE_H2O;
if isnan(x_LLE_H2O) %full miscibility
    molStruct(k).w_H2O = NaN; %no limit on mass fraction
else %limited miscibility
   if x_LLE_H2O(2)>x_LLE_H2O(4) %second phase is solvent phase
      molStruct(k).w_H2O = x_LLE_H2O(4)*18.015/(x_LLE_H2O(4)*18.015+x_LLE_H2O(3)*molStruct(k).MW_solvent);
   else %first phase is solvent phase
      molStruct(k).w_H2O = x_LLE_H2O(2)*18.015/(x_LLE_H2O(2)*18.015+x_LLE_H2O(1)*molStruct(k).MW_solvent);
   end
end

end


end
