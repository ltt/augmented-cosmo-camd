function [ molStruct ] = logPScore( molStruct, specs)
% Score the molStruct molecules using the mass-based logP Score
% System is always Solvent-Water

MW_water = 18.02;

if strcmp(specs.Mode, 'screening')
display (['Score being calculated: logP mass-based']);
end


%% log P:
for k=1:length(molStruct)
    
    x_LLE = molStruct(k).x_LLE;
    if ~isnan(x_LLE(1))
        gamma_I = molStruct(k).gammaI;
        gamma_II = molStruct(k).gammaII;
        MW_solvent = molStruct(k).MW_solvent;
        
            if x_LLE(3) >= x_LLE(1) % x_LLE(3) is aqueous phase
            %calc MW_aq, 
            MW_aq = x_LLE(3)*MW_water + x_LLE(4) * MW_solvent;
            
            %calc MW_org
            MW_org = x_LLE(1)*MW_water + x_LLE(2) * MW_solvent;
            
            P = exp(gamma_II(1)-gamma_I(1))*MW_aq/MW_org;
            
            else                    % x_LLE(1) is aqueous phase
            %calc MW_aq, 
            MW_aq = x_LLE(1)*MW_water + x_LLE(2) * MW_solvent;
            
            %calc MW_org
            MW_org = x_LLE(3)*MW_water + x_LLE(4) * MW_solvent;
            
            P = exp(gamma_I(1)-gamma_II(1))*MW_aq/MW_org;
            
            end
        molStruct(k).logP = log10(P);
    else
        molStruct(k).logP = 0;
    end
    
end

end

