function [minEnergyFound,dpinch,bpinch,dset,bset] = call_rbm_cmo(hpd,p,azeo,d,f,b,reflux,dpline,bpline,options) 
% [minEnergyFound,dpinch,bpinch,dset,bset] = call_rbm(hpd,p,azeo,d,f,b,qb,dpline,bpline) 
% hpd: pointer on property struct
% p: pressure in bar
% azeo: struct of azeotropes
% d,f,b: struct with distillate,feed,bottom product (use get_stream)
% qb: reboiler energy demand
% dpline,bpline: distillat,bottom pinch line (use get_pline): if it is 
% empty [], it is calculated; if possible provide pline to speed up
% calculation
% ---
% minEnergyFound: Flag: 1: rectification bodys intersect, 0: rectification
% bodys don't intersect
% dpinch, bpinch: pinch points for distillate/bottom section
% dset,bset: rectification bodys for distillate/bottom section
% ---

% Vorbelegen
dset = [] ;
bset = [] ;
minEnergyFound = 0 ;


%% CALCULATE PINCH-LINES
if(isempty(bpline))
bpline=mexprops('get_pline',hpd,b.z,p,'bot',[],azeo,[], ...
                'reactive=FALSE');     
end            

if(isempty(dpline))
dpline=mexprops('get_pline',hpd,d.z,p,'dist',[],azeo,[], ...
                'reactive=FALSE');
end
%% CALCULATE PINCH-POINTS
% ToDo ist die Umrechnung richtig?
% VpD soll Dampfanteil in Kolonne PRO Destillat sein
%

VpD = reflux  + 1 ;
dpinch=mexprops('get_pinch_cmo',dpline,VpD,[],31,'reactive=FALSE');

%boilup = d.menge + reflux.*d.menge ;
VpB = d.menge/b.menge*(1+reflux) ;
bpinch=mexprops('get_pinch_cmo',bpline,-VpB,[],31,'reactive=FALSE');

%% FIX BUG IN STABILITY
% There is a problem with the calculation of the stability of pinch points.
% In distillat section heavy boiling pinch point should always be stable. 
% In bottom section light boiling component should always be stable.
% This is fixed here,
% alghoug this is not the proper way:
thisTemp = 0 ;
thisIsStablePinch = 1 ;
for i=1:size(dpinch,2)
    if(dpinch(1,i).t > thisTemp)
        thisIsStablePinch = i ;
        thisTemp = dpinch(1,i).t ;
    end
end
dpinch(1,thisIsStablePinch).imisc = size(d.z,2)-1 ;

thisTemp = 1e99 ;
thisIsStablePinch = 1 ;
for i=1:size(bpinch,2)
    if(bpinch(1,i).t < thisTemp)
        thisIsStablePinch = i ;
        thisTemp = bpinch(1,i).t ;
    end
end
bpinch(1,thisIsStablePinch).imisc = size(d.z,2)-1 ;
%% END: FIX BUG
if(isempty(bpinch) || isempty(dpinch))
    return ;
elseif(size(bpinch,2) == 0 || size(dpinch,2) == 0)
    return ;
elseif(isempty(dpinch(1,1).hpd) || isempty(bpinch(1,1).hpd) )
    return ;
end

[minEnergyFound] = check_for_correct_pinch_topology(dpinch,bpinch,d,b,options) ;

if(minEnergyFound==0)
    dset = [] ;
    bset = [] ;
   return ; 
end


%% CALCULATE RECTIFICATION BODYS+keyboard
if(~isempty(bpinch))
bset = mexprops('get_set_cmo',hpd,b.z,bpinch,0);
end

if(~isempty(dpinch))
dset = mexprops('get_set_cmo',hpd,d.z,dpinch,0);
end

if(isempty(dset) || isempty(bset))
    disp('could not find set') ;
    minEnergyFound = 0 ;
    return
end

if(isempty(bset.simplex) || isempty(dset.simplex))
    disp('could not find set') ;
    minEnergyFound = 0 ;
    return
end


%% CHECK FOR INTERSECTION
[res,fehler,which]=mexprops('get_set_schnitt',bset,dset,0,'STANDARD') ;
minEnergyFound = res ;

end

%% --------------------------------------------------------------------  %%

function [minEnergyFound] = check_for_correct_pinch_topology(dpinch,bpinch,d,b,options)

minEnergyFound = 1 ;

if(options.enforceSaddlePinch ==1)
    iSolvent = options.iSolvent ;
iSolute = options.iSolute ;
    saddleFound = 0 ;
    for i=1:size(dpinch,2),
        if(dpinch(1,i).imisc == 1)
            saddleFound = 1 ;
        end
    end
    
    if(saddleFound == 0)

        minEnergyFound =0 ;
        return
    end
    
    saddleFound = 0 ;
    for i=1:size(bpinch,2),
        if(bpinch(1,i).imisc == 1)
            saddleFound = 1 ;
        end
    end
    
    if(saddleFound == 0)

        minEnergyFound =0 ;
        return
    end
    
end

if(options.enforceSaddleOnEdge)
    iSolvent = options.iSolvent ;
iSolute = options.iSolute ;

    pinchOnEdgeFound =0 ;
   for i=1:size(dpinch,2)
      if(dpinch(1,1).imisc ==1)
         if( (abs(dpinch(1,i).x(1,iSolvent) < 1e-6) ) && (dpinch(1,i).x(1,iSolute) > d.z(1,iSolute) ) )
             pinchOnEdgeFound = 1 ;
         end
          
      end
   end
   
   if(pinchOnEdgeFound == 0)
 
        minEnergyFound =0 ;
        return
    end
    
end

end