function [reflux,dpinch,bpinch,dset,bset,dpline,bpline,d,f,b,minEnergyFound] = calcMinEn_cmo(hpd,p,azeo,d,f,b,dpline,bpline,options)



flag.value = 0 ;
flag.description = 'ok' ;

reflux = 1e99 ;
dpinch = [] ;
bpinch = [] ;
dset = [] ;
bset = [] ;
dpline = [] ;
bpline = [] ;
minEnergyFound =  0 ;

% scale flow-rates:
f_scale = f.menge ;
f.menge = 1 ;
d.menge = d.menge./f_scale ;
b.menge = b.menge./f_scale ;

% berechne Siedetemperaturen:
[d.x,d.y,d.t,d.p,d.vfrac]= mexprops('pzv_flash',hpd,d.z,[],[],[],p,0);
[b.x,b.y,b.t,b.p,b.vfrac]= mexprops('pzv_flash',hpd,b.z,[],[],[],p,0);
[f.x,f.y,f.t,f.p,f.vfrac]= mexprops('pzv_flash',hpd,f.z,[],[],[],p,0);

% check boiling temperatures:
if(d.t > b.t) % switch streams
    temp_stream = b ;
    b = d ;
    d = temp_stream ;
end


ok=mexprops('aze_check_topo',hpd, p,  [],   []);
if(ok == 0)
    disp('WARNING - no consistent topology found; wrong property parameters? Treat results with care...');
    flag.value = -1 ;
    flag.description = 'no consisten topology found;';
    minEnergyFound = 0 ;
    return ;
end



if(isempty(bpline))
    bpline=mexprops('get_pline',hpd,b.z,p,'bot',[],azeo,[], ...
        'reactive=FALSE');
end

if(isempty(dpline))
    dpline=mexprops('get_pline',hpd,d.z,p,'dist',[],azeo,[], ...
        'reactive=FALSE');
end

ok = test_pline(bpline,dpline) ;
if(ok == 0)
    return
end

EXIT = 0 ;
DESIRED_ACCURACY = 1e-12.*1e6 ;
REFLUX_MIN = 1e-7 ;

while(EXIT ==0 )
    refluxUp = 1e99 ;
    
    if(size(dpline,2) == 0 || size(dpline,2) == 0)
        EXIT = 1 ;
        break ;
    end
    
    reflux1 = refluxUp ;
    [minEnergyFound,dpinch,bpinch,dset,bset] = call_rbm_cmo(hpd,p,azeo,d,f,b,reflux1,dpline,bpline,options) ;
    
    if(minEnergyFound == 0)
        disp('no feasible separation found at infinite reflux - exit') ;
        EXIT = 1 ;
    else
        
        reflux1 = 1000 ;
        while(minEnergyFound ==1 && reflux1 >REFLUX_MIN)
            reflux1= reflux1 .*0.5 ;
            [minEnergyFound,dpinch,bpinch,dset,bset] = call_rbm_cmo(hpd,p,azeo,d,f,b,reflux1,dpline,bpline,options) ;
        end
        
        if(reflux>REFLUX_MIN)
            refluxUp = reflux1*2 ;
            [minEnergyFound,dpinch,bpinch,dset,bset] = call_rbm_cmo(hpd,p,azeo,d,f,b,refluxUp,dpline,bpline,options) ;
            
            refluxLo = reflux1 ;
            
            accuracy = abs( (refluxUp - refluxLo)./refluxUp );
            while(accuracy > DESIRED_ACCURACY)
                
                reflux1 = refluxLo + (refluxUp - refluxLo).*0.5 ;
                [minEnergyFound,dpinch,bpinch,dset,bset] = call_rbm_cmo(hpd,p,azeo,d,f,b,reflux1,dpline,bpline,options) ;
                
                if(minEnergyFound == 1)
                    refluxUp = reflux1 ;
                else
                    refluxLo = reflux1 ;
                end
                
                accuracy = abs(refluxUp - refluxLo ) ;
                
            end
        else
            % reflux almost zero
            refluxUp = reflux1 ;            
        end
        
        
    end
    EXIT = 1 ;
    
end

flag.val = 1 ;

reflux = refluxUp ;
[minEnergyFound,dpinch,bpinch,dset,bset] = call_rbm_cmo(hpd,p,azeo,d,f,b,reflux,dpline,bpline,options) ;

% unscale flowrates:
f.menge = f.menge.*f_scale ;
d.menge = d.menge.*f_scale ;
b.menge = b.menge.*f_scale ;
reflux = reflux.*f_scale ;

end

%% -------------------------------------------------------------------- %%


function ok = test_pline(bpline,dpline)

ok = 1 ;

if(size(bpline,2) == 0)
    disp('Could not calculate bottom pinch line - EXIT')
    ok = 0 ;
    return
end

if(size(bpline,2) == 1 )
    if(bpline.nj == 1)
        disp('Could not calculate bottom pinch line - EXIT')
        
        ok = 0 ;
        return
    end
end


if(size(dpline,2) == 0)
    disp('Could not calculate bottom pinch line - EXIT')
    ok = 0 ;
    return
end


if(size(dpline,2) == 1 )
    if(dpline.nj == 1)
        disp('Could not calculate destillate pinch line - EXIT')
        
        ok = 0 ;
        return
    end
end


end
