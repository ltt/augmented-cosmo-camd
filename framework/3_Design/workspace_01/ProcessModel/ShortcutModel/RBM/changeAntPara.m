function newAntPara = changeAntPara(antPara)

 T = 200:10:600;
 
 p = antPara(1) + log(100) - antPara(2)./(antPara(3)+T);

 ft = fittype('a+ b*x^-1 +c*x+d*log(x)');
 
 newAntPara = fit(T', p', ft);
 

 
 p2 = newAntPara.a + newAntPara.b./T + newAntPara.c .* T + newAntPara.d * log(T);
 
 p=exp(p);
 p2 = exp(p2); 
 
 hold on 
 
 plot(T,p,T,p2,'*')
end

