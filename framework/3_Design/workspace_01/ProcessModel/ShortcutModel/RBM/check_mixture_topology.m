function topology = check_mixture_topology(azeo,iSolvent,iSolute)

  %% teste auf tern�res Azeotrop
    
    ternaryAzeoFound = 1 ;
    
    for i=1:size(azeo,2),        
        ternaryAzeoFound = 1 ;
        for j=1:size(azeo(1,1).x,2), % durchsuche all, f�r tern�res Azeo muss einer groesser 0 sein
            if( abs(azeo(1,i).x(1,j) < 1e-6) )
                ternaryAzeoFound = 0;
                break ;
            end
        end
        
        topology.ternaryAzeo = ternaryAzeoFound ;        
        if(ternaryAzeoFound ==1)
            break ;
        end        
    end
    
    %% teste auf azeotrop auf der Ethanol-Solvent Kante
    % Bedingung: Konzentration Ethanol UND Solvent > 1e-6
    
    soluteSolventAzeoFound = 0 ;
    for i=1:size(azeo,2)
       if( azeo(1,i).x(iSolvent) > 1e-6 && azeo(1,i).x(iSolute) > 1e-6 )
           soluteSolventAzeoFound = 1 ;
       end       
    end
    
    topology.soluteSolventAzeoFound = soluteSolventAzeoFound ;
    
    
    %% einordnung der gemisch-topologie
    if(azeo(1,iSolvent).imisc == size(azeo(1,1).x,2)-1)
        topology.solventIsHeavyBoiler = 1 ;
        topolgoy.solventIsLightBoiler = 0 ;
        topolgoy.solventIsMiddleboiler = 0 ;
    elseif(azeo(1,iSolvent).imisc == 0)
        topolgoy.solventIsLightBoiler = 1 ;
        topolgoy.solventIsMiddleboiler = 0 ;
        topology.solventIsHeavyBoiler = 0 ;
    else
        topolgoy.solventIsLightBoiler = 0 ;
        topolgoy.solventIsMiddleboiler = 1 ;
        topology.solventIsHeavyBoiler = 0 ;
    end
    
    
    
end