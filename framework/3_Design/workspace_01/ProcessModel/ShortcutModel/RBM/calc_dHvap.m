function dHvap = calc_dHvap(AntPar,x,T)

R = 8.314;
dHvap = 0;

for i = 1:length(x)
    
    dHvap = x(i)*AntPar{i,1}(2)/(T+AntPar{i,1}(3))^2 * R*T^2 +dHvap;

end