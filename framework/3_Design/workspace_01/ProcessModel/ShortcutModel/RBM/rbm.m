function [reflux, f, b, d,Qreb] = rbm(antoineParam, alphaParam, tauParam, xExtr, specs)
%% Implemented by A. Demuth, bachelor thesis supervised by Jan Scheffczyk @LTT, RWTH Aachen University.	
    %% Initiate mex
    system = [pwd, '/ProcessModel/ShortcutModel/RBM/dummy.dat']; % Folder for temporary files 
    hdl=mexrbm('init',['',system]); 
    
    % Initialize solution vector
    reflux = NaN;
    Qreb = NaN;
    f.z = NaN;
    f.menge = NaN;
    b.z = NaN;
    b.menge = NaN;
    d.z = NaN;
    d.menge = NaN;
    
    %% Set Antoine parameters
    % Change Antoine equation parameters from ln(p)= A-B/(T+C) in mbar (COSMOtherm 
    % spec) to ln(p)= A+B/(T+C) in Pa
    % HMF and water parameters are the same for every calculation
    for k=1:length(antoineParam)

        c1 = k ;
        antc1 = antoineParam{k,1}(1) + log(100) ;
        antc2 = -antoineParam{k,1}(2);
        antc3 = antoineParam{k,1}(3);
        antc4 = 0;
        antc5 = 0;
        antc6 = 0;
        antc7 = 0;
        antc8 = 0;
        antc9 = 9000;
        ok = mexrbm('set_antoine',hdl,antc1,antc2,antc3,antc4,antc5,antc6,antc7,antc8,antc9,c1) ;
        
    end

    %% Set NRTL parameters
    for i=1:length(alphaParam)
        for ii=(i+1):length(alphaParam)

            c1 = i;
            c2 = ii;
            alpha = alphaParam{c1,c2}(1) + 273.15*alphaParam{c1,c2}(2); % mexrbm expects alpha for �C
            alphat = alphaParam{c1,c2}(2) ;
            tau12_1 = tauParam{c1,c2}(1) ;
            tau12_2 = tauParam{c1,c2}(2) ;
            tau12_3 = tauParam{c1,c2}(3) ;
            tau12_4 = tauParam{c1,c2}(4) ;
            tau21_1 = tauParam{c2,c1}(1) ;
            tau21_2 = tauParam{c2,c1}(2) ;
            tau21_3 = tauParam{c2,c1}(3) ;
            tau21_4 = tauParam{c2,c1}(4) ;
            ok = mexrbm('set_nrtl_alpha',hdl,alpha,alphat,c1,c2) ;
            ok = mexrbm('set_nrtl_tau',hdl,tau12_2,tau12_1,tau12_3,tau12_4,c1,c2) ;
            ok = mexrbm('set_nrtl_tau',hdl,tau21_2,tau21_1,tau21_3,tau21_4,c2,c1) ;
    
        end
    end


    %% Calculate results
%     x = xExtr;
%     % gammas
%     gamma=mexrbm('get_prop',hdl,'gml','all',x,[],25,1.013);  
%     % p
% 
%     psat=mexrbm('get_prop',hdl,'plb','all',x,[],25,1.013) 
%     psat=mexrbm('get_prop',hdl,'plb','all',x,[],50,1.013)  
%     psat=mexrbm('get_prop',hdl,'plb','all',x,[],75,1.013)  
    % fuer reinstoffe: x=[ 1 0 0] etc mit werten aus cosmo vergleichen 
    
    % Azeo
%     p = 0.002161024 ; % Vapor pressure of HMF @ 120�C in [bar] (~2kPa)
    p = specs.pRBM;
    azeo = mexrbm('azeo',hdl,[],[],[],[],p);
    azeoCell = {azeo.x};
    
%     if length(azeoCell)>3;
%         azeo.x
%         reflux = 'azeotrope';
%         clear mex
%         return
%     end
    reflux = azeo;
    
    if length(azeoCell)>3
        if azeoCell{4}(1)>0
            reflux = 'azeotrope with hk';
            return;
        end
    end    
    if length(azeoCell)>4
        if azeoCell{5}(1)>0
            reflux = 'azeotrope with hk';
            return;
        end
    end        
    if length(azeoCell)>5
        if azeoCell{6}(1)>0
            reflux = 'azeotrope with hk';
            return;
        end
    end   
    if length(azeoCell)>6
        if azeoCell{7}(1)>0
            reflux = 'azeotrope with hk';
            return;
        end
    end       
%     
    a = size(azeo);
    if a(2) == 0
        reflux = 'ERROR';
        clear mex
        return;
    end
    clear a;
    
    % Check if HMF is hk
%     temperatureTable = table({'HMF'; 'water'; 'solvent'},[azeo(1,1).t; azeo(1,2).t; azeo(1,3).t], 'VariableNames', {'Molecule', 'Temperature'});
%     disp(temperatureTable);
    if azeo(1,1).t < azeo(1,3).t %azeo (1,1) = solute, % azeo(1,3) = solvent
        reflux = NaN;
        %disp('Solvent is heavy key => not feasible');
        return;
    elseif azeo(1,1).t > azeo(1,3).t
%         disp('HMF is heavy key => feasible');
    end
    
    % Molar balances, specification: Sharp split with hk (HMF) pure in bottom
    f.menge = 1 ;
    f.z = xExtr ; % xExtr = [ x_HMF,Extr, x_H2O,Extr, x_Solv,Extr ]
    %     disp(sum(f.z));
    f.vfrac = 0 ;
    
    d.menge = f.menge*(1-xExtr(1)) ;
    d.z = [0, xExtr(2)/(1-xExtr(1)), xExtr(3)/(1-xExtr(1))] ;
    %     disp(sum(d.z));
    d.vfrac = 0 ;

    b.menge = f.menge*xExtr(1) ;
    b.z = [1 0 0] ;
    %     disp(sum(b.z));
    b.vfrac = 0 ;

    D_result = mexrbm('CALC_RBM',hdl,p,azeo,b.z,d.z,d.menge/f.menge,specs.TcRBM,0) ;
    reflux = D_result(6);
    Qreb = D_result(1);


    clear mex
end
