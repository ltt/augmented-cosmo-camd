function [result,ok] = mextract(tau, alpha, tc , pExtr, molStruct, xF)
    %% Implemented by A. Demuth, bachelor thesis supervised by Jan Scheffczyk @LTT, RWTH Aachen University.	
  
    % Mixture is HMF - Water - Solvent: Indices of components are 1 - 2 - 3
    mixtures = [];
    mixtemp = [];
    for i=1:length(alpha)
        for ii=(i+1):length(alpha)
            mixtemp = [mixtemp;i,ii];
        end
    end
    
    for i=0:length(mixtemp)-1
        mixtures = [mixtures;mixtemp(end-i,:)];
    end
    
    % Initialize mextraction:
    system = [pwd, '/ProcessModel/ShortcutModel/Mextraction/dummy.dat']; % Folder for temporary files  
    hpd = mextraction('init',system);
    
    for i = 1:length(mixtures),  
        ok = mextraction('set_alpha', hpd, alpha(mixtures(i,1), mixtures(i,2)), mixtures(i,1)-1, mixtures(i,2)-1) ;
        ok = mextraction('set_tau', hpd, tau(mixtures(i,1), mixtures(i,2)), mixtures(i,1)-1, mixtures(i,2)-1) ;
        ok = mextraction('set_tau', hpd, tau(mixtures(i,2), mixtures(i,1)), mixtures(i,2)-1, mixtures(i,1)-1) ;        
    end
   
	%% Call extraction shortcut by C. Redepenning
    pressure = pExtr;
    temperature = tc;

    xLLE = calcPhaseSplit(molStruct);
    xS = [0.0, xLLE(2),  xLLE(3)];
    %xS=[0.0 0.0 0 0 1.0]; %pure solvent
    purity = 0.0;
    [extrResult,ok] = mextraction('EXMIN',hpd,temperature,pressure,xF,xS,purity,1,2);
    clear mex
    if(ok>0)     
        result{1} = extrResult.S; % S = (S/F)_min because F=1mol/s
        result{2} = extrResult.xen; % Final composition in extract (stage N)
        result{3} = extrResult; % Complete Mextraction ouput in struct
    else 
        result = {NaN, NaN, NaN, NaN, NaN};
    end
end
