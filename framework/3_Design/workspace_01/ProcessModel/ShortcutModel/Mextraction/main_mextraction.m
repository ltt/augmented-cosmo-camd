function molStruct = main_mextraction(molStruct, specs)
%% Load structures with parameters, call mextraction and rbm shortcuts
      
% Import COSMOdata screening results
%load([tempFolder, name, '.mat'], 'molStruct');
        
    % Preallocate fields for output of mextraction
    for i=1:length(molStruct)
        molStruct(i).SFmin = NaN;
        molStruct(i).xExtr = NaN;
        molStruct(i).extrResult =NaN ;
    end    
        
    % Set specifications for shortcuts
    tcExtr = specs.TcExtr; % Select extraction temperature
    tkExtr = tcExtr + 273.15;
    pExtr = specs.pExtr;
    xF = specs.xF;
    
    % Calculate Results of mextraction
    for i=1:length(molStruct)

        %%% This section needs to go to COSMOdata calculation
        % Find alpha and tau parametrization for NRTL
        alphaParam      = molStruct(i).AlphaParam;
        tauParam        = molStruct(i).TauParam;
        % Calculate alpha_ij and tau_ij for given temperature
        [tau, alpha] = nrtlParam(tauParam, alphaParam, tkExtr);
        %%% This section needs to go to COSMOdata calculation
        
        
        % Display info
        if strcmp(specs.Mode, 'screening')
            disp(['Compound No. ', num2str(i), '\', num2str(length(molStruct)), ': ', molStruct(i).name]);
        end
        errorList = {'trifluoperazine','maritima','perphenazine'};
        if sum(strcmp(molStruct(i).name, errorList))==0 % Exclude components with the Error list
        % MEXTRACTION shortcut
        [result, ok] = mextract(tau, alpha, tcExtr, pExtr, molStruct(i), xF);
%         if ~isempty(molStruct(i).SFmin) && sum(strcmp(molStruct(i).name, errorList))==0
        if ok == 1 || ok == 2 || ok == 3
            % Add (S/F)_min, xExtr (composition in extract) to struct
            molStruct(i).SFmin = result{1};
            molStruct(i).xExtr = result{2};
            molStruct(i).extrResult = result{3};           
            clear mex;
        end
        end
    end
      
end
