function [ molStruct ] = ScoreMexRBM_tempopt( molStruct, specs )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% Preallocate fields
for k=1:length(molStruct)
        molStruct(k).SFmin = NaN;
        molStruct(k).xExtr = NaN;
        molStruct(k).extrResult = NaN;
        molStruct(k).Rmin = NaN;
        molStruct(k).RBM = NaN;
        molStruct(k).Qreb = NaN;
        molStruct(k).Tc_Extr_opt = NaN;
end
Tc_Extraction = zeros(length(molStruct),1);

 % Suppresses output during optimization
specs.Mode = 'optimization';

% Optimize Tc_Extr
parfor k=1:length(molStruct)
    func = @(T) GetQreb(T, molStruct(k), specs);
    options = optimoptions('fmincon', 'Algorithm', 'interior-point', 'MaxFunEvals', 1000, 'Display', 'off');

    T_opt_max = min(molStruct(k).T_boil-288.15, specs.Tc_Extr_max);

    [Tc_Extr_opt,Q_reb_min,exitflag,output] = fmincon(func,25,[],[],[],[],specs.Tc_Extr_min,T_opt_max,[],options); 

    Tc_Extraction(k) = Tc_Extr_opt;
end

% Get all mextraction and RBM results
    
specs.Mode = 'screening'; % Turn on output again
for k=1:length(molStruct)
    molStruct(k).Tc_Extr_opt = Tc_Extraction(k);
    specs.TcExtr = Tc_Extraction(k);
    % Execute Mextration
    addpath([specs.maindir,'/ProcessModel/ShortcutModel/Mextraction']);
    molStruct(k) = main_mextraction(molStruct(k), specs);
    rmpath([specs.maindir,'/ProcessModel/ShortcutModel/Mextraction']);

    % Execute RBM
    addpath([specs.maindir,'/ProcessModel/ShortcutModel/RBM']);
    molStruct(k) = main_RBM(molStruct(k), specs);
    rmpath([specs.maindir,'/ProcessModel/ShortcutModel/RBM'])

    if ~isnan(molStruct(k).Qreb)
        molStruct(k).Score = 1/molStruct(k).Qreb(2)*10^6;
    else
        molStruct(k).Score = NaN;
    end        
end


end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Qreb] = GetQreb(Tc_Extr, molStruct, specs)

specs.TcExtr = Tc_Extr;

% Execute Mextration
addpath([specs.maindir,'/ProcessModel/ShortcutModel/Mextraction']);
molStruct = main_mextraction(molStruct, specs);
rmpath([specs.maindir,'/ProcessModel/ShortcutModel/Mextraction']);

% Execute RBM
addpath([specs.maindir,'/ProcessModel/ShortcutModel/RBM']);
molStruct = main_RBM(molStruct, specs);
rmpath([specs.maindir,'/ProcessModel/ShortcutModel/RBM']);

if ~isnan(molStruct.Qreb)
    Qreb = molStruct.Qreb(2);
else
    Qreb = 1e20;
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%