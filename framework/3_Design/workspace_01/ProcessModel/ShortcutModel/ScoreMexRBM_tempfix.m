function [ molStruct ] = ScoreMexRBM_tempfix( molStruct, specs )
% Call mextraction and RBM shortcut to evaluate molecule

% Execute Mextration
addpath([specs.maindir,'/ProcessModel/ShortcutModel/Mextraction']);
molStruct = main_mextraction(molStruct, specs);
rmpath([specs.maindir,'/ProcessModel/ShortcutModel/Mextraction']);

% Execute RBM
addpath([specs.maindir,'/ProcessModel/ShortcutModel/RBM']);
molStruct = main_RBM(molStruct, specs);
rmpath([specs.maindir,'/ProcessModel/ShortcutModel/RBM']);

end
