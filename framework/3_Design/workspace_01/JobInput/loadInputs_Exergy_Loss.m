%
%% Input specifications for LEA3D optimization "Exergy-Loss"
%

%% Specify necessary parameters

% objective function
objfunc = 'Exergy-Loss';

% components
%components = {'h2', 'co2', 'co', 'h2o', 'methylformate', 'methanol', 'Solvent_X'};
components = {'h2', 'co2', 'co', 'h2o', 'dimethylformamide', 'dimethylamine', 'Solvent_X'};

% Set NRTL Temperature vector
T_start = 25; % /C
T_end = 200; % /C 
T_interval = 10; % Number of points in T interval    

% Set storage molecule for Superstructre optimization
storage = 'MeF'; 
%storage = 'DMF';

% Specify temperatures for T_sat constraint
T_boil_min = 25; % Celsius or 'no_T_min'
T_boil_max = 'no_T_max'; % Celsius or 'no_T_max'

% Conformer treatment
nconf_max = 10; % set nconf_max = Inf for all confomers

%% Save parameters in specs-struct

% Define struct carrying specifications
specs = struct;

specs.Number = noofcalc;
specs.Objfunc = objfunc;
specs.components = components;
specs.Solvent = solvent;
specs.COSMOpara = parametrization;
specs.Mode = 'optimization';

% Write solvent specific compounds
specs.compounds = components;
specs.compounds{length(components)} = solvent;

% Write objective function specific specifications
specs.T_start = T_start;
specs.T_end = T_end;
specs.T_interval = T_interval;
specs.composition = 0:0.1:1;
specs.storage = storage;

% Conformer treatment
specs.nconf = {nconf_max, '', ''};

% Constraints
specs.T_boil_min = T_boil_min;
specs.T_boil_max = T_boil_max;

% General paths
load('Paths/user.mat');
specs.paths = paths;
specs.maindir = pwd;
specs.Gate = gate;

