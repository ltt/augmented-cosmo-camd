%
%% Input specifications for LEA3D optimization "Mex-RBM"
%

%% Specify necessary parameters

% objective function
objfunc = 'Mex-RBM';

% components
%components{1} = 'hydroxymethylfurfural';
components{1} = 'dihydro-5-methyl-2(3h)-furanone';
components{2} = 'h2o';
components{3} = 'Solvent X';

% Set NRTL Temperature vector
T_start = 25; % /C
T_end = 200; % /C 
T_interval = 10; % Number of points in T interval    

% Set temperature and pressure of extraction column
% For fixed temperature Tc_Extr
% Tc_Extr = 25; %/C

% For optimization of temperature Tc_Extr
Tc_Extr_max = 80; %/C
Tc_Extr_min = 20; %/C
pExtr = 1.013; %/bar

% Set temperature and pressure for RBM:
TcRBM = 25; %/C
pRBM = 1;%/bar

% Specify temperatures for T_sat constraint
T_boil_min = 25; % Celsius or 'no_T_min'
T_boil_max = 'no_T_max'; % Celsius or 'no_T_max'

% Conformer treatment
nconf_max = 10; % set nconf_max = Inf for all confomers

%% Save parameters in specs-struct

% Define struct carrying specifications
specs = struct;

specs.Number = noofcalc;
specs.Objfunc = objfunc;
specs.components = components;
specs.Solvent = solvent;
specs.COSMOpara = parametrization;
specs.Mode = 'optimization';

% Write solvent specific compounds
specs.compounds = components;
specs.compounds{length(components)} = solvent;

% Write objective function specific specifications
specs.T_start = T_start;
specs.T_end = T_end;
specs.T_interval = T_interval;
specs.composition = 0:0.1:1;
specs.Tc_Extr_min = Tc_Extr_min;
specs.Tc_Extr_max = Tc_Extr_max;
%specs.TcExtr = Tc_Extr;
specs.pExtr = pExtr;
specs.TcRBM = TcRBM;
specs.pRBM = pRBM;

% Conformer treatment
specs.nconf = {nconf_max, '', ''};

% Constraints
specs.T_boil_min = T_boil_min;
specs.T_boil_max = T_boil_max;

% General paths
load('Paths/user.mat');
specs.paths = paths;
specs.maindir = pwd;
specs.Gate = gate;


