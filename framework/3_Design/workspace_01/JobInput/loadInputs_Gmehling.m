%
%% Input specifications for LEA3D optimization "Gmehling"
%

%% Specify necessary parameters

% objective function
objfunc = 'Gmehling';

% components
%components{1} = 'hydroxymethylfurfural';
components{1} = 'dihydro-5-methyl-2(3h)-furanone';
components{2} = 'h2o';
components{3} = 'Solvent X';

% Set Temperature of job
T = 25; %�C]

% Set mixture for LLE calculations
LLEcomp = '2 3';

% Specify temperatures for T_sat constraint
T_boil_min = 25; % Celsius or 'no_T_min'
T_boil_max = 'no_T_max'; % Celsius or 'no_T_max'

% Conformer treatment
nconf_max = 10; % set nconf_max = Inf for all confomers


%% Save parameters in specs-struct

% Define struct carrying specifications
specs = struct;

specs.Number = noofcalc;% write...--Yifan
specs.Objfunc = objfunc;
specs.components = components;
specs.Solvent = solvent;
specs.COSMOpara = parametrization;
specs.Mode = 'optimization';

% Write solvent specific compounds
specs.compounds = components; % the same as line27 --Yifan ??
specs.compounds{length(components)} = solvent;

% Write objective function specific specifications
specs.T = T;
specs.LLEcomp = LLEcomp;

% Conformer treatment
specs.nconf = {nconf_max, '', ''};

% Constraints
specs.T_boil_min = T_boil_min;
specs.T_boil_max = T_boil_max;

% General paths
load('Paths/user.mat');
specs.paths = paths;
specs.maindir = pwd;
specs.Gate = gate;

