#!/usr/bin/perl


system '/bin/rm -f ANTECHAMBER*';
system '/bin/rm -f ATOMTYPE.INF';
system '/bin/rm -f divcon*';


	$file='';
	$type='';
	$charge='';
	my($file,$type,$charge)=@ARGV;

	if($file eq '' || $type eq ''){
		print "usage: \n charge <file.mol2> <method: bcc (AM1, the best but time-cost expensive) or gas (for Gasteiger)> <empty (automatic detection) or set charge: 0, 1, -2, ....>\n";
        	exit;
	};

	$checkcharge=&readmol2($file,"");
	if($charge eq ""){
		$charge=$checkcharge;
		print "LEA3D Automatic setting: charge = $checkcharge\n";
	}
	else{
		if($charge != $checkcharge){
			print "Warning: automatic detection of charges finds charge = $checkcharge whereas your setting is $charge !\n" if($charge != $checkcharge); 
		}
		else{
			print "input charge = $charge match the automatic charge detection\n";
		};	
		
	};

	$fileb=$file;
	$fileb=~s/\.mol2$//;
	system "/bin/rm -f charge$fileb.*";

	# RQ: If we dont set the option -nc then divcon seems to do it automatically with the right charge !
	if($charge ne "" && $charge != 0 ){
		#print "option: -nc $charge\n";
 		chop($tmplog = `antechamber -i $file -fi mol2 -o charge$fileb.ac -fo ac -c $type -nc $charge` );
		#system("antechamber -i $file -fi mol2 -o charge$fileb.ac -fo ac -c $type -nc $charge");
	}
	else{
		#print "no charge !\n";
		#system("antechamber -i $file -fi mol2 -o charge$fileb.ac -fo ac -c $type");
		chop($tmplog = `antechamber -i $file -fi mol2 -o charge$fileb.ac -fo ac -c $type` );
	};

	system '/bin/rm -f ANTECHAMBER*';
	system '/bin/rm -f ATOMTYPE.INF';
	system '/bin/rm -f divcon*';


	if(-e "charge$fileb.ac"){
		@assign='';
		$ai=1;
		open(OUT,"<charge$fileb.ac");
		while (<OUT>){
			@get= split(' ',$_);
			if($_ =~/^ATOM/){
				$lenac=@get;
				$assign[$ai]=sprintf "%4.3f",$get[8] if($lenac == 10);
				$assign[$ai]=sprintf "%4.3f",$get[7] if($lenac == 9);
				print "charge$fileb.ac format problem ($lenac fields) !\n" if($lenac != 9 && $lenac != 10);
				$ai++;	
			};
		};	
		close(OUT);
		$ai=$ai-1;
		if($ai != $istratom){
			print "$file charge assignation failed (different number of atoms) !\n";	
		}
		else{
			&readmol2($file,"charge$file");
		};
	}
	else{
		print "$file charge assignation failed !\n";
	}; 

	system "/bin/rm -f charge$fileb.ac";
	
###############################################
###############################################


sub readmol2{
        local($chemin,$new)=@_;

	$reschg=0;

                %tabmm=(
                'C',12,
                'O',16,
                'N',14,
                'S',32,
                'P',31,
                'Br',80,
                'Cl',35,
                'I',127,
                'F',19,
                'H',1,
                );

	%valence=(
                'C.3',4,
                'C.2',3,
                'C.1',2,
                'C.ar',3,
                'C.cat',3,
                'N.3',3,
                'N.2',2,
                'N.1',1,
                'N.ar',3,
                'N.am',3,
                'N.pl3',3,
                'N.4',4,
                'O.3',2,
                'O.2',1,
                'O.co2',1,
                'O.spc',2,
                'O.t3p',2,
                'S.3',2,
                'S.2',1,
                'S.o',3,
                'S.o2',4,
                'P.3',4,
                'H',1,
                'H.spc',1,
                'H.t3p',1,
                'F',1,
                'Cl',1,
                'Br',1,
                'I',1,
                'Si',4,
                'LP',1,
                'Du',1,
                'Na',1,
                'K',1,
                'Ca',1,
                'Li',1,
                'Al',1,
        );
	
		$masse=0;

	open(IN,">$new") if ($new ne '');
        open(OUT,"<$chemin");

        $flagatom=0;
        $flagbond=0;
        $istratom=1;
        $istrbond=1;
	@checkcharge="";
	@strbond1="";
	@strbond2="";
	@strbondtype="";
	@strx="";
	@stry="";
	@strz="";
	@stratomtype="";
	@typepdb="";
        $fn=0;
                while (<OUT>){
                        @getstr = split(' ',$_);

			
                        if ($fn){
                                $name=$getstr[0];
#print"$name\n";
                        };

                        if ($getstr[0] eq '@<TRIPOS>SUBSTRUCTURE'){
                                $flagbond=0;
                        };
                        if ($getstr[0] eq ''){
                                $flagbond=0;
                        };

                        if (($flagbond)&&($getstr[0] ne '')){
                                $strbond1[$istrbond]=$getstr[1];
                                $strbond2[$istrbond]=$getstr[2];
                                $getstr[3]="am" if (($stratomtype[$getstr[1]]=~/N.am/) && ($stratomtype[$getstr[2]]=~/C.2/));
                                $getstr[3]="am" if (($stratomtype[$getstr[2]]=~/N.am/) && ($stratomtype[$getstr[1]]=~/C.2/));
                                $strbondtype[$istrbond]=$getstr[3];

				$checkcharge[$getstr[1]]=$checkcharge[$getstr[1]] + 1;
				$checkcharge[$getstr[2]]=$checkcharge[$getstr[2]] + 1;

                                $istrbond++;
                        };

                        if ($getstr[0] eq '@<TRIPOS>BOND'){
                                $flagatom=0;
                                $flagbond=1;
				print IN $_ if ($new ne '');
                        };

                        if (($flagatom )&&($getstr[0] ne '')){
                                $strx[$istratom]=sprintf "%4.3f",$getstr[2];
                                $stry[$istratom]=sprintf "%4.3f",$getstr[3];
                                $strz[$istratom]=sprintf "%4.3f",$getstr[4];

                                $getstr[5]="N.am" if ($getstr[5]=~/N.3/);
                                $stratomtype[$istratom]=$getstr[5];
                                @ma=split('\.',$getstr[5]);
                                $typepdb[$istratom]=$ma[0];
                                $masse=$masse+$tabmm{$ma[0]};

				if($new ne ''){
					@lena6b=split('',$getstr[1]);
					$lena6=@lena6b;
					$a2=$getstr[1];
		                        $a2="$a2   " if($lena6 == 1);
                        		$a2="$a2  " if($lena6 == 2);
                        		$a2="$a2 " if($lena6 == 3);

					$a5=$getstr[5];
					@lengetstr5=split('',$getstr[5]); 
					$len5=@lengetstr5;
					$a5="$getstr[5]    " if($len5 == 1); 
					$a5="$getstr[5]   " if($len5 == 2);
					$a5="$getstr[5]  " if($len5 == 3);
					$a5="$getstr[5] " if($len5 == 4);

					$a6=$getstr[6];
					$a6="1" if($a6 eq '');
					
					$a7=$getstr[7];
					$a7="lig" if($a7 eq '');
					
					$a8=$assign[$istratom];	
		   			printf IN "%4s %4s%5s%10s %10s %10s %5s %6s %3s %8s\n",$getstr[0],$a2,$blanc,$getstr[2],$getstr[3],$getstr[4],$a5,$a6,$a7,$a8;
				};

				$istratom++;
			};

                        if ($getstr[0] eq '@<TRIPOS>ATOM'){
                                $flagatom=1;
                        };
                        $fn=0;
                        if ($getstr[0] eq '@<TRIPOS>MOLECULE'){
                                $fn=1;
                        };

			if(($istratom == 1 || $istrbond > 1) && $new ne ''){
				print IN $_;
			};

                };

        close(OUT);
	close(IN) if($new ne '');

        $istrbond=$istrbond-1;
        $istratom=$istratom-1;
        $strrgyrno=$istratom;
#print"$strrgyrno, $istrbond\n";

	foreach $chgci (1..$istratom){
		#print "$chgci ok !\n" if($checkcharge[$chgci] == $valence{$stratomtype[$chgci]});
		
		if($stratomtype[$chgci] eq "C.cat"){
			if($checkcharge[$chgci] == $valence{$stratomtype[$chgci]}){
				#print "$chgci ok !\n";
				$reschg=$reschg + 1;
			}
			else{
				print "WARNING C.cat (atom no $chgci) has $checkcharge[$chgci] neighbours ! we expected less !\n";
			};
		};

		if($stratomtype[$chgci] eq "O.co2" && $checkcharge[$chgci] == $valence{$stratomtype[$chgci]}){
			$reschg=$reschg - 0.5;
			print "O.co2 $chgci chg=$reschg\n";
		};

		if($typepdb[$chgci] eq "O" && $checkcharge[$chgci] != $valence{$stratomtype[$chgci]}){
			if($checkcharge[$chgci] < $valence{$stratomtype[$chgci]}){
				$diffchg=$valence{$stratomtype[$chgci]} - $checkcharge[$chgci];
				$reschg=$reschg - $diffchg ;
				print "O $chgci chg=$reschg\n";
			}
			else{
				print "WARNING O (atom no $chgci) has $checkcharge[$chgci] neighbours ! we expected less !\n";
			};
		};

		if($typepdb[$chgci] eq "N" && $checkcharge[$chgci] != $valence{$stratomtype[$chgci]}){
			if($checkcharge[$chgci] > $valence{$stratomtype[$chgci]}){
				$diffchg= $checkcharge[$chgci] - $valence{$stratomtype[$chgci]};
				$reschg=$reschg + $diffchg;	
				print "N $chgci chg=$reschg\n";
			}
			else{
				print "WARNING N (atom no $chgci) has $checkcharge[$chgci] neighbours ! we expected more !\n";
			};
		};
	};
	#print "Charge is set to $reschg\n" if($reschg != 0);
	$reschg;
};



 	
