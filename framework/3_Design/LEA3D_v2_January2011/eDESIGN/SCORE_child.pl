#!/usr/bin/perl

###################################################################################################
#------------------------------------- SCORE_child.pl---------------------------------------------#
# 												  #
# Scoring Code for child testing procedure, calling COSMOconf and MATLAB to calculate the score	  #
#												  #
# Updated and Extended by Lorenz Fleitmann, supervised by Jan Scheffcyzk 	  		  #
# Lehrstuhl f�r Technische Thermodynamik (LTT), RWTH Aachen University,           Juni, 2016      #
#												  #
###################################################################################################

print"SUBROUTINE SCORE_CHILD OK \n";

sub score_child{

	local($file,$nbchildevaluer)=@_; #$nbchildevaluer1

	@score="";
	foreach $sj (1..$nbchildevaluer){
		$score[$sj]=0;
		$properties[$sj]="";
	};

########## Fonction � �valuer

    print "COMPUTE ENERGY\n" if($param{VERBOSITY} >= 1);
        
    # a molecule must have >= $percentproperties % of the properties to go through docking
    $percentproperties=80;

        
    if($nbpre_fonction > 0){

       	#General properties: separate tmpi.sdf and create tmpi.mol2 for each molecule in sdf
        $file = 'mol.sdf';
        ($mwSolvent,  $nbatom) = &getproperties($file);
        system("cp mol.sdf mol_child.sdf");

    #########################################################################################################################################################################
    # COSMO-CAMD evaluation
    #########################################################################################################################################################################
            if($evaluate_cosmo){
                print "\n\n";
                print ('-'x80);
                print "\nHello LEA: Test scoring of child\n";
                print "Current molecule code: ";
                print "$memomol[$gi]\n"; # ?????????????????? --Yifan Wang

    #------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    # Molecules: Get-USMILES-Code and check for nbatom-constraint, desired-moles constraint and atom limit

                # Call COSMOfrag to get USMILES-code of current molecule
                $USMILES = &getUSMILES('child');
                unlink('mol_child.sdf');
                    
                # Initialize:
                $targetingnbatom = 1;
                $desiredtype = 1;
                $atom_limit_ok = 1; # perhabs not ok.
        
                foreach $type (@undesired_mols){
                    $desiredtype = 0 if (index($USMILES, $type) != -1);
                }
                
                # Check if inside the desired nbatom region (disable for nbatom-soft-constraint)
                $targetingnbatom = 0 if (($nb_max ne "no_nbatom_cutoff") and ($nbatom > $nb_max));

                # Check atom limit of atom specified in $atom_to_limit:
                $atom_limit_ok = &evaluate_atom_limit($USMILES) if defined($atom_to_limit);

                print "Child:\n\tUSMILES: $USMILES\n";
                print "\tmol weight: $mwSolvent\n";
                print "\tnbatom:     $nbatom\n";
        

    #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    # If inside the desired nbatom region and containing no undesired functional groups, go on and create new sdf-File, otherwise go to line 443
                if ($targetingnbatom and $desiredtype and $atom_limit_ok){
                    
                    # Initialize Score_COSMOfrag
                    $fragprecalc = 1 ;

                    # If COSMOfrag precalculation is desired, calculate score once with COSMOfrag
                    if ($cosmofrag_precalc){
                        print "\nCOSMOfrag precalculation procedure...\n\n";
                        &call_matlab('Scoring', 1, 'child', 'COSMOfrag', $USMILES) if(-f "$param{WORKDIR}/$gatecosmo/$USMILES.mcos");

                        chdir($gateout2lea);

                        # Grep values from result file out_LEA_#.txt
                        open(Outputmatlab, "<out_LEA_child.txt") or print "out_LEA_child.txt did not exist, Matlab was not called for calculation child";
                        while($reihe = <Outputmatlab>){ chomp $reihe;
                            $results[$.-1] = $reihe;
                        }

                        chomp($results[0]);
                        $Score_COSMOfrag = $results[0];
                        print "\n";
                        
                        foreach $w (2..$#results){
                            print "$results[$w] \n";
                        }

                        @results=();
                        close Outputmatlab;

                        # COSMOfrag precalculation result
                        $fragprecalc = 0 if ($Score_COSMOfrag < $lb_cosmofrag);

                        # Collect molStruct and add to molStruct_precalculation
                        chdir("$param{WORKDIR}");
                        print "\n";
                        &call_matlab('Sum_molStructs', 1);

                        # Delete all temporary files
                        unlink("$gateout2lea/out_LEA_child.txt");
                        system("rm $param{WORKDIR}/$gatecosmo/*.mcos");
                    }
                    
                    # If COSMOfrag_Score is ok, continue on higher parametrization
                    if ($fragprecalc){

                        if ($cosmofrag_precalc){
                            print ('-'x40);
                            print"\nCOSMOfrag precalculation score is ok!\nContinue with scoring on higher COSMO-RS parametrization\n";
                        }
                        
                        &createcosmofiles if !($cosmomethod eq "COSMOfrag") ;
                    
                        if ($cosmomethod eq "COSMOfrag"){
                            $conformer_0 = $USMILES.".mcos";
                        }
                        else{
                            $conformer_0 = $USMILES."_c0.cosmo";
                        }
                
                        if (-f "$param{WORKDIR}/$gatecosmo/$conformer_0"){
                            
                            &call_matlab('Scoring', 0, 'child', $cosmomethod, $USMILES);
                            
                            unlink("log_matlab_child.log");
                        }
                        
                        print "\n";			
                        &results_of_mol('child');						
                        &call_matlab('Sum_molStructs', 0);
                        print "\n";
                    }
                    else{
                        print "\n\nMolecule's COSMOfrag precalculation score is too low ($Score_COSMOfrag < $lb_cosmofrag)\n";	
                        $cosmoscore[$gi+1]= 0;
                    }
                }

    #--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    # Molecules which do not fulfill nbatom-target, desired atom type or atom_limit
            else{
                
                print "\n\nMolecule does not meet nbatom target\n" if(!$targetingnbatom);
                print "\n\nMolecule is of undesired type\n" if(!$desiredtype);
                print "\n\nMolecule does not meet n_max_$atom_to_limit\n" if (!$atom_limit_ok);
                $cosmoscore[$gi+1]= 0;
                &writetoFails;
            };

    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            # For statistical purpose add USMILES on summary list
            &summary_of_SMILES($USMILES,$cosmoscore[$gi+1]);
        };

            


        #Chemical Function occurences: use the first molecule of the sdf file (no mol2 file)
        if($evaluate_function){
            $pointeur_function=-1;
            foreach $prop (0..@fprop-1){
                $pointeur_function=$prop if($fprop[$prop] eq "function");
            };
            
            $tmpprop="";
            $tmpprop=&chemfunction($file); # apply at the first molecule only
            #print "tmpprop=$tmpprop\n";
            
            $prop3=0;
            @listfonc=split('_',$listefonction);
            $llistfonc=@listfonc;
            $fract=1/$llistfonc;
            foreach $lfi (0..@listfonc-1){
                if($tmpprop =~ /$listfonc[$lfi]/){
                    $prop3=$prop3+$fract;
                    print"$listfonc[$lfi] OK ($prop3)\n" if($param{VERBOSITY} >= 1);
                };
            };	
            print "Function (search for $listefonction) Score=$prop3 for all conformers (1 means full match)\n";

            # fill @score for each conformers = same score for each
            foreach $sj (1..$nbchildevaluer){ 
                $score[$sj]=$score[$sj]+(&composite($prop3,$fmin[$pointeur_function],$fmax[$pointeur_function]))*$fw[$pointeur_function];
                $properties[$sj]=$properties[$sj]."FUNCTIONS conformer $sj = $tmpprop\n";
            };	
        };


        #Pharmacophore: separate tmpi.sdf and creates tmpi.mol2 for each molecule in sdf 
        if($evaluate_pharm){
            $pointeur_function=-1;
            foreach $prop (0..@fprop-1){
                $pointeur_function=$prop if($fprop[$prop] eq "pharm");
            };

            $tmpprop="";
            $tmpprop=&pharmacophore; # match=1 else 0
            print "PHARMACOPHORE ($pharmk constraints):\n";

            @listfonc=split(' ',$tmpprop);
            foreach $sj (1..$nbchildevaluer){
                @listfonc2=split('=',$listfonc[$sj-1]);
                $score[$sj]=$score[$sj]+(&composite($listfonc2[1],$fmin[$pointeur_function],$fmax[$pointeur_function]))*$fw[$pointeur_function];
                $properties[$sj]=$properties[$sj]."PHARMACOPHORE conformer $sj = $listfonc2[1]\n";
                print "PHARMACOPHORE conformer $sj = $listfonc2[1]\n";
            };	
        };



        #120 GC Fingerprint: the conversion by sdfmol2 makes that only the first molecule of $file is analysed
        if($evaluate_finger){
            $pointeur_function=-1;
            foreach $prop (0..@fprop-1){
                $pointeur_function=$prop if($fprop[$prop] eq "fingerprint");
            };

            $fingerprint=0;
            # in similarities_GC.pl the conversion by sdfmol2 makes that only the first molecule of $file is analysed
            chop($tmpfinger=`$leaexe/similarities_GC.pl $simmeasure $simdescriptor $fingerref $file`);
            $tmpfinger=~s/(.*)= (.*)/$2/;
            $fingerprint=sprintf"%3.2f",$tmpfinger;

            print "GC SIMILARITIES with $fingerref ($simmeasure on $simdescriptor representation) = $fingerprint for all conformers\n";

            # fill @score for each conformers = same score for each
            foreach $sj (1..$nbchildevaluer){
                $score[$sj]=$score[$sj]+(&composite($fingerprint,$fmin[$pointeur_function],$fmax[$pointeur_function]))*$fw[$pointeur_function];
                $properties[$sj]=$properties[$sj]."GC SIMILARITIES conformer $sj with $fingerref ($simmeasure on $simdescriptor representation) = $fingerprint\n";
            };
        };


    };


	if($nbpre_fonction > 0 && $evaluate_dock){
		$flagdock=0;
		foreach $sj (1..$nbchildevaluer){
			#print "$sj $score[$sj]\n";
			# $sumnbpre_fonction is the portion of non docking score
			$tmpscore=sprintf"%3.2f",($score[$sj]/$sumnbpre_fonction)*100;
			$flagdock=1 if($tmpscore >= $percentproperties);
			#one conformers with score > percentproperties is enough to launch docking on all conformers
		};
		if($flagdock){
			print "Conformers have properties ($tmpscore) >= $percentproperties \% => docking step accepted\n";
		}
		else{
			print "Conformers have not required properties ($tmpscore) < $percentproperties \% => docking step rejected\n";
		};	
	}
	elsif($evaluate_dock){
		$flagdock=1;
	};

	if($flagdock){
		
		#Docking
		$dockres=&docking($file);
		print "\nsub SCORE get:\n$dockres\n";
	
		$sj=1;	
		@listfonc=split('\n',$dockres);
		foreach $prop (0..@listfonc-1){
			@getline=split(' ',$listfonc[$prop]);
			if($getline[0]!~/^#/){
				foreach $prop (0..@fprop-1){
					if($fprop[$prop] eq "dock"){

					# CASE FLEXX
					if($docking_program eq "FLEXX"){ 
						if($getline[1]<0){ 
							$val = abs($getline[1]/($fmin[$prop]));
							$val=1.0 if($val > 1);
							$score[$sj]=$score[$sj]+($val*$fw[$prop]);
						};	
						
						#old calculation in LEA
						#$val = abs($prop{$fprop[$propi]}/(-60));
						#$val = abs($prop{$fprop[$propi]}/($fmin[$propi]));
						#$val=0.0 if($val < 0);
						#$val=1.0 if($val > 1);
						
						# disabled: ne permet pas un classement des scores car 100% pour
						# tout ce qui est entre $fmin et $fmax puis degressif
						#if($getline[1]<0){
						#	$score[$sj]=$score[$sj]+(&composite($getline[1],$fmin[$prop],$fmax[$prop]))*$fw[$prop];
							#print "conformer $sj global score= $getline[1]\n";
						#};	
					}
					elsif($docking_program eq "SURFLEX"){
						if($getline[1]>0){
							$val = abs($getline[1]/($fmax[$prop]));	
							$val=1.0 if($val > 1);
							$score[$sj]=$score[$sj]+($val*$fw[$prop]);
						};	
					}
					elsif($docking_program eq "PLANTS"){
						if($getline[1]<0){
							$val = abs($getline[1]/($fmin[$prop]));
							$val=1.0 if($val > 1);
							$score[$sj]=$score[$sj]+($val*$fw[$prop]);
						};	
					};
					
					};
				};
				$properties[$sj]=$properties[$sj].$listtitle."\n".$listfonc[$prop];
				$sj++;
			}
			else{
				$listtitle=$listfonc[$prop];
			};	
		};	
	};	


########## Convert total score in percentage
## LF: New Score calculation:
        print "-------------------\nSCORE:\n-------------------\n";
        print "nbatom Score: $score[1]\n" if ($sumw == 2);
	#$score[1]=0; #uncomment for nbatom hard constraint
	#$sumw = 2;  #for nbatom hard constraint set to 1
        foreach $sj (1..$nbchildevaluer){
            print "COSMO-Score: $cosmoscore[$gi+1]\n";
            $score[$sj] = $score[$sj]+$cosmoscore[$gi+1]*$fw[0];
            $totalscore = $score[$sj]/$sumw;
            print "TOTAL SCORE: $totalscore\n-------------------\n\n";
        }
        foreach $sj (1..$nbchildevaluer){
            print "sj= $sj score[sj]= $score[$sj] und sumw= $sumw \n";
            $score[$sj]=sprintf"%3.2f",($score[$sj]/$sumw)*100;
            print "$sj $score[$sj]\n";
        };

        @scoretmp="";
        @scoretmp=@score;
        &decreasing_order;
        print "Conformers: decreasing order of scores:\n";
        foreach $sj (1..$nbchildevaluer){
            print "$sj $score[$ranking[$sj]] mol$ranking[$sj]\n";
        };

};

################################################################################
################################################################################


######################################################################################
sub addtolibrary{
# ------------------------------------------------------------------------------------
# addtolibrary
# Add the cosmo-Files of the current molecule to the database
# This subroutine is only called when generating a new molecule by testing procedure
# ------------------------------------------------------------------------------------
# Library of all newly converted molecules with cosmo.files
    my $currentlength = scalar(@existingMolecules);
	
	$existingMolecules[$currentlength] = $USMILES;					# insert the USMILES of new molecule		
    $cosmofile =  $USMILES."_c";      #"_c0.cosmo";				

    #$cosmofiledatab = $USMILES."_c";

    chdir("$databasedir");
    mkdir $USMILES ;
    chdir("$param{WORKDIR}");
    
    system("cp $param{WORKDIR}/$COSMOFileGen/'$filename' $databasedir/'$USMILES'/'$USMILES'.sdf"); 		# copy .sdf file
    system("cp $param{WORKDIR}/$COSMOFileGen/$resultsdir/'$cosmofile'* $databasedir/'$USMILES'/"); # copy .cosmo file
}
######################################################################################

######################################################################################
sub createcosmofiles {
# ------------------------------------------------------------------------------------
# createcosmofiles
# Check: Use COSMO conf or copy files from Central_COSMO_Database
# if = true => use cosmoconf, else copy files
# true is if $notindatabase == 1 
# subroutine is only used for testing of new childs
# -----------------------------------------------------------------------------------
    $currentplace = 0;
    
    for(@existingMolecules){
        
        if($USMILES eq $_){ print "\nCentral_COSMO_Database: Molecule is already in database. Copy Files.\n\n";
            $notindatabase = 0; # = IN database
            $placeindatabase = $currentplace ;
            print "Molecule's place in database: $placeindatabase\n\n";
            last; 
        }
        else {
            $notindatabase = 1;    # = NOT in database
            $currentplace++;
        };
    }
        # Copy Files or call COSMO conf	
    $cosmofile =  $USMILES."_c";
    
    if($notindatabase){
        
        print "Central_COSMO_Database: Molecule is not in database. Call COSMO conf.\n";
                
        # Get new sdf-file from RDKIT
        &changesdf;

        # Write molecule's name on list to be calculated by COSMOconf
        $filename = $USMILES.".sdf";
        system("cp mol.sdf $COSMOFileGen/'$filename'");
    
        open(COSL,">$COSMOFileGen/liste");
        print COSL "$filename\n";
        close(COSL);

        # Execute COSMOconf
        chdir("$COSMOFileGen");

        $no_of_cosmoconf_mols_testing++;  # ??????????

        system("$generalFolder/$cosmoconfdir -l liste -m BP-$cosmomethod-COSMO -np $numberofCPU > liste.out");
        chdir ('..');

        $conformer_0 = $USMILES."_c0.cosmo";
        system("cp $COSMOFileGen/$resultsdir/'$cosmofile'* $param{WORKDIR}/$gatecosmo/") if (-f "$COSMOFileGen/$resultsdir/$conformer_0");

        # Add newly calculated cosmo-Files of molecule to Central_COSMO_Databank if COSMOconf was successful
        &addtolibrary if (-f "$COSMOFileGen/$resultsdir/$conformer_0");
    }
    else { 
        system("cp $databasedir/'$USMILES'/'$cosmofile'* $param{WORKDIR}/$gatecosmo/");

    }   
}
######################################################################################

