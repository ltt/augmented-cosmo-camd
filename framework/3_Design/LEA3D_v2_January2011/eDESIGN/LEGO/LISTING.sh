#! /bin/csh

	alias calc 'awk "BEGIN{ print \!* }" '
	
	set nb=0
	foreach i (./*.sdf)
		set nbi=`$LEA3D/NBSDF.pl $i `
		set nb = ` calc "$nb+$nbi" `
		echo $i $nbi 
	end
	echo pwd "TOTAL = $nb" 
	
