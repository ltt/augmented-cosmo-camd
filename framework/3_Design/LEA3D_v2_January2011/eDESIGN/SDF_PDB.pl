#!/usr/bin/perl


        local($fileopen)=@ARGV;

        #chop($workdir = `pwd` );
	#print "$workdir\n";


        if ($fileopen eq ''){
		die "usage: sdfpdb <file.sdf>\n";
	}


	if(!-e "$fileopen"){
		die "$fileopen does not exist !\n";
	};
	
	$nombre_molecule=0;
	$flagnew=1;
	$fileout=$fileopen;
	$fileout=~s/\.sdf/\.pdb/;

	open(MOL,"<$fileopen");
	while(<MOL>){
		if($flagnew){
			$moli="";
			$masse=0;
			$getinfo=0;
			$info='';
			$compt=0;
			$ig=1;
			$jg=0;
			@strx='';
			@stry='';
			@strz='';
			@atom='';
			@coval='';
			@fonc='';
			@ifonc='';
			@covfonc='';
			@bond='';
			@listb='';
			@typeb='';
			$blanc=' ';	
			@radius='';
			@lignebond='';		
			$atomlourd=0;
			$flagnew=0;
			$nombre_molecule++;
			if($nombre_molecule > 1){
				$fileout=$fileopen;
				$fileout=~s/\.sdf/_$nombre_molecule\.pdb/;
			};
			open(OUT,">$fileout");
		 	print OUT "REMARK convert $fileopen molecule no $nombre_molecule\n";
		};
		@getstr = split(' ',$_);
		$compt++;
		
		if($getinfo){
			$getinfo=0;
			$jointxt=join(' ',@getstr);
			$info=$info.":".$jointxt;	
		};
		
		if (($compt > 4) && ($ig <= $istratom)){
			$strx[$ig]=$getstr[0];
			$stry[$ig]=$getstr[1];
			$strz[$ig]=$getstr[2];
			$debnom="MOL";
			$atom[$ig]=$getstr[3];

			@cara=split(' *',$atom[$ig]);
                        $lcara=@cara;
                        $tmp=" ".$cara[0]." " if($lcara == 1);
                        $tmp=$cara[0].$cara[1]." " if($lcara == 2);

			printf OUT "HETATM %4s %3s %4s     1    %8s%8s%8s\n",$ig,$tmp,$debnom,$strx[$ig],$stry[$ig],$strz[$ig];

			$atomlourd ++ if($getstr[3] ne 'H');
			$radius[$ig]=$tabR{$getstr[3]};
			$masse=$masse+$tabmm{$getstr[3]};
			$ig++;
		};
		if (($compt > 4) && ($ig > $istratom) && ($jg <=$istrbond)){
			if ($jg == 0){
				$jg++;
			}
			else{
				@coller=split(' *',$getstr[0]);
				@coller2=split(' *',$getstr[1]);
				if(@coller==6 && $getstr[1] ne ""){
					$getstr[0]=$coller[0].$coller[1].$coller[2];
					$getstr[2]=$getstr[1];
					$getstr[1]=$coller[3].$coller[4].$coller[5];
				}
				elsif(@coller==6 && $getstr[1] eq ""){
					$getstr[0]=$coller[0].$coller[1];
					$getstr[1]=$coller[2].$coller[3].$coller[4];
					$getstr[2]=$coller[5];
				}
				elsif(@coller==5){
					if($_=~/^\s/){
						$getstr[0]=$coller[0].$coller[1];
						$getstr[2]=$getstr[1];
						$getstr[1]=$coller[2].$coller[3].$coller[4];
					}
					else{
						$getstr[0]=$coller[0].$coller[1].$coller[2];
						$getstr[2]=$getstr[1];
						$getstr[1]=$coller[3].$coller[4];
					};
                                }
                                elsif(@coller==4){
                                        if($_=~/^\s/){
                                                $getstr[0]=$coller[0];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[1].$coller[2].$coller[3];
                                        }
                                        else{
                                                $getstr[0]=$coller[0].$coller[1].$coller[2];
						$getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[3];
                                        };					
				}
				elsif(@coller2==4){
					$getstr[1]=$coller2[0].$coller2[1].$coller2[2];
					$getstr[2]=$coller2[3];
				}
				elsif(@coller==7){
					$getstr[0]=$coller[0].$coller[1].$coller[2];
					$getstr[1]=$coller[3].$coller[4].$coller[5];
					$getstr[2]=$coller[6];
				};

				$bond[$getstr[0]]=$bond[$getstr[0]].$blanc.$getstr[1].$blanc.$getstr[2];
				$listb[$getstr[0]]=$listb[$getstr[0]].$blanc.$getstr[1];
				$typeb[$getstr[0]]=$typeb[$getstr[0]].$blanc.$getstr[2];

				$bond[$getstr[1]]=$bond[$getstr[1]].$blanc.$getstr[0].$blanc.$getstr[2];
				$listb[$getstr[1]]=$listb[$getstr[1]].$blanc.$getstr[0];
				$typeb[$getstr[1]]=$typeb[$getstr[1]].$blanc.$getstr[2];


				$fonc[$getstr[0]]=$fonc[$getstr[0]].$blanc.$getstr[2].'-'.$atom[$getstr[1]].$blanc;
				$ifonc[$getstr[0]]=$ifonc[$getstr[0]].$blanc.$getstr[1].$blanc;
				$covfonc[$getstr[0]]=$covfonc[$getstr[0]].$blanc.$getstr[2];
				$coval[$getstr[0]]=$coval[$getstr[0]]+$getstr[2];

				$fonc[$getstr[1]]=$fonc[$getstr[1]].$blanc.$getstr[2].'-'.$atom[$getstr[0]].$blanc;
				$ifonc[$getstr[1]]=$ifonc[$getstr[1]].$blanc.$getstr[0].$blanc;
				$covfonc[$getstr[1]]=$covfonc[$getstr[1]].$blanc.$getstr[2];
				$coval[$getstr[1]]=$coval[$getstr[1]]+$getstr[2];
				$lignebond[$jg]=$_;

				printf OUT "CONECT %4s %4s\n",$getstr[0],$getstr[1];

				$jg++;
			};
		};
		if ($compt == 4){
			$istratom=$getstr[0];
			$istrbond=$getstr[1];

			@coller=split(' *',$istratom);
			if(@coller>3 && @coller==6){
				$istratom=$coller[0].$coller[1].$coller[2];
				$istrbond=$coller[3].$coller[4].$coller[5];
			}
			elsif(@coller>3 && @coller==5){
				if($_=~/^\s/){
					$istratom=$coller[0].$coller[1];
					$istrbond=$coller[2].$coller[3].$coller[4];
				}
				else{
					$istratom=$coller[0].$coller[1].$coller[2];
					$istrbond=$coller[3].$coller[4];
				};
			};

			#$compt++;
		};
		if ($_=~/\$\$\$\$/){
			$flagnew=1;
			
			print "$fileout DONE\n";
			
		};
		
	};
	close(MOL);
	close(OUT);

###################################################################################
###################################################################################

