#!/usr/bin/perl

$workdir='';
chop($workdir = `pwd` );

$leaexe=$ENV{LEA3D};
die "\nCheck environment variable (setenv LEA3D) \n\n" if (!-e "$leaexe/MAIN");

$f='';
my($f)=@ARGV;

if($f eq ''){
	print "usage: autolego <file.sdf> \nsdf file must not contain salts and must be converted into 3D\nPlease use eDESIGN tools nosel and drug\n";
	exit;
};

chop($nb1 = `$leaexe/NBSDF.pl $f` );

	$nomfile3d=$f;
	$nb=$nb1;

# decouper les molecules
	system("$leaexe/MAKE_FGTS.pl $nomfile3d");

# remove molecules without X and count

	chop($tmp= `$leaexe/ENLEVE_NO_POINTS.pl ring.sdf` );
	print "$tmp\n";
	chop($tmp= `$leaexe/ADD_GC_DATABLOCK_SDF.pl ring.sdf bit` );
	print "$tmp\n";
	chop($nb = `$leaexe/NBSDF.pl ring.sdf` );
	print "cycles : $nb\n";

	chop($tmp= `$leaexe/ENLEVE_NO_POINTS.pl fused_rings.sdf` );
	print "$tmp\n";
	chop($tmp= `$leaexe/ADD_GC_DATABLOCK_SDF.pl fused_rings.sdf bit` );
	print "$tmp\n";
	chop($nb = `$leaexe/NBSDF.pl fused_rings.sdf` );
	print "cycles fusionnes : $nb\n";

	chop($tmp= `$leaexe/ENLEVE_NO_POINTS.pl substituent.sdf` );
	print "$tmp\n";
	chop($tmp= `$leaexe/ADD_GC_DATABLOCK_SDF.pl substituent.sdf bit` );
	print "$tmp\n";
	chop($nb = `$leaexe/NBSDF.pl substituent.sdf` );
	print "substituent: $nb\n";

	chop($tmp= `$leaexe/ENLEVE_NO_POINTS.pl linker.sdf` );
	print "$tmp\n";
	chop($tmp= `$leaexe/ADD_GC_DATABLOCK_SDF.pl linker.sdf bit` );
	print "$tmp\n";
	chop($nb = `$leaexe/NBSDF.pl linker.sdf` );
	print "linker : $nb\n";

	#chop($nb = `$leaexe/NBSDF.pl acyclic.sdf` );
	#print "acyclic : $nb\n";

	chop($tmp= `$leaexe/ENLEVE_NO_POINTS.pl special.sdf` );
	print "$tmp\n";
	chop($tmp= `$leaexe/ADD_GC_DATABLOCK_SDF.pl special.sdf bit` );
	print "$tmp\n";
	chop($nb = `$leaexe/NBSDF.pl special.sdf` );
	print "special : $nb\n";
	
#	unlink "$nomfile3d" if($rep eq 'y');

	@list=0;
	$li=0;
	
# cycle

        #system("rm -f ring_*.sdf");
	$flag=1;
	$flagname=0;
	$flagnew=1;
	$fit=0;
	@ligne='';
	$i=0;
        open(IN,"<$workdir/ring.sdf");
	while (<IN>){

		$conv2=$_;
                @get=split(' ',$_);

		if ($flagnew){
			$fit++;
			$flagnew=0;
			$flagname=0;
			@ligne='';
			$i=0;
			
			$natomescycle=0;
			$cyclearom=0;
			$arcenter=0;
		};

		$ligne[$i]=$_;
		$i++;

                if($natomescycle){
			$natomescyclenb=$get[0];
			$natomescycle=0;
		};
		if($conv2 =~/natomescycle/){
			$natomescycle=1;
		};
		
		if($cyclearom){
			$cyclearomnb=$get[0];
			$cyclearom=0;
		};
		if($conv2 =~/cyclearom/){
			$cyclearom=1;
		};
		if($conv2 =~/ARcenter/){
			$arcenter=1;
		};	
		
		if ($conv2 =~/(\$\$\$\$)/){
			$flagnew=1;
			$fclass="ring_".$natomescyclenb.".sdf";
			$fclass="ring_3-4.sdf" if($natomescyclenb==3 || $natomescyclenb==4);
			$fclass="ring_7andmore.sdf" if($natomescyclenb >=7 );
			$fclass="ring_".$natomescyclenb."_ar.sdf" if($arcenter);
			$fclass="ring_".$natomescyclenb."_insat.sdf" if($natomescyclenb==5 && $arcenter); # would be ar instead of insat
			
			if(!-e "$fclass"){
			        $list[$li]=$fclass;
			        $li++;
			};
			
			open(OUT,">>$fclass");
				foreach $i (0..@ligne-1){
					print OUT $ligne[$i];
				};
			close(OUT);
				
		};
	};
        close(IN);


# cyclefus


        #system("rm -f fused_rings_*.sdf");
	$flag=1;
	$flagname=0;
	$flagnew=1;
	$fit=0;
	@ligne='';
	$i=0;
        open(IN,"<$workdir/fused_rings.sdf");
	while (<IN>){

		$conv2=$_;
                @get=split(' ',$_);

		if ($flagnew){
			$fit++;
			$flagnew=0;
			$flagname=0;
			@ligne='';
			$i=0;
			
			$natomescycle=0;
			$cyclearom=0;
			$ncycles=0;
			$arcenter=0;
		};

		$ligne[$i]=$_;
		$i++;

                if($natomescycle){
			$natomescyclenb=$get[0];
			$natomescycle=0;
		};
		if($conv2 =~/natomescycle/){
			$natomescycle=1;
		};
		
		if($cyclearom){
			$cyclearomnb=$get[0];
			$cyclearom=0;
		};
		if($conv2 =~/cyclearom/){
			$cyclearom=1;
		};
		
		if($ncycles){
			$ncyclesnb=$get[0];
			$ncycles=0;
		};
		if($conv2 =~/ncycles/){
			$ncycles=1;
		};
		if($conv2 =~/ARcenter/){
			$arcenter=1;
		};
		
		if ($conv2 =~/(\$\$\$\$)/){
			$flagnew=1;
			$fclass="fused_rings_".$ncyclesnb.".sdf";
			$fclass="fused_rings_6_6".".sdf" if($natomescyclenb==10);
			
			#$fclass="fused_rings_6_6"."_ar.sdf" if($natomescyclenb==10 && $cyclearomnb==5);
			$fclass="fused_rings_6_6"."_ar.sdf" if($natomescyclenb==10 && $arcenter);
			
			$fclass="fused_rings_6_5".".sdf" if($natomescyclenb==9);
			#$fclass="fused_rings_6_5"."_insat.sdf" if($natomescyclenb==9 && $cyclearomnb==4);
			$fclass="fused_rings_6_5"."_insat.sdf" if($natomescyclenb==9 && $arcenter);
			
			$fclass="fused_rings_3.sdf" if($ncyclesnb == 3);
			$fclass="fused_rings_4.sdf" if($ncyclesnb == 4);
			$fclass="fused_rings_5andmore.sdf" if($ncyclesnb > 4);
			$fclass="fused_rings_5andmore_ar.sdf" if($ncyclesnb > 4 && $arcenter);
			
			if(!-e "$fclass"){
			        $list[$li]=$fclass;
			        $li++;
			};
			
			open(OUT,">>$fclass");
				foreach $i (0..@ligne-1){
					print OUT $ligne[$i];
				};
			close(OUT);
				
		};
	};
        close(IN);

# special cases

	#system("rm -f special_*.sdf");	
	$flag=1;
	$flagname=0;
	$flagnew=1;
	$fit=0;
	@ligne='';
	$i=0;
	open(IN,"<$workdir/special.sdf");
	while (<IN>){
		$conv2=$_;
		@get=split(' ',$_);

		if ($flagnew){
			$fit++;
			$flagnew=0;
			$flagname=0;
			@ligne='';
			$i=0;

			$natomescycle=0;
			$arcenter=0;
		};

		$ligne[$i]=$_;
		$i++;

		if($natomescycle){
			$natomescyclenb=$get[0];
			$natomescycle=0;
		};
		if($conv2 =~/natomescycle/){
			$natomescycle=1;
		};
		if($conv2 =~/ARcenter/){
			$arcenter=1;
		};

		if ($conv2 =~/(\$\$\$\$)/){
			$flagnew=1;
			$fclass="special_all.sdf";
			#$fclass="special_".$natomescyclenb.".sdf";		
			#$fclass="special_".$natomescyclenb."_ar.sdf" if($arcenter);

			if(!-e "$fclass"){
				$list[$li]=$fclass;
				$li++;
			};

			open(OUT,">>$fclass");
				foreach $i (0..@ligne-1){
					print OUT $ligne[$i];
			}	;
			close(OUT);

		};
	};
	close(IN);



# linkers and substituents

foreach $j (1..2){

	$ouvrir="";
	$ouvrir="linker" if($j==1);
	$ouvrir="substituent" if($j==2);

        #system("rm -f $ouvrir_*.sdf");

	$flag=1;
	$flagname=0;
	$flagnew=1;
	$fit=0;
	@ligne='';
	$i=0;
        open(IN,"<$workdir/$ouvrir.sdf");
	while (<IN>){
		$conv2=$_;
                @get=split(' ',$_);
		if ($flagnew){
			$fit++;
			$flagnew=0;
			$flagname=0;
			@ligne='';
			$i=0;
			$natomes=0;
		};
		$ligne[$i]=$_;
		$i++;
		if($natomes){
			$natomesnb=$get[0];
			$natomes=0;
		};
		if($conv2 =~/natomes/){
			$natomes=1;
		};
		if ($conv2 =~/(\$\$\$\$)/){
			$flagnew=1;
			$fclass="$ouvrir"."_small.sdf" if($natomesnb <= 15);
			$fclass="$ouvrir"."_15-20.sdf" if($natomesnb > 15 && $natomesnb <= 20);
			$fclass="$ouvrir"."_big.sdf" if($natomesnb > 20);
			if(!-e "$fclass"){
			        $list[$li]=$fclass;
			        $li++;
			};
			open(OUT,">>$fclass");
				foreach $i (0..@ligne-1){
					print OUT $ligne[$i];
				};
			close(OUT);
		};
	};
        close(IN);
};


	$rep="y";
 	#print "Must X be taken into account (y or n) ? ";
 	#chop($rep=<STDIN>);

        print "\nListe des fichiers crees :\n";
        open(OUT,">list_lego");
        foreach $i (0..@list-1){
        	
		print "$list[$i]\n";
        	print OUT "$list[$i]\n";

        	system("cp -f $list[$i] old_$list[$i]");
		
		unlink "$list[$i]"; #case of 1 mol in $list[$i] but same key as old key thus diff.sdf does not exist and no replacement of $list[$i](==$fileadd in KEY.pl) 
		
		$filelistold="old_$list[$i]";
		
		if($rep eq "y"){
        		system("$leaexe/CLASS_FGT.pl $filelistold key_$list[$i] X $list[$i]");
		}
		else{
        		system("$leaexe/CLASS_FGT.pl $filelistold key_$list[$i] - $list[$i]");
		};
        };

        print "\n\n";
        close(OUT);



