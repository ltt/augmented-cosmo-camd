
 #    #  #        ####    ####   #####
  #  #   #       #    #  #    #  #    #	
   ##    #       #    #  #       #    #	
   ##    #       #    #  #  ###  #####
  #  #   #       #    #  #    #  #		By Renxiao Wang, Ph.D. 
 #    #  ######   ####    ####   #    v2.0	August, 1999

@Copyright belongs to the Institute of Physical Chemistry, Peking University


  XLOGP is an atom-additive method for calculating the octanol/water partition
coefficient(logP). It gives the logP value for a give compound by summing the 
contributions from component atoms and correction factors.  

  Our original work on XLOGP was published on J.Chem.Inf.Comput.Sci. 1997, 37, 
615-621. XLOGP v1.1 was released to the public in September, 1997. Since then, 
we have improved the XLOGP algorithm to version 2.0. In the current version, 
totally 90 atom types are used to classify carbon, nitrogen, oxygen, sulfur, 
phosphorus, and halogen atoms. Ten additional correction factors are also 
applied to deal with some special chemical structures. Compared to its 
previous version, XLOGP v2.0 is more robust and gives better statistical 
results. The comparison of various logP calculation procedures demonstrates 
that our method gives much better results than other atom-additive approaches 
and is at least comparable to some popular fragmental approaches like CLOGP. 
Because of the simple methodology, missing fragment problem does not occur to 
our method.

  Under this directory, you will find the following files:

  README			this text 
  Register.txt			user registration form
  xlogp.tar.gz.en		XLOGP v2.0 package, encrypted
  training_set.tar.gz.en	training set used by XLOGP v2.0, encrypted
  test_set.tar.gz.en		test set used by XLOGP v2.0, encrypted

  You are required to fill in the registration form and send it back to us by
E-mail, Fax, or conventional mail. Please contact us at:

	Prof. Luhua Lai 
	Institute of Physical Chemistry, Peking University
	Beijing 100871, P.R.China 
	E-mail: lhlai@mdl.ipc.pku.edu.cn  
	Fax: 86-10-62751725 
	http://mdl.ipc.pku.edu.cn/

  Upon receiving your registration form, we will inform you the key to decrypt
those files by E-mail.

  To encrypt and uncompress the above files, please use the following commands
in a UNIX shell:

  crypt key < xlogp.tar.gz.en > xlogp.tar.gz
  gunzip xlogp.tar.gz
  tar xvf xlogp.tar

  Then a directory named as "xlogp2.0" will be created. Under that directory, 
you will find a user manual composed in HTML. Please read it carefully to 
learn how to compile and how to use XLOGP. 

  Hope you enjoy XLOGP!


