#!/usr/bin/perl


$leaexe=$ENV{LEA3D};
die "\nCheck environment variable (setenv LEA3D) \n\n" if (!-e "$leaexe/MAIN");

	local($filesdf,$scale)=@ARGV;

	if($filesdf eq "" || $scale eq ""){
		die "usage: addgcsdf <sdf file> <bit or occ or charge>\n";
	};
	
	
	# The default van der Waals radii are taken from A. Bondi (1964) "van der Waals Volumes and Radii" J.Phys.Chem. 68, 441-451
	#If an element does not appear in the table it is assigned a value of 2.0?

	#   Ag  1.72      Ar  1.88     As  1.85     Au  1.66
	#   Br  1.85      C   1.70     Cd  1.58     Cl  1.75
	#   Cu  1.40      F   1.47     Ga  1.87     H   1.20
	#   He  1.40      Hg  1.55     I   1.98     In  1.93
	#   K   2.75      Kr  2.02     Li  1.82     Mg  1.73
	#   N   1.55      Na  2.27     Ne  1.54     Ni  1.63
	#   O   1.52      P   1.80     Pb  2.02     Pd  1.63
	#   Pt  1.72      S   1.80     Se  1.90     Si  2.10
	#   Sn  2.17      Te  2.06     Tl  1.96     U  1.86
	#   Xe  2.16      Zn  1.39

	%tabR=(
		'C',1.70,
		'O',1.52,
		'N',1.55,
		'S',1.80,
		'P',1.80,
		'Br',1.85,
		'Cl',1.75,
		'I',1.98,
		'F',1.47,
		'H',1.2,
		'Hp',1.1,
	);

	%tabmm=(
		'C',12,
		'O',16,
		'N',14,
		'S',32,
		'P',31,
		'Br',80,
		'Cl',35.4,
		'I',127,
		'F',19,
		'H',1,
	);

	##### PROPRIETES FROM SDF

	$newname=$filesdf;
	$newname=~s/\.sdf$//;
	
	$moli=0;
	$flagnew=1;
	open(FIN,">$newname.out");
	print FIN "#file no finger_cg\n";
	@finger='';

	open(HOO,">fingercgsdf.sdf");
	
	open(MOL,"<$filesdf");
	while(<MOL>){
		if($flagnew){
			$moli++;
			$masse=0;
			$compt=0;
			$ig=1;
			$jg=0;
			@ligne="";
			@strx='';
			@stry='';
			@strz='';
			@atom='';
			@coval='';
			@fonc='';
			@ifonc='';
			@covfonc='';
			$blanc=' ';	
			@radius='';
			$atomlourd=0;
			$flagnew=0;
			$setchg=0;
			open(OUT,">tmpi.sdf");
		};
		@getstr = split(' ',$_);
		print OUT $_;
		$ligne[$compt]=$_;
		
		$compt++;
		if (($compt > 4) && ($ig <= $istratom)){
			$strx[$ig]=$getstr[0];
			$stry[$ig]=$getstr[1];
			$strz[$ig]=$getstr[2];
			$atom[$ig]=$getstr[3];
			$atomlourd ++ if($getstr[3] ne 'H');
			$radius[$ig]=$tabR{$getstr[3]};
			$masse=$masse+$tabmm{$getstr[3]};
			$ig++;
		};
		if (($compt > 4) && ($ig > $istratom) && ($jg <=$istrbond)){
			if ($jg == 0){
				$jg++;
			}
			else{
                               @coller=split(' *',$getstr[0]);
                                @coller2=split(' *',$getstr[1]);
                                if(@coller==6 && $getstr[1] ne ""){
                                        $getstr[0]=$coller[0].$coller[1].$coller[2];
                                        $getstr[2]=$getstr[1];
                                        $getstr[1]=$coller[3].$coller[4].$coller[5];
                                }
                                elsif(@coller==6 && $getstr[1] eq ""){
                                        $getstr[0]=$coller[0].$coller[1];
                                        $getstr[1]=$coller[2].$coller[3].$coller[4];
                                        $getstr[2]=$coller[5];
                                }
                                elsif(@coller==5){
                                        if($_=~/^\s/){
                                                $getstr[0]=$coller[0].$coller[1];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[2].$coller[3].$coller[4];
                                        }
                                        else{
                                                $getstr[0]=$coller[0].$coller[1].$coller[2];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[3].$coller[4];
                                        };
                                }
				elsif(@coller==4){
					if($_=~/^\s/){
						$getstr[0]=$coller[0];
						$getstr[2]=$getstr[1];
						$getstr[1]=$coller[1].$coller[2].$coller[3];
					}
					else{
						$getstr[0]=$coller[0].$coller[1].$coller[2];
						$getstr[2]=$getstr[1];
						$getstr[1]=$coller[3];
					};
				}	
                                elsif(@coller2==4){
                                        $getstr[1]=$coller2[0].$coller2[1].$coller2[2];
                                        $getstr[2]=$coller2[3];
                                }
                                elsif(@coller==7){
                                        $getstr[0]=$coller[0].$coller[1].$coller[2];
                                        $getstr[1]=$coller[3].$coller[4].$coller[5];
                                        $getstr[2]=$coller[6];
                                };

				$fonc[$getstr[0]]=$fonc[$getstr[0]].$blanc.$getstr[2].'-'.$atom[$getstr[1]].$blanc;
				$ifonc[$getstr[0]]=$ifonc[$getstr[0]].$blanc.$getstr[1].$blanc;
				$covfonc[$getstr[0]]=$covfonc[$getstr[0]].$blanc.$getstr[2];
				$coval[$getstr[0]]=$coval[$getstr[0]]+$getstr[2];

				$fonc[$getstr[1]]=$fonc[$getstr[1]].$blanc.$getstr[2].'-'.$atom[$getstr[0]].$blanc;
				$ifonc[$getstr[1]]=$ifonc[$getstr[1]].$blanc.$getstr[0].$blanc;
				$covfonc[$getstr[1]]=$covfonc[$getstr[1]].$blanc.$getstr[2];
				$coval[$getstr[1]]=$coval[$getstr[1]]+$getstr[2];

				$jg++;
			};
		};
		if ($compt == 4){
			$istratom=$getstr[0];
			$istrbond=$getstr[1];

			@coller=split(' *',$istratom);
			if(@coller>3 && @coller==6){
				$istratom=$coller[0].$coller[1].$coller[2];
				$istrbond=$coller[3].$coller[4].$coller[5];
			}
			elsif(@coller>3 && @coller==5){
				if($_=~/^\s/){
					$istratom=$coller[0].$coller[1];
					$istrbond=$coller[2].$coller[3].$coller[4];
				}
				else{
					$istratom=$coller[0].$coller[1].$coller[2];
					$istrbond=$coller[3].$coller[4];
				};
			};

		};
		if ($_=~/\$\$\$\$/){
			close(OUT);

			chop($fingertmpi=`$leaexe/fingerprint_GC.pl $scale tmpi.sdf`);
			print FIN "$filesdf $moli $fingertmpi\n";
			$finger[$moli]=$fingertmpi;	
			unlink "tmpi.sdf" if(-e "tmpi.sdf");
			unlink "tmpi_1.mol2" if(-e "tmpi_1.mol2");
			$flagnew=1;

			$motif="";	
			@get=split(' ',$fingertmpi);
			foreach $j (0..@get-1){
				$j2=$j+1;
				$motif=$motif." ".$j2." " if($get[$j] > 0);
			};
			foreach $r (0..@ligne-2){
				print HOO "$ligne[$r]";
			};
			print HOO "> <GC>\n$motif\n\n" if($motif ne "");	
			print HOO "\$\$\$\$\n";
		};
	};
	close(MOL);
	close(FIN);
	close(HOO);

	rename "fingercgsdf.sdf", "$newname.sdf" if(!-z "$newname.sdf"); 
	#print "See $newname.sdf with datablock <GC> and $newname.out\n"; 
