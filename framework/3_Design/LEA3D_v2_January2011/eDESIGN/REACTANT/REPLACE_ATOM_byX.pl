#!/usr/bin/perl


$f='';
my($f,$file,$out)=@ARGV;

if($f eq '' || $file eq "" || $out eq ""){
	print "usage: replacebyx <file.sdf to treat> <atom specification in settings.in> <name of the ouput file> \n";
	exit;
};

$elt="";
@envatom="";
@envbond="";
$ei=1;
$keepi="";

open(IN,"<$file");
while(<IN>){
@get = split(' ',$_);
if($get[0]!~/^#/){
	if($_=~/ATOMX/){
		$elt=$get[1];
		$envatom[0]="itself";
		$envbond[0]="0";
	}
	if($_=~/ATOM/){
		$elt=$get[1];
		$keepi=0 if($get[2] eq 'keep');
	}
	elsif($_=~/ENVX/){
		$envatom[0]=$get[1];
		$envbond[0]=$get[2];

	}
	elsif($_=~/ENV/){
		$envatom[$ei]=$get[1];
		$envbond[$ei]=$get[2];
		$keepi=$ei if($get[3] eq 'keep' && $keepi eq "");
		$ei++;
	};
};
};
close(IN);
if($envatom[0] ne ''){
	print "element=$elt\nENVATOM=@envatom\nENVBOND=@envbond\nKEEP part with indice =$keepi\n";
}
else{
	die "One ENVX must be set !\n";
};	

$flagnew=1;
open(DOC,">$out");
open(OUT,">noX.sdf");
open(IN,"<$f");
while(<IN>){

	if($flagnew){
                        $masse=0;
			$blanc=" ";
                        $compt=0;
                        $ig=1;
                        $jg=0;
                        @strx='';
                        @stry='';
                        @strz='';
                        @atom='';
                        @coval='';
                        @fonc='';
                        @ifonc='';
                        @covfonc='';
                        $blanc=' ';
                        @radius='';
                        $atomlourd=0;
                        $flagnew=0;
			@ligne="";
        };
        @getstr = split(' ',$_);
        $compt++;
		$ligne[$compt]=$_;

                if (($compt > 4) && ($ig <= $istratom)){
                        $strx[$ig]=$getstr[0];
                        $stry[$ig]=$getstr[1];
                        $strz[$ig]=$getstr[2];
                        $atom[$ig]=$getstr[3];
                        $atomlourd ++ if($getstr[3] ne 'H');
                        $radius[$ig]=$tabR{$getstr[3]};
                        $masse=$masse+$tabmm{$getstr[3]};
                        $ig++;
                };
                if (($compt > 4) && ($ig > $istratom) && ($jg <=$istrbond)){
                        if ($jg == 0){
                                $jg++;
                        }
                        else{
                               @coller=split(' *',$getstr[0]);
                                @coller2=split(' *',$getstr[1]);
                                if(@coller==6 && $getstr[1] ne ""){
                                        $getstr[0]=$coller[0].$coller[1].$coller[2];
                                        $getstr[2]=$getstr[1];
                                        $getstr[1]=$coller[3].$coller[4].$coller[5];
                                }
                                elsif(@coller==6 && $getstr[1] eq ""){
                                        $getstr[0]=$coller[0].$coller[1];
                                        $getstr[1]=$coller[2].$coller[3].$coller[4];
                                        $getstr[2]=$coller[5];
                                }
                                elsif(@coller==5){
                                        if($_=~/^\s/){
                                                $getstr[0]=$coller[0].$coller[1];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[2].$coller[3].$coller[4];
                                        }
                                        else{
                                                $getstr[0]=$coller[0].$coller[1].$coller[2];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[3].$coller[4];
                                        };
                                }
                                elsif(@coller2==4){
                                        $getstr[1]=$coller2[0].$coller2[1].$coller2[2];
                                        $getstr[2]=$coller2[3];
                                }
                                elsif(@coller==7){
                                        $getstr[0]=$coller[0].$coller[1].$coller[2];
                                        $getstr[1]=$coller[3].$coller[4].$coller[5];
                                        $getstr[2]=$coller[6];
                                };

                                $fonc[$getstr[0]]=$fonc[$getstr[0]].$blanc.$getstr[2].'-'.$atom[$getstr[1]].$blanc;
                                $ifonc[$getstr[0]]=$ifonc[$getstr[0]].$blanc.$getstr[1].$blanc;
                                $covfonc[$getstr[0]]=$covfonc[$getstr[0]].$blanc.$getstr[2];
                                $coval[$getstr[0]]=$coval[$getstr[0]]+$getstr[2];

                                $fonc[$getstr[1]]=$fonc[$getstr[1]].$blanc.$getstr[2].'-'.$atom[$getstr[0]].$blanc;
                                $ifonc[$getstr[1]]=$ifonc[$getstr[1]].$blanc.$getstr[0].$blanc;
                                $covfonc[$getstr[1]]=$covfonc[$getstr[1]].$blanc.$getstr[2];
                                $coval[$getstr[1]]=$coval[$getstr[1]]+$getstr[2];


                                $jg++;
                        };
                };
                if ($compt == 4){
                        $istratom=$getstr[0];
                        $istrbond=$getstr[1];

                        @coller=split(' *',$istratom);
                        if(@coller>3 && @coller==6){
                                $istratom=$coller[0].$coller[1].$coller[2];
                                $istrbond=$coller[3].$coller[4].$coller[5];
                        }
                        elsif(@coller>3 && @coller==5){
                                if($_=~/^\s/){
                                        $istratom=$coller[0].$coller[1];
                                        $istrbond=$coller[2].$coller[3].$coller[4];
                                }
                                else{
                                        $istratom=$coller[0].$coller[1].$coller[2];
                                        $istrbond=$coller[3].$coller[4];
                                };
                        };

                        $compt++;
                };
                if ($_=~/\$\$\$\$/){
                        $flagnew=1;
                        #print "atomes = $istratom\n";
			$newatom="";
			
			&fonction;

			if($newatom eq ""){ # no solutions !
				foreach $li (1..@ligne-1){
					print OUT $ligne[$li];
				};
			}
			else{
				foreach $li (1..@ligne-1){
					print DOC $ligne[$li] if($ligne[$li] ne "");
					if($li==5+$istratom){
						foreach $li2(0..@ligneplus-1){
							print DOC $ligneplus[$li2];
							#print $ligneplus[$li2];
						};
					};	
				};
			};
                };
};
close(IN);
close(DOC);
close(OUT);


###################################################################################
###################################################################################
#
#                SUBROUTINES
#
###################################################################################
###################################################################################


sub fonction{

	$atomx="X";

	@ligneplus="";
	$lp=0;
	
	$finbond=5+$istratom+$istrbond;

	$getstr[4]="0";
        $getstr[5]="0";
        $getstr[6]="0";
        $getstr[7]="0";
        $getstr[8]="0";
        $getstr[9]="0";
        $getstr[10]="0";
        $getstr[11]="0";
        $getstr[12]="0";
        $getstr[13]="0";
        $getstr[14]="0";
        $getstr[15]="0";

## SEARCH FUNCTIONS

	$onlyone=0;
	foreach $l (1..$istratom){
	if($onlyone==0){
		
		if($atom[$l] eq $elt){

			$listvu="";
			$all=1;
			@det=split(' ',$fonc[$l]);
			@det2=split(' ',$ifonc[$l]);
			$keepiatom="";

			# ATOMX (e.g. -Br -> -X)
			if($envatom[0] eq "itself" && $envbond[0] == 0){
				foreach $ei (1..@envatom-1){
					$see=0;
					$seei="";
					foreach $k (0..@det-1){
						if($det[$k] =~/$envbond[$ei]-$envatom[$ei]/ && $listvu!~/ $k /){
							$see=1;
							$seei=$k;
							if($ei==$keepi && $keepi ne ""){
								$keepiatom=$det2[$k];
							};	
						};
					};
					if($seei ne ""){
						$listvu=$listvu." ";
					}
					else{
						$all=0;
					};
				};
				if($all){
					$onlyone=1;
					$p2=$l;
					$newatom=sprintf"%10s%10s%10s%1s%1s  %2s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s\n",$strx[$p2],$stry[$p2],$strz[$p2],$blanc,$atomx,$getstr[4],$getstr[5],$getstr[6],$getstr[7],$getstr[8],$getstr[9],$getstr[10],$getstr[11],$getstr[12],$getstr[13],$getstr[14],$getstr[15];
					$ligne[$p2+5]=$newatom;
				
					# check if other bonds connected to $p2
					$vubond=0;
					foreach $ei (6+$istratom..$finbond){
						#print "bond $ligne[$ei] search $p2 et $keepiatom\n";
						@get=split(' ',$ligne[$ei]);
						if($get[0]==$p2 || $get[1]==$p2){
						
							if($keepi ne "" && (($get[0]==$p2 && $get[1]==$keepiatom) || ($get[1]==$p2 && $get[0]==$keepiatom))){
								# no change
								$vubond=0;
								#print "OK $p2 and $keepiatom in ($get[0] $get[1])\n";
							}
							elsif($keepi ne ""){
								# add one atom H and replace bond line
								$vubond=1;
							};

							if($vubond > 0){# first bond not changed but next
								$atomH="Y";
								$ligneplus[$lp]=sprintf"%10s%10s%10s%1s%1s  %2s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s\n",$strx[$p2],$stry[$p2],$strz[$p2],$blanc,$atomH,$getstr[4],$getstr[5],$getstr[6],$getstr[7],$getstr[8],$getstr[9],$getstr[10],$getstr[11],$getstr[12],$getstr[13],$getstr[14],$getstr[15];
								$lp++;
								
								if($get[0]==$p2){
									$ligne[$ei]=sprintf"%3s%3s%3s  0  0  0  0\n",$istratom+1,$get[1],$get[2];
								}
								else{
									$ligne[$ei]=sprintf"%3s%3s%3s  0  0  0  0\n",$istratom+1,$get[0],$get[2];
								};

								# add one atom istratom line
								@get1=split(' ',$ligne[4]);
								$get1[0]++;
								$ligne[4]=sprintf"%3s%3s  0  0  0  0  0  0  0  0999 V2000\n",$get1[0],$get1[1];
							};	
							$vubond++;
						};
					};
				};
			}	
			else{ # ATOM (e.g. S-H -> S-X) First search S and then search for H bonded to S
				foreach $ei (0..@envatom-1){
					$see=0;
					$seei="";
					foreach $k (0..@det-1){
						if($det[$k] =~/$envbond[$ei]-$envatom[$ei]/ && $listvu!~/ $k /){
							$see=1;
							$seei=$k;
						};
					};
					if($seei ne ""){
						$listvu=$listvu." ";
					}
					else{
						$all=0;
					};
				};
				if($all){
					$onlyone=1;
					@det=split(' ',$fonc[$l]);
					@det2=split(' ',$ifonc[$l]);
					$p=0;
					foreach $k (0..@det-1){
						if($det[$k] =~/$envbond[0]-$envatom[0]/ && $atom[$det2[$k]] eq $envatom[0] ){
							$p=$k;
							$p2=$det2[$p];
							last;
						};
					};
					$keepiatom=$l;

					$newatom=sprintf"%10s%10s%10s%1s%1s  %2s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s\n",$strx[$p2],$stry[$p2],$strz[$p2],$blanc,$atomx,$getstr[4],$getstr[5],$getstr[6],$getstr[7],$getstr[8],$getstr[9],$getstr[10],$getstr[11],$getstr[12],$getstr[13],$getstr[14],$getstr[15];
					$ligne[$p2+5]=$newatom;

					# check if other bonds connected to $p2
					$vubond=0; 
					foreach $ei (6+$istratom..$finbond){ 
						@get=split(' ',$ligne[$ei]);
						if($get[0]==$p2 || $get[1]==$p2){

							if($keepi ne "" && (($get[0]==$p2 && $get[1]==$keepiatom) || ($get[1]==$p2 && $get[0]==$keepiatom))){
								$vubond=0;
							}
							elsif($keepi ne ""){
								$vubond=1;
							}
							elsif($keepi eq "" && (($get[0]==$p2 && $get[1]==$keepiatom) || ($get[1]==$p2 && $get[0]==$keepiatom))){
								$vubond=1;
							}
							elsif($keepi eq ""){
								$vubond=0;
							};

							if($vubond > 0){
								$atomH="Y";	
								$ligneplus[$lp]=sprintf"%10s%10s%10s%1s%1s  %2s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s\n",$strx[$p2],$stry[$p2],$strz[$p2],$blanc,$atomH,$getstr[4],$getstr[5],$getstr[6],$getstr[7],$getstr[8],$getstr[9],$getstr[10],$getstr[11],$getstr[12],$getstr[13],$getstr[14],$getstr[15];
								$lp++;
								
								if($get[0]==$p2){
									$ligne[$ei]=sprintf"%3s%3s%3s  0  0  0  0\n",$istratom+1,$get[1],$get[2];
								}
								else{	
									$ligne[$ei]=sprintf"%3s%3s%3s  0  0  0  0\n",$istratom+1,$get[0],$get[2];
								};
								
								# add one atom istratom line
								@get1=split(' ',$ligne[4]);
								$get1[0]++;
								$ligne[4]=sprintf"%3s%3s  0  0  0  0  0  0  0  0999 V2000\n",$get1[0],$get1[1];
							};	
							$vubond++;
						};	
					};       
				};
			};
		};	 

	};
	};

};


##########################################################################################################################

