#!/usr/bin/perl



local($mol2)=@ARGV;

	if ($mol2 eq ''){
		die "usage: mol2pdb <file.mol2>\n";
	};


	&readmol2($mol2);
	$mol2=~s/\// /g;
	@gety=split(' ',$mol2);
	$mol2=$gety[@gety-1];		
	$nom=$mol2;
	$nom=~s/\.mol2/\.pdb/;
	$nom2=$mol2;
	$nom2=~s/\.mol2//;
	@epel=split('',$nom2);
        $debnom=$epel[0].$epel[1].$epel[2];

       # print "$nom\n";
	$blanc=' ';
	open(OUT,">$nom");
		printf OUT "REMARK $nom\n";
	        foreach $k (1..$istratom){
	                @cara=split(' *',$typepdb[$k]);
	                $lcara=@cara;
	
	                $typepdb[$k]=" ".$cara[0]." " if($lcara == 1);
	                $typepdb[$k]=$cara[0].$cara[1]." " if($lcara == 2);

		if($stratomchg[$k] ne ""){	
			printf OUT "HETATM %4s %3s %4s     1    %8s%8s%8s%8s%7s \n",$k,$typepdb[$k],$debnom,$strx[$k],$stry[$k],$strz[$k],$stratomchg[$k],$radius[$k];
		}
		else{
			printf OUT "HETATM %4s %3s %4s     1    %8s%8s%8s \n",$k,$typepdb[$k],$debnom,$strx[$k],$stry[$k],$strz[$k];
		};
		
                };
                foreach $k (1..$istrbond){
                        printf OUT "CONECT %4s %4s\n",$strbond1[$k],$strbond2[$k];
                };
                printf OUT "TER\n";
	close(OUT);

	if($stratomchg[1] ne ""){	
		print "$nom DONE -> pqr format with charge and radius\n";
	}
	else{
		print "$nom DONE\n";
	};	

###############################################
###############################################


sub readmol2{
	local($chemin)=@_;

        %tabR=(
              'C',1.70,
               'O',1.52,
               'N',1.55,
                'S',1.80,
               'P',1.80,
               'Br',1.85,
             'Cl',1.75,
          'I',1.98,
         'F',1.47,
          'H',1.2,
         'Hp',1.1,
   );
																									
	
	open(OUT,"<$chemin");

	$flagatom=0;
	$flagbond=0;
	$istratom=1;
	$istrbond=1;
	$fn=0;
	@strx="";
	@stry="";
	@strz="";
	@stratomchg="";
	@radius="";
	
		while (<OUT>){
			@getstr = split(' ',$_);

			if ($fn){
				$name=$getstr[0];
#print"$name\n";
			};

			if ($getstr[0] eq '@<TRIPOS>SUBSTRUCTURE'){
				$flagbond=0;
			};
			if ($getstr[0] eq ''){
				$flagbond=0;
			};

			if (($flagbond)&&($getstr[0] ne '')){
				$strbond1[$istrbond]=$getstr[1];
				$strbond2[$istrbond]=$getstr[2];
				$getstr[3]="am" if (($stratomtype[$getstr[1]]=~/N.am/) && ($stratomtype[$getstr[2]]=~/C.2/));
				$getstr[3]="am" if (($stratomtype[$getstr[2]]=~/N.am/) && ($stratomtype[$getstr[1]]=~/C.2/));
				$strbondtype[$istrbond]=$getstr[3];
				$istrbond++;
			};

			if ($getstr[0] eq '@<TRIPOS>BOND'){
				$flagatom=0;
				$flagbond=1;
			};

			if (($flagatom )&&($getstr[0] ne '')){
				@strx[$istratom]=sprintf "%4.3f",$getstr[2];
				@stry[$istratom]=sprintf "%4.3f",$getstr[3];
				@strz[$istratom]=sprintf "%4.3f",$getstr[4];
			
				$stratomchg[$istratom]=sprintf "%3.4f",$getstr[8] if($getstr[8] ne '');
				
				$getstr[5]="N.am" if ($getstr[5]=~/N.3/);
				$stratomtype[$istratom]=$getstr[5];
				@ma=split('\.',$getstr[5]);
				@typepdb[$istratom]=$ma[0];
				$radius[$istratom]=sprintf "%2.4f",$tabR{$ma[0]};
				$masse=$masse+$tabmm{$ma[0]};
				$istratom++;
			};

			if ($getstr[0] eq '@<TRIPOS>ATOM'){
				$flagatom=1;
			};
			$fn=0;
			if ($getstr[0] eq '@<TRIPOS>MOLECULE'){
				$fn=1;
			};

		};

	close(OUT);
	$istrbond=$istrbond-1;
	$istratom=$istratom-1;
	$strrgyrno=$istratom;
#print"$strrgyrno, $istrbond\n";

};
