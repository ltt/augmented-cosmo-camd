#!/usr/bin/perl


### VARIABLES
$rms=5; # recherche dans centre du site actif

#lance FLEXX et met les resultats dans DOCK.out

$workdir='';
$list_mol2='';
$prot='';
$site='';
$flexxexe='';
$file_bat="";

# Coordonn�es d'un point dans le site actif
$refx='';
$refy='';
$refz='';

local($flexxexe,$workdir,$prot,$site,$refx,$refy,$refz,$rms,$list_mol2,$file_bat)=@ARGV;

if ($flexxexe eq '' || $workdir eq '' || $list_mol2 eq '' || $prot eq '' || $site eq '' || $refx eq '' || $refy eq '' || $refz eq ''){
	die " \n \t ~~~~~~~~~~~   DOCK  ~~~~~~~~~~~\n
	usage: FLEXX.pl <addresse FlexX> <WorkDIR> <protein> <actif_site> <refx> <refy> <refz> <list_mol2> <file.bat (optional)>\n
	~~~~~~~~~~~ ~~~~~~~~~~~ ~~~~~~~~~~~ ";
}



#*************************************************************************
# Fichiers protein.rdf config.dat  protein.pdb  site_actif list_mol2

$flexx_config=$ENV{FLEXX_HOME};
	
if(!-e "config.dat"){
	print "Warning : FLEXX will use $ENV{FLEXX_HOME}/config.dat !\n";
};

if (!-e "$flexx_config/config.dat" && !-e "config.dat"){
	die "\n
		Il manque la variable d'environnement FLEXX_HOME pour situer le fichier config.dat de FlexX \n
	\n";
};
	

if(!-e $prot){
	die "Il manque le fichier $prot dans la directory $workdir\n";
};

@getrdf=split('\.',$prot);
$rdf=$getrdf[0].".rdf";
$diminutif=$getrdf[0];

if(!-e $rdf){
	die "Il manque le fichier $rdf dans la directory $workdir\n";
};
	
if(!-e $site){
	die "Il manque le fichier $site dans la directory $workdir\n";
};

if(!-e $list_mol2){
	die "Il manque le fichier $list_mol2 dans la directory $workdir\n";
};


#*************************************************************************
# ecrit le script dock_list.bat

if ($file_bat eq ""){

	open(DOC,">dock_list.bat");
	printf DOC "
			# script to dock a list of ligand
			# exemple : flexx -b dock_list.bat -a '\$(p)=rarg;\$(list)=list_mol2;\$(n)=15' -o dbflexx.out &

			output \" >> docking \" \$(l) \" into \" \$(p)

			set verbosity 			3
			set assign_formal_charges 	1
			set place_particles		0

			receptor
				read \$(p)
			end

	for_each   \$(l) in \$(list)
       			lig
       				read   \$(l)
       			end

			docking
			selbas   a
			placebas 3
			complex  all

			info y 0
			listsol \$(n)
			end

			lig
  	 		write ./\$(l) n n 1-\$(n)
			end
			output \">>written:  \$(l)_xxx.mol2\"
	end_for

			delall y
			quit y	
	
	\n";
	
	close(DOC);

	#*************************************************************************
	# lance FLEXX
	
	if($param{VERBOSITY} > 0){
        	print "\n\n Docking par FlexX (dock.out contient les resultats de toutes les generations)\n\n";
        	print ('\\'x80);
        	print "\n";
	};
	system("$flexxexe -b dock_list.bat -a '\$(p)=$diminutif;\$(list)=$list_mol2;\$(n)=15' -o dbflexx.out");
	

}
else{

       if(!-e $file_bat){
       		print "Il manque le fichier $file_bat dans la directory $workdir\n";
       }
       else{
		if($param{VERBOSITY} > 0){
       			print "\n\n Docking par FlexX (dock.out contient les resultats de toutes les generations)\n\n";
        		print ('\\'x80);
        		print "\n";
		};
		system("$flexxexe -b $file_bat -a '\$(p)=$diminutif;\$(list)=$list_mol2;\$(n)=15' -o dbflexx.out");
	
       };
};


#*************************************************************************
# extrait les resultats

@list='';
$i=0;
$limite=0;#Match_score negative
$rien1=0;

open(DOC,"<$workdir/dbflexx.out");
while(<DOC>){
	if($_=~/Ligand/){
		@getnomb=split(' ',$_);
		$nomb=$getnomb[2];
	};
	if($_=~/Number of solutions :    0/){
		$list[$i]="$nomb.mol2 0.0 0.0";
		$i++;
	};
	if ($_=~/SELECTED SOLUTIONS:/){
		@get1=split(' ',$_);
		$name=$get1[4];
		$rien1=0;
	};
	@get=split('\|',$_);
	@getb=split(' ',$_);
	@getc=split('\|',$getb[1]);

	# modif jan2007
	# only one solution printed in $name !	
	if($_=~/dock entry/ && $_=~/written to/ && $rien1==1){
		@getc=split(' ',$list[$i-1]);
		print "Only one solution: mv file $name.mol2 into $getc[0]\n";
		system("cp $name.mol2 $getc[0]");
	};
	
	if (($_=~/^\|  1|/)&&($get[2] <= $limite)&&($getb[0] eq '|')&&($getc[0]==1)){
		#print $_;
		$list[$i]="$get1[4]_001.mol2 $get[2] 0.0";
		$i++;
		$printed=$get1[4];
		$diff=' ';
		$rien1++;
	}
	elsif($_=~/^\|/ && $printed eq $name){
		#print $_;
		$rien1++;
		#if (($get[2] <= $limite)&&($getb[0] eq '|')&&($get[8] >  3.0)){
		#modif 11 sep 2009:
		if (($get[2] <= $limite)&&($getb[0] eq '|')&&($get[8] >1.0)){
			$entier=int($get[8]);#keep only one per int(rms)
			$test=' '.$entier.' ';
			if ($diff=~/$test/){
				$rien=0;
			}
			else{
				$diff=$diff.$entier.' ';
				$list[$i]="$get1[4]_00$getc[0].mol2 $get[2] $get[8]" if ($getc[0]<10);
				$list[$i]="$get1[4]_0$getc[0].mol2 $get[2] $get[8]" if (($getc[0]>=10)&&($getc[0]<100));
				$list[$i]="$get1[4]_$getc[0].mol2 $get[2] $get[8]" if (($getc[0]>=100)&&($getc[0]<1000));
				$i++;
			};
		};	
	};
		
};	
close(DOC);

$nombre=@list-1;
#print"$nombre structures de score < $limite && de RMSvalue > 3.0 (par rapport � la premiere) && un seul par cluster de RMSD [int(rmsd) equivalent]\n";
#print"@list\n";

#print "ref= $refx ; $refy ; $refz\n";

%tabmm=(
	'C',12,
	'O',16,
	'N',14,
	'S',32,
	'P',31,
	'Br',80,
	'Cl',35,
	'I',127,
	'F',19,
	'H',1,
);

open(KEE,">$workdir/flexx.tmp"); # contains different rms solutions for each molecule/conformer
printf KEE "#nom score rms_1er dist_ref\n";
@result='';

foreach $i (0..@list-1){
	
	@get=split(' ',$list[$i]);

	if (-e "$workdir/$get[0]" && $get[1] < 0.0){
	
		&readmol2("$workdir/$get[0]");
		$rgyrmm=0;
		$rgx=0;
		$rgy=0;
		$rgz=0;
		foreach $rgyrk (1..$strrgyrno){
			@sepstr = split('\.',$stratomtype[$rgyrk]);
			$rgyrmm=$rgyrmm+$tabmm{$sepstr[0]};
			$rgx=$rgx+$strx[$rgyrk]*$tabmm{$sepstr[0]};
			$rgy=$rgy+$stry[$rgyrk]*$tabmm{$sepstr[0]};
			$rgz=$rgz+$strz[$rgyrk]*$tabmm{$sepstr[0]};
		};
		if($rgyrmm != 0){
			$rgx=$rgx/$rgyrmm;
			$rgy=$rgy/$rgyrmm;
			$rgz=$rgz/$rgyrmm;
		};
#		print "La masse moleculaire $rgyrmm\n";
#		print "Coordonnees du centre de masse ($rgx ; $rgy ; $rgz)\n";

		if($refx ne "-" && $refy ne "-" && $refz ne "-"){
#			calcul de la distance entre le centre de masse de 2 molecules
			$distance=sqrt(($rgx-$refx)**2+($rgy-$refy)**2+($rgz-$refz)**2);
		}
		else{
		        $distance="-";
			#print "Distance / REF =  $distance\n";
                };

                $result[$i]="$get[0] $get[1] $get[2] $distance";
		printf KEE"$get[0] $get[1] $get[2] $distance\n";
		#print "$get[0] $get[1] $get[2] $distance\n";
		
	}
	else{
		$result[$i]="$get[0] 0.0 0.0 -";
		printf KEE "$get[0] 0.0 0.0 -\n";
	};
	
	#unlink "$workdir/mol*_*.mol2";	
};
close(KEE);

#modif 11 july 2008
$go=0; 
if($go==0){ # print only solutions with correct rms
	
	open(KEE,">$workdir/flexx.out");
	print KEE "#nom score rms_1er dist_ref\n";
	foreach $i (0..@result-1){
		@get=split(' ',$result[$i]);
		if($get[3] <= $rms){
			printf KEE "$result[$i]\n";
		};
	};	
	close(KEE);
}
else{# print only the best one !
	
open(KEE,">$workdir/flexx.out"); # keep the best flexx solution (number from 1 to 15) for each molecule/conformer
printf KEE "#nom score rms_1er dist_ref\n";
	$keep=0;
	foreach $i (1..@result-1){
		
		@get=split(' ',$result[$i]);
		@get2=split('_',$get[0]);
		
		@getold=split(' ',$result[$keep]);
		@getold2=split('_',$getold[0]);
		
		#if($get2[0] eq $getold2[0]){
		if($get2[1] eq $getold2[1]){	
		
			if($refx ne "-" && $refy ne "-" && $refz ne "-"){
				if($get[3] < $rms && $getold[3] >= $rms){
					$keep=$i;
				};
			}
			else{
			       if($get[1] < $getold[1]){
					$keep=$i;
				};
			};
		}
		else{
			printf KEE "$result[$keep]\n";
			#print "$result[$keep]\n";
			$keep=$i;
		};
	};
printf KEE "$result[$keep]\n";
close(KEE);
};


###############################################
###############################################


sub readmol2{
	local($chemin)=@_;

	open(KEN,"<$chemin");

	$flagatom=0;
	$flagbond=0;
	$istratom=1;
	$istrbond=1;
	$fn=0;
		while (<KEN>){
			@getstr = split(' ',$_);

			if ($fn){
				$name=$getstr[0];
#print"$name\n";
			};

			if ($getstr[0] eq '@<TRIPOS>SUBSTRUCTURE'){
				$flagbond=0;
			};
			if ($getstr[0] eq ''){
				$flagbond=0;
			};

			if (($flagbond)&&($getstr[0] ne '')){
				@strbond1[$istrbond]=$getstr[1];
				@strbond2[$istrbond]=$getstr[2];
				$getstr[3]="am" if (($stratomtype[$getstr[1]]=~/N.am/) && ($stratomtype[$getstr[2]]=~/C.2/));
				$getstr[3]="am" if (($stratomtype[$getstr[2]]=~/N.am/) && ($stratomtype[$getstr[1]]=~/C.2/));
				@strbondtype[$istrbond]=$getstr[3];
				$istrbond++;
			};

			if ($getstr[0] eq '@<TRIPOS>BOND'){
				$flagatom=0;
				$flagbond=1;
			};

			if (($flagatom )&&($getstr[0] ne '')){
				@strx[$istratom]=	$getstr[2];
				@stry[$istratom]=	$getstr[3];
				@strz[$istratom]=	$getstr[4];
				$getstr[5]="N.am" if ($getstr[5]=~/N.3/);
				$stratomtype[$istratom]=$getstr[5];
				@ma=split('\.',$getstr[5]);
				$masse=$masse+$tabmm{$ma[0]};
				$istratom++;
			};

			if ($getstr[0] eq '@<TRIPOS>ATOM'){
				$flagatom=1;
			};
			$fn=0;
			if ($getstr[0] eq '@<TRIPOS>MOLECULE'){
				$fn=1;
			};

		};

	close(KEN);
	$istrbond=$istrbond-1;
	$istratom=$istratom-1;
	$strrgyrno=$istratom;
#print"$strrgyrno, $istrbond\n";

};


#*************************************************************************
#*************************************************************************


