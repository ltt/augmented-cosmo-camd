#!/usr/bin/perl


print "SUBROUTINE DOCKING \n";

sub docking{

	local($filesdf)=@_;

	# launch all conformers of mol 

	####### Preparation for docking
	#sdf: $filesdf
	#mol2:  mol_$dj.mol2 $dj from 1 to $nbchildevaluer 

	chop($tmpmol2ref=`$leaexe/SDF_MOL2.pl $filesdf`);	
	#print "$tmpmol2ref\n";
	
	# defines if conformers must continue for other dockings if multiple docking
	
	@continuedock=""; # indice begins at 1
	@dockingcomments=""; #get results for all conformers
	@globaldock="";
	
	foreach $dj (1..$nbchildevaluer){
		$continuedock[$dj]=1;
	};

	
##################################################################

	# output to be parsed by SCORE.pl
	$dockingresults="#Conformer Global_Score ";
	@dockingbis="";
	
	foreach $docki(0..@prot-1){

		#multiple docking: specificity
		if($docki > 0){
			if($docki==1){
				#keep memory of first dock score
				@dockingbis=@docking;
			};
			
			foreach $dj (1..$nbchildevaluer){
				if($dockingbis[$dj]==0 && $specificity[$docki] ne "OR"){
					print "conformer $dj: 1st dock_score($prot[0])= $dockingbis[$dj] discarded for docking in $prot[$docki] ($specificity[$docki])\n";
				       $continuedock[$dj]=0;
				}
				elsif($docking[$dj]==0 && $specificity[$docki] eq "OR"){
					$continuedock[$dj]=1;
					print "conformer $dj: 1st dock_score($prot[0])= $dockingbis[$dj] accepted for docking in $prot[$docki] ($specificity[$docki])\n";
				}
			};
		};		
				       
	       	$dockingresults=$dockingresults." $prot[$docki]($specificity[$docki])"; 
		@docking="";
		@apolar_sas="";

		# print list to dock : used by FLEXX, SURFLEX and PLANTS
		$nblistmol2dock=0;
		open(DOL,">list_mol2_dock"); 
		foreach $dj (1..$nbchildevaluer){
			if(-e "mol_$dj.mol2"){	
				if($continuedock[$dj]==1){
					print DOL "mol_$dj.mol2\n";
					$nblistmol2dock++;
					#	print "mol_$dj.mol2 accepted\n";
				}
				else{
					print "mol_$dj.mol2 discarded\n";
				};		
			}
			else{
				print "mol_$dj.mol2 does not exist\n";
			};	
			$docking[$dj]=0;
			$apolar_sas[$dj]=0;	
		};		
		close(DOL);

       		if($nblistmol2dock != 0){
			if($param{VERBOSITY} >= 1 && $refx[$docki] eq "-" && $refy[$docki] eq "-" && $refz[$docki] eq "-"){
				print "Keep docked solutions if score < $limitscore[$docki] (position in the active site is not checked)\n";
			}
			elsif($param{VERBOSITY} >= 1){
				print "Keep docked solutions if score < $limitscore[$docki] and if position of the center of mass is close to ref coordinates < $rmsdock[$docki]\n";	
			};
			
			
			######################################## CASE of FLEXX

			if($docking_program eq "FLEXX"){
				
				system("$leaexe/FLEXX.pl $flexxexe $param{WORKDIR} $prot[$docki] $site[$docki] $refx[$docki] $refy[$docki] $refz[$docki] $rmsdock[$docki] list_mol2_dock $file_bat[$docki]");

				$dockingfile="flexx.out";
				#format:
				#mol2_solution_filename   Score   RMS_relative_to_first_Solution   Distance_to_refx_refy_refz

			################################################### CASE of SURFLEX	
			}
			elsif($docking_program eq "SURFLEX"){
				
				# pl-protomol.mol2 -> protomol_$prot[$docki] and $protmol2 must exist
				$protmol2=$prot[$docki];
				$protmol2=~s/\.pdb$/\.mol2/;
				$proto="protomol_".$protmol2;
				
				if(!-e "$proto" || !-e "$protmol2"){
				       die "$proto or $protmol2 not found !\n";
			       	};	       
				system("$leaexe/SURFLEX.pl $surflexexe $protmol2 $proto $refx[$docki] $refy[$docki] $refz[$docki] $rmsdock[$docki] list_mol2_dock");

				$dockingfile="surflex.out";
				#format:
				#mol2_solution_filename   Score   RMS_relative_to_first_Solution   Distance_to_refx_refy_refz penality_score polar_score
			
			################################################### CASE of PLANTS	
			}
			elsif($docking_program eq "PLANTS"){
				
				#$protmol2 must exist
				$protmol2=$prot[$docki];
				$protmol2=~s/\.pdb$/\.mol2/;
				
				if(!-e "$protmol2"){
					die "$protmol2 not found !\n";
				};		
				
				system("$leaexe/PLANTS.pl $plantsexe $protmol2 $refx[$docki] $refy[$docki] $refz[$docki] $rmsdock[$docki] list_mol2_dock $file_bat[$docki]");

				$dockingfile="plants.out";
				#format:
				#mol2_solution_filename   Score   RMS_relative_to_first_Solution   Distance_to_refx_refy_refz
				
			};		
			
			###################################################			

			print "Docking on protein $prot[$docki]:\n";
			$dockscore="";
			open(DOC,"<$dockingfile");
			while(<DOC>){
			if($_!~/^#/){
				
				@getdoc=split(' ',$_);
				
				$solutiondock=$getdoc[0];
				
				# no of conformer
				$suppnom=$getdoc[0];
				$suppnom=~s/\.mol2//;
				$suppnom=~s/mol_//;
				$suppnom=~s/(.*)_(.*)/$1/;

				if($dockscore ne "" && $suppnom ne $suppnomold){
					$dockscore="";
				};	
				$suppnomold=$suppnom;

				# CASES SURFLEX (positive scores) or FLEXX (negative scores)
				# PLANTS negative scores
				  	
				if($dockscore eq "" || (($dockscore > $getdoc[1] || $summarydock=~/discard/) && $docking_program eq "FLEXX") || (($dockscore < $getdoc[1] || $summarydock=~/discard/) && $docking_program eq "SURFLEX") || (($dockscore > $getdoc[1] || $summarydock=~/discard/) && $docking_program eq "PLANTS")){
				
				$dockscore=sprintf"%5.3f",$getdoc[1];
				$dockrms=sprintf"%5.3f",$getdoc[3];
				
				$summarydock="Conformer $suppnom";
				
				if(($docking_program eq "SURFLEX" && $dockscore > $limitscore[$docki]) || ($docking_program eq "FLEXX" && $dockscore < $limitscore[$docki]) || ($docking_program eq "PLANTS" && $dockscore < $limitscore[$docki])){
					
					$summarydock=$summarydock." solution=$solutiondock score=$dockscore";
					$summarydock=$summarydock." penality=$getdoc[4] polar=$getdoc[5]" if($docking_program eq "SURFLEX");
				
					if($dockrms <= $rmsdock[$docki] || ($refx[$docki] eq "-" && $refy[$docki] eq "-" && $refz[$docki] eq "-")){
						$summarydock=$summarydock." distref=$dockrms";
						
						#	print "$summarydock\n";

						# check interactions and charge bumping -/- and +/+
						# interactions only for $prot[0]
						# charge bumping for all $prot if $distchargebumping ne ""
						$flagdiscard=&dock_interaction;
						#print "flagdiscard= $flagdiscard (1=yes)\n";
						 
						if($flagdiscard==0){

							if($nomtab ne "child"){
							$gi2=$gi+1; #set in MAIN

							# cp mol2 conformer and cat with active site
							$genericdock="g".$generation."_mol$gi2"."_conf$suppnom"."_prot$docki";
							$genericdockfull=$genericdock."full";
							$genericdock_docklog=$genericdock."_docklog";
 
							chop($tmppdb=`$leaexe/MOL2PDB.pl $solutiondock`);
							$tmppdb=$solutiondock;
							$tmppdb=~s/\.mol2/\.pdb/;
							system("cat $site[$docki] $tmppdb > $protdir[$docki]/$genericdock.pdb");
							system("cat $prot[$docki] $tmppdb > $protdir[$docki]/$genericdockfull.pdb");
							system("cp $solutiondock $protdir[$docki]/$genericdock.mol2");
							if($docking_program eq "FLEXX"){
								system("cp dbflexx.out $protdir[$docki]/$genericdock_docklog");
							}
							elsif($docking_program eq "SURFLEX"){
								system("cp surflex_log $protdir[$docki]/$genericdock_docklog");
							}
							elsif($docking_program eq "PLANTS"){
								system("cp plants_log $protdir[$docki]/$genericdock_docklog");	
							};

							unlink "$tmppdb";	
							
							#XSCORE
							chop($xscoremax=`$leaexe/xscore.sh $site[$docki] $solutiondock log_xscore `);
							$xscoremax2=$xscoremax;
							$xscoremax =~ /(\d+.\d\d)/;
							$xscoremax = $1;
							unlink "log_xscore";
							unlink "score.mol2";
							$summarydock=$summarydock." xscore=$xscoremax";
						      	};

							$docking[$suppnom]=$dockscore."($xscoremax)";	

							#DISABLED: apolar calculation
							#if($flagdock_apolar_sas && $docki==0){
								# Calcul du gain de surface hydrophobe buried
								#&sas_protein("$protdir[$docki]/$genericdock.pdb");
								#$apolar_sas[$suppnom]=$prot_area;
								#print "ASA=$apolar_sas[$suppnom]\n";

								#if($apolar_sas[$suppnom] > 0 && $prot_arearef > 0){
									#$surfpolairetab is the area to be calculated for the molecule
									#$delta_sas=$apolar_sas[$suppnom]-($prot_arearef + $surfpolairetab);
									#$gnptot=$delta_sas * 0.00542 + 0.92;
									#print "Buried surface = $delta_sas\n";
								#};	
							#};
						}
						else{
							$summarydock=$summarydock." (discarded by interaction/chargebumping)";
						};	
					}
					else{
						$summarydock=$summarydock." distref=$dockrms (discarded by rms > limit=$rmsdock[$docki])";
					};	
				}
				else{
					$summarydock=$summarydock." score=$dockscore (discarded by score > limit=$limitscore[$docki])";
				};	
				
				print "$summarydock\n";
				
				# CLEAN
				#e.g.: mol_1_001.mol2 is the first solution from flexx for conformer 1
				#$genericdock="mol_".$suppnom."_";
				#$genericdock1=$genericdock."001.mol2";
				#$genericdock2=$genericdock."1.mol2";
				#system("rm -f $genericdock*.mol2") if(-e "$genericdock1" || -e "$genericdock2" );
				};#end of if dockscore 
				
			};# end of if
			};#end of while
			close(DOC);

			# CLEAN
			#FLEXX mol_1_001.mol2 is the first solution from flexx for conformer 1
			#SURFLEX mol_1_0.mol2 is the first solution from surflex for conformer 1
			#PLANTS mol_1_0.mol2 is the first solution from plants for conformer 1
			
			#system("rm -f mol*_*.mol2"); 
			foreach $rmi (1..$nbchildevaluer){
				$rmname="mol_".$rmi."_*.mol2";
				system("rm -f $rmname");
			};	

			# will changed after each docking thus after each mol$gi
			if($docking_program eq "FLEXX"){ 
				system("mv dbflexx.out* $protdir[$docki]/.");
				system("mv flexx.* $protdir[$docki]/.");
				system("mv dock_list.bat $protdir[$docki]/.") if(-e "dock_list.bat");
				system("mv list_mol2_dock $protdir[$docki]/.");
			}
			elsif($docking_program eq "SURFLEX"){
				system("mv dbsurflex.out* $protdir[$docki]/.");
				system("mv surflex.* $protdir[$docki]/.");
				system("mv list_mol2_dock $protdir[$docki]/.");
				system("mv surflex_log $protdir[$docki]/.");
				#system("mv dbsurflex.out-results.mol2 $protdir[$docki]/.");
			}
			elsif($docking_program eq "PLANTS"){
				$dirplants="plants_".$protdir[$docki];
				system("rm -rf $dirplants");		
				if($file_bat[$docki] eq ""){		
					system("mv $protdir[$docki].com $protdir[$docki]/.");
				}
				else{
					system("cp $file_bat[$docki] $protdir[$docki]/.");
				};	
				system("mv plants.* $protdir[$docki]/.");	
				system("mv list_mol2_dock $protdir[$docki]/.");
				system("mv plants_log $protdir[$docki]/.");
				system("rm -f PLANTS-*.pid");
				system("mv PLANTS.err $protdir[$docki]/.") if(-e "PLANTS.err");
			};		
			
		};#end of if $nblistmol2dock != 0
		
		foreach $dj (1..$nbchildevaluer){

			if($docki > 0){

				#multiple docking: specificity: ajust scores
				$tmpscoredock=$docking[$dj];
				$tmpscoredock=~s/(.*)\((.*)/$1/;
		
				$tmpscoredock1=$dockingbis[$dj];
				$tmpscoredock1=~s/(.*)\((.*)/$1/;
				
				#print "docki=$docki dj=$dj dock1=$tmpscoredock from $docking[$dj] ; dock=$tmpscoredock1 from $dockingbis[$dj]\n";
				
				if($specificity[$docki] eq "OR"){
					if($docking_program eq "FLEXX" && $tmpscoredock1==0 && $tmpscoredock < 0){ #case FLEXX
						$globaldock[$dj]=$tmpscoredock;
					}
					elsif($docking_program eq "SURFLEX" && $tmpscoredock1==0 && $tmpscoredock > 0){
						$globaldock[$dj]=$tmpscoredock;
					}
					elsif($docking_program eq "PLANTS" && $tmpscoredock1==0 && $tmpscoredock < 0){
						$globaldock[$dj]=$tmpscoredock;	
					};	
				}		
				elsif($specificity[$docki] eq "NOT" && $tmpscoredock < 0 && $docking_program eq "FLEXX"){
					#then pas specific
					$globaldock[$dj]=0;
				}
				elsif($specificity[$docki] eq "NOT" && $tmpscoredock > 0 && $docking_program eq "SURFLEX"){
					#then pas specific
					$globaldock[$dj]=0;	
				}
				elsif($specificity[$docki] eq "NOT" && $tmpscoredock < 0 && $docking_program eq "PLANTS"){
					#then pas specific
					$globaldock[$dj]=0;
				}	
				elsif($specificity[$docki] eq "AND" && $tmpscoredock == 0){
					#then pas dual docking
					$globaldock[$dj]=0;
				}
				else{
					$globaldock[$dj]=$tmpscoredock1;
				};	
					
			}
			else{
				$tmpscoredock=$docking[$dj];
				$tmpscoredock=~s/(.*)\((.*)/$1/;
				$globaldock[$dj]=$tmpscoredock;
			};	

			$dockingcomments[$dj]=$dockingcomments[$dj]." $docking[$dj]";
		};
		
		
	};#end of $prot

	foreach $dj (1..$nbchildevaluer){
		unlink "mol_$dj.mol2" if (-e "mol_$dj.mol2");
	};
	
	$dockingresults=$dockingresults."\n";
	foreach $dj (1..$nbchildevaluer){
		$dockingresults=$dockingresults." $dj $globaldock[$dj]".$dockingcomments[$dj]."\n";
	};
	$dockingresults;
};

#################################################################################################"
#################################################################################################"

sub  sas_protein{
	local($pdb)=@_;

	
        $k=0;
        @cox='';
        @coy='';
        @coz='';
        @atom='';

	open(IN,"<$pdb");
	while(<IN>){

		@getp=split('',$_);
	
	        if(($_ =~/^ATOM/ || $_ =~/^HETATM/) && ($residu!~/H2O/ && $residu!~/WAT/ && $residu!~/HOH/ && $residu!~/OH2/)){
		
			$residu=$getp[17].$getp[18].$getp[19];
			$residu=~s/ //g;	
			$polcha=$getp[21];
			$no=$getp[22].$getp[23].$getp[24].$getp[25].$getp[26];
			$no=~s/ //g;
			$id=$getp[70].$getp[71].$getp[72].$getp[73].$getp[74].$getp[75];
		
		        $type_atom=$getp[12].$getp[13].$getp[14].$getp[15];
	        	$type_atom=~s/ //g;

	        	$type_atombis='';
	        	$type_atombis=$getp[76].$getp[77];
	        	$type_atombis=~s/\n//g;
	        	$type_atombis=~s/ //g;
			if($type_atombis eq ''){
				@type_atombis2=split('',$type_atom);
				$type_atombis=$type_atombis2[0];

				if($type_atombis2[0] eq "B" && ($type_atombis2[1] eq "R" || $type_atombis2[1] eq "r")){
					$type_atombis=$type_atombis2[0].$type_atombis2[1];
				};
				
				if($type_atombis2[0] eq "C" && ($type_atombis2[1] eq "L" || $type_atombis2[1] eq "l")){
					$type_atombis=$type_atombis2[0].$type_atombis2[1];
				};
			};

	        	$atom[$k]=$type_atombis;
			print "Atom type undefined !\n" if($atom[$k] eq "");
			$cox[$k]=$getp[30].$getp[31].$getp[32].$getp[33].$getp[34].$getp[35].$getp[36].$getp[37];
			$coy[$k]=$getp[38].$getp[39].$getp[40].$getp[41].$getp[42].$getp[43].$getp[44].$getp[45];
			$coz[$k]=$getp[46].$getp[47].$getp[48].$getp[49].$getp[50].$getp[51].$getp[52].$getp[53];
	                $k++;

			
    		};
   	
	};
	close(IN);

	
        #================================= Surface accessible

	open(GP,">psa.in");
		printf GP "* XYZR\n";
		$nbatomprot=@cox;
		printf GP "%8s\n",$nbatomprot;
		foreach $ig (0..@cox-1){
			$radius[$ig]=$tabR{$atom[$ig]};
			$cox[$ig]= sprintf "%5.4f",$cox[$ig];
			$coy[$ig]= sprintf "%5.4f",$coy[$ig];
			$coz[$ig]= sprintf "%5.4f",$coz[$ig];
			printf GP"%9s%1s%9s%1s%9s%1s%6s%1s%6s\n",$cox[$ig],$blanc,$coy[$ig],$blanc,$coz[$ig],$blanc,$radius[$ig],$blanc,$atom[$ig];  	
		};  	
	 	
	close(GP);

        unlink "psa.out";
	if($param{UNIX}){	
		system("$leaexe/ncs_accessible_unix $param{WORKDIR}/psa.in > control_ncs");
	}
	elsif($param{LINUX}){
		system("$leaexe/ncs_accessible_linux $param{WORKDIR}/psa.in > control_nsc");
	};

        $prot_area=0;
        $prot_volume=0;
	$prot_surfpolaire=0;
	$prot_surfapolaire=0;
	
	open(IN2,"<psa.out");
	while(<IN2>){
		@get=split(' ',$_);
		if($get[1] eq 'ATOM'){

			if($atom[$get[0]] eq 'N' || $atom[$get[0]] eq 'O'){
				$prot_surfpolaire=$prot_surfpolaire+$get[7];
			};
			if($atom[$get[0]] eq 'H' && ($fonc[$get[0]]=~/ 1-O / || $fonc[$get[0]] =~ /1-N /)){  		
				$prot_surfpolaire=$prot_surfpolaire+$get[7];
			};
		}
		elsif ($get[0] eq 'area'){
			$prot_area=sprintf"%5.3f",$get[2];
		}
		elsif ($get[0] eq 'volume'){
			$prot_volume=sprintf"%5.3f",$get[2];
		};
	};
	close(IN2);
	
	unlink "psa.in";
	unlink "psa.out";
	unlink "control_nsc" if(-e "control_nsc");	
	
	print "PROTEIN $pdb\n";
	$prot_surfapolaire=$prot_area-$prot_surfpolaire;
	$prot_surfapolaire=sprintf"%5.3f",$prot_surfapolaire;
	$prot_surfpolaire=sprintf"%5.3f",$prot_surfpolaire;
	print "\tPolar SAS = $prot_surfpolaire Lipophilic SAS = $prot_surfapolaire\n";
	print "\tAREA (Probe = 1.40 A) = $prot_area\n";
	print "\tVOLUME = $prot_volume\n" ;
	
	
};

#################################################################################################"
#################################################################################################"

sub dock_interaction{

	$tmpvuinteraction=0;
	
	unlink "interaction.dat" if(-e "interaction.dat");

	if(($interaction[0] ne "" && $docki == 0) || $distchargebumping ne ""){
		system("$leaexe/interactpdbmol2 $site[$docki] $solutiondock 0.0 $distmaxinteract >interaction.dat");
	};

	if($distchargebumping ne ""){
	$flagbumpcharge=0;
	open(PAM,"<interaction.dat");
	while(<PAM>){
		@interac=split(' ',$_);
		# ASP
		if(($_=~/PROT_ATOMTYPE_OD2 / || $_=~/PROT_ATOMTYPE_OD1 /) && $_=~/LIG_ATOMTYPE_O.co2 /){
		       if($interac[@interac-1] <= $distchargebumping){
		       		$flagbumpcharge=1;
		 		print "charges bumping !$_\n";# if($param{VERBOSITY} >= 1);
			};
		};
		#GLU
		if(($_=~/PROT_ATOMTYPE_OE1 / || $_=~/PROT_ATOMTYPE_OE2 /) && $_=~/LIG_ATOMTYPE_O.co2 /){
			if($interac[@interac-1] <= $distchargebumping){
				$flagbumpcharge=1;
				print "charges bumping ! $_\n";# if($param{VERBOSITY} >= 1);
			};
		};
	};
	close(PAM);
	$tmpvuinteraction=1 if($flagbumpcharge);
	};

	$dismissed=0;
	if($interaction[0] ne "" && $docki == 0){
		@vinteraction='';
		open(PAM,"<interaction.dat");
		while(<PAM>){
			@interac=split(' ',$_);
			foreach $ini (0..@interaction-1){
				if($interaction[$ini]=~/ OR / || $interaction[$ini]=~/ or /){
					$interaction[$ini]=~s/ or / OR /g;
					$interaction[$ini]=~s/interaction//g;

					@interac3=split(' OR ',$interaction[$ini]);
					
					$interactvu2=0;
					foreach $inv (0..@interac3-1){
						@interac2=split(' ',$interac3[$inv]);
						$interactvu=1;
						foreach $inj (0..@interac2-1){
							$interactvu=0 if($_!~/$interac2[$inj]/);
							#print "$ini @interac3:\n\t$interac2[$inj]\n";
						};
						if($interactvu && ($interac[@interac-1] < $mininteraction[$ini] || $interac[@interac-1] > $maxinteraction[$ini])){
							#print "$mininteraction[$ini] $interac[@interac-1] $maxinteraction[$ini]\n";	
							$interactvu=0;		
						};
						$interactvu2=1 if($interactvu);
					};	
					$vinteraction[$ini]=1 if($interactvu2);
				}
				else{
					@interac2=split(' ',$interaction[$ini]);
					$interactvu=1;
					foreach $inj (0..@interac2-1){
						$interactvu=0 if($_!~/$interac2[$inj]/);
					};
					if($interactionif[$ini]==1 && $interactvu){
						$vinteraction[$ini]=2 if($vinteraction[$ini] eq "");
					};
		 			if($interactvu && ($interac[@interac-1] < $mininteraction[$ini] || $interac[@interac-1] > $maxinteraction[$ini])){						 
						$interactvu=0;		
					};
					$vinteraction[$ini]=1 if($interactvu);
				};
			};
		};
		close(PAM);		

		$dismissed=0;	
		foreach $inj (0..@interaction-1){
			if($interactionif[$inj]==1 && $vinteraction[$inj] eq ""){
				#pas vu donc ok
				print "Optional Interaction $interactionif[$inj] $interaction[$inj] [$mininteraction[$inj] ; $maxinteraction[$inj]] not found ! molecule $solutiondock accepted !\n";# if($param{VERBOSITY} >= 1);
			}
			elsif($vinteraction[$inj]==2 && $interactionif[$inj]==1){
				#vu mais pas ok
				$dismissed=1;
				print "Optional Interaction $interactionif[$inj] $interaction[$inj] found but not into [$mininteraction[$inj] ; $maxinteraction[$inj]] ! molecule $solutiondock dismissed !\n";# if($param{VERBOSITY} >= 1);
			}
			elsif($vinteraction[$inj] eq ""){
				$dismissed=1;
				print "Interaction $interactionif[$inj] $interaction[$inj] [$mininteraction[$inj] ; $maxinteraction[$inj]] not found ! molecule $solutiondock dismissed !\n";# if($param{VERBOSITY} >= 1)
			}
			else{
				print "Interaction $interactionif[$inj] $interaction[$inj] [$mininteraction[$inj] ; $maxinteraction[$inj]] exists ! molecule $solutiondock accepted !\n";# if($param{VERBOSITY} >= 1)
			};
		};
	};

	unlink "interaction.dat" if(-e "interaction.dat");

	$tmpvuinteraction=1 if($dismissed);
	$tmpvuinteraction;				
};

#################################################################################################

