#!/usr/bin/perl



#################################
#
# Les coordonn�es x, y, z sont dans les tableaux @strx, @stry, @strz
# $istrbond est le nombre de liaisons
# $istratom est le nombre d'atomes
# *** @radius contient le rayon de l'atome
# *** @stratomchg contient la charge si elle existe dans le mol2
# *** @type contient l'hybridation (type atomique) de l'atome
# *** @typeint contient l'hybridation (type atomique) de l'atome interne au pgm
# @atom contient le type d'�lement atomique
# *** l'atome @strbond1 est lie a l'atome @strbond2 par liaison de type @strbondtype
# *** le nom de la molecule est $name
#
#################################


local($filealire)=@ARGV;

if ($filealire eq ''){
	die "usage: makescaffold  <file.sdf>\n";
};

$workdir='';
chop($workdir = `pwd` );

$leaexe=$ENV{LEA3D};
die "\nCheck environment variable(setenv LEA3D) \n\n" if (!-e "$leaexe/MAIN");

require "$leaexe/CYCLE_SDF.pl";


	system("/bin/rm -f $workdir/make_scaffold.sdf");

	$debug=0;	
	$rasmol=0;

##### PROPRIETES FROM SDF
	
	$moli=0;
	$flagnew=1;
	$flagmend=0;
	$nbmol=0;
	$blanc=' ';
	$printsca=0;
	
	open(MOL,"<$filealire");
	while(<MOL>){
	
		if($flagnew){
			
			$nbmol++;
			@tabfilesdf='';
			$compt=0;
			$ig=1;
			$jg=0;
			$moli++;
			
			@strx='';
			@stry='';
			@strz='';
			@atom='';
			
			@coval='';
			@nbcoval='';
			@fonc='';
			@ifonc='';
			@covfonc='';
			@listb='';
			
			$flagnew=0;
		};
		
		$tabfilesdf[$compt]=$_;
		@getstr = split(' ',$_);
		
		$compt++;
		
		if (($compt > 4) && ($ig <= $istratom)){
		
			$strx[$ig]=$getstr[0];
			$stry[$ig]=$getstr[1];
			$strz[$ig]=$getstr[2];
			$atom[$ig]=$getstr[3];

			if($atom[$ig] eq 'X'){
	                        print "Warning \'X\' dummy atom detected and changed to H !\n";
				$atom[$ig]="H";
			};	
                	
			$ig++;
		};
		if (($compt > 4) && ($ig > $istratom) && ($jg <=$istrbond)){
			if ($jg == 0){
				$jg++;
			}
			else{

                               @coller=split(' *',$getstr[0]);
                                @coller2=split(' *',$getstr[1]);
                                if(@coller==6 && $getstr[1] ne ""){
                                        $getstr[0]=$coller[0].$coller[1].$coller[2];
                                        $getstr[2]=$getstr[1];
                                        $getstr[1]=$coller[3].$coller[4].$coller[5];
                                }
                                elsif(@coller==6 && $getstr[1] eq ""){
                                        $getstr[0]=$coller[0].$coller[1];
                                        $getstr[1]=$coller[2].$coller[3].$coller[4];
                                        $getstr[2]=$coller[5];
                                }
                                elsif(@coller==5){
                                        if($_=~/^\s/){
                                                $getstr[0]=$coller[0].$coller[1];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[2].$coller[3].$coller[4];
                                        }
                                        else{
                                                $getstr[0]=$coller[0].$coller[1].$coller[2];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[3].$coller[4];
                                        };
                                }
                                elsif(@coller==4){
                                        if($_=~/^\s/){
                                                $getstr[0]=$coller[0];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[1].$coller[2].$coller[3];
                                        }
                                        else{
                                                $getstr[0]=$coller[0].$coller[1].$coller[2];
						$getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[3];
                                        };					
                                }
                                elsif(@coller2==4){
                                        $getstr[1]=$coller2[0].$coller2[1].$coller2[2];
                                        $getstr[2]=$coller2[3];
                                }
                                elsif(@coller==7){
                                        $getstr[0]=$coller[0].$coller[1].$coller[2];
                                        $getstr[1]=$coller[3].$coller[4].$coller[5];
                                        $getstr[2]=$coller[6];
                                };

                                $listb[$getstr[0]]=$listb[$getstr[0]].$blanc.$getstr[1];

                                $listb[$getstr[1]]=$listb[$getstr[1]].$blanc.$getstr[0];

				$fonc[$getstr[0]]=$fonc[$getstr[0]].$blanc.$getstr[2].'-'.$atom[$getstr[1]].$blanc;
				$ifonc[$getstr[0]]=$ifonc[$getstr[0]].$blanc.$getstr[1].$blanc;
				$covfonc[$getstr[0]]=$covfonc[$getstr[0]].$blanc.$getstr[2];
				$coval[$getstr[0]]=$coval[$getstr[0]]+$getstr[2];
                                $nbcoval[$getstr[0]]=$nbcoval[$getstr[0]]+1;

				$fonc[$getstr[1]]=$fonc[$getstr[1]].$blanc.$getstr[2].'-'.$atom[$getstr[0]].$blanc;
				$ifonc[$getstr[1]]=$ifonc[$getstr[1]].$blanc.$getstr[0].$blanc;
				$covfonc[$getstr[1]]=$covfonc[$getstr[1]].$blanc.$getstr[2];
				$coval[$getstr[1]]=$coval[$getstr[1]]+$getstr[2];
                                $nbcoval[$getstr[1]]=$nbcoval[$getstr[1]]+1;

				$jg++;
			};
		};
		if ($compt == 4){
			$istratom=$getstr[0];
			$istrbond=$getstr[1];
			$longaold=$istratom;

			@coller=split(' *',$istratom);
			if(@coller>3 && @coller==6){
				$istratom=$coller[0].$coller[1].$coller[2];
				$istrbond=$coller[3].$coller[4].$coller[5];
			}
			elsif(@coller>3 && @coller==5){
				if($_=~/^\s/){
					$istratom=$coller[0].$coller[1];
					$istrbond=$coller[2].$coller[3].$coller[4];
				}
				else{
					$istratom=$coller[0].$coller[1].$coller[2];
					$istrbond=$coller[3].$coller[4];
				};
			};

		
			#$compt++;
		};
		if ($_=~/\$\$\$\$/){
			
			$flagnew=1;
			$flagmend=0;
			print "MOL $moli " if($debug);
			
			#Recherche des cycles
			@cyclef='';
			&cyclesdf;
				
			#ecrit le make_base.sdf
			&makebase;
			
			#print "$moli done\n";
		};
		
		if ($_=~/>/ && $_=~/<ID>/){
			@getmoli=split(' ',$_);
			$moli=$getmoli[2];
			$moli=~s/\(//;
			$moli=~s/\)//;
		};	
			
	};
	close(MOL);
	

print "SEE make_scaffold.sdf\n" if($printsca);
print "No available segmentation !\n" if($printsca==0);	
	
###################################################################################
###################################################################################

sub makebase{

	@hionis='';
	foreach $cyc (1..$istratom){
		$hionis[$cyc]=0;    # contient les atomes a garder
	};
	 	
	@lignebond=''; # contient les nouveaux numero atomes
        @accroch='';
        	
        # atomes a garder
	$longa=0;
	
	# atomes des cycles
	foreach $p (1..$istratom){
		foreach $cyc (0..@cyclef-1){
			$car=" ".$cyclef[$cyc]." ";
			if($car=~/ $p / && $hionis[$p] == 0){
				print "$p match $cyclef[$cyc] donc appartient a un cycle\n" if($debug);
				$hionis[$p]=1;
				$longa++;
				if($fonc[$p]=~/ 1-H /){
					@det=split(' ',$fonc[$p]);
					@det2=split(' ',$ifonc[$p]);
					foreach $k (0..@det-1){
			        		if($det[$k] =~/1-H/){
			        			$hionis[$det2[$k]]=1;
			        			$longa++;
			       	 		};
					};
				};
			};
		};
	};
	print "longa $longa \n" if($debug);
	if($debug){
	 	foreach $cyc (1..$istratom){
			print "hionis$cyc $hionis[$cyc] $correspond[$cyc] $accroch[$cyc]\n" if($debug);
		};
	};
	
	# atomes des links
	$suitelinker="";
	foreach $p (1..$istratom){
	#print "$p $atom[$p] $fonc[$p] [$hionis[$p]]\n";	
		if($hionis[$p] == 0){
		        print "$p \n" if($debug);
			$suite=" $p ";
			@fget=split(' ',$suite);
			$qs=0;
			$contactcycle=0;
			$contactdl=0;
			while($qs <= (@fget-1)){
				print "$qs $suite\n" if($debug);
		        	@gsuite=split(' ',$ifonc[$fget[$qs]]);
		        	foreach $s (0..@gsuite-1){
					#print "$gsuite[$s] $fonc[$gsuite[$s]]\n";
		        		if($hionis[$gsuite[$s]]==0 && $suite!~/ $gsuite[$s] /){
		        	  		$suite=$suite."$gsuite[$s] ";
		        	 	}
		        	 	elsif($hionis[$gsuite[$s]]==1){
						 $suitelinker=$suitelinker." $fget[$qs] " if($suitelinker!~/ $fget[$qs] /);
		        	 		 $contactcycle++;
		        	 		 #print "fonc $atom[$gsuite[$s]] : $fonc[$gsuite[$s]]\n";
						 $entrezici=0;
		        	 		 if($fonc[$gsuite[$s]] =~/ 2-/ || $fonc[$gsuite[$s]] =~/ 3-/ || $atom[$gsuite[$s]] eq "P" ){
		        	 		 	@det=split(' ',$fonc[$gsuite[$s]]);
						 	@det2=split(' ',$ifonc[$gsuite[$s]]);
							foreach $k (0..@det-1){
			        				if(($det[$k] =~/2-/ || $det[$k] =~/3-/ || ($det[$k] =~/1-/ && $atom[$gsuite[$s]] eq "P")) && $det2[$k] == $fget[$qs]){
			        				  	$contactdl=1;
									$entrezici=1;
			       	 				};
							};
						};
						if($fonc[$gsuite[$s]] =~/ 1-/ && $entrezici==0){
							@det=split(' ',$fonc[$gsuite[$s]]);
							@det2=split(' ',$ifonc[$gsuite[$s]]);
							foreach $k (0..@det-1){
			        				if($det[$k] =~/1-/  && $det2[$k] == $fget[$qs]){
			        					$accroch[$fget[$qs]]=$gsuite[$s];
			       	 				};
							};						
		    				};
		        		};
		        		
		        	};
		        	@fget=split(' ',$suite);
		        	$qs++;
		 	};
			print "contactdl = $contactdl ;  contactcycle = $contactcycle ; \n" if($debug);
		        if($contactdl){
		                if($contactcycle==1){
		        		foreach $s (0..@fget-1){
		        			$longa++  if($hionis[$fget[$s]] == 0);
	            				$hionis[$fget[$s]]=1 if($hionis[$fget[$s]] ==0);
	            			
					};
				}
				elsif($contactcycle>=2){
				     	foreach $s (0..@fget-1){
						@gsuite2=split(' ',$ifonc[$fget[$s]]);
						$longgsuite2=@gsuite2;
						$longa++ if($hionis[$fget[$s]] ==0);
	            				$hionis[$fget[$s]]=2;
					};	
				};
			print "ici longa=$longa \n" if($debug);	
			}
			elsif($contactcycle>=2){
			
				foreach $s (0..@fget-1){
					@gsuite2=split(' ',$ifonc[$fget[$s]]);
					$longgsuite2=@gsuite2;
					$longa++ if($hionis[$fget[$s]] ==0);
					$hionis[$fget[$s]]=2;
					
				};			
                           print "ici2 longa=$longa \n" if($debug);	
			};
		};
	};
	
	# remplace les substituant par 1 X
	print "longa $longa \n" if($debug);


# REMOVE extra substituent onto the linker
# ############################################

$removeextra=1;
if($removeextra){
	
	#print "suite contact cycle: $suitelinker\n";
	$suitededeux="";
	$oldsuitelinker=$suitelinker;
	$suitelinker="";
	@hioniscopy="";
	@hioniscopy=@hionis;
	foreach $cyc (1..$istratom){
		if($hioniscopy[$cyc]==2 && $oldsuitelinker=~/ $cyc /){
			$suitelinker=$suitelinker." $cyc ";
			#	print "atom $cyc contact cycle et linker\n";
		};	
		$suitededeux=$suitededeux." $cyc " if($hioniscopy[$cyc]==2);
	};
	#print "suitededeux: $suitededeux\n";
	#print "suitelinker $suitelinker\n";

	if($suitededeux ne ""){
		
	@fget=split(' ',$suitededeux);
	$s=0;
	#foreach $s (0..@fget-1){
	while ($s <= @fget-1){
		#print "s=$s\n";
		$contactcinq=-1;
		if($hioniscopy[$fget[$s]]!=5){
			@gsuite2=split(' ',$ifonc[$fget[$s]]);
			foreach $qs (0..@gsuite2-1){
				$contactcinq=$gsuite2[$qs] if($hioniscopy[$gsuite2[$qs]]==5);
			};	
		};	
		if(($contactcinq > -1 || $suitelinker=~/ $fget[$s] /) && $hioniscopy[$fget[$s]]!=5){
			$newhionis5=0;
			$suite=" $fget[$s] ";
			$psuite=" -1 ";
			$dejavusuite="";
			@gsuite=split(' ',$suite);
			#print "depart $fget[$s]\n";
			while($gsuite[0] ne ""){
				@gsuite2=split(' ',$ifonc[$gsuite[@gsuite-1]]);
				#print "\t$gsuite[@gsuite-1]: voisins @gsuite2\n"; 
				$puitec=@gsuite-1;
				#print "puitec $puitec\n";
				$continuesuite=0;
				$continuefin=0;
				$dejavusuite=$dejavusuite." $gsuite[@gsuite-1] ";
				foreach $qs (0..@gsuite2-1){
					#	print "test -$gsuite2[$qs]-\n";
					if($suitededeux=~/ $gsuite2[$qs] / && $suite!~/ $gsuite2[$qs] / && $dejavusuite!~/ $gsuite2[$qs] / && $gsuite2[$qs]!= $contactcinq){
						$suite=$suite." $gsuite2[$qs] ";
						#print "\t\tadd $gsuite2[$qs]\n";
					 	$psuite=$psuite." $puitec ";
						$continuesuite=1;
						if($suitelinker=~/ $gsuite2[$qs] / || $hioniscopy[$gsuite2[$qs]]==5){	
							$continuefin=1;
							last;
						};	
					};
				};
				#print "\t* $suite\n";
				#print "\t* $psuite\n";
				if($continuesuite==0){
					@fget2=split(' ',$suite);
				       	$fget2[@fget2-1]="";
					$suite=join(' ',@fget2);
					$suite=" ".$suite." ";
					@fget2=split(' ',$psuite);
					$fget2[@fget2-1]="";
					$psuite=join(' ',@fget2);
					$psuite=" ".$psuite." ";
					if($fget2[@fget2-2]==-1){# return to the initial point so end
						$continuefin=1 if($contactcinq==-1);
					};	
				};
				if($continuefin){
					@fget3=split(' ',$suite);
					@fget2=split(' ',$psuite);
					$suitefinal="";
					$suitefinalp="";
					foreach $qs3 (0..@fget3-1){
						if($suitefinalp!~/ $fget2[@fget3-1-$qs3] /){
							$suitefinal=$suitefinal." $fget3[@fget3-1-$qs3] ";
							$suitefinalp=$suitefinalp." $fget2[@fget3-1-$qs3] ";
							$hioniscopy[$fget3[@fget3-1-$qs3]]=5;
							$newhionis5=1;
						};	
					};
					#print "suite finale: $suitefinal\n";
					$psuite=" ";
					$suite=" ";
				};	
				@gsuite=split(' ',$suite);
				#print "\t- $suite\n";
				#print "\t- $psuite\n";
			};
		};
		
		$contactcinq2=-1;
		if($newhionis5){
		foreach $s2 (0..($s-1)){
			$contactcinq2=-1;
			if($hioniscopy[$fget[$s2]]!=5){
				@gsuite2=split(' ',$ifonc[$fget[$s2]]);
				foreach $qs (0..@gsuite2-1){
					if($hioniscopy[$gsuite2[$qs]]==5){
						$contactcinq2=$s2;
						last;
					};	
				};
			};
			if($contactcinq2 > -1){
				last;
			};	
		};	
		};
		if($contactcinq2 > -1){# go back to $s2 to take account for new hionis=5
			$s=$contactcinq2;
			#print "back indice $s\n";
		}
		else{
			$s++;
		};	
	};	
	
	
	# replace linker's substituents by X
	foreach $cyc (1..$istratom){
		#print "atom $cyc $hioniscopy[$cyc] $accroch[$cyc]\n";
		if($hioniscopy[$cyc]==5){
			@gsuite=split(' ',$ifonc[$cyc]);
			@gsuite2=split(' ',$fonc[$cyc]);
			foreach $s (0..@gsuite-1){
				if($hioniscopy[$gsuite[$s]]==2){
				       	if($gsuite2[$s]=~/1-/ && $atom[$gsuite[$s]] ne "H" && $atom[$cyc] ne "P"){
						$hioniscopy[$gsuite[$s]]=0;
						$accroch[$gsuite[$s]]=$cyc;
						$longa--;
						#print "\tatom $gsuite[$s] branche X -> $hioniscopy[$gsuite[$s]] $accroch[$gsuite[$s]]\n";
						# change hioniscopy of branch 2 into 0
						$suite=" $gsuite[$s] ";
						$qs=0;
						@fget=split(' ',$suite);
						while($qs <=@fget-1){
							@gsuite3=split(' ',$ifonc[$fget[$qs]]);
							foreach $qs2 (0..@gsuite3-1){
								if($hioniscopy[$gsuite3[$qs2]]==2){
									$hioniscopy[$gsuite3[$qs2]]=0;
									$longa--;
									#print "\tatom $gsuite3[$qs2] mis a 0\n";
									$suite=$suite." $gsuite3[$qs2] ";
								};
							};
							@fget=split(' ',$suite);	
							$qs++;
						};	
					}
					elsif($atom[$cyc] eq "P"){
						#print "$nbmol keep phosphore branch group\n";	
					}
					elsif($gsuite2[$s]=~/2-/ || $gsuite2[$s]=~/3-/){
						@gsuite3=split(' ',$ifonc[$gsuite[$s]]);
						@gsuite4=split(' ',$fonc[$gsuite[$s]]);
						if(@gsuite3 > 1){# ex: linker=N-H
							$endsuite=0;
							$nocc=0;
							$allH=1;
							foreach $qs2 (0..@gsuite3-1){
								$nocc++ if($hioniscopy[$gsuite3[$qs2]]==2 && ($gsuite4[$qs2]=~/2-/ || $gsuite4[$qs2]=~/3-/));
								$endsuite=1 if($hioniscopy[$gsuite3[$qs2]]==2 && @gsuite3==2 && $atom[$gsuite3[$qs2]] eq "H");# keep
								$allH=0 if($hioniscopy[$gsuite3[$qs2]]==2 && $atom[$gsuite3[$qs2]] ne 'H');
							};
							$endsuite=1 if($allH);
							if($endsuite==0	&& $nocc > 0){
								print "$nbmol keep a special double bonded branch of linker !\n";
							}
							elsif($endsuite==0){	
								# replace single bond by X
								$suite="";
								#print"atom $gsuite[$s]: @gsuite3\n";
								foreach $qs2 (0..@gsuite3-1){
									 if($hioniscopy[$gsuite3[$qs2]]==2 && $gsuite4[$qs2]=~/1-/ && $gsuite3[$qs2]!=$gsuite[$s]){
										 #print "\ttest atom $gsuite3[$qs2]\n";
										 $hioniscopy[$gsuite3[$qs2]]=0;
										 $accroch[$gsuite3[$qs2]]=$gsuite[$s];
										 $longa--;
										 $suite=$suite." $gsuite3[$qs2] ";
										 $qs=0;
										 @fget=split(' ',$suite);
										 while($qs <=@fget-1){
											 @gsuite5=split(' ',$ifonc[$fget[$qs]]);
											 foreach $qs3 (0..@gsuite5-1){
												 if($hioniscopy[$gsuite5[$qs3]]==2 && $gsuite5[$qs3]!=$gsuite[$s]){
													 $hioniscopy[$gsuite5[$qs3]]=0;
													 $longa--;
													 #print "\tatom $gsuite5[$qs3] mis a 0\n";
													 $suite=$suite." $gsuite5[$qs3] ";
												 };
											 };
											 @fget=split(' ',$suite);
											 $qs++;
										 };	 
									 }; 
								};       	
							};	
						};	
					};	
				};
			};	
			
		};
	};
	
	foreach $p (1..$istratom){
		$hioniscopy[$p]=$hionis[$p] if($hioniscopy[$p] ==5);
	};
	@hionis=@hioniscopy;

	};

};# removeextra

###############################################################################
	
	$ptsaccroch='';	
	@correspond='';
	$nbi=1;
	foreach $p (1..$istratom){
		if($hionis[$p] ==1 || $hionis[$p] ==2){
		   	$correspond[$p]=$nbi;
		   	$nbi++;
		}
		elsif($hionis[$p]==0 && $accroch[$p] ne '') {
			print "accroch $p \n" if($debug);
			
		        $hionis[$p]=3;
			$correspond[$p]=$nbi;
		 	if($ptsaccroch eq ''){
		  		$ptsaccroch=$ptsaccroch."$accroch[$p]";
		   	}
		   	else{
		   		$ptsaccroch=$ptsaccroch."-$accroch[$p]";
		   	};			
		   	$nbi++;
		   	$longa++;
		};
	};
	
	@getacc=split('-',$ptsaccroch);
	$ptsaccroch='';	
	foreach $p (0..@getacc-1){
		if($ptsaccroch eq ''){
			$ptsaccroch=$ptsaccroch."$correspond[$getacc[$p]]";
		}
		else{
			$ptsaccroch=$ptsaccroch."-$correspond[$getacc[$p]]";
		};	
	};
	
	$printx=0;
	#print "longa: $longa longaold: $longaold\n";	
 	print "longa $longa \n" if($debug);
	if($debug){
	foreach $cyc (1..$istratom){
		print "hionis$cyc $hionis[$cyc] $correspond[$cyc]\n";
	};
	};
		
        $niveaubond=$istratom+3;
        $niveaubondf=$istratom+3+$istrbond;

        print "bond deb $tabfilesdf[$niveaubond+1]" if($debug);
        print "bond fin $tabfilesdf[$niveaubondf]" if($debug);

        $longb=0;
        $z="  0  0  0  0";
	@pbxconnect=0;
	foreach $p ($niveaubond+1..$niveaubondf){
		@fget=split(' ',$tabfilesdf[$p]);
		print "$tabfilesdf[$p]\n" if($debug);
                                
				@coller=split(' *',$fget[0]);
                                @coller2=split(' *',$fget[1]);
                                if(@coller==6 && $fget[1] ne ""){
                                        $fget[0]=$coller[0].$coller[1].$coller[2];
                                        $fget[2]=$fget[1];
                                        $fget[1]=$coller[3].$coller[4].$coller[5];
                                }
                                elsif(@coller==6 && $fget[1] eq ""){
                                        $fget[0]=$coller[0].$coller[1];
                                        $fget[1]=$coller[2].$coller[3].$coller[4];
                                        $fget[2]=$coller[5];
                                }
                                elsif(@coller==5){
                                        if($tabfilesdf[$p]=~/^\s/){
                                                $fget[0]=$coller[0].$coller[1];
                                                $fget[2]=$fget[1];
                                                $fget[1]=$coller[2].$coller[3].$coller[4];
                                        }
                                        else{
                                                $fget[0]=$coller[0].$coller[1].$coller[2];
                                                $fget[2]=$fget[1];
                                                $fget[1]=$coller[3].$coller[4];
                                        };
                                }
                                elsif(@coller==4){
                                        if($tabfilesdf[$p]=~/^\s/){
                                                $fget[0]=$coller[0];
                                                $fget[2]=$fget[1];
                                                $fget[1]=$coller[1].$coller[2].$coller[3];
                                        }
                                        else{
                                                $fget[0]=$coller[0].$coller[1].$coller[2];
						$fget[2]=$fget[1];
                                                $fget[1]=$coller[3];
                                        };
					
                                }
                                elsif(@coller2==4){
                                        $fget[1]=$coller2[0].$coller2[1].$coller2[2];
                                        $fget[2]=$coller2[3];
                                }
                                elsif(@coller==7){
                                        $fget[0]=$coller[0].$coller[1].$coller[2];
                                        $fget[1]=$coller[3].$coller[4].$coller[5];
                                        $fget[2]=$coller[6];
                                };
	
                print "b1 = $fget[0] b2 =$fget[1] \n" if($debug);

		if($correspond[$fget[0]] ne '' && $correspond[$fget[1]] ne ''){	
			
			#print "$fget[0] -> bond with X \n" if($hionis[$fget[0]]==3);
			#print "$fget[1] -> bond with X \n" if($hionis[$fget[1]]==3);
			$pbxconnect[$fget[0]]++ if($hionis[$fget[0]]==3);	
			$pbxconnect[$fget[1]]++ if($hionis[$fget[1]]==3);
			$printx=1 if($hionis[$fget[0]]==3 || $hionis[$fget[1]]==3);
			
 			#$lignebond[$longb]=pack("A3A3A3A12",$correspond[$fget[0]],$correspond[$fget[1]],$fget[2],$z) if($debug);
 			
 			$lignebond[$longb]=sprintf "%3s%3s%3s$z",$correspond[$fget[0]],$correspond[$fget[1]],$fget[2],$z;
 			print "\t $lignebond[$longb]\n" if($debug);
			$longb++;
		};
	};

if($longa > 0 && ($longa < $longaold || ($longa==$longaold && $printx))){
	
	$printsca=1;
	open(OUG,">>make_scaffold.sdf");
	foreach $p (0..2){
		printf OUG "$tabfilesdf[$p]";
	};
	printf OUG "%3s%3s  0  0  0  0  0  0  0  0999 V2000\n",$longa,$longb;
							
	foreach $rt (4..$niveaubond){		
		$cup= $rt-3;
		print "n $cup $tabfilesdf[$rt]\n" if($debug);
		if($hionis[$cup]!=0){
			if($hionis[$cup]==3){
				@fget=split(' ',$tabfilesdf[$rt]);
				$fget[3]="X";
				printf OUG "%10s%10s%10s%1s%1s%1s%3s%3s%3s\n",$fget[0],$fget[1],$fget[2],$blanc,$fget[3],$blanc,$fget[4],$fget[5],$fget[6];

			}
			else{
				printf OUG "$tabfilesdf[$rt]";
			};
		};
		
	};			
	
	foreach $rt (0..@lignebond-1){			
    		printf OUG "$lignebond[$rt]\n";
       	};
       	
       	$ptsaccroch="null" if($ptsaccroch eq '');
       	
	foreach $rt ($niveaubondf+1..@tabfilesdf-1){
	
		if($tabfilesdf[$rt]=~/\$\$\$\$/ && $ptsaccroch ne ''){
			printf OUG "> <POINTS>\n";
			printf OUG "$ptsaccroch\n";
			printf OUG "\n";
			printf OUG "$tabfilesdf[$rt]";
		}
		else{
			printf OUG "$tabfilesdf[$rt]";
		};
	};       	
	close(OUG);
}
else{
	print "$moli : no scaffold!\n";

# not printed
# 
 	
	#$ptsaccroch="null";
	#print " aucun cycles -> pas de scaffold\n" if($debug);
	#print "$nbmol aucun cycles -> pas de scaffold\n";
	#open(OUG,">>make_scaffold.sdf");
	#foreach $p (0..@tabfilesdf-1){
#		if($tabfilesdf[$p]=~/\$\$\$\$/ && $ptsaccroch ne ''){
#			printf OUG "> <POINTS>\n";
#			printf OUG "$ptsaccroch\n";
#			printf OUG "\n";
#			printf OUG "$tabfilesdf[$p]";
#		}
#		else{
#			printf OUG "$tabfilesdf[$p]";
#		};		
#	};
#	close(OUG);

};
	 				   								   			
};


###################################################################################

