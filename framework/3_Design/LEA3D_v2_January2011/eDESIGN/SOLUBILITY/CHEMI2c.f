************************************************************************
      Program CHEMI2
************************************************************************
C     Partition Coefficients & Aqueous Solubility Estimation Program 
C     
C     CHEMICALC2
C
C                                Takahiro Suzuki
C                                Dept. of Chem. Engineering
C                                Tokyo Institute of Technology
C
C                                June 20, 1991


C     Description of Variables
C        ATYPE  ........ Atom Types ( C, N, O, etc.)
C        FTYPE  ........ Key Fragment for Estimating log 1/S
C        FRAGMT ........ Fragment(Key Fragment + A Atoms) for Estimating
C                        log 1/S
C        PFRAG  ........ Fragment for Printing
C        GROUP  ........ Atomic Groups ( CH3, CH2, OH-(C),etc.)
C        TGROUP ........ Atomic Groups in the log P Group Contribution 
C                        Table
C        NATOM  ........ Number of Heavy Atoms(C & Hetero) in a Molecule
C        NGROUP ........ Number of Groups for Estimating log P
C        ITF    ........ Total Frequency of Groups for Determining VLP
C        INC    ........ Number of Compounds Used for Determining VLP
C        VLP    ........ Group Contributions to log P
C        MATCT  ........ Matrix of Connection Table
C        TM     ........ Melting Point ( C or K )
C        LOGP   ........ Calculated log P Value
C        TFRAG  ........ Atomic Groups defined by W-Y-M-W Method
C        FS     ........ Contributions of TFRAG to log 1/S
C        IFRAG  ........ Atomic Groups defined by Irmann Method
C        YS     ........ Contributions of IFRAG to log 1/S
C     Correction Factors Used in W-Y-M-W Method for Estimating log 1/S
C        NRING  ........ Number of Aliphatic Rings in a Molecule
C        NBRN   ........ Number of Aliphatic Chain Branching
C        NHAL2  ........ Frequency of Dihalogens Attached to a Carbon
C        NHAL3  ........ Frequency of Trihalogens Attached to a Carbon
C        NHAL4  ........ Frequency of Tetrahalogens Attached to a Carbon
C     Correction Factors Used in Irmann Method for Estimating log 1/S
C        NBRN   ........ Ibid. for W-Y-M-W Method
C        NZHAL  ........ Frequency of Groups with H Besides Halogens on
C                        the Same Saturated C

      CHARACTER*5  ATYPE(100),FTYPE(100),FRAGMT(100)*20,PFRAG(100)*20,
     *             GROUP(100)*80,TGROUP(800)*56,TFRAG(60)*20,
     *             IFRAG(30)*10
      INTEGER      NATOM,NRING,NBRN,NHAL2,NHAL3,NHAL4,NZHAL,ITF(800),
     *             INC(800),NGROUP
      REAL         VLP(800),FS(60),YS(30),LOGP,TM
      COMMON /CTI/ NATOM,MATCT(100,100)
      COMMON /CTA/ ATYPE
      COMMON /GRN/ NGROUP
      COMMON /GRP/ GROUP
      COMMON /LOG/ LOGP
      COMMON /LG1/ TGROUP
      COMMON /LG2/ ITF,INC,VLP
      COMMON /LS1/ TFRAG,IFRAG
      COMMON /LS2/ FS,YS
      COMMON /TEM/ TM
      COMMON /NUM/ NUM1,NUM2,NUM3
      COMMON /FRG/ FTYPE,FRAGMT,PFRAG
      COMMON /COR/ NRING,NBRN,NHAL2,NHAL3,NHAL4,NZHAL

	OPEN(25,ACCESS='APPEND',file='CTL_LOGP_SUZUKI') 
      WRITE(25,*)  '***************************************************'
      WRITE(25,*)  '*                                                 *'
      WRITE(25,*)  '*                                                 *'
      WRITE(25,*)  '*            C H E M I C A L C - 2                *'
      WRITE(25,*)  '*                                                 *'
      WRITE(25,*)  '*                                                 *'
      WRITE(25,*)  '*      Log P & Log 1/S  Estimation Program        *'
      WRITE(25,*)  '*                                                 *'
      WRITE(25,*)  '*                                                 *'
      WRITE(25,*)  '*                                                 *'
      WRITE(25,*)  '*                 Ver. 1.0  (1991)                *'
      WRITE(25,*)  '*                                                 *'
      WRITE(25,*)  '***************************************************'
      WRITE(25,*)  'This version is capable of estimating both log Pand'
      WRITE(25,*)  'the aqueous solubility from the structure.         '
      WRITE(25,*)  'The compounds containing C, H, N, O, S, or halogen '
      WRITE(25,*)  'can be handled.                                    '
      CALL DATAIN
      WRITE(25,*)  '                                                   '
100   WRITE(25,*)  'Choose the mode for inputting structural formula.  '
      WRITE(25,*) ' MODE 1 => Input "1" '
      WRITE(25,*) ' MODE 2 =>       "2" '
      WRITE(25,*) ' MODE 3 =>       "3" '
      WRITE(25,*) '         No.  =>  ? '

c modif par Dom
c      READ(*,*,ERR=100) MN
	MN=2
      IF(MN.EQ.1)                CALL INPUT1
      IF(MN.EQ.2)                CALL INPUT2
      IF(MN.EQ.3)                CALL INPUT3
      IF(MN.LE.0 .OR. MN.GE.4)   GO TO 100
      CALL AUGTDA
      CALL PGROUP
      CALL CALLGP
200   WRITE(25,*) 'Do you want to calculate Aqueous Solubility ?'
      WRITE(25,*) 'Yes/No  IF Yes =>  Input "1" '
      WRITE(25,*) '           No  =>        "0" '
      WRITE(25,*) '                  No. =>  ? '

c Modif par Dom
c      READ(*,*,ERR=200) YN
	YN=1

      IF(YN.EQ.1)  THEN
         CALL SGROUP
         CALL CALLGS
         CALL CALLRS
      END IF
      CALL SOLREG
300   WRITE(25,*) 'Do you want to calculate next molecule ?'
      WRITE(25,*) 'Yes/No  IF Yes =>  Input "1" '
      WRITE(25,*) '           No  =>        "0" '
      WRITE(25,*) '                  No. =>  ? '
	CLOSE(25)
c Modif par Dom
c      READ(*,*,ERR=300) YN
	YN=0

      IF(YN.EQ.1)               GO TO 100
      IF(YN.NE.1.AND.YN.NE.0)   GO TO 300
400   STOP
      END
***********************************************************************
*      End of Main Routine                                            *
***********************************************************************
C-----------------------------------------------------------------------
       Subroutine AUGTDA
C-----------------------------------------------------------------------
C      Subroutine for Perception of Augmented Atoms

       Character*5 ATYPE(100)
       Common /CTI/NATOM,MATCT(100,100)
       Common /CTA/ATYPE

************************************************************************
*                                                                      *
*     - C # N       =>     " CN "       - N = C = S   =>     " NCS "   *
*                                                                      *
*                                          O                           *
*     - N = O       =>     " NO "         ||          =>     " SO "    *
*                                        - S -                         *
*                                                                      *
*     = N = O       =>     " NaO "         O                           *
*       |                                 ||                           *
*                                        - S -        =>     " SO2 "   *
*       O                                  ||                          *
*      ||                                  O                           *
*     - N = O       =>     " NO2 "                                     *
*                                                                      *
*       O                               - I = O       =>     " IO  "   *
*       ||          =>     " CO "                                      *
*     - C -                                                            *
*                                         O                            *
*       S                                 ||                           *
*       ||          =>     " CS "       - I = O       =>     " IO2 "   *
*     - C -                                                            *
*                                                                      *
************************************************************************

       DO 5000 I=1,NATOM

            IF(ATYPE(I).EQ.'N')    THEN
                 IO = 0
                 INa= 0
                 DO 1000 J=1,NATOM
                      IF(MATCT(I,J).EQ.2)   THEN
                           IF(ATYPE(J).NE.'O')   GO TO 1000
                           ATYPE(J)=' '
                           IO=IO+1
                      END IF
                      IF(MATCT(I,J).EQ.3)   THEN
                           IF(ATYPE(J).NE.'C')   GO TO 1000
                           ATYPE(J)='CN'
                           ATYPE(I)=' '
                      END IF
                      IF(MATCT(I,J).EQ.4)   INa=1
 1000            CONTINUE
                 IF(IO.EQ.1)   THEN
                      IF(INa.EQ.0)   ATYPE(I)='NO'
                      IF(INa.EQ.1)   ATYPE(I)='NaO'
                 END IF
                 IF(IO.EQ.2)       ATYPE(I)='NO2'
            END IF

            IF(ATYPE(I).EQ.'O')    THEN
                 DO 2000 J=1,NATOM
                      IF(MATCT(I,J).NE.2)  GO TO 2000
                      IF(ATYPE(J).NE.'C')  GO TO 2000
                      ATYPE(J)='CO'
                      ATYPE(I)=' '
 2000            CONTINUE 
            END IF

            IF(ATYPE(I).EQ.'S')    THEN
                 IO=0
                 DO 3000 J=1,NATOM
                      IF(MATCT(I,J).NE.2)   GO TO 3000
                      IF(ATYPE(J).EQ.'O')   THEN
                           ATYPE(J)=' '
                           IO=IO+1
                      END IF
                      IF(ATYPE(J).EQ.'C')   THEN
                           ATYPE(J)='CS'
                           ATYPE(I)=' '
                           DO 3500 K=1,NATOM
                                IF(K.EQ.I)            GO TO 3500
                                IF(MATCT(J,K).NE.2)   GO TO 3500
                                IF(ATYPE(K).NE.'N')   GO TO 3500
                                ATYPE(K)='NCS'
                                ATYPE(J)='  '
 3500                      CONTINUE
                      END IF
 3000            CONTINUE
                 IF(IO.EQ.1)   ATYPE(I)='SO '
                 IF(IO.EQ.2)   ATYPE(I)='SO2'
            END IF

            IF(ATYPE(I).EQ.'I')    THEN
                 IO=0
                 DO 4000 J=1,NATOM
                      IF(MATCT(I,J).NE.2)   GO TO 4000
                      IF(ATYPE(J).NE.'O')   GO TO 4000
                      ATYPE(J)=' '
                      IO=IO+1
 4000            CONTINUE
                 IF(IO.EQ.1)  ATYPE(I)='IO '
                 IF(IO.EQ.2)  ATYPE(I)='IO2'
            END IF

 5000  CONTINUE


************************************************************************
*                                                                      *
*       Modification of MATCT                                          *
*                                                                      *
*  (Ex)   C     0 1 0 0               C    0 1 0                       *
*         CO    1 0 2 1       =>      CO   1 0 1                       *
*               0 2 0 0               C    0 1 0                       *
*         C     0 1 0 0                                                *
*                                                                      *
************************************************************************

 6000  N=0
       DO 9000 I=1,NATOM
            IF(ATYPE(I).EQ.' ')   GO TO 9000
            N=N+1
            ATYPE(N)=ATYPE(I)
            IF(N.EQ.I)  GO TO 9000
            DO 7000 K=1,NATOM
 7000            MATCT(N,K)=MATCT(I,K)
            DO 8000 K=1,NATOM
 8000            MATCT(K,N)=MATCT(K,I)
 9000  CONTINUE
       NATOM=N

************************************************************************
*                                                                      *
*        " -C=, =C= "               =>     " Cd "                      *
*        "    -C#   "               =>     " Ct "                      *
*        " C in Aromatic Ring "     =>     " Ca "                      *
*        " Aromatic Fuzed C   "     =>     " Cf "                      *
*        "    -N=   "               =>     " Nd "                      *
*        " N in Aromatic Ring "     =>     " Na "                      *
*                                                                      *
************************************************************************

       DO 10000 I=1,NATOM
            IF(ATYPE(I).EQ.'N')   THEN
                 DO 9200 J=1,NATOM
                      IF(MATCT(I,J).EQ.2)  ATYPE(I)='Nd'
                      IF(MATCT(I,J).EQ.4)  ATYPE(I)='Na'
 9200            CONTINUE
            END IF
            IF(ATYPE(I).EQ.'C')   THEN
                 IA=0
                 DO 9400 J=1,NATOM
                      IF(MATCT(I,J).EQ.2)  ATYPE(I)='Cd'
                      IF(MATCT(I,J).EQ.3)  ATYPE(I)='Ct'
                      IF(MATCT(I,J).EQ.4)  IA=IA+1
 9400            CONTINUE
                 IF(IA.EQ.2)   ATYPE(I)='Ca'
                 IF(IA.EQ.3)   ATYPE(I)='Cf'
            END IF
10000  CONTINUE

       RETURN
       END
C-----------------------------------------------------------------------
      Subroutine CALLGP
C-----------------------------------------------------------------------
C     Subroutine for Calculating Log P(octanol/water) from the Structure

C     Description of Variables
C        ELNAME ...... Groups for Estimating log P Based on Atomic 
C                      Additivity
C        ALOGPV ...... log P Contributions of ELNAME
C        FLOGP  ...... Group log P Values
C        FITF   ...... = ITF
C        FINC   ...... = INC

      CHARACTER*5  ATYPE(100),ELNAME(40)
      CHARACTER*56 TGROUP(800)
      CHARACTER*80 GROUP(100)
      REAL    FLOGP(100),ALOGP,ALOGPV(40),LOGP,VLP(800)
      INTEGER FITF(100),FINC(100),ITF(800),INC(800)
      COMMON /CTI/ NATOM,MATCT(100,100)
*                  NATOM: Number of Groups
      COMMON /CTA/ ATYPE
      COMMON /GRN/ NGROUP
      COMMON /GRP/ GROUP
      COMMON /LOG/ LOGP
      COMMON /LG1/ TGROUP
      COMMON /LG2/ ITF,INC,VLP
      DATA(ELNAME(K),K=1,40)/' ','CH3','CH2','CH','C','CdH2','CdH','Cd',
     *                      'CtH','Ct','CaH','Ca','Cf','NH2','NH','N',
     *                      'NdH','Nd','Na','OH','O','SH','S','CHO',
     *                      'CO','CHS','CS','CN','NO','NO2','SO','SO2',
     *                      'NCS','F','Cl','Br','I','NaO','IO','IO2'/
      DATA(ALOGPV(K),K=1,40)/ 0.0,   0.408, 0.368, 0.328, 0.288, 0.368,
     *                        0.328, 0.288, 0.328, 0.288, 0.328, 0.288,
     *                        0.288,-0.443,-0.483,-0.523,-0.483,-0.523,
     *                       -0.523,-0.287,-0.327, 0.119, 0.079, 0.001,
     *                       -0.039, 0.407, 0.367,-0.235,-0.850,-1.177,
     *                       -0.248,-0.575,-0.156, 0.144, 0.471, 0.796,
     *                        1.285,-0.850, 0.958, 0.631/

      DO 9000 I=1,NGROUP
         FLOGP(I)=0.0
         IF(I.EQ.1)    GO TO 2000
*----------------------------------------------------------------------
         DO 1000 M=1,I-1
            IF(GROUP(I).EQ.GROUP(M))    THEN
               FLOGP(I)=FLOGP(M)
               FITF (I)=FITF (M)
               FINC (I)=FINC (M)
               GO TO 9000
            END IF
1000     CONTINUE
*----------------------------------------------------------------------
2000     KK=0
         DO 3000 K=1,797
            IF(GROUP(I)(1:32).NE.TGROUP(K)(1:32))      GO TO 3000
            IF(TGROUP(K)(33:33).EQ.' ')    THEN
               KK=K
               IF(GROUP(I)(33:33).EQ.' ')      GO TO 4000
            END IF
            IF(GROUP(I)(33:56).NE.TGROUP(K)(33:56))    GO TO 3000
            KK=K
            GO TO 4000
3000     CONTINUE
         IF(KK.EQ.0)  GO TO 5000
4000     FLOGP(I)=VLP(KK)
         FITF (I)=ITF(KK)
         FINC (I)=INC(KK)
         GO TO 9000
*-----------------------------------------------------------------------
5000     ALOGP=0.
         DO 6000 J=1,13,4
            DO 5500 K=1,40
               IF(GROUP(I)(J:J+3).EQ.ELNAME(K))   THEN
                  ALOGP=ALOGP+ALOGPV(K)
                  GO TO 6000
               END IF
5500        CONTINUE
6000     CONTINUE
         FLOGP(I)=ALOGP
         FITF(I)=0
         FINC(I)=0
9000  CONTINUE

C     -----  for CCl4, CCl3Br, etc -------------------------------------
      IF(NGROUP.EQ.1) THEN
         DO 9500 K=1,40
            IF(GROUP(1)(17:20).EQ.ELNAME(K))   THEN
               FLOGP(1)=ALOGP+ALOGPV(K)
               END IF
9500     CONTINUE
      END IF
************************************************************************
*                                                                      *
*       Estimated log P Value                                          *
*                                                                      *
************************************************************************
      WRITE(25,*)'-----------------------------------------------------'
      WRITE(25,*)'         Contribution  Frequency  Compounds'
      DO 6500 I=1,NGROUP
         IF(FITF(I).EQ.0)  THEN
            WRITE(25,6450) I, FLOGP(I)
 6450       FORMAT(' Gi(',I2,') = ',F7.3,12X,'-',10X,'-')
            GO TO 6500
         END IF
         WRITE(25,6300) I, FLOGP(I),FITF(I),FINC(I)
 6300    FORMAT(' Gi(',I2,') = ',F7.3, 8X,I5,6X,I5)
 6500 CONTINUE

      LOGP=0.0
      DO 7000 I=1,NGROUP
         LOGP = LOGP + FLOGP(I)
 7000 CONTINUE
      WRITE(25,*)'                                 '
      WRITE(25,*)' ****  Estimated log P Value *****'
      WRITE(25,8000) LOGP
 8000 FORMAT('     log P(octanol/water)= ',F7.3)
      WRITE(25,*)' *********************************'

      RETURN
      END
C-----------------------------------------------------------------------
      Subroutine CALLGS
C-----------------------------------------------------------------------
C     Subroutine for Calculating Log 1/S from the Structure

C     Description of Variables
C        ALOGS ..... Atomic contributions to log 1/S
C        TFRAG ..... Fragments Used in W-Y-M-W Method
C        FS    ..... log 1/S Contributions of TFRAG
C        PFS   ..... = FS (for printing)
C        VRING ..... Values of NRING
C        VBRN  ..... Values of NBRN
C        VHAL2 ..... Values of NHAL2
C        VHAL3 ..... Values of NHAL3
C        VHAL4 ..... Values of NHAL4
C        IFRAG ..... Fragments Used in Irmann Method
C        YS    ..... log 1/S Contributions of IFRAG
C        PYS   ..... = YS (for printing)
C        CTM   ..... = 0.0095(TM-25)
C        VZHAL ..... Values of ZHAL
C        MW    ..... Molecular weight
C        ITYPE ..... Available Atom Types for Irmann Method
C        MTYPE ..... Fragment Types for Calculating Molecular Weight
C        VMW   ..... Values of MTYPE
C        LOGS1 ..... log 1/S Calculated Based on W-Y-M-W Method
C        S1    ..... Unit in [mol/L]
C        LOGS2 ..... log 1/S Calculated Based on Irmann Method
C        S2    ..... Unit in [mol/L]
C        LOGS3 ..... log 1/S calculated based on atomic additive scheme
C        AIND  ..... Indication Variables for Denoting Aromatic Carbon
C                    Atoms Present or Not
C        HIND  ..... Variables for Denoting Presence of Hydrogen Atoms
C        FIND  ..... Variables for Denoting Presence of F Atoms
C        XIND  ..... Variables for Denoting Presence of Halogens
C        CIND  ..... Variables for Denoting Presence of Cd or Ct Atoms
C        DIND  ..... Variables for Denoting Presence of X-(Cd) or X-(Ct)

      CHARACTER  FRAGMT(100)*20,TFRAG(60)*20,PFRAG(100)*20,IFRAG(30)*10,
     *           FTYPE(100)*5,ATYPE(100)*5,ITYPE(10)*5,MTYPE(50)*5
      INTEGER    NRING,NBRN,NHAL2,NHAL3,NHAL4,NZHAL,AIND,HIND,FIND,
     *           CIND,XIND,DIND
      REAL       VRING,VBRN,VHAL2,VHAL3,VHAL4,LOGS1,FS(60),LOGS2,YS(30),
     *           VZHAL,TYPE,MW,VMW(50),TM,ALOGS(40),LOGS3,PFS(100),
     *           PYS(100)
      COMMON /CTI/ NATOM,MATCT(100,100)
      COMMON /CTA/ ATYPE
      COMMON /GRP/ GROUP
      COMMON /TEM/ TM
      COMMON /LS1/ TFRAG,IFRAG
      COMMON /LS2/ FS,YS
      COMMON /FRG/ FTYPE,FRAGMT,PFRAG
      COMMON /COR/ NRING,NBRN,NHAL2,NHAL3,NHAL4,NZHAL
      DATA(ITYPE(I),I=1,9)/'C','Cd','Ct','Ca','Cf','F','Cl','Br','I'/
*    ------  Molecular Weight and Atomic Contributions to log 1/S  -----
      DATA(MTYPE(K),K=1,40)/'CH3','CH2','CH','C','CdH2','CdH','Cd',
     * 'CtH','Ct','CaH','Ca','Cf','F','Cl','Br','I','NH2','NH','N',
     * 'NdH','Nd','Na','OH','O', 'SH','S','CHO','CO','CHS','CS','CN',
     * 'NO','NO2','SO','SO2','NCS','CH2OH','NaO','IO','IO2'/
      DATA(VMW(K),K=1,40) / 15.034,  14.026,  13.018,  12.011,  14.026,
     *  13.018,  12.011,  13.018,  12.011,  13.018,  12.011,  12.011,
     *  18.998,  35.453,  79.904, 126.904,  16.022,  15.014,  14.006,
     *  15.014,  14.006,  14.006,  17.007,  15.999,  31.060,  30.060,
     *  29.018,  28.010,  43.078,  42.070,  26.017,  30.006,  46.005,
     *  46.059,  62.058,  56.077,  31.034,  30.006, 142.903, 158.903/
      DATA(ALOGS(K),K=1,40)/ 0.40, 0.37, 0.34, 0.31, 0.37,
     *  0.34, 0.31, 0.34, 0.31, 0.34, 0.31, 0.31, 0.16, 0.55, 0.65,
     *  1.00,-0.15,-0.18,-0.21,-0.18,-0.21,-0.21,-0.56,-0.59,-0.27,
     * -0.30,-0.25,-0.28, 0.04, 0.01, 0.10,-0.80,-1.39,-0.89,-1.48,
     * -0.20,-0.19,-0.80, 0.41,-0.18/

*    ------ Correction factors used in W-Y-M-W method ------------------
      DATA VRING/-0.40/,VBRN/-0.10/,VHAL2/0.67/,VHAL3/1.64/,VHAL4/2.60/

*    ------ Correction factors used in Irmann Method -------------------
      DATA VZHAL/ -0.30 /

************************************************************************
*                                                                      *
*     Calculation of Molecular Weight                                  *
*                                                                      *
************************************************************************
      MW = 0.0
      DO 500 I=1,NATOM
         DO 300 J=1,40
            IF(FTYPE(I).EQ.MTYPE(J))  THEN
               MW = MW + VMW(J)
               GO TO 500
            END IF
300      CONTINUE
500   CONTINUE

************************************************************************
*                                                                      *
*     Perception of the compound type                                  *
*                                                                      *
************************************************************************

      DO 600 I=1,NATOM
         DO 600 J=17,40
            IF(FTYPE(I).EQ.MTYPE(J))    GO TO  900
600   CONTINUE
      DO 620 I=1,NATOM
         DO 620 J=13,16
            IF(FTYPE(I).EQ.MTYPE(J))    GO TO 3010
620   CONTINUE
      DO 640 I=1,NATOM
         DO 640 J=10,12
            IF(FTYPE(I).EQ.MTYPE(J))    GO TO 3010
640   CONTINUE

************************************************************************
*                                                                      *
*     W-Y-M-W Method        - log S          S : mol/l                 *
*                                                                      *
************************************************************************
900   WRITE(25,*)'                                '
      WRITE(25,*)' < W-Y-M-W Method >             '

      LOGS1=0.0
      IC   = 0
      DO 2000 I=1,NATOM
         DO 1000 J=1,60
            IF(FRAGMT(I).EQ.TFRAG(J))   THEN
               LOGS1 = LOGS1 + FS(J)
               PFS(I)=FS(J)
               GO TO 2000
            END IF
1000     CONTINUE
         DO 1300 J=I-1,1,-1
            IF(PFRAG(I).EQ.PFRAG(J))  GO TO 2000
1300     CONTINUE
         WRITE(25,1500) PFRAG(I)
1500     FORMAT(1H , '   Value is unavailable for Group ',A20)
         IC = 1
2000  CONTINUE
      IF(IC.EQ.0)   THEN
         LOGS1 = LOGS1 + NRING*VRING + NBRN*VBRN + NHAL2*VHAL2 
     *       + NHAL3*VHAL3 + NHAL4*VHAL4 + 0.0095*(TM-25.0)
         WRITE(25,*)'                            Contribution(mol/L)'
         DO 2200 I=1,NATOM
            WRITE(25,2100) I, PFRAG(I), PFS(I)
2100        FORMAT(1H , 'GROUP(',I2,') = ', A20, F7.2)
2200     CONTINUE
         IF(NRING.NE.0)  THEN
            WRITE(25,2250)  NRING,VRING
2250        FORMAT(1H ,'Aliphatic ring ',15X,I2,'(',F5.2,')')
         END IF
         IF(NBRN.NE.0)  THEN
            WRITE(25,2300)  NBRN,VBRN
2300        FORMAT(1H ,'Aliphatic branch',15X,I2,'(',F5.2,')')
         END IF
         IF(NHAL2.NE.0) THEN
            WRITE(25,2350)  NHAL2,VHAL2
2350        FORMAT(1H ,'2-Hal on carbon ',15x,I2,'(',F5.2,')')
         END IF
         IF(NHAL3.NE.0) THEN
            WRITE(25,2400)  NHAL3,VHAL3
2400        FORMAT(1H ,'3-Hal on carbon ',15X,I2,'(',F5.2,')')
         END IF
         IF(NHAL4.NE.0) THEN
            WRITE(25,2450)  NHAL4,VHAL4
2450        FORMAT(1H ,'4-Hal on carbon ',15X,I2,'(',F5.2,')')
         END IF
         IF(TM.NE.25.0)  THEN
            CTM=0.0095*(TM-25.0)
            WRITE(25,2470)  CTM
2470        FORMAT(1H ,'Melting point correction',10X, F5.2)
         END IF
         WRITE(25,*)'                                      '
         WRITE(25,*)' *** Estimated log 1/S Value ********* '
         WRITE(25,2500) LOGS1
2500     FORMAT('   log 1/S = ', F6.2, '    (S: mol/L)')
         WRITE(25,*)' ************************************* '
         GO TO 9000
      END IF
      WRITE(25,*)'  '
      WRITE(25,*)' This compound cannot be estimated by this method.'
      WRITE(25,*)'  '
      GO TO 8000
************************************************************************
*                                                                      *
*     Irmann Method         - log S         S : g/g-H2O                *
*                                                                      *
************************************************************************

3010  WRITE(25,*) '                   '
      WRITE(25,*) '  < Irmann Method > '
      AIND=0
      HIND=0
      FIND=0
      XIND=0
      CIND=0
      DIND=0
      IC  =0
      DO 3400 I=1,NATOM
         DO 3050 J=1,9
            IF(ATYPE(I).EQ.ITYPE(J))   GO TO 3100
3050     CONTINUE
         GO TO 7000
3100     IF(J.GE.6)   THEN
            XIND=1
            IF(J.EQ.6)  FIND=1
            IF((FRAGMT(I)(6:7).EQ.'Cd').OR.
     *                       (FRAGMT(I)(6:7).EQ.'Ct'))  DIND=1
            GO TO 3400
         END IF
         HIND=INDEX(FTYPE(I),'H')+HIND
         IF(J.EQ.2.OR.J.EQ.3)   CIND=1
         IF(J.EQ.4)             AIND=1
3400  CONTINUE

      LOGS2 = 0.0
      DO 4000 I=1,NATOM
         DO 3500 J=1,26
            IF(FRAGMT(I)(:10).EQ.IFRAG(J))  THEN
               LOGS2 = LOGS2 + YS(J)
               PYS(I)=YS(J)
               GO TO 4000
            END IF
3500     CONTINUE
         DO 3600 J=I-1,1,-1
            IF(PFRAG(I).EQ.PFRAG(J))  GO TO 4000
3600     CONTINUE
         WRITE(25,3800) PFRAG(I)
3800     FORMAT(1H ,'    Value is unavailable for GROUP ', A20)
         IC = 1
4000  CONTINUE

      IF(IC.EQ.0)  THEN

C     ----- Determine the Compound Type and its Contribution -----------
         IF(AIND.EQ.1)   THEN
            TYPE = 0.50
            WRITE(25,*)'Basic compound type is aromaic;'
            WRITE(25,*)'                          Contribution(g/g-H2O)'
            WRITE(25,4100) TYPE
4100        FORMAT(1H , 'Basic value', 21X, F7.2)
            GO TO 5000
         END IF
         IF(XIND.EQ.0)   THEN
            TYPE = 1.50
            WRITE(25,*)'Basic compound type is aliphatic hydrocarbon;'
            WRITE(25,*)'                          Contribution(g/g-H2O)'
            WRITE(25,4200) TYPE
4200        FORMAT(1H , 'Basic value', 21X, F7.2)
            GO TO 5000
         END IF
         IF(CIND.EQ.0)   THEN
            IF(HIND.EQ.0)   THEN
            TYPE = 1.25
            WRITE(25,*)'Basic compound type is saturated perhalogenated'
            WRITE(25,*)'derivative(no H);'
            WRITE(25,*)'                          Contribution(g/g-H2O)'
            WRITE(25,4300) TYPE
4300        FORMAT(1H , 'Basic value', 21X, F7.2)
            GO TO 5000
            END IF
            IF(FIND.EQ.1)   THEN
            TYPE = 0.50
            WRITE(25,*)'Basic compound type is saturated aliphatic'
            WRITE(25,*)'halogen derivative, containing H besides F;'
            WRITE(25,*)'                          Contribution(g/g-H2O)'
            WRITE(25,4400)  TYPE
4400        FORMAT(1H , 'Basic value', 21X, F7.2)
            GO TO 5000
            END IF
            TYPE = 0.90
            WRITE(25,*)'Basic compound type is saturated aliphatic'
            WRITE(25,*)'halogen derivative(no F);'
            WRITE(25,*)'                          Contribution(g/g-H2O)'
            WRITE(25,4500)  TYPE
4500        FORMAT(1H , 'Basic value', 21X, F7.2)
            GO TO 5000
         END IF
         IF((DIND.EQ.1).AND.(HIND.NE.0).AND.(FIND.EQ.0))  THEN
            TYPE = 0.50
            WRITE(25,*)'Basic compound type is unsaturated aliphatic'
            WRITE(25,*)'halogen  derivative,  with  halogen  on  the'
            WRITE(25,*)'unsaturated C as well as with H(no F);'
            WRITE(25,*)'                          Contribution(g/g-H2O)'
            WRITE(25,4600) TYPE
4600        FORMAT(1H , 'Basic value',21X, F7.2)
            GO TO 5000
         END IF
         GO TO 7000
C     ------------------------------------------------------------------
5000     LOGS2=TYPE + LOGS2 + NBRN*VBRN + NZHAL*VZHAL + 0.0095*(TM-25.0)
         S2 = (10**(-LOGS2))*1000.0/MW
         LOGS2 = -LOG10(S2)
         DO 5200 I=1,NATOM
            WRITE(25,5100) I,PFRAG(I),PYS(I)
5100        FORMAT(1H ,'GROUP(',I2,') = ',A20, F7.2)
5200     CONTINUE 
         IF(NBRN.NE.0)  THEN
            WRITE(25,5300)  NBRN,VBRN
5300        FORMAT(1H ,'Aliphatic branch',15X,I2,'(',F5.2,')')
         END IF
         IF(NZHAL.NE.0) THEN
            WRITE(25,*) 'Group with H and halogen on same saturated C'
            WRITE(25,5400) NZHAL, VZHAL
5400        FORMAT(1H ,31X,I2,'(',F5.2,')')
         END IF
         IF(TM.NE.25.0)  THEN
            CTM=0.0095*(TM-25.0)
            WRITE(25,5450) CTM
5450        FORMAT(1H , 'Melting point correction',10X,F5.2)
         END IF
         WRITE(25,*)'                                      '
         WRITE(25,*)' *** Estimated log 1/S Value ********* '
         WRITE(25,5560) LOGS2
5560     FORMAT('   log 1/S = ', F6.2, '    (S: mol/L) ')
         WRITE(25,*)' ************************************* '
         GO TO 9000
      END IF
7000  WRITE(25,*)'                                                     '
      WRITE(25,*)' This compound cannot be estimated by this method.'
      WRITE(25,*)'                                                     '
      GO TO 900
************************************************************************
*                                                                      *
*     Atomic additive scheme                                           *
*                                                                      *
************************************************************************
8000  LOGS3=0.0
      DO 8400 I=1,NATOM
         DO 8200 J=1,40
            IF(FTYPE(I).EQ.MTYPE(J))   THEN
               LOGS3 = LOGS3 + ALOGS(J)
               GO TO 8400
            END IF
8200     CONTINUE
8400  CONTINUE
      LOGS3 = LOGS3 + 0.0095*(TM-25.0)
      WRITE(25,*) '                    '
      WRITE(25,*) ' < Atomic additive scheme > '
      WRITE(25,*) '                           '
      WRITE(25,*) ' *** Estimated log 1/S Value ********* '
      WRITE(25,8600) LOGS3
8600  FORMAT('   log 1/S = ', F6.2, '  (S: mol/L)')
      WRITE(25,*) ' ************************************* '

9000  RETURN
      END
C-----------------------------------------------------------------------
      Subroutine CALLRS
C-----------------------------------------------------------------------
C     Subroutine for Calculating Log 1/S from Log P

C     Description of Variables  ----------------------------------------
C        OGRP ..... Original groups in the compounds used to obtain
C                   log S - log P regression equation
C        S    ..... Solubility in Water [mol/L]

C     log P - log 1/S Regression Equations Used in This Program --------
C
C        log 1/S = 1.050 log P + 0.00956(TM-25.0) - 0.515   [S:mol/L]
C
C            ( n = 497, r = 0.976, s = 0.505 )

C            ( Regression Range: -1.05<=log P<=8.85)
C
C       [Ref.] Suzuki,T., J. Comput.-Aided Mol. Design, 5(1991)149.


      CHARACTER  FTYPE(100)*5,FRAGMT(100)*20,PFRAG(100)*20,OGRP1(50)*5,
     *           OGRP(50)*5
      REAL       LOGP,LOGS,TM
      COMMON /CTI/ NATOM,MATCT(100,100)
      COMMON /FRG/ FTYPE,FRAGMT,PFRAG
      COMMON /NUM/ NUM1,NUM2,NUM3
      COMMON /LOG/ LOGP
      COMMON /TEM/ TM
      DATA(OGRP(I),I=1,28)/'CH3','CH2','CH','C','CdH2','CdH','Cd','CtH',
     *                    'Ct','CaH','Ca','Cf','F','Cl','Br','I','NH2',
     *                    'NH','CN','NO2','CHO','CO','O','OH','SH','S',
     *                    'SO2','CH2OH'/

      WRITE(25,*) '                              '
      WRITE(25,*)' < Estimation of S from Log P > '

C     --------   Applicability Check  ----------------------------------
      DO 2000 I=1,NATOM
         DO 1000 J=1,28
            IF(FTYPE(I).EQ.OGRP(J))   GO TO 2000
1000     CONTINUE
            WRITE(25,*)'Warning !'
            WRITE(25,*)'This compound type was not included in the origin
     *al data set.'
            GO TO 3000
2000  CONTINUE
3000  IF(LOGP.LT.-1.05.OR.LOGP.GT.8.85)  THEN
         WRITE(25,*) 'Warning !'
         WRITE(25,*) 'Extrapolation. Out of range of log P value.'
      END IF
C     ------------------------------------------------------------------

      IF(TM.EQ.0)    TM=25.0
      LOGS = 1.050*LOGP + 0.00956*(TM-25.0) - 0.515

      WRITE(25,*)' *** Estimated log 1/S value using Reg. Eq. *****'
      WRITE(25,4000) LOGS
4000  FORMAT('   log 1/S = ', F6.2, '    (S: mol/L) ')
      WRITE(25,*)' ************************************************'
      WRITE(25,*)'                                           '

      RETURN
      END
*----------------------------------------------------------------------
      Subroutine DATAIN  
*----------------------------------------------------------------------
C     Subroutine for Reading Group Values of Log P and Log 1/S

      CHARACTER     TGROUP(800)*56,TFRAG(60)*20,IFRAG(30)*10
      REAL          VLP(800),FS(60),YS(30)
      INTEGER       ITF(800),INC(800)
      COMMON /LG1/  TGROUP
      COMMON /LG2/  ITF,INC,VLP
      COMMON /LS1/  TFRAG,IFRAG
      COMMON /LS2/  FS,YS

      OPEN(10,FILE='LOGPF.DAT',STATUS='OLD')
         DO 200 J=1,797
            READ(10,100,END=300) TGROUP(J),VLP(J),ITF(J),INC(J)
100         FORMAT(A56,F7.3,2I5)
200      CONTINUE
300   CLOSE(10)

      OPEN(11,FILE='LOGS1.DAT',STATUS='OLD')
         DO 500 J=1,60
            READ(11,400,END=600) TFRAG(J),FS(J)
400         FORMAT(A20,F7.3)
500      CONTINUE
600   CLOSE(11)

      OPEN(12,FILE='LOGS2.DAT',STATUS='OLD')
         DO 800 J=1,26
            READ(12,700,END=900) IFRAG(J),YS(J)
700         FORMAT(A10,F7.3)
800      CONTINUE
900   CLOSE(12)

      RETURN
      END
C-----------------------------------------------------------------------
      Subroutine INPUT1
C-----------------------------------------------------------------------
C     Subroutine for Inputting Structural Formula by the Method 1

C     Description of Variables
C        ELTYPE ...... Atom Types for Atom Checking
C        NBONDS ...... Number of Bonds
C        IFA    ...... Starting Atom No.of Bonds
C        ITA    ...... Terminal Atom No.of Bonds
C        NBTYPE ...... Bond Types (1:-, 2:=, 3:#, 4:*(aromatic))
C        NATM   ...... Variables for Checking Structural Formula
C        SBOND  ...... ibid.

      CHARACTER*5  ATYPE(100),ELTYPE(16),BOND(100)*1, SBOND(4)*1
      INTEGER      NBONDS,IFA(100),ITA(100),NBTYPE(100)
      DIMENSION    NATM(100)
      COMMON /CTI/ NATOM,MATCT(100,100)
      COMMON /CTA/ ATYPE
      DATA SBOND /'-','=','#','*'/
      DATA ELTYPE/'F','Cl','Br','I','CN','NCS','NO','NO2',
     *           'CO','CS','SO2','SO','O','C','N','S'/ 

 5    WRITE(25,*) 'Input Total Number of Atoms in Molecule = '
      READ (*,*,ERR=5) NATOM
      DO 10 I=1, NATOM
         DO 10 J=1,NATOM
            MATCT(I,J)=0
 10   CONTINUE
      WRITE(25,*) 'Input Atom Types - C,O,N,S,F,Cl,Br,I '
      WRITE(25,*) '                or CN,CO,CS,NO,NO2,NCS,SO,SO2 '
      DO 30 I=1,NATOM
 20      WRITE(25,22) I
 22      FORMAT(1H ,'No.',I2,'= ')
         READ (*,25,ERR=20) ATYPE(I)
 25      FORMAT(A4)
*        -------  Atom Check  ----------------------------------------
         DO 28 K=1,16
            IF(ATYPE(I).EQ.ELTYPE(K))    GO TO 30
 28      CONTINUE
         WRITE(25,*) 'Input Correct Atom Type.'
         GO TO 20
*        -------------------------------------------------------------
 30   CONTINUE
 32   WRITE(25,*) 'Input No. of Bonds =' 
      READ(*,*,ERR=32) NBONDS
      WRITE(25,*)'Input Bond Information by Following Format !'
      WRITE(25,*)'Bond Type:  1= Single'
      WRITE(25,*)'            2= Double'
      WRITE(25,*)'            3= Triple'
      WRITE(25,*)'            4= Aromatic'
      WRITE(25,*)'        [From Atom No.] , [To Atom No.] , [Bond Type]'
      DO 40 I=1, NBONDS
 33      WRITE(25,35) I
 35      FORMAT(1H , 'Bond No.',I2,'=')
         READ(*,*,ERR=33) IFA(I), ITA(I), NBTYPE(I)
         IF((IFA(I).GT.NATOM).OR.(ITA(I).GT.NATOM).OR.
     *   (NBTYPE(I).GE.5))    THEN
            WRITE(25,*) 'Bad Bond Information.'
            GO TO 33
         END IF
 40   CONTINUE
      DO 110 I=1,NBONDS
         MATCT(IFA(I),ITA(I))=NBTYPE(I)
         MATCT(ITA(I),IFA(I))=NBTYPE(I)
110   CONTINUE
200   WRITE(25,205) (K,K=1,NATOM)
205   FORMAT(1H ,' < Structural Formula > '/ ' ',100I3)
      WRITE(25,210) (ATYPE(K),K=1,NATOM)
210   FORMAT(1H ,2X,100A3)
      DO 390 J=1,NATOM
310      LPAR=0
         NBOND=0
         K=J
         GO TO 370
320      IF(LPAR.EQ.0)   NTOP=K
         NBOND=NBOND+1
         BOND(NBOND)=SBOND(MATCT(K,I))
         NATM(NBOND)=I
         MATCT(K,I)=-MATCT(K,I)
         MATCT(I,K)=-MATCT(I,K)
         LPAR=1
         K1=K
         K=I
370      DO 380 I=1,NATOM
            IF(I.EQ.K1)          GO TO 380
380         IF(MATCT(K,I).GT.0)  GO TO 320
         IF(LPAR.EQ.1)   THEN
            WRITE(25,385) NTOP,(BOND(K),NATM(K),K=1,NBOND)
385         FORMAT(1H0,1X,I2,20(A,I2))
            GO TO 310
         END IF
390   CONTINUE
      DO 400 J=1,NATOM
         DO 400 I=1,NATOM
400   MATCT(J,I)=-MATCT(J,I)
*     ------------  Bond Check  ----------------------------------------
      DO 445 I=1,NATOM
         NB=0
	 
         DO 405 J=1,NATOM
405         IF(MATCT(I,J).NE.0)   NB=NB+MATCT(I,J)
	 
         DO 408 K=1,16
            IF(ATYPE(I).EQ.ELTYPE(K))   THEN
		
               GO TO(410,410,410,410,410,410,410,410,418,418,
     *                   415,415,418,420,425,430), K
               END IF
408      CONTINUE
410      IF(NB.NE.1)  THEN
            GO TO 435
         ELSE
            GO TO 445
         END IF
415      IF(NB.NE.2)  THEN
            GO TO 435
         ELSE
            GO TO 445
         END IF
418      IF(NB.GT.2)  THEN
            GO TO 435
         ELSE
            GO TO 445
         END IF
420      IF((NB.GT.4).AND.(NB.NE.8).AND.(NB.NE.9).AND.(NB.NE.12)) THEN
            GO TO 435
         ELSE
            GO TO 445
         END IF
425      IF((NB.GT.3).AND.(NB.NE.5).AND.(NB.NE.8).AND.(NB.NE.10)) THEN
            GO TO 435
         ELSE 
            GO TO 445
         END IF
430      IF((NB.GT.2).AND.(NB.NE.4).AND.(NB.NE.6 ))  THEN
            GO TO 435
         ELSE
            GO TO 445
         END IF
435      WRITE(25,440) I
440      FORMAT(' Warning !  The Valency of Atom No.',I2 )
445   CONTINUE
*     ------------------------------------------------------------------
450   WRITE(25,*) '               '
      WRITE(25,*) 'Any change ?   '
      WRITE(25,*) '0 : No Change. '
      WRITE(25,*) '1 : Atom.      '
      WRITE(25,*) '2 : Bond.      '
      WRITE(25,*) '3 : Total Atoms.'
      WRITE(25,*) '      No. => ? '

c Modif par Dom
c      READ(*,*,ERR=450) N
	N=0

      IF(N.LT.0.OR.N.GT.3)  GO TO 450
      IF(N.EQ.0)  GO TO 1000
*     ***** Atom Change ****
      IF(N.EQ.1)  THEN
500      WRITE(25,*) '    No. [0(zero) for END] =  ? '
         READ(*,510,ERR=500) NN
510      FORMAT(I3)
         IF(NN.EQ.0)  GO TO 200
515      WRITE(25,520) 'New Element = '
520      FORMAT(1H ,A)
         READ(*,530) ATYPE(NN)
530      FORMAT(A4)
*        -------  Atom Check  ---------------------------------------
         DO 540 K=1,16
            IF(ATYPE(NN).EQ.ELTYPE(K))   GO TO 200
540      CONTINUE
         WRITE(25,*) 'Input Correct Atom Types. '
         GO TO 515
*        ------------------------------------------------------------
      END IF
*     ***** Bond Change ****
      IF(N.EQ.2)  THEN
600      WRITE(25,*) '  Between [ 0(Zero) for END ]  = ? '
         READ(*,*,ERR=600)  IFA(I)
         IF(IFA(I).EQ.0)    GO TO 200
610      IF(IFA(I).NE.0)    WRITE(25,*) IFA(I),'  and  '
         READ(*,*,ERR=610)  ITA(I)
620      WRITE(25,*) ' Bond Degree (1,2,3,4 or -1 [CUT] OR 0 [END])  ?'
630      READ(*,*,ERR=620)  NBTYPE(I)
         IF(NBTYPE(I).LE.-2 .OR. NBTYPE(I).GT.5)   GO TO 630
         IF(NBTYPE(I).EQ.0)    GO TO 200
         IF(NBTYPE(I).NE.-1)   THEN
            MATCT(IFA(I),ITA(I))=NBTYPE(I)
            MATCT(ITA(I),IFA(I))=NBTYPE(I)
         END IF
         IF(NBTYPE(I).EQ.-1)   THEN
            MATCT(IFA(I),ITA(I))=0
            MATCT(ITA(I),IFA(I))=0
         END IF
         GO TO 200
      END IF
*     **** Total Atoms Change ****
      IF(N.EQ.3)  THEN
700      WRITE(25,710) 'From ',NATOM, ' to (max.= 100 ) =  ? '
710      FORMAT(1H ,A,I3,A)
         READ(*,*,ERR=700) N
         IF(N.LE.0 .OR. N.GT.100)  GO TO 700
         IF(N.EQ.NATOM)            GO TO 200
         IF(N.GT.NATOM)  THEN
            DO 720 K1=NATOM+1,N
               ATYPE(K1)='C'
               DO 720 K2=1,N
                  MATCT(K1,K2)=0
                  MATCT(K2,K1)=0
720         CONTINUE
         END IF
         NATOM=N
         GO TO 200
      END IF

1000  RETURN
      END
C-----------------------------------------------------------------------
      Subroutine  INPUT2
C-----------------------------------------------------------------------
C     Subroutine for Inputting Structural Formula by the Method 2

      CHARACTER*5    ATYPE(100),ANS*1
      COMMON /MAX/   MAXAT
      COMMON /CTI/   NATOM,MATCT(100,100)
      COMMON /CTA/   ATYPE

      MAXAT=100

  100 CALL STRSET
      IF(NATOM.EQ.0)  THEN
           WRITE(25,*) 'Analysis will be skipped because of no atom. '
           GO TO 9000
      END IF 
 9000 WRITE(25,*)'  All for the structure.  '

      CALL SETUPC

      RETURN
      END

C     ------------------------------------------------------------------
      SUBROUTINE STRSET
C     ------------------------------------------------------------------
      CHARACTER*5 ATYPE(100),ELTYPE(16),BOND(150)*1,SBOND(4)*1
      DIMENSION   NATM(150)
      COMMON /MAX/   MAXAT
      COMMON /CTI/   NATOM,MATCT(100,100)
      COMMON /CTA/   ATYPE
      DATA SBOND /'-','=','#','*'/
      DATA ELTYPE/'F','Cl','Br','I','CN','NCS','NO','NO2',
     *           'CO','CS','SO2','SO','O','C','N','S'/

      CALL DATAIO(1)

  200 WRITE(25,205)(K,K=1,NATOM)
  205 FORMAT(1H ,' < Structural Formula > '/' ',100I3)
      WRITE(25,210)(ATYPE(K),K=1,NATOM)
  210 FORMAT(1H ,2X,100A3)
      DO 390 J=1,NATOM
  310      LPAR=0
           NBOND=0
           K=J
           GO TO 370
  320      IF(LPAR.EQ.0)  NTOP=K
           NBOND=NBOND+1
           BOND(NBOND)=SBOND(MATCT(K,I))
           NATM(NBOND)=I
           MATCT(K,I)=-MATCT(K,I)
           MATCT(I,K)=-MATCT(I,K)
           LPAR=1
           K1=K
           K=I
  370      DO 380 I=1,NATOM
                IF(I.EQ.K1)          GO TO 380
  380           IF(MATCT(K,I).GT.0)  GO TO 320
           IF(LPAR.EQ.1)  THEN
                WRITE(25,385) NTOP,(BOND(K),NATM(K),K=1,NBOND)
  385           FORMAT(1H0,1X,I2,20(A,I2))
                GO TO 310
           END IF
  390 CONTINUE
      DO 400 J=1,NATOM
           DO 400 I=1,NATOM
  400           MATCT(J,I)=-MATCT(J,I)
*     -------  Bond Check  ---------------------------------------------
      DO 445 I=1,NATOM
           NB=0
           DO 405 J=1,NATOM
  405           IF(MATCT(I,J).NE.0)   NB=NB+MATCT(I,J)
	
           DO 408 K=1,16
                IF(ATYPE(I).EQ.ELTYPE(K))   THEN
                     GO TO (410,410,410,410,410,410,410,410,418,418,
     *                      415,415,418,420,425,430), K
                END IF
  408      CONTINUE
  410      IF(NB.NE.1)  THEN
                     GO TO 435
                ELSE
                     GO TO 445
           END IF
  415      IF(NB.NE.2)  THEN
                     GO TO 435
                ELSE
                     GO TO 445
           END IF
  418      IF(NB.GT.2)  THEN
                     GO TO 435
                ELSE
                     GO TO 445
           END IF
  420      IF((NB.GT.4).AND.(NB.NE.8).AND.(NB.NE.9).AND.(NB.NE.12)) THEN
                     GO TO 435
                ELSE
                     GO TO 445
           END IF
  425      IF((NB.GT.3).AND.(NB.NE.5).AND.(NB.NE.8).AND.(NB.NE.10)) THEN
                     GO TO 435
                ELSE
                     GO TO 445
           END IF
  430      IF((NB.GT.2).AND.(NB.NE.4).AND.(NB.NE.6))  THEN
                     GO TO 435
                ELSE
                     GO TO 445
           END IF
  435      WRITE(25,440) I
  440      FORMAT(' Warning !   The Valency of Atom No.',I2 )
  445 CONTINUE
*     ------------------------------------------------------------------
  900 WRITE(25,*) '                '
      WRITE(25,*) ' Any change  ?  '
      WRITE(25,*) ' 0 : No change. '
      WRITE(25,*) ' 1 : Element - C,O,N,S,F,Cl,Br,I '
      WRITE(25,*) '            or CN,CO,CS,NO,NO2,NCS,SO,SO2 '
      WRITE(25,*) ' 2 : Bond.      '
      WRITE(25,*) ' 3 : Total atoms.'
      WRITE(25,*) '       No. =>  ? '

c Modif par Dom
c  945 read(*,950,ERR=900)N
	N=0
  945 CONTINUE

  950 FORMAT(I1)
      IF(N.LT.0.OR.N.GT.3)GOTO 900
      IF(N.NE.0) THEN
           IF(N.EQ.1) CALL ELMCHG
           IF(N.EQ.2) CALL CNCCHG
           IF(N.EQ.3) CALL NTLCHG(MAXAT)
           GOTO 200
      END IF

      CALL DATAIO(0)

      RETURN
      END
C     ------------------------------------------------------------------
      SUBROUTINE DATAIO(NIO)
C     ------------------------------------------------------------------
      CHARACTER*5 ATYPE(100)
      DIMENSION MS(200,3)
      COMMON /CTI/ NATOM,MATCT(100,100)
      COMMON /CTA/ ATYPE

      OPEN(20,FILE='STRUCT.DAT')

C     NIO=1   for input(read),    0  for output(write)

      IF(NIO.EQ.0) GO TO 1000

      READ(20,25) NATOM, NBONDS
   25 FORMAT(2I3)
      READ(20,26)(ATYPE(K),K=1,NATOM)
   26 FORMAT(100A3)

      DO 40 J=1,3
   40      READ(20,30)(MS(K,J),K=1,NBONDS)
   30      FORMAT(200I3)
	
  100 WRITE(25,105)NATOM
  105 FORMAT(1H ,'The total number of atoms = ',I3)

      DO 140 J=1,NATOM
           DO 140 I=1,NATOM
  140           MATCT(I,J)=0

      DO 150 K=1,NBONDS
		
           MATCT(MS(K,1),MS(K,2))=MS(K,3)
  150      MATCT(MS(K,2),MS(K,1))=MS(K,3)
      CLOSE(20)

      RETURN

C     DATA OUT (WRITE ON THE FILE)

 1000 NBONDS=0
      DO 1100 J=1,NATOM-1
           DO 1100 I=J+1,NATOM
                IF(MATCT(J,I).NE.0)  THEN
                     NBONDS=NBONDS+1
                     MS(NBONDS,1)=J
                     MS(NBONDS,2)=I
                     MS(NBONDS,3)=MATCT(J,I)
                END IF
 1100 CONTINUE

      WRITE(20,1110)NATOM,NBONDS
 1110 FORMAT(2I3)
      WRITE(20,1120)(ATYPE(K),K=1,NATOM)
 1120 FORMAT(100A3)

      DO 1130 J=1,3
 1130      WRITE(20,1140)(MS(K,J),K=1,NBONDS)
 1140      FORMAT(200I3)

      CLOSE(20)
      RETURN
      END

C     ------------------------------------------------------------------
      SUBROUTINE ELMCHG
C     ------------------------------------------------------------------
      CHARACTER*5  ATYPE(100),ELM,ELTYPE(16)
      COMMON /CTI/ NATOM,MATCT(100,100)
      COMMON /CTA/ ATYPE
      DATA ELTYPE/'F','Cl','Br','I','CN','NCS','NO','NO2',
     *           'CO','CS','SO2','SO','O','C','N','S'/

  100 WRITE(25,110)'New Element [ 0(Zero) for END ] = '
  110 FORMAT(1H ,A)
      read(*,120)ELM
  120 FORMAT(A3)
      IF(ELM.EQ.'0')  GO TO 900
*     -------  Atom Check ----------------------------------------------
      DO 150 K=1,16
           IF(ELM.EQ.ELTYPE(K))   GO TO 200
  150 CONTINUE
      WRITE(25,*) ' Input Correct Atom Type.'
      GO TO 100
*     ------------------------------------------------------------------
  200 WRITE(25,110)'   No.  [ 0(Zero) for END ] ? '
      READ(*,220,ERR=200) NN
  220 FORMAT(I3)
      IF(NN.EQ.0)                    GO TO 100
      IF(NN.GE.1 .AND. NN.LE.NATOM)  ATYPE(NN)=ELM
      GO TO 200

  900 RETURN
      END

C     ------------------------------------------------------------------
      SUBROUTINE CNCCHG
C     ------------------------------------------------------------------

      CHARACTER*5 ATYPE(100)
      COMMON /CTI/NATOM,MATCT(100,100)
      COMMON /CTA/ATYPE

  100 WRITE(25,110)'Bond degree (1,2,3,4 or -1 [CUT] or 0(Zero) [END])?'
  110 FORMAT(1H ,A)
      WRITE(25,*) ' Bond Type =>  1 = Single '
      WRITE(25,*) '               2 = Double '
      WRITE(25,*) '               3 = Triple '
      WRITE(25,*) '               4 = Aromatic'
      WRITE(25,*) '    No. => ?   '
      READ(*,120,ERR=100)NVB
  120 FORMAT(I2)
      IF(NVB.LE.-2 .OR. NVB.GE.5)GOTO 100
      IF(NVB.EQ.0)GOTO 900
      IF(NVB.EQ.-1)  NVB=0
      K1=0
  210 K2=K1
  220 WRITE(25,110)'Between  ? '
      IF(K2.NE.0)  WRITE(25,225) K2,'  and  '
  225              FORMAT(1H ,I3,A)
      READ(*,250,ERR=220) K1
  250 FORMAT(I4)  
  
      IF(K1.EQ.0 .AND. K2.EQ.0)  GOTO 100
      IF(K2.EQ.0 .AND. K1.LT.0)  THEN
            WRITE(25,*) ' - values are not allowed for a top.  '
            GO TO 220
      END IF  
      K4=K1
  300 IF(K4.LT.0)   K4=-K4
  310 IF(K4.GT.NATOM)  THEN
           WRITE(25,*) ' Too big.   Can change the size if want   ? '
           CALL NTLCHG
           GO TO 220
      END IF

      IF(K1.EQ.K2) THEN
           WRITE(25,*) ' Dont make a bond ! '
           GO TO 210
      END IF
      IF(K1.EQ.0.OR.K2.EQ.0)  GO TO 210
     
      IF(K1.LT.0)  THEN
                K1=-K1
                IF(K1.LT.K2)  THEN
                          K3=K1
                          K4=K2
                     ELSE
                          K3=K2
                          K4=K1
                END IF
                DO 270 K5=K3,K4-1
                     MATCT(K5,K5+1)=NVB
  270                MATCT(K5+1,K5)=NVB
           ELSE
                MATCT(K1,K2)=NVB
                MATCT(K2,K1)=NVB
      END IF
      GO TO 210

  900 RETURN
      END     


C     ------------------------------------------------------------------
      SUBROUTINE NTLCHG(MAXAT)
C     ------------------------------------------------------------------
      CHARACTER*5  ATYPE(100)
      COMMON /CTI/ NATOM,MATCT(100,100)  
      COMMON /CTA/ ATYPE

  100 WRITE(25,110)'From ',NATOM, ' to (max. ',MAXAT,')=  ?'
  110 FORMAT(1H ,A,I3,A,I3,A)
      read(*,120,ERR=100)N
  120 FORMAT(I3)
      IF(N.LE.0 .OR.  N.GT.MAXAT)GOTO 100
      IF(N.EQ.NATOM)GOTO 900
      IF(N.GT.NATOM)   THEN
           DO 200 K1=NATOM+1,N
                ATYPE(K1)='C'
          	DO 200 K2=1,N
                     MATCT(K1,K2)=0
                     MATCT(K2,K1)=0
  200           CONTINUE
      END IF
      NATOM=N

  900 RETURN
      END
C     ------------------------------------------------------------------
      SUBROUTINE SETUPC
C     ------------------------------------------------------------------
      CHARACTER*5 ATYPE(100)
      INTEGER IFA(100),ITA(100),NBTYPE(100)
      COMMON /CTI/NATOM,MATCT(100,100)
      COMMON /CTA/ATYPE

      OPEN(20,FILE='STRUCT.DAT')
           READ(20,30) NATOM,NBONDS
 30        FORMAT(2I3)
           READ(20,40) (ATYPE(I),I=1,NATOM)
 40        FORMAT(100A3)
           READ(20,50) (IFA(I),I=1,NBONDS)
           READ(20,50) (ITA(I),I=1,NBONDS)
           READ(20,50) (NBTYPE(I),I=1,NBONDS)
 50        FORMAT(100I3)
           DO 60 I=1,NATOM
                DO 60 J=1,NATOM
                     MATCT(I,J)=0
 60        CONTINUE
           DO 70 I=1,NBONDS
                MATCT(IFA(I),ITA(I))=NBTYPE(I)
                MATCT(ITA(I),IFA(I))=NBTYPE(I)
 70        CONTINUE
      CLOSE(20)

      RETURN
      END 
*---------------------------------------------------------------------
      Subroutine  INPUT3                                              
*---------------------------------------------------------------------
C     Subroutine for Inputting Structural Formula by the Method 3
      IMPLICIT INTEGER(A-Z)
      INTEGER   MI(8),RX(52),RY(52),PPX(8),PPY(8),OI(10),
     2          BI(20),KT(100),NUM(6),
     3          RRX(55),RRY(55),
     4          SI(100),UUX(0:100),UUY(0:100),VI(100,13),
     5          VB(100),RGN(100),RGX(100),
     6          C1,E1,FLG,LENA,NA,NC,NMAX,NP,NTEMP,OX,OY,
     7          PCT,UX,UY,TA,NOC,SW,MX,MY,MX2,MY2
      CHARACTER OC(8)*3,FC(11)*1,CC(20)*5,WC(100,13)*1,ZC(100)*70,
     1          B1(15)*1,B2(15)*1,EC(90)*2,TC(100)*2,
     2          AC*30,CA*5,WB*1,DR*3,BTEMP*1,CAW*3,
     3          ETEMP*2,TS*2,VC*1,T1*2,EL*5,WWC*1,WTC*2,
     4          SN*2,DT1*200,DT2*200,DT3*200,DT4*200,
     5          DT5*240,DT6*200
      REAL      TH,COSTH,SINTH,X9,Y9,Z0,Z1,X,X1,X2,X3,Y1,Y2,Y3,
     1          AA,BB,D1,Y31D,X4,Y4P,Y4M,DD,EE,RR1,RR2,RR3,X4M,X4U,
     2          X4P,X4L,Y4L,Y4

      CHARACTER*5 ATYPE(100)

*  *******************************************************************
      COMMON /SEISU/  RX,RY,UUX,UUY,VI,VB,RGN,RGX,
     1                C1,E1,FLG,LENA,NA,NC,NMAX,NP,NTEMP,
     2                OI,OX,OY,PCT,TA,UX,UY,SW,NOC
     3       /JISU/   SINTH,COSTH,
     4       /MOJI/   WC,ZC,TC,AC,CA,VC,T1,WWC,ETEMP

      COMMON /CTI/ NATOM,MATCT(100,100)
      COMMON /CTA/ ATYPE

      DATA(MI(I),I=1,8)/1,4,8,13,19,26,34,43/
      DATA(PPX(I),I=1,8)/3,3,0,-3,-3,-3,0,3/
      DATA(PPY(I),I=1,8)/0,-3,-3,-3,0,3,3,3/
      DATA(OC(I),I=1,8) /' RR',' RU',' UU',' LU',' LL',' LD',
     1                  ' DD',' RD'/
      DATA(FC(I),I=1,11)/'-','=','#','+','*','@','$','&','%',',','/'/
      DATA(CC(I),I=1,20)/'A','B','CT','D','E','J','HELP',
     1                   'NEW','OLD','ST','T','X','Z','OK','H',
     2                  'ZZZ','ZZZ','ZZZ','ZZZ','ZZZ'/
**********************************************************************
* CLEAR
      DO 1 I=1,100
        DO 1 J=1,100
          MATCT(I,J)=0
    1 CONTINUE
* STRB      
      NMAX=12
      DO 3 I = 1,100
        RGN(I) = 1
        RGX(I) = 1
    3 CONTINUE
* NW
      TA = 0
      DO 10 I = 1,100
        VB(I) = 0
        DO 5 J = 1,NMAX
          VI(I,J) = 0
          WC(I,J) = '-'
    5   CONTINUE
   10 CONTINUE
**********************************************************************
* DATAIN
 1000 WRITE(6,*)'( H for HELP ) ? '
      READ (5,1001) AC
 1001 FORMAT(30A)
      WRITE(6,*)'Command = ',AC
      LENA = INDEX(AC,'  ') -1
      IF(LENA.EQ.0)    GOTO 1000
      DO 20 I = 1,LENA
        CA = AC(I:I)
        IF(CA.EQ.' ')THEN
        ELSE
          K = I
          GO TO 30 
        ENDIF
   20 CONTINUE
      GO TO 2000
   30 POS = INDEX(AC(K:LENA),' ')
      IF(POS.EQ.0) THEN
        CA = AC(K:LENA)
        IF(CA.NE.'HELP'.AND.CA.NE.'NEW'.AND.CA.NE.'CT'.AND.
     1     CA.NE.'ST'.AND.CA.NE.'Z'.AND.CA.NE.'OK'.AND.CA.NE.'H'.AND.
     2     CA.NE.'ZZZ') THEN
          GOTO 2000
        ELSE 
          GOTO 85
        ENDIF
      ELSE 
        POS = POS + K - 1
      ENDIF
      CA   = AC(K:POS-1)
      AC   = AC(POS+1:LENA)
   70 LENA = LENA - POS
      NP   = 1
      NN   = 0
      DO 80 I = 1,15
        EC(I) = 'C '
   80 CONTINUE
   85 DO 90 I = 1,20
        IF(CA.EQ.CC(I))THEN
          GO TO(101,103,104,105,106,115,108,109,100,111,
     1          112,113,114,8000,108,100,100,100,100,100),I
        ENDIF
   90 CONTINUE
      WRITE(25,*)'Available Commands:A,B,D,E,H(HELP),J,CT,NEW,ST,OK,X,Z'
      GO TO 2000
********************************************************************
* ZZ ZZZ
  100 STOP
********************************************************************
* A
  101 CALL CHAANL
      IF(FLG.EQ.1) THEN
        GO TO 2000
      ELSE
        PCT = NA
      ENDIF
      TH = 0
      DR = AC(LENA-2:LENA)
      DO 200 NN = 1,8
        IF(DR.EQ.OC(NN))THEN
          LENA = LENA -3
          AC   = AC( :LENA)
          GOTO 220
        ELSE
          TH = TH + .7854
        ENDIF
  200 CONTINUE
      NN = 1 
      TH = 0
  220 COSTH = COS(TH)
      SINTH = SIN(TH)
      DO 230 NOC = 1,15
        WWC= AC(NP:NP)
        NP = NP +1
        DO 240 I = 1,10
          IF(WWC.NE.FC(I))THEN
          ELSE
            GOTO 250
          ENDIF
  240   CONTINUE
        WWC = '-'
        IF(NOC.GT.1)THEN
          NP = NP - 1
        ENDIF
  250   B1 (NOC) = WWC
        CALL CHAANL
        IF(FLG.EQ.1)             GOTO 2000
        IF(C1.LT.1.OR.C1.GT.2)   GOTO 2000
        WTC = CA
        EC(NOC) = WTC
        CALL CHAANL
        IF(FLG.EQ.1)THEN
          GOTO 2000
        ELSE 
          IF(WTC.EQ.'R ')THEN
            IF(NA.LT.3.OR.NA.GT.10)  GOTO 2000
          ENDIF
        ENDIF
        BI(NOC) = NA
        IF(E1.EQ.0)THEN
        ELSE
          GOTO 260
        ENDIF
  230 CONTINUE
 
*     STRT
  260 IF(PCT.EQ.0)       UUY(0) = UUY(TA) + 5
      IF(UUX(TA).LT.90)  UUX(0) = UUX(TA) + 5
      OX =  COSTH * 3
      OY = -SINTH * 3
      DO 270 A1 = 1,NOC
        UX    = UUX(PCT)
        UY    = UUY(PCT)
        NTEMP = BI(A1)
        BTEMP = B1(A1)
        ETEMP = EC(A1)
        IF(ETEMP.EQ.'R ')THEN
          WB1 = 3
        ELSE
          IF(NTEMP.EQ.1)THEN
            WB1 = 1 
          ELSE
            WB1 = 2
          END IF
        ENDIF
        A4 = TA + 1
        DO 280 W3 = 1,NMAX
          IF(VI(PCT,W3).GT.0)THEN
            ELSE 
              GOTO 290
          ENDIF
  280   CONTINUE
        WRITE(6,*) 'Too much adjacent atoms !'
        GOTO 2000
  290   VI(PCT,W3) = A4
        VI(A4,WB1)  = PCT
        WC(PCT,W3) = BTEMP
        WC(A4,WB1)  = BTEMP
        IF(ETEMP.EQ.'R ')THEN
          CALL RINGST
        ELSE 
          CALL CHAINS
        ENDIF
  270 CONTINUE
      CALL DISPLA
      GOTO 1000
********************************************************************
* E  
  106 CALL CHAANL
      IF(FLG.EQ.1)    GOTO 2000
      POS = INDEX(CA,' ')
      IF(POS.EQ.0)    GOTO 2000
      LI = POS-1
      IF(LI.GT.2.OR.LI.LT.1)  GOTO 2000
      EL = CA
      NOC = 0
  600 CALL CHAANL
      IF(FLG.EQ.1)    GOTO 2000
      IF (NC.EQ.2)    GOTO 2000
      IF (NA.GT.TA.OR.NA.EQ.0) THEN
        WRITE(6,*) NA,'     Neglected because of out of the range !'
        GOTO 610
      ELSE
        IF (NOC.GT.0) THEN
          DO 620 J=1,NOC
            IF (BI(J).EQ.NA) THEN
              WRITE(6,*) NA,
     1       '     Neglected because of duplication '
              GOTO 610
            ENDIF
  620     CONTINUE
        ENDIF
          NOC     = NOC + 1
          BI(NOC) = NA
          TC(NA)  = EL
  610   IF (NP.GT.LENA) THEN
          CALL DISPLA
          GOTO 1000
        ELSE
          NP = NP + 1
          GOTO 600
        ENDIF
      ENDIF
********************************************************************
* D     
  105 NOC = 0
  740 CALL CHAANL
      IF(FLG.EQ.1)   GOTO 2000
      IF(NC.EQ.2)    GOTO 2000
      IF(NA.GT.TA) THEN
        GOTO 700
      ELSE
        IF(NOC.EQ.0) THEN
          GOTO 710
        ELSE
          DO 720 I = 1,NOC 
            IF(BI(I).EQ.NA)  GOTO 700
  720     CONTINUE
        ENDIF
  710   NOC     = NOC + 1
        BI(NOC) = NA
      ENDIF
  700 IF(E1.EQ.1) THEN
        GOTO 730
      ELSE 
        CALL CHAANL
        IF(FLG.EQ.1)  GOTO 2000
        IF(CA.EQ.' ') THEN
          GOTO 740
          ELSE
            GOTO 2000
        ENDIF
      ENDIF
  730 DO 750 I = 1,NOC
        BII = BI(I)
        DO 760 J = 1,NMAX
          BJJ = VI(BII,J)
          IF(BJJ.EQ.0) THEN
            GOTO 770
          ELSE
            DO 780 K = 1,NMAX
              IF(VI(BJJ,K).EQ.BII)  GOTO 790
  780       CONTINUE
              GOTO 2000
          ENDIF
  790     DO 800 L =K,NMAX
            VI(BJJ,L) = VI(BJJ,L+1)
            WC(BJJ,L) = WC(BJJ,L+1)
  800     CONTINUE
  760   CONTINUE
  770   DO 810 J = BII,TA
          UUX(J) = UUX(J+1)
          UUY(J) = UUY(J+1)
          TC(J)  = TC(J+1)
          VB(J)  = VB(J+1)
          DO 820 K = 1,NMAX
            VI(J,K) = VI(J+1,K)
            WC(J,K) = WC(J+1,K)
  820     CONTINUE
  810   CONTINUE
        TA = TA - 1
        DO 830 I1 = 1,TA
          DO 840 J1 = 1,NMAX
            BJJ = VI(I1,J1)
            IF(BJJ.EQ.0) THEN
              GOTO 830
            ELSE 
              IF(BJJ.GT.BII)   VI(I1,J1) = BJJ - 1
            ENDIF
  840     CONTINUE
  830   CONTINUE
  850   IF(I.EQ.NOC) THEN
          ELSE
            DO 870 K = 1,NOC
              IF(BI(K).GT.BII)  BI(K) = BI(K) - 1
  870       CONTINUE
        ENDIF
  750 CONTINUE
      IF(SW.EQ.2) THEN
        SW = 0
        GOTO 1895 
      ENDIF
      CALL DISPLA
      GOTO 1000
********************************************************************
* B       
  103 NP = 1
      CALL CHAANL
      IF(FLG.EQ.1)     GOTO 2000
      IF(NC.NE.1)      GOTO 2000
      IF(NA.GT.TA)     GOTO 2000
      IF(NP.GT.LENA)   THEN
        GOTO 2000
      ELSE
        N2 = NA
        NB = 0
      ENDIF
  900 NB = NB + 1 
      CALL CHAANL
      IF(FLG.EQ.1)     GOTO 2000
      IF(NP.GT.LENA)   GOTO 2000
      IF(C1.NE.1) THEN
        GOTO 2000
      ELSE 
        IF(NC.NE.2) THEN
          GOTO 2000
        ELSE
          DO 910 I=1,11
            IF(CA.EQ.FC(I)) THEN
              EC(NB) = CA
              GOTO 920
            ENDIF
  910     CONTINUE
          EC(NB) = '?'
        ENDIF
      ENDIF
  920 CALL CHAANL
      IF(FLG.EQ.1)   GOTO 2000
      IF(NC.NE.1)    GOTO 2000
      IF(NA.GT.TA)   GOTO 2000
      BI(NB) = NA
      IF(NP.LE.LENA) GOTO 900
  925 DO 930 I = 1,NB
        N1 = N2
        N2 = BI(I)
        CA = EC(I)
        IF(CA.EQ.'/')  GOTO 940
        DO 950 I1 = 1,NMAX
          IF(VI(N1,I1).EQ.0) THEN
            VI(N1,I1) = N2
            GOTO 960
          ELSE
            IF(VI(N1,I1).NE.N2) THEN
              ELSE
                GOTO 960
            ENDIF
        ENDIF
  950   CONTINUE
  955   WRITE(6,*)'Too many adjacent atoms,therefore CANCELLED ! '
        GOTO 1000
  960   WC(N1,I1) = CA
        DO 970 I1 = 1,NMAX
          IF (VI(N2,I1).EQ.0) THEN
            GOTO 980
          ELSE
            IF(VI(N2,I1).NE.N1) THEN
              ELSE
                GOTO 985
            ENDIF
          ENDIF
  970   CONTINUE
        GOTO 955
  980   VI(N2,I1) = N1
  985   WC(N2,I1) = CA
        GOTO 930
  940   DO 1005 I1 = 1,NMAX
          IF(VI(N1,I1).EQ.0)   GOTO 1010
          IF(VI(N1,I1).NE.N2)  THEN
          ELSE
            GOTO 1020
          ENDIF
 1005   CONTINUE
 1010   WRITE(6,*)' There is no such bond between No.',N1,
     1           ' -No.',N2
        GOTO 1000
 1020   DO 1030 I2=I1,NMAX
          VI(N1,I2) = VI(N1,I2+1)
          WC(N1,I2) = WC(N1,I2+1)
 1030   CONTINUE
        DO 1040 I1 = 1,NMAX
          IF(VI(N2,I1).EQ.0) THEN
            GOTO 1010
          ELSE
              IF(VI(N2,I1).NE.N1) THEN
                ELSE 
                  GOTO 1050
              ENDIF
          ENDIF
 1040   CONTINUE
        GOTO 1010
 1050   DO 1060 I2 = I1,NMAX
          VI(N2,I2) = VI(N2,I2+1)
          WC(N2,I2) = WC(N2,I2+1)
 1060   CONTINUE
  930 CONTINUE
      IF(SW.EQ.3) THEN 
        SW = 0
        GOTO 1896
      ELSE 
        IF(SW.EQ.4) THEN
          SW = 0
          GOTO 1897
        ENDIF
      ENDIF
      CALL DISPLA
      GOTO 1000
********************************************************************
* CT 
  104 IF(TA.EQ.0) THEN
        WRITE(6,*) 'There is no atom.'
        GOTO 1000
      ELSE
        DO 3000 I = 1,TA
          DO 3100 J = 1,NMAX
            IF(VI(I,J).EQ.0) THEN
              GOTO 3000
            ELSE
            IF(J.EQ.1)  WRITE(25,1225)I,TC(I),WC(I,J),VI(I,J)
 1225                   FORMAT(1H ,10X,I2,3X,A2,3X,A,2X,I2)
            IF(J.GT.1)  WRITE(25,1226)WC(I,J),VI(I,J)
 1226                   FORMAT(1H ,20X,A,2X,I2)
            ENDIF
 3100     CONTINUE
 3000   CONTINUE
        GOTO 1000
      ENDIF
*3001 FORMAT(I2,' ',A5)
*3002 FORMAT(A1,5X,I2)
********************************************************************
* NEW
  109 TA = 0
      DO 4000 I = 1,100
        VB(I) = 0
        DO 4100 J = 1,NMAX
          VI(I,J) = 0
          WC(I,J) = '-'
 4100   CONTINUE
 4000 CONTINUE
      GOTO 1000
  
********************************************************************
* S
  111 CALL DISPLA
      GOTO 1000
 
********************************************************************
* X
  113 CALL CHAANL
      IF(FLG.EQ.1)  GOTO 2000
      IF(NC.EQ.2)   THEN
        GOTO 2000
      ELSE
        PCT = NA
      ENDIF
      IF(AC(NP:NP+1).EQ.' /') THEN
        PI = 0
        NP = NP + 2
      ELSE
        PI = 1
        NP = NP + 1
      ENDIF
      CALL CHAANL
      IF(FLG.EQ.1)  GOTO 2000
      IF(NC.EQ.2)   GOTO 2000
      X = NA
      IF(PI.EQ.0)   X = 1/X
      DO 1100 I= 1,TA
        KT(I) = 1
 1100 CONTINUE
      I = PCT
      UX = UUX(PCT)
      UY = UUY(PCT)
 1110 KT(I) = -1
      DO 1120 J = 1,NMAX
        KI = VI(I,J)
        IF(KI.EQ.0) THEN
          GOTO 1130
        ELSE
          IF(KT(KI).GT.0) THEN
            KT(KI)  = 0
            UUX(KI) = UX + (UUX(KI) - UX) * X
            UUY(KI) = UY + (UUY(KI) - UY) * X
          ENDIF
        ENDIF
 1120 CONTINUE
 1130 DO 1140 I = 1,TA
        IF(KT(I).EQ.0)  GOTO 1110
 1140 CONTINUE
      CALL DISPLA
      GOTO 1000
  
********************************************************************
* T
  112 CALL CHAANL
      IF(FLG.EQ.1)   GOTO 2000
      IF(NC.EQ.2)    GOTO 2000
      PCT = NA
      CALL CHAANL
      IF(FLG.EQ.1)   GOTO 2000
      CAW= CA
      CA = ' '//CAW(1:3)
      DO 1200 NN = 1,8
        IF(CA.EQ.OC(NN)) THEN
          TH    = 0.7854 * (NN-1)
          COSTH = COS(TH)
          SINTH = SIN(TH)
          GOTO 1210
        ENDIF
 1200 CONTINUE
      GOTO 2000
 1210 CALL CHAANL
      IF (FLG.EQ.1)  GOTO 2000
      IF(NC.EQ.2) THEN
        GOTO 2000
      ELSE
        TRANS= NA
      ENDIF
      DO 1220 I = 1,TA
        KT(I) = 1
 1220 CONTINUE
      UX =  COSTH * NA
      UY = -SINTH * NA
      I  = PCT
 1230 KT(I)  = -1
      UUX(I) = UUX(I) + UX
      UUY(I) = UUY(I) + UY
      DO 1240 J = 1,NMAX
        KI = VI(I,J)
        IF(KI.EQ.0) THEN
          GOTO 1250
        ELSE
          IF(KT(KI).GT.0)    KT(KI) = 0
        ENDIF
 1240 CONTINUE
 1250 DO 1260 I = 1,TA
        IF(KT(I).EQ.O)   GOTO 1230
 1260 CONTINUE
      CALL DISPLA
      GOTO 1000
 
********************************************************************
* Z
  114 CALL CHAANL
      IF(FLG.EQ.1)   GOTO 2000
      IF(NC.EQ.2)    GOTO 2000
      IF(NA.GT.TA)   GOTO 2000
      NS1 = NA
      PCT = NA
      CALL CHAANL
      CALL CHAANL
      IF(FLG.EQ.1)   GOTO 2000
      IF(NC.EQ.2)    GOTO 2000
      NS2 = NA
      I   = PCT
      DO 1900 J = 1,NMAX
        IF(VI(I,J).NE.NS2) THEN
        ELSE 
          GOTO 1905
        ENDIF
 1900 CONTINUE
      WRITE(6,*)'ERROR NUMBER',NS2
 1905 DO 1910 I = 1,TA
        KT(I) = 1
 1910 CONTINUE
      I = PCT
 1912 KT(I) = -1
      DO 1915 J = 1,NMAX
        K = VI(I,J)
        IF(K.EQ.0) THEN
          GOTO 1920
        ELSE
          IF(KT(K).GT.0)   KT(K) = 0
        ENDIF
 1915 CONTINUE
 1920 DO 1925 I = 1,TA
        IF(KT(I).EQ.0)     GOTO 1912
 1925 CONTINUE
      X1 = UUX(NS1)
      X3 = UUX(NS2)
      Y1 = UUY(NS1)
      Y3 = UUY(NS2)
      DO 1930 I = 1,TA
        IF(KT(I).EQ.-1.AND.I.NE.NS1.AND.I.NE.NS2) THEN
          X2 = UUX(I)
          Y2 = UUY(I)
          AA = (X1-X2)**2 + (Y1-Y2)**2
          BB = (X2-X3)**2 + (Y2-Y3)**2
          D1 = AA - BB + X3**2 - X1**2 + Y3**2 - Y1**2
          Y31D = Y3 - Y1
          IF(Y31D.EQ.0) THEN
            X4 = D1/(2*(X3-X1))
            Y4P= Y3 + SQRT(BB-((X3-X4)**2))
            Y4M= Y3 - SQRT(BB-((X3-X4)**2))
            GOTO 1940
          ENDIF
          DD = D1/(2*(Y3-Y1))
          EE = (X1-X3)/(Y3-Y1)
          RR1= EE*Y1 + X1 - DD*EE
          RR2= (DD*EE-EE*Y1-X1)**2-(EE**2+1)*
     1          (X1**2+Y1**2-2*DD*Y1+DD**2-AA)
          RR3= EE**2+1
          X4M= (RR1-SQRT(RR2))/RR3
          X4P= (RR1+SQRT(RR2))/RR3
          X4U= X2+0.1
          X4L= X2-0.1
          IF(X4M.LT.X4L.OR.X4M.GT.X4U) THEN
            X4 = X4M
          ELSE
            IF(X4P.LT.X4L.OR.X4P.GT.X4U) THEN
              X4 = X4P
            ELSE
              X4 = X2
            ENDIF
          ENDIF
          Y4 = DD + EE*X4
          GOTO 1950
 1940     Y4U = Y2 + 0.1
          Y4L = Y2 - 0.1
          IF(Y4M.LT.Y4L.OR.Y4M.GT.Y4U) THEN
            Y4 = Y4M
          ELSE
            IF(Y4P.LT.Y4L.OR.Y4P.GT.Y4U) THEN
              Y4 = Y4P
            ELSE
              Y4 = Y2
            ENDIF
          ENDIF
 1950     UUX(I) = X4
          UUY(I) = Y4
        ENDIF
 1930 CONTINUE
      CALL DISPLA
      GOTO 1000
********************************************************************
* J
  115 J = 0
      DO 1800 I = 1,7
      CALL CHAANL
      IF(I.EQ.1.OR.I.EQ.3.OR.I.EQ.5.OR.I.EQ.7) THEN
        IF(FLG.EQ.1)   GOTO 2000
        IF(NC.EQ.2)    GOTO 2000
        IF(NA.GT.TA)   GOTO 2000
        J = J + 1
        NUM(J) = NA
      ENDIF
 1800 CONTINUE
      IF(NUM(3).LT.NUM(4)) THEN
        PCT = NUM(3)
      ELSE
        IF(NUM(3).GT.NUM(4)) THEN
          PCT = NUM(4)
        ELSE
          GOTO 2000
        ENDIF
      ENDIF
      I = NUM(1)
      DO 1810 J = 1,NMAX
        IF(VI(I,J).NE.NUM(2)) THEN
        ELSE
          GOTO 1820
        ENDIF
 1810 CONTINUE
      WRITE(6,*)'ERROR NUMBER',NUM(2)
      GOTO 2000
 1820 I = NUM(3)
      DO 1830 J = 1,NMAX
        IF(VI(I,J).NE.NUM(4)) THEN
        ELSE
          GOTO 1835
        ENDIF
 1830 CONTINUE
      WRITE(6,*)'ERROR NUMBER',NUM(4)
      GOTO 2000
 1835 I = NUM(3)
      DO 1836 J = 1,NMAX
        IF(VI(I,J).NE.NUM(4)) THEN
          NUM(5) = VI(I,J)
          IF(VI(I,J).GT.NUM(3))    NUM(5) = NUM(5) - 1
          IF(VI(I,J).GT.NUM(4))    NUM(5) = NUM(5) - 1
          GOTO 1837
        ENDIF
 1836 CONTINUE
 1837 I = NUM(4)
      DO 1838 J = 1,NMAX
        IF(VI(I,J).NE.NUM(3)) THEN
          NUM(6) = VI(I,J)
          IF(VI(I,J).GT.NUM(3))    NUM(6) = NUM(6) - 1
          IF(VI(I,J).GT.NUM(4))    NUM(6) = NUM(6) - 1
          GOTO 1840
        ENDIF
 1838 CONTINUE
 1840 DO 1850 I = 1,TA
        KT(I) = 1
 1850 CONTINUE
      I = PCT
 1855 KT(I) = -1
      DO 1860 J = 1,NMAX
        K = VI(I,J)
        IF(K.EQ.0) THEN
          GOTO 1870
        ELSE
          IF(KT(K).GT.0)     KT(K) = 0
        ENDIF
 1860 CONTINUE
 1870 DO 1880 I = 1,TA
        IF(KT(I).EQ.0)       GOTO 1855
 1880 CONTINUE
      MX = UUX(NUM(3))-UUX(NUM(1))
      MY = UUY(NUM(3))-UUY(NUM(1))
      MX2= UUX(NUM(4))-UUX(NUM(2))
      MY2= UUY(NUM(4))-UUY(NUM(2))
      IF(MX.NE.MX2.OR.MY.NE.MY2)  GOTO 2000
      DO 1890 I = 1,TA
        IF(KT(I).EQ.-1.AND.I.NE.NUM(3).AND.I.NE.NUM(4)) THEN
          UUX(I) = UUX(I) - MX
          UUY(I) = UUY(I) - MY
        ENDIF
 1890 CONTINUE
      SW = 2
      BI(1) = NUM(3)
      BI(2) = NUM(4)
      NOC   = 2
      GOTO 730
 1895 SW = 3
      NB    = 1
      N2    = NUM(1)
      BI(1) = NUM(5)
      EC(1) = '-'
      GOTO 925
 1896 SW = 4
      NB    = 1
      N2    = NUM(2)
      BI(1) = NUM(6)
      EC(1) = '-'
      GOTO 925
 1897 CALL DISPLA
      GOTO 1000
 
********************************************************************
* HELP
  108 WRITE(25,*)'< Commands for Structure Input > '
      WRITE(25,*)'A N1 L DX  Attachment of a chain or a ring L to atom '
      WRITE(25,*)'           no. N1 (ex.L=C2 for 2 carbon atoms, C1 for'
      WRITE(25,*)'            one C atom, L=R6 for 6-membered ring )   '
      WRITE(25,*)'           The direction parameter DX can be set for '
      WRITE(25,*)'           8 main directions: UU,upward; DD downward,'
      WRITE(25,*)'           RR,rightward; LL,leftward; combinations of'
      WRITE(25,*)'            RU, LU, RD, and LD                       '
      WRITE(25,*)'B N b M     Bond change between N and M with bond b  '
      WRITE(25,*)'B N/M       Delete bonds between atoms N and M       '
      WRITE(25,*)'D N1...Nk   Delete atoms N1,....,Nk                  '
      WRITE(25,*)'E X N1...Nk Replace the atom type of atoms N1,...,Nk '
      WRITE(25,*)'            with chemical element X                  '
      WRITE(25,*)'J N1...Nk  Formation of bonds connecting atoms N1,...'
      WRITE(25,*)'H(HELP)     Display the list of commands             '
      WRITE(25,*)'CT          Show the connection table                '
      WRITE(25,*)'NEW         Abandon the structure                    '
      WRITE(25,*)'ST          Display strctural formula                '
      WRITE(25,*)'OK          Termination of structure input           '
      WRITE(25,*)'(Other Utility Commands)                             '
      WRITE(25,*)'X N M       Magnification of structural formula      '
      WRITE(25,*)'X N/M       Reduction of structural formula          '
      WRITE(25,*)'Z N M      Turn the structure along the bond between '
      WRITE(25,*)'            atoms N and M                            '

      GOTO 1000
********************************************************************
*ERRP
 2000 WRITE(6,*) 'Bad format!'
      GOTO 1000
********************************************************************

*SET UP CONNECTION TABLE
 
 8000 NATOM=TA
      DO 5000 I=1,NATOM
         ATYPE(I)=TC(I)
 5000 CONTINUE
 
      DO 5500 I=1,NATOM
         DO 5500 J=1,NMAX
            IF(VI(I,J).NE.0) THEN
                  IV=VI(I,J)
               IF(WC(I,J).EQ.'-') THEN
                  MATCT(I,IV)=1
                  MATCT(IV,I)=1
               END IF
               IF(WC(I,J).EQ.'=') THEN
                  MATCT(I,IV)=2
                  MATCT(IV,I)=2
               END IF
               IF(WC(I,J).EQ.'#') THEN
                  MATCT(I,IV)=3
                  MATCT(IV,I)=3
               END IF
               IF(WC(I,J).EQ.'*') THEN
                  MATCT(I,IV)=4
                  MATCT(IV,I)=4
               END IF
            END IF
 5500 CONTINUE 

C      WRITE(25,6300)(I,I=1,NATOM)
C 6300 FORMAT('         ',3X,50I2,/) 

C      DO 6500 I=1,TA
C         WRITE(25,6600) ATYPE(I),I,(MATCT(I,J),J=1,NATOM)
C 6600    FORMAT(' ',1X,A4,4X,I2,1X,50I2)
C 6500 CONTINUE

      RETURN
      END
 
      BLOCKDATA
*    ***************************************************************
      IMPLICIT INTEGER(A-Z)
      INTEGER   RX(52),RY(52),UUX(0:100),UUY(0:100),
     1          VI(100,13),VB(100),RGN(100),RGX(100),OI(10),
     2          C1,E1,FLG,LENA,NA,NC,NMAX,NP,NTEMP,
     3          OX,OY,PCT,TA,UX,UY,SW,NOC
      CHARACTER WC(100,13),ZC(100)*70,TC(100)*2,
     1          AC*30,CA*5,VC*1,T1*2,WWC*1,S2*3,ETEMP*2
      REAL      SINTH,COSTH
      COMMON /SEISU/  RX,RY,UUX,UUY,VI,VB,RGN,RGX,
     1                C1,E1,FLG,LENA,NA,NC,NMAX,NP,NTEMP,
     2                OI,OX,OY,PCT,TA,UX,UY,SW,NOC
     3       /JISU/   SINTH,COSTH,
     4       /MOJI/   WC,ZC,TC,AC,CA,VC,T1,WWC,ETEMP
*    ******************************************************************
      DATA(RX(I),I=1,52)/0,3,3, 0,3,6,3, 0,3,6,6,3, 0,3,6,9,6,3, 0,3,6,
     1                   9,9,6,3, 0,3,6,9,9,6,3,0, 0,3,6,9,6,6,3,0,3,
     2                   0,3,6,9,6,9,6,3,0,3/
      DATA(RY(I),I=1,52)/0,-3,3, 0,-3,0,3, 0,-3,-3,3,3, 0,-3,-3,0,3,3,
     1                   0,-3,-3,-3,3,3,3, 0,-3,-3,0,3,6,6,3, 0,-3,-3,
     2                   0,3,9,9,6,3, 0,-3,-3,0,3,6,9,9,6,3/
      DATA(OI(I),I=1,10)/0,0,2,3,3,4,4,4,4,4/
      END
   
***********************************************************************
*****           END OF MAIN ROUTINE                               *****
***********************************************************************
  
* DISPLA
      SUBROUTINE DISPLA
*    ******************************************************************
      IMPLICIT INTEGER(A-Z)
      INTEGER   RX(52),RY(52),UUX(0:100),UUY(0:100),
     1          VI(100,13),VB(100),RGN(100),RGX(100),OI(10),
     2          C1,E1,FLG,LENA,NA,NC,NMAX,NP,NTEMP,
     3          OX,OY,PCT,TA,UX,UY,SW,WI1,WI2,NOC
      CHARACTER WC(100,13),ZC(100)*70,TC(100)*2,
     1          AC*30,CA*5,VC*1,T1*2,WWC*1,S2*2,ETEMP*2,
     2          N9*1,WC11*1,WC12*1
      REAL      SINTH,COSTH,X9,Y9,Z0,Z1,X
      COMMON /SEISU/  RX,RY,UUX,UUY,VI,VB,RGN,RGX,
     1                C1,E1,FLG,LENA,NA,NC,NMAX,NP,NTEMP,
     2                OI,OX,OY,PCT,TA,UX,UY,SW,NOC
     3       /JISU/   SINTH,COSTH,
     4       /MOJI/   WC,ZC,TC,AC,CA,VC,T1,WWC,ETEMP

      CHARACTER*5 ATYPE(100)
      COMMON /CTI/ NATOM,MATCT(100,100) 
      COMMON /CTA/ ATYPE

*    ******************************************************************
      IF(TA.EQ.0) THEN
        SW = 1
        RETURN
      ENDIF
      DO 1400 I = 1,TA
        KB = 0
        DO 1410 J = 1,NMAX
          IF(VI(I,J).GT.0) THEN
            KB = KB + 1
          ELSE
            GOTO 1420
          ENDIF
 1410   CONTINUE
 1420   VB(I) = KB
 1400 CONTINUE
      XMAX = 0
      YMAX = 0
      XMIN = 9999
      YMIN = 9999
      DO 1430 I = 1,TA
        IF(XMAX.LT.UUX(I))   XMAX = UUX(I)
        IF(XMIN.GT.UUX(I))   XMIN = UUX(I)
        IF(YMAX.LT.UUY(I))   YMAX = UUY(I)
        IF(YMIN.GT.UUY(I))   YMIN = UUY(I)
 1430 CONTINUE
      XLEN = XMAX - XMIN + 3
      DO 1440 I = 1,67
        ZC(I) = ' '
 1440 CONTINUE
      DO 1450 N1 = 1,TA
        DO 1460 N2 = 1,VB(N1)
          V12 = VI(N1,N2)
          IF(V12.LT.N1) THEN
            GOTO 1460
          ELSE
            N9 = WC(N1,N2)
            X1 = UUX(N1) - XMIN + 2
            X2 = UUX(V12)- XMIN + 2
            X9 = X1 - X2
            Y1 = UUY(N1) - YMIN + 2
            Y2 = UUY(V12)- YMIN + 2
            Y9 = Y1 - Y2
            IF(Y9.LT.0) THEN
            ELSE
              YI = Y1
              Y1 = Y2
              Y2 = YI
            ENDIF
          ENDIF
 1470     IF(X9.EQ.0) THEN
          ELSE
            IF(X9.GT.0) THEN
              XI = X1
              X1 = X2
              X2 = XI
            ENDIF
            Z0 = Y9 / X9
            IF(Z0.LT.0) THEN
              Z1 = Y2
              ELSE
                Z1 = Y1
            ENDIF
            DO 1480 XI = X1+1,X2
              Z1 = Z1 + Z0
              II = Z1
              ZC(II) = ZC(II)(1:XI-1) // N9 // ZC(II)(XI+1:XLEN)
 1480       CONTINUE
              GOTO 1460
          ENDIF
          DO 1490 YI = Y1+1,Y2
            IF(Y9.NE.0) THEN
              ZC(YI) = ZC(YI)(1:X1-1) // N9 // ZC(YI)(X1+1:XLEN)
            ENDIF
 1490     CONTINUE
 1460   CONTINUE
 1450 CONTINUE
      DO 1500 N3 = 1,TA
        XI  = UUX(N3) - XMIN + 2
        YI  = UUY(N3) - YMIN + 2
        S1  = N3
        S4  = S1 / 10 
        WI1 = S1 / 10
        WC11= CHAR(WI1+48)
        WI2 = S1 - WI1*10
        WC12= CHAR(WI2+48)
        IF(WC11.EQ.'0') THEN
          S2 = ' ' // WC12
        ELSE
          S2  = WC11 // WC12
        ENDIF
        IF(S4.EQ.0) THEN
          S2 = S2(LEN(S2):LEN(S2)) // ZC(YI)(XI+1:XI+1)
        ELSE
          S2 = S2(LEN(S2)-1:LEN(S2))
        ENDIF
        ZC(YI) = ZC(YI)(1:XI-1) // S2 // ZC(YI)(XI+2:XLEN)
        IF(TC(N3).NE.'C') THEN
          ZC(YI-1)=ZC(YI-1)(1:XI-1)//TC(N3)//ZC(YI-1)(XI+2:XLEN)
        ENDIF
 1500 CONTINUE
      DO 1510 I = 1,YMAX-YMIN+3
        WRITE(6,*) ZC(I)
 1510 CONTINUE
      DO 1520 K = 1,TA
        IF(TC(K).EQ.'AK') THEN
          WRITE(6,*) 'Alkyl(' , K , ')C' , RGN(K) , 'To', RGX(K)
        ENDIF
 1520 CONTINUE
      RETURN 
      END
 
* CHAANL
      SUBROUTINE CHAANL
*    ******************************************************************
      IMPLICIT INTEGER(A-Z)
      INTEGER   RX(52),RY(52),UUX(0:100),UUY(0:100),
     1          VI(100,13),VB(100),RGN(100),RGX(100),OI(10),
     2          C1,E1,FLG,LENA,NA,NC,NMAX,NP,NTEMP,
     3          OX,OY,PCT,TA,UX,UY,SW,NOC
      CHARACTER WC(100,13)*1,ZC(100)*70,TC(100)*2,
     1          AC*30,CA*5,VC*1,T1*2,WWC*1,S2*3,ETEMP*2,
     2          N9*1
      REAL      SINTH,COSTH
      COMMON /SEISU/  RX,RY,UUX,UUY,VI,VB,RGN,RGX,
     1                C1,E1,FLG,LENA,NA,NC,NMAX,NP,NTEMP,
     2                OI,OX,OY,PCT,TA,UX,UY,SW,NOC
     3       /JISU/   SINTH,COSTH,
     4       /MOJI/   WC,ZC,TC,AC,CA,VC,T1,WWC,ETEMP

      CHARACTER*5 ATYPE(100)
      COMMON /CTI/ NATOM,MATCT(100,100)
      COMMON /CTA/ ATYPE

*    ******************************************************************
      IF(LENA.LT.NP) THEN
        FLG = 1
        RETURN
      ELSE
        CA = ' '
        NA = 0 
        E1 = 0 
        C1 = 0
        NC = 0
        FLG= 0
      ENDIF
      DO 1600 I = NP,LENA
        WWC = AC(I:I)
        WN  = ICHAR(WWC) - 48
        IF(WN.LT.0.OR.WN.GT.9) THEN
          IF(NC.EQ.1) THEN
            RETURN
          ELSE 
            NC  = 2
            POS = INDEX(CA,' ')
            CA  = CA(1:POS-1) // WWC
          ENDIF
        ELSE
          IF(NC.EQ.2) THEN
            RETURN
          ELSE 
            IF(C1.GT.2) THEN
              FLG = 1
              RETURN
            ELSE
              NC = 1
              NA = NA * 10 + (ICHAR(WWC)-48)
            ENDIF
          ENDIF
        ENDIF
        NP = NP + 1
        C1 = C1 + 1
 1600 CONTINUE
      E1 = 1
      RETURN
      END
  
***********************************************************************
* CHAINS
      SUBROUTINE CHAINS
*    ******************************************************************
      IMPLICIT INTEGER(A-Z)
      INTEGER   RX(52),RY(52),UUX(0:100),UUY(0:100),
     1          VI(100,13),VB(100),RGN(100),RGX(100),OI(10),
     2          C1,E1,FLG,LENA,NA,NC,NMAX,NP,NTEMP,
     3          OX,OY,PCT,TA,UX,UY,SW,NOC
      CHARACTER WC(100,13),ZC(100)*70,TC(100)*2,
     1          AC*30,CA*5,VC*1,T1*2,WWC*1,S2*3,ETEMP*2,
     2          N9*1
      REAL      SINTH,COSTH
      COMMON /SEISU/  RX,RY,UUX,UUY,VI,VB,RGN,RGX,
     1                C1,E1,FLG,LENA,NA,NC,NMAX,NP,NTEMP,
     2                OI,OX,OY,PCT,TA,UX,UY,SW,NOC
     3       /JISU/   SINTH,COSTH,
     4       /MOJI/   WC,ZC,TC,AC,CA,VC,T1,WWC,ETEMP

      CHARACTER*5 ATYPE(100)
      COMMON /CTI/ NATOM,MATCT(100,100)
      COMMON /CTA/ ATYPE

*    ******************************************************************
      DO 400 I1 = 1,NTEMP
        TA     = TA + 1
        TC(TA) = ETEMP
        UX     = UX + OX
        UUX(TA)= UX
        UY     = UY + OY
        UUY(TA)= UY
        IF (NTEMP.EQ.1) THEN
        ELSE 
          IF (I1.EQ.1) THEN
            VI(TA,1) = TA + 1
          ELSE
            VI(TA,1) = TA - 1
            IF (I1.LT.NTEMP)    VI(TA,2) = TA + 1
          ENDIF
        ENDIF
  400 CONTINUE
      PCT = TA
      RETURN
      END
***********************************************************************
      SUBROUTINE RINGST
*    ******************************************************************
      IMPLICIT INTEGER(A-Z)
      INTEGER   RX(52),RY(52),UUX(0:100),UUY(0:100),
     1          VI(100,13),VB(100),RGN(100),RGX(100),OI(10),
     2          C1,E1,FLG,LENA,NA,NC,NMAX,NP,NTEMP,
     3          OX,OY,PCT,TA,UX,UY,SW,NOC
      CHARACTER WC(100,13),ZC(100)*70,TC(100)*2,
     1          AC*30,CA*5,VC*1,T1*2,WWC*1,S2*3,ETEMP*2,
     2          N9*1
      REAL      WRX,WRY,SINTH,COSTH
      COMMON /SEISU/  RX,RY,UUX,UUY,VI,VB,RGN,RGX,
     1                C1,E1,FLG,LENA,NA,NC,NMAX,NP,NTEMP,
     2                OI,OX,OY,PCT,TA,UX,UY,SW,NOC
     3       /JISU/   SINTH,COSTH,
     4       /MOJI/   WC,ZC,TC,AC,CA,VC,T1,WWC,ETEMP

      CHARACTER*5 ATYPE(100)
      COMMON /CTI/ NATOM,MATCT(100,100)
      COMMON /CTA/ ATYPE

*    ******************************************************************
      PCT = TA + OI(NTEMP)
      IR  = (NTEMP * NTEMP - NTEMP - 6) / 2
      UX  = UX + OX
      UY  = UY + OY
      T1  = 'C '
      DO 500 I1 = 1,NTEMP
        I2     = TA + I1
        TC(I2) = T1
        I3     = IR + I1
        WRX     = RX(I3)
        WRY     = RY(I3)
        UUX(I2)= WRX * COSTH + WRY * SINTH + UX
        UUY(I2)=-WRX * SINTH + WRY * COSTH + UY
        IF (I1.EQ.1) THEN
          V1 = TA + NTEMP
        ELSE
          V1 = I2 - 1
        ENDIF
        VI(I2,1) = V1
        IF (I1.EQ.NTEMP) THEN
          V1 = TA + 1
        ELSE
          V1 = I2 + 1
        ENDIF
        VI(I2,2) = V1
  500 CONTINUE
      TA = I2
      RETURN
      END
C-----------------------------------------------------------------------
      Subroutine PGROUP
C-----------------------------------------------------------------------
C       Subroutine for Setting up Groups for Log P Estimation

C       Description of Variables
C          CGROUP ....... Key(Central) Groups
C          AATOMS ....... A(Neighboring) Atoms of CGROUP
C          BATOMS ....... B(Next-neighboring) Atoms of CGROUP
C          PGR    ....... Groups for printing
C          CG     ....... Variables for Substitution of CGROUP
C          CCCC   ....... Variables Used for Printing of Groups
C          AAAA   ....... ibid.
C          NAAA   ....... ibid.
C          BBBB   ....... ibid.
C          NBBB   ....... ibid.
C          W      ....... Variables Used for Sorting of Groups
C          CTAB   ....... Atom Types for Classification of Atoms
C          DTYPE  ....... Variables for Classifying Atoms into  Atom
C                         in CGROUP or AATOMS ,and BATOMS
C          LENC   ....... Length of CGROUP
C          LENA   ....... Length of AATOMS
C          LENB   ....... Length of BATOMS

        Character*4   ATYPE(100)*5,CGROUP(100,4),AATOMS(100,4),
     *                BATOMS(100,12),CG(100,4),CTAB(15),W
        Character*10  CCCC(4),AAAA(4),NAAA(4),BBBB(12),NBBB(12)
        Character*1   DTYPE(100)
        Character*80  GROUP(100),PGR
        Integer       LENC(4),LENA(4),LENB(12)
        Common /CTI/  NATOM,MATCT(100,100)
        Common /CTA/  ATYPE
        Common /GRP/  GROUP
        Common /GRN/  NGROUP
        COMMON /NUM/  NUM1,NUM2,NUM3
        DATA(CTAB(I),I=1,15)/'F','Cl','Br','I','CN','NO','NO2','NCS',
     *             'IO','IO2','C','Cd','Ct','Ca','Cf'/

      IF(NUM1.EQ.0)  THEN
      WRITE(25,*) 'This estimation is based on the group-contribution '
      WRITE(25,*) 'method proposed by Suzuki and Kudo [J. Comp.-Aided '
      WRITE(25,*) 'Mol. Design, 4(1990)155-198 ].      Note that this '
      WRITE(25,*) 'version  covers only  the calculation using  Basic '
      WRITE(25,*) 'Group Set.  You should be aware of the propagation '
      WRITE(25,*) 'of errors if you calculate  a structurally complex '
      WRITE(25,*) 'molecule.                                         '
      WRITE(25,*) '                                                  '
      END IF
      NUM1 = 1

************************************************************************
*                                                                      *
*       Key Group -  A Atoms - B Atoms                                 *
*                                                                      *
************************************************************************

        DO 100 I=1,NATOM
        DTYPE(I)='Y'
             DO 30 K=1,10
 30               IF(ATYPE(I).EQ.CTAB(K))  DTYPE(I)='X'
             DO 50 K=11,15
 50               IF(ATYPE(I).EQ.CTAB(K))  DTYPE(I)='C'
             DO 80 J=1,4
                  CGROUP(I,J)=' '
 80               AATOMS(I,J)=' '
             DO 100 K=1,12
                  BATOMS(I,K)=' '
100          CONTINUE
        DO 1000 I=1,NATOM
             IF(DTYPE(I).EQ.'X' )     GO TO 1000
             CGROUP(I,1)=ATYPE(I)
             J=1
             L=0
             LB=0
             DO 900 JJ=1,NATOM
                  IF(MATCT(I,JJ).EQ.0)   GO TO 900
                  IF(MATCT(I,JJ).EQ.1)   THEN
                       IF(DTYPE(JJ).EQ.'X')      GO TO 200
                       GO TO 300
 200                   J=J+1
                       CGROUP(I,J)=ATYPE(JJ)
                       GO TO 900
                  END IF
 300              L=L+1
                  AATOMS(I,L)=ATYPE(JJ)
                  IF(DTYPE(JJ).EQ.'C')     GO TO 700
                  DO 600 KK=1,NATOM
                       IF(KK.EQ.JJ)             GO TO 600
                       IF(MATCT(I,KK).EQ.0)     GO TO 600
                       IF(DTYPE(KK).EQ.'C')     GO TO 400
                       GO TO 700
 400                   DO 500 MM=1,NATOM
                            IF(MM.EQ.I)              GO TO 500
                            IF(MATCT(KK,MM).EQ.0)    GO TO 500
                            IF(DTYPE(MM).EQ.'C')     GO TO 500
                            GO TO 700
 500                   CONTINUE
 600              CONTINUE
                  GO TO 900
 700              DO 800 LL=1,NATOM
                       IF(I.EQ.LL)             GO TO 800
                       IF(MATCT(JJ,LL).EQ.0)   GO TO 800
                       IF(DTYPE(LL).EQ.'C' )   GO TO 800
                       LB=LB+1
                       BATOMS(I,LB)=ATYPE(LL)
 800              CONTINUE
 900        CONTINUE
1000    CONTINUE

************************************************************************
*                                                                      *
*       Addition of H Atoms to Key Group                               *
*                                                                      *
************************************************************************

        DO 3000 I=1,NATOM
             IF(ATYPE(I).EQ.' ' )    GO TO 3000
             NBOND=0
             DO 2100 J=1,NATOM
                  NBOND=NBOND+MATCT(I,J)
 2100        CONTINUE
             IF(NBOND.EQ.1)   THEN
                  IF(ATYPE(I).EQ.'C')   CGROUP(I,1)='CH3'
                  IF(ATYPE(I).EQ.'N')   CGROUP(I,1)='NH2'
                  IF(ATYPE(I).EQ.'O')   CGROUP(I,1)='OH'
                  IF(ATYPE(I).EQ.'S')   CGROUP(I,1)='SH'
                  IF(ATYPE(I).EQ.'CO')  CGROUP(I,1)='CHO'
                  IF(ATYPE(I).EQ.'CS')  CGROUP(I,1)='CHS'
             END IF
             IF(NBOND.EQ.2)   THEN
                  IF(ATYPE(I).EQ.'C')   CGROUP(I,1)='CH2'
                  IF(ATYPE(I).EQ.'Cd')  CGROUP(I,1)='CdH2'
                  IF(ATYPE(I).EQ.'N')   CGROUP(I,1)='NH'
                  IF(ATYPE(I).EQ.'Nd')  CGROUP(I,1)='NdH'
             END IF
             IF(NBOND.EQ.3)   THEN
                  IF(ATYPE(I).EQ.'C')   CGROUP(I,1)='CH'
                  IF(ATYPE(I).EQ.'Cd')  CGROUP(I,1)='CdH'
                  IF(ATYPE(I).EQ.'Ct')  CGROUP(I,1)='CtH'
             END IF
             IF(NBOND.EQ.8)   THEN
                  IF(ATYPE(I).EQ.'Ca')  CGROUP(I,1)='CaH'
             END IF
3000    CONTINUE

************************************************************************
*                                                                      *
*       Sorting & Setting up of Groups                                 *
*                                                                      *
************************************************************************
C      ----  Sorting of CGROUP & AATOMS---------------------------------
        K=0
        DO 8900 I=1,NATOM
             IF(CGROUP(I,1).EQ.' ')  GO TO 8900
             K=K+1
             DO 6000 L=1,4
                  CG(K,L)=CGROUP(I,L)
6000         CONTINUE
             DO 6800 K1=1,3
                  DO 6500 K2=K1+1,4
                       IF(CGROUP(I,K1).LT.CGROUP(I,K2)) THEN
                            W=CGROUP(I,K2)
                            CGROUP(I,K2)=CGROUP(I,K1)
                            CGROUP(I,K1)=W
                       END IF
                       IF(AATOMS(I,K1).LT.AATOMS(I,K2)) THEN
                            W=AATOMS(I,K2)
                            AATOMS(I,K2)=AATOMS(I,K1)
                            AATOMS(I,K1)=W
                       END IF
6500              CONTINUE
6800         CONTINUE

C       ---- Sorting of BATOMS  ----------------------------------------

             DO 8800 K1=1,11
                  DO 8500 K2=K1+1,12
                       IF(BATOMS(I,K1).LT.BATOMS(I,K2)) THEN
                             W=BATOMS(I,K2)
                             BATOMS(I,K2)=BATOMS(I,K1)
                             BATOMS(I,K1)=W
                       END IF
 8500             CONTINUE
 8800       CONTINUE
 8900   CONTINUE

C       ---- Deleting of Null Group ------------------------------------
        K=0
        DO 10000 I=1,NATOM
             IF(CGROUP(I,1).EQ.' ')   GO TO 10000
             K=K+1
             DO 9000 J=1,4
                  CGROUP(K,J)=CGROUP(I,J)
 9000             AATOMS(K,J)=AATOMS(I,J)
             DO 9500 J=1,12
 9500             BATOMS(K,J)=BATOMS(I,J)
10000   CONTINUE
        NGROUP=K

C       ---- Setting up of Group ---------------------------------------
        DO 20000 I=1,NGROUP
             GROUP(I)=CGROUP(I,1)//CGROUP(I,2)//CGROUP(I,3)//CGROUP(I,4)
     *              //AATOMS(I,1)//AATOMS(I,2)//AATOMS(I,3)//AATOMS(I,4)
     *              //BATOMS(I,1)//BATOMS(I,2)//BATOMS(I,3)//BATOMS(I,4)
     *              //BATOMS(I,5)//BATOMS(I,6)//BATOMS(I,7)//BATOMS(I,8)
     *           //BATOMS(I,9)//BATOMS(I,10)//BATOMS(I,11)//BATOMS(I,12)
20000   CONTINUE

************************************************************************
*                                                                      *
*       Groups for Printing                                            *
*                                                                      *
************************************************************************
      WRITE(25,*)'----<GROUP>------------------------------------------'
      DO 5000 I=1,NGROUP
         DO 3200 K1=2,3
            DO 3100 K2=K1+1,4
               IF(CG(I,K1).LT.CG(I,K2))  THEN
                  W=CG(I,K2)
                  CG(I,K2)=CG(I,K1)
                  CG(I,K1)=W
                END IF
3100        CONTINUE
3200     CONTINUE
         DO 3250 L=1,4
            CCCC(L)=CG(I,L)
3250     CONTINUE
         IF(CCCC(2).NE.' ')   THEN
            IF(CCCC(2).EQ.CCCC(4))   THEN
               CCCC(3)='3'
               CCCC(4)=' '
               GO TO 3300
            END IF
            IF(CCCC(3).NE.' ')    THEN
               IF(CCCC(2).EQ.CCCC(3))   CCCC(3)='2'
               IF(CCCC(3).EQ.CCCC(4))   CCCC(4)='2'
            END IF
         END IF
3300     DO 3350 J=1,4
            NAAA(J)=AATOMS(I,J)
            LENA(J)=INDEX(NAAA(J),' ')-1
            IF(LENA(J).EQ.0)  AAAA(J)=' '
            IF(LENA(J).NE.0)  AAAA(J)='('//NAAA(J)(:LENA(J))//')'
            LENC(J)=INDEX(CCCC(J),' ')-1
            LENA(J)=INDEX(AAAA(J),' ')-1
3350     CONTINUE
         DO 3400 J=1,12
            NBBB(J)=BATOMS(I,J)
            LENB(J)=INDEX(NBBB(J),' ')-1
            IF(LENB(J).EQ.0)  BBBB(J)=' '
            IF(LENB(J).NE.0)    THEN
               IF(J.EQ.1)  BBBB(J)=' --('//NBBB(J)(:LENB(J))//')'
               IF(J.NE.1)  BBBB(J)='('//NBBB(J)(:LENB(J))//')'
            END IF
            LENB(J)=INDEX(BBBB(J)(2:),' ')
3400     CONTINUE

         PGR=CCCC(1)(:LENC(1))//CCCC(2)(:LENC(2))//CCCC(3)
     *      (:LENC(3))//CCCC(4)(:LENC(4))//' -'//
     *      AAAA(1)(:LENA(1))//AAAA(2)(:LENA(2))//AAAA(3)(:LENA(3))//
     *      AAAA(4)(:LENA(4))//BBBB(1)(:LENB(1))//BBBB(2)(:LENB(2))//
     *      BBBB(3)(:LENB(3))//BBBB(4)(:LENB(4))//BBBB(5)(:LENB(5))//
     *      BBBB(6)(:LENB(6))//BBBB(7)(:LENB(7))//BBBB(8)(:LENB(8))//
     *      BBBB(9)(:LENB(9))//BBBB(10)(:LENB(10))//
     *      BBBB(11)(:LENB(11))//BBBB(12)(:LENB(12))
         WRITE(25,4500) I, PGR
4500     FORMAT(1H ,' GROUP(',I2,') = ',A60)
5000  CONTINUE

      RETURN
      END
C-----------------------------------------------------------------------
      Subroutine SGROUP
C-----------------------------------------------------------------------
C     Subroutine for Setting up Groups for Calculating log 1/S

C     Description of Variables
C        NAB    ..... Number of aromatic bonds
C        NTRING ..... Total number of rings
C        NCF    ..... Number of aromatic fuzed carbon atoms
C        KFRAG  ..... = FTYPE
C        AATOMS ..... A Atoms of FTYPE
C        CTYPE  ..... Carbons
C        XTYPE  ..... Halogens
C        LENF   ..... Length of FTYPE
C        LENA   ..... Length of A Atoms
C        NBOND  ..... Number of Bonds

      CHARACTER*5   ATYPE(100),KFRAG*6,AATOMS(100,4),FRAGMT(100)*20,
     *              NAAA(3),AAAA(3)*8,PFRAG(100)*20,W,CTYPE(5),
     *              XTYPE(4),FTYPE(100)
      INTEGER       LENF,LENA(3),NHAL,NHAL2,NHAL3,NHAL4,NBRN,NZHAL,
     *              NBOND(100)
      COMMON /CTI/  NATOM,MATCT(100,100)
      COMMON /CTA/  ATYPE
      COMMON /TEM/  TM
      COMMON /NUM/  NUM1,NUM2,NUM3
      COMMON /FRG/  FTYPE,FRAGMT,PFRAG
      COMMON /COR/  NRING,NBRN,NHAL2,NHAL3,NHAL4,NZHAL
      DATA(CTYPE(I),I=1,5)/'C','Cd','Ct','Ca','Cf'/
      DATA(XTYPE(I),I=1,4)/'F','Cl','Br','I'/

      IF(NUM2.EQ.0) THEN
      WRITE(25,*)'  Aqueous solubility is estimated via two pathways.'
      WRITE(25,*)'  The first is based  on  the combined handling of '
      WRITE(25,*)'two available group contribution methods of Irmann '
      WRITE(25,*)'[Chem.Ing.Tech., 37(1965)789-798] and Wakita et al.'
      WRITE(25,*)'[Chem. Pharm. Bull., 34(1986)4663-4681].  According'
      WRITE(25,*)'to the  compound  type,  this  program  selects  an'
      WRITE(25,*)'appropriate estimetion method.  The molecules whose'
      WRITE(25,*)'log 1/S values are not  available by  both  methods'
      WRITE(25,*)'can be  estimated  by the zero-th order additivity '
      WRITE(25,*)'scheme.                                           '
      WRITE(25,*)'   The second is based on the following regression '
      WRITE(25,*)'equation that correlate the aqueous solubility with'
      WRITE(25,*)'the 1-octanol/water partition coefficient.        '
      WRITE(25,*)'[T.Suzuki, J. Comput.-Aided Mol. Design, 5(1991)149'
      WRITE(25,*)'-166]                                             '
      WRITE(25,*)'                                                  '
      WRITE(25,*)'log 1/S =1.050logP + 0.00956(Tm-25) + 0.515 [S:mol/L]'
      WRITE(25,*)'                                                  '
      END IF
      NUM2  =1

1000  WRITE(25,*)'If the compound is Solid at 25C;'
      WRITE(25,*)'                    ====> Input Melting Point in C.'
      WRITE(25,*)'If Liquid or Melting Point is unavailable;'
      WRITE(25,*)'                    ====> Input 0 (zero).'
      WRITE(25,*)'(In such case log 1/S of the supercooled liquid will'
      WRITE(25,*)' be estimated.)'
      WRITE(25,*)'          '
1100  WRITE(25,*)' Tm ?  => '

c Modif par Dom
c      READ(*,*,ERR=1000) TM
	TM=0

      IF((TM.NE.0).AND.(TM.LT.25.0))  GO TO 1100
      IF(TM.EQ.0.0)  TM=25.0

***********************************************************************
*                                                                     *
*     Enumeration of the Number of Aliphatic Rings                    *
*                                                                     *
***********************************************************************
     
      NAB   =0
      NBONDS=0
      NTRING=0
      NRING =0
      NCF   =0
      DO 1200 I=1,NATOM
         IF(ATYPE(I).EQ.'Cf')   NCF=NCF+1
         DO 1200 J=I+1,NATOM
            IF(MATCT(I,J).NE.0)   THEN
               IF(MATCT(I,J).EQ.4)  NAB=NAB+1
               NBONDS=NBONDS+1
            END IF
1200  CONTINUE

      NTRING=NBONDS+1-NATOM

      IF(NAB.EQ.0)  THEN
            NRING=NTRING
         ELSE IF(NAB.EQ.6)  THEN
            NRING=NTRING-1
         ELSE IF((NAB.EQ.11).OR.(NAB.EQ.12))   THEN
            NRING=NTRING-2
         ELSE IF((NAB.GE.15).AND.(NAB.LE.18))  THEN
            NRING=NTRING-3
         ELSE IF(((NAB.GE.19).AND.(NAB.LE.22)).AND.(NCF.LT.8))   THEN
            NRING=NTRING-4
         ELSE IF(((NAB.EQ.23).OR.(NAB.EQ.24)).AND.(NCF.LT.8))    THEN
            NRING=NTRING-4
         ELSE IF(((NAB.EQ.23).OR.(NAB.EQ.24)).AND.(NCF.EQ.8))    THEN
            NRING=NTRING-5
         ELSE IF((NAB.EQ.25).OR.(NAB.EQ.26))   THEN
            NRING=NTRING-5
         ELSE IF(((NAB.GE.27).AND.(NAB.LE.30)).AND.(NCF.LT.8))   THEN
            NRING=NTRING-5
         ELSE IF(((NAB.GE.27).AND.(NAB.LE.30)).AND.(NCF.LE.10))  THEN
            NRING=NTRING-6
         ELSE IF((NAB.EQ.31).AND.((NCF.GE.6).AND.(NCF.LE.10)))   THEN
            NRING=NTRING-6
         ELSE IF(((NAB.GE.32).AND.(NAB.LE.35)).AND.(NCF.LE.8))   THEN
            NRING=NTRING-6
         ELSE IF((NAB.EQ.36).AND.(NCF.EQ.0))   THEN
            NRING=NTRING-6
         ELSE
1500        WRITE(25,*) 'Input Number of Aliphatic Rings ?  => '
            READ (*,*,ERR=1500) NRING
           IF(NRING.LT.0)  GO TO 1500
      END IF
      DO 1700 I=1,NATOM
           FTYPE(I)=ATYPE(I)
1700  CONTINUE
************************************************************************
*                                                                      *
*     (Ca)-CH2-OH   =>  (Ca)- CH2OH                                    *
*                                                                      *
************************************************************************
      INATOM=0
      DO 1800 I=1,NATOM
         IF(FTYPE(I).NE.'O')    GO TO 1800
         NBOND(I)=0
         DO 1720 J=1,NATOM
            NBOND(I)=NBOND(I)+MATCT(I,J)
            IF(MATCT(I,J).NE.0)   JJ=J
1720     CONTINUE
         IF(NBOND(I).EQ.1.AND.FTYPE(JJ).EQ.'C')    THEN
            NBOND(I)=0
            DO 1750 K=1,NATOM
               IF(K.EQ.I)      GO TO 1750
               NBOND(I)=NBOND(I)+MATCT(JJ,K)
               IF(MATCT(JJ,K).NE.0)   KK=K
1750        CONTINUE
            IF(NBOND(I).EQ.1.AND.FTYPE(KK).EQ.'Ca')  THEN
               FTYPE(JJ)='CH2OH'
               N=I
               DO 1780 L=I,NATOM
                  N=N+1
                  FTYPE(L)=FTYPE(N)
                  DO 1760 K=1,NATOM
1760                 MATCT(L,K)=MATCT(N,K)
                  DO 1770 K=1,NATOM
1770                 MATCT(K,L)=MATCT(K,N)
1780           CONTINUE
               INATOM=INATOM+1
            END IF
         END IF
1800  CONTINUE
      NATOM=NATOM-INATOM

************************************************************************
*                                                                      *
*     Fragment -  A Atoms                                              *
*                                                                      *
************************************************************************

      DO 3000 I=1,NATOM
         DO 2000 J=1,4
            AATOMS(I,J)=' '
2000     CONTINUE
         NB=0
         DO 2100 J=1,NATOM
            IF(MATCT(I,J).NE.0)     NB=NB+1
2100     CONTINUE
         IA=0
         DO 2200 L=1,NATOM
            IF(MATCT(I,L).EQ.0)   GO TO 2200
            IA=IA+1
            AATOMS(I,IA)=FTYPE(L)
            IF(FTYPE(I).EQ.'O'.AND.NB.EQ.1.AND.FTYPE(L).EQ.'C') THEN
               NBS=0
               DO 2150 M=1,NATOM
                  IF(MATCT(L,M).EQ.1)  NBS=NBS+1
2150           CONTINUE
               IF(NBS.EQ.2)    AATOMS(I,1)='Cpri'
               IF(NBS.EQ.3)    AATOMS(I,1)='Csec'
               IF(NBS.EQ.4)    AATOMS(I,1)='Cter'
               GO TO 3000
            END IF
2200     CONTINUE
3000  CONTINUE

************************************************************************
*                                                                      *
*     Enumaration of A-Halogens & Branches                             *
*                                                                      *
************************************************************************
      NHAL2=0
      NHAL3=0
      NHAL4=0
      NZHAL=0
      NBRN=0
      DO 3050 I=1,NATOM
         NBOND(I)=0
         DO 3002 J=1,NATOM
            NBOND(I)=NBOND(I)+MATCT(I,J)
3002     CONTINUE
         DO 3005 J=1,2
            IF(FTYPE(I).EQ.CTYPE(J))  GO TO 3008
3005     CONTINUE
         DO 3006 J=3,5
            IF(FTYPE(I).EQ.CTYPE(J))  GO TO 3025
3006     CONTINUE
         GO TO 3050

3008     NHAL=0
         DO 3020 K=1,4
            DO 3010 L=1,4
            IF(AATOMS(I,K).EQ.XTYPE(L))   NHAL=NHAL+1
3010     CONTINUE
3020     CONTINUE
         IF(AATOMS(I,3).NE.'    ')    NBRN=NBRN+1
         IF(AATOMS(I,4).NE.'    ')    NBRN=NBRN+1
         IF(NHAL.EQ.2)                 NHAL2=NHAL2+1
         IF(NHAL.EQ.3)                 NHAL3=NHAL3+1
         IF(NHAL.EQ.4)                 NHAL4=NHAL4+1
         IF(NHAL.NE.0)     THEN
            IF(FTYPE(I).NE.'C')                  GO TO 3025
            IF(NBOND(I).EQ.2.OR.NBOND(I).EQ.3)   NZHAL=NZHAL+1
         END IF
*      ----- Deletion of A-Atoms in C Fragment ---------------------
3025     DO 3030 M=1,3
            AATOMS(I,M)='    ' 
3030     CONTINUE
3050  CONTINUE
************************************************************************
*                                                                      *
*     Addition of H Atoms to Fragment                                  *
*                                                                      *
************************************************************************
      DO 4000 I=1,NATOM
         IF(NBOND(I).EQ.1)   THEN
            IF(FTYPE(I).EQ.'C')     FTYPE(I)='CH3'
            IF(FTYPE(I).EQ.'N')     FTYPE(I)='NH2'
            IF(FTYPE(I).EQ.'O')     FTYPE(I)='OH'
            IF(FTYPE(I).EQ.'S')     FTYPE(I)='SH'
            IF(FTYPE(I).EQ.'CO')    FTYPE(I)='CHO'
            IF(FTYPE(I).EQ.'CS')    FTYPE(I)='CHS'
         END IF
         IF(NBOND(I).EQ.2)   THEN
            IF(FTYPE(I).EQ.'C')     FTYPE(I)='CH2'
            IF(FTYPE(I).EQ.'Cd')    FTYPE(I)='CdH2'
            IF(FTYPE(I).EQ.'N')     FTYPE(I)='NH'
            IF(FTYPE(I).EQ.'Nd')    FTYPE(I)='NdH'
         END IF
         IF(NBOND(I).EQ.3)   THEN
            IF(FTYPE(I).EQ.'C')     FTYPE(I)='CH'
            IF(FTYPE(I).EQ.'Cd')    FTYPE(I)='CdH'
            IF(FTYPE(I).EQ.'Ct')    FTYPE(I)='CtH'
         END IF
         IF(NBOND(I).EQ.8)   THEN
            IF(FTYPE(I).EQ.'Ca')    FTYPE(I)='CaH'
         END IF
4000  CONTINUE

************************************************************************
*                                                                      *
*     Sorting of A-atoms                                               *
*                                                                      *
************************************************************************

      DO 8900 I=1,NATOM
         IF(AATOMS(I,1).EQ.' ')    GO TO 8900
         DO 6800 K1=1,2
            DO 6500 K2=K1+1,3
               IF(AATOMS(I,K1).LT.AATOMS(I,K2)) THEN
                  W=AATOMS(I,K2)
                  AATOMS(I,K2)=AATOMS(I,K1)
                  AATOMS(I,K1)=W
               END IF
6500        CONTINUE
6800     CONTINUE
8900  CONTINUE


C     ---- Setting up of Groups --------------------------------------
      DO 20000 I=1,NATOM
         FRAGMT(I)=FTYPE(I)//AATOMS(I,1)//AATOMS(I,2)//AATOMS(I,3)
20000 CONTINUE

************************************************************************
*                                                                      *
*     Groups for Printing                                              *
*                                                                      *
************************************************************************
      DO 5000 I=1,NATOM
         KFRAG=FTYPE(I)
         LENF=INDEX(KFRAG,' ')-1
3300     DO 3350 J=1,3
            NAAA(J)=AATOMS(I,J)
            LENA(J)=INDEX(NAAA(J),' ')-1
            IF(LENA(J).EQ.0)  AAAA(J)=' '
            IF(LENA(J).NE.0)  THEN
               IF(J.EQ.1)  AAAA(1)='-('//NAAA(J)(:LENA(J))//')'
               IF(J.NE.1)  AAAA(J)= '('//NAAA(J)(:LENA(J))//')'
            END IF
            LENA(J)=INDEX(AAAA(J),' ')-1
3350     CONTINUE

         PFRAG(I)=KFRAG(:LENF)//AAAA(1)(:LENA(1))//AAAA(2)(:LENA(2))
     *           //AAAA(3)(:LENA(3))
5000  CONTINUE

      RETURN
      END
C---------------------------------------------------------------------
      Subroutine SOLREG
C---------------------------------------------------------------------
C     Subroutine for Calculating Log P in Solvent/Water Systems

C     Estimation with Solvent Regression Equations
C     [Ref.] Leo, A., Hansch, C. and Elkins, D., Chem. Rev., 71(1971)
C      525.
C                         log Psolv = a log P + b
C       [Solvent]                [a]        [b]     [Solute Class]
C     Cyclohexane               0.675     -1.842           A
C                               1.063     -0.734           B
C     Heptane                   1.056     -2.851           A
C                               1.848     -2.223           B
C     Carbon tetrachloride      1.168     -2.163           A
C                               1.207     -0.219           B
C                               0.862     -0.626           N
C     Xylene                    0.942     -1.694           A
C                               1.027     -0.595           B
C     Toluene                   1.135     -1.777           A
C                               1.398     -0.922           B
C     Benzene                   1.015     -1.402           A
C                               1.223     -0.573           B
C     Chloroform                1.126     -1.348           A
C                               1.276      0.171           B
C                               1.100     -0.649           N
C     Oils                      1.099     -1.310           A
C                               1.119     -0.325           B
C     Ethyl Ether               1.130     -0.170           A
C                               1.142     -1.070           B
C     Nitrobenzene              1.176     -1.072           A
C     Isopentyl acetate         1.027      0.072           A
C     Oleyl alcohol             0.999     -0.575         A,B,N
C     Methyl isobutyl ketone    1.094      0.050           "
C     Ethyl acetate             0.932      0.052           "
C     Cyclohexanone             1.035      0.896           "
C     Primary pentanols         0.808      0.271           "
C     sec- and tert-Pentanols   0.892      0.288           "
C     2-Butanone                0.493      0.315           "
C     Cyclohexanol              0.745      0.866           "
C     Primary butanols          0.697      0.381           "
C     --------------------------------------------------------------
C        [Solute Classes]
C       ---------------------------------------------------------
C        Always  "A"    1. Acids     2. Phenols
C       ---------------------------------------------------------
C        Usually "A"    3. Barbiturates   4. Alcohols
C        "N" in CCl4    5. Amides(negatively substituted, but not
C        and CHCl3      di-N-substituted)   6. Sulfonamides
C                       7. Nitriles    8. Imides   *9. Amides
C       ---------------------------------------------------------
C                     *10. Aromatic amines(not di-N-substituted)
C                      11. Hydrocarbons   12. Aliphatic amines and
C                      imines   13. Tertiary amines(including
C        Always  "B"   ring N compounds)   14. Ethers
C                      15. Esters   16. Ketones    
C                      17. Compounds with intramolecular H-bonds
C                      (e.g., o-nitrophenol)
C                      18. Miscellaneous H-acceptors
C       ---------------------------------------------------------
C       *: These classes must be reversed when considering the ether
C          and oil solvent systems.

C     Description of Variables
C        LOGPS ..... log P in other solvent-water systems
      REAL   LOGP, LOGPS
      CHARACTER YN*1
      COMMON /LOG/ LOGP
      COMMON /NUM/ NUM1,NUM2,NUM3

1000  LOGPS=0.0
      WRITE(25,*)'Do you want to estimate log P in other solvent/water sy
     *stems (Y/N)? '

c Modif par Dom
1100  CONTINUE
c1100  READ(*,1200,ERR=1000)  YN
	YN='N'

1200  FORMAT(A1)
      IF(YN.EQ.'Y'.OR.YN.EQ.'y')   GO TO 2000
      IF(YN.EQ.'N'.OR.YN.EQ.'n')   GO TO 9000
      WRITE(25,*) 'Please press Y for yes, or N for no.  => '
      GO TO 1100

2000  IF(NUM3.EQ.0)  THEN
         WRITE(25,*)'This estimation is based on the solvent regression'
         WRITE(25,*)'equations given by Leo and Hansch [J. Org. Chem., '
         WRITE(25,*)'36(1971)1539-1544 ].                             '
         WRITE(25,*)' '
      END IF
      NUM3 = 1
************************************************************************
*                                                                      *
*     Choice of the Solvent System                                     *
*                                                                      *
************************************************************************
      WRITE(25,*) 'Choose the Solvent. '
      WRITE(25,*) ' 1. Cyclohexane      11. Isopentyl acetate     '
      WRITE(25,*) ' 2. Heptane          12. Oleyl alcohol         '
      WRITE(25,*) ' 3. Xylene           13. Methyl isobutyl ketone '
      WRITE(25,*) ' 4. Toluene          14. Ethyl acetate         '
      WRITE(25,*) ' 5. Benzene          15. Cyclohexanone         '
      WRITE(25,*) ' 6. Oils             16. Primary pentanols     '
      WRITE(25,*) ' 7. Ethyl ether      17. sec- & tert-Pentanols '
      WRITE(25,*) ' 8. CCl4             18. 2-Butanone            '
      WRITE(25,*) ' 9. CHCl3            19. Cyclohexanol          '
      WRITE(25,*) '10. Nitrobenzene     20. Primary butanols      '
      WRITE(25,*) '                                               '
      WRITE(25,*) ' Input the Number.                             '
      WRITE(25,*) '               No. =>  ? '
      READ(*,*,ERR=2000) SV
      IF(SV.LT.1.OR.SV.GE.21)    GO TO 2000
      IF(SV.EQ.12)    LOGPS = 0.999*LOGP - 0.575
      IF(SV.EQ.13)    LOGPS = 1.094*LOGP + 0.050
      IF(SV.EQ.14)    LOGPS = 0.932*LOGP + 0.052
      IF(SV.EQ.15)    LOGPS = 1.035*LOGP + 0.896
      IF(SV.EQ.16)    LOGPS = 0.808*LOGP + 0.271
      IF(SV.EQ.17)    LOGPS = 0.892*LOGP + 0.288
      IF(SV.EQ.18)    LOGPS = 0.493*LOGP + 0.315
      IF(SV.EQ.19)    LOGPS = 0.745*LOGP + 0.866
      IF(SV.EQ.20)    LOGPS = 0.697*LOGP + 0.381
      IF(SV.GE.12)    GO TO 8000
************************************************************************
*                                                                      *
*     Classification of the Solutes                                    *
*                                                                      *
************************************************************************
3000  WRITE(25,*) 'Choose the solute type.                        '
      WRITE(25,*) ' 1. Acids                                      '
      WRITE(25,*) ' 2. Phenols                                    '
      WRITE(25,*) ' 3. Barbiturates                               '
      WRITE(25,*) ' 4. Alcohols                                   '
      WRITE(25,*) ' 5. Amides(negatively substituted, but not di-N-sub
     *stituted)'
      WRITE(25,*) ' 6. Sulfonamides                               '
      WRITE(25,*) ' 7. Nitriles                                   '
      WRITE(25,*) ' 8. Imides                                     '
      WRITE(25,*) ' 9. Amides                                     '
      WRITE(25,*) '10. Aromatic Amines(not di-N-substituted)      '
      WRITE(25,*) '11. Hydrocarbons                               '
      WRITE(25,*) '12. Aliphatic Amines & Imines                  '
      WRITE(25,*) '13. Tertiary amines(including ring N compounds) '
      WRITE(25,*) '14. Ethers                                     '
      WRITE(25,*) '15. Esters                                     '
      WRITE(25,*) '16. Ketones                                    '
      WRITE(25,*) '17. Compounds with intramolecular H-bonds      '
      WRITE(25,*) '18. Miscellaneous H-acceptors                  '
      WRITE(25,*) '19. Others                                     '
      WRITE(25,*) ' Input the Number. '
      WRITE(25,*) '               No. => ? '
      READ(*,*,ERR=3000) SL
      IF(SL.EQ.19)              GO TO 8700
      IF(SL.LT.1.OR.SL.GE.20)   GO TO 3000
      IF(SV.EQ.1)   THEN
         IF(SL.LE.9)                   LOGPS = 0.675*LOGP - 1.842
         IF(SL.GE.10 .AND. SL.LE.18)   LOGPS = 1.063*LOGP - 0.734
      END IF
      IF(SV.EQ.2)   THEN
         IF(SL.LE.9)                   LOGPS = 1.056*LOGP - 2.851
         IF(SL.GE.10 .AND. SL.LE.18)   LOGPS = 1.848*LOGP - 2.223
      END IF
      IF(SV.EQ.3)   THEN
         IF(SL.LE.9)                   LOGPS = 0.942*LOGP - 1.694
         IF(SL.GE.10 .AND. SL.LE.18)   LOGPS = 1.027*LOGP - 0.595
      END IF
      IF(SV.EQ.4)   THEN
         IF(SL.LE.9)                   LOGPS = 1.135*LOGP - 1.777
         IF(SL.GE.10 .AND. SL.LE.18)   LOGPS = 1.398*LOGP - 0.922
      END IF
      IF(SV.EQ.5)   THEN
         IF(SL.LE.9)                   LOGPS = 1.015*LOGP - 1.402
         IF(SL.GE.10 .AND. SL.LE.18)   LOGPS = 1.223*LOGP - 0.573
      END IF
      IF(SV.EQ.6)   THEN
         IF(SL.LE.8 .OR. SL.EQ.10)     LOGPS = 1.099*LOGP - 1.310
         IF(SL.EQ.9 .OR.(SL.GE.11.AND.SL.LE.18))   THEN
                                       LOGPS = 1.119*LOGP - 0.325
         END IF
      END IF
      IF(SV.EQ.7)   THEN
         IF(SL.LE.8 .OR. SL.EQ.10)     LOGPS = 1.130*LOGP - 0.170
         IF(SL.EQ.9 .OR.(SL.GE.11.AND.SL.LE.18))   THEN
                                       LOGPS = 1.142*LOGP - 1.070
         END IF
      END IF
      IF(SV.EQ.8)   THEN
         IF(SL.LE.2)                   LOGPS = 1.168*LOGP - 2.163
         IF(SL.GE.3 .AND. SL.LE.9)     LOGPS = 0.862*LOGP - 0.626
         IF(SL.GE.10.AND. SL.LE.18)    LOGPS = 1.207*LOGP - 0.219
      END IF
      IF(SV.EQ.9)   THEN
         IF(SL.LE.2)                   LOGPS = 1.126*LOGP - 1.348
         IF(SL.GE.3 .AND. SL.LE.9)     LOGPS = 1.100*LOGP - 0.649
         IF(SL.GE.10.AND. SL.LE.18)    LOGPS = 1.276*LOGP + 0.171
      END IF
      IF(SV.EQ.10)  THEN
         IF(SL.LE.9)                   LOGPS = 1.176*LOGP - 1.072
         IF(SL.GE.10)                  GO TO 8700
      END IF
      IF(SV.EQ.11)  THEN
         IF(SL.LE.9)                   LOGPS = 1.027*LOGP + 0.072
         IF(SL.GE.10)                  GO TO 8700
      END IF

************************************************************************
*                                                                      *
*     Print Out of log Psolv                                           *
*                                                                      *
************************************************************************

8000  WRITE(25,*) ' *** Estimated log Psolv Value *** '
      WRITE(25,8500) LOGPS
8500  FORMAT('    log P(solv/water) =',F6.2)
      WRITE(25,*) ' ********************************* '
      GO TO 1000

8700  WRITE(25,*) 'This solvent system can not be estimated. '
      WRITE(25,*) '                                         '
      GO TO 1000

9000  RETURN
      END
  
