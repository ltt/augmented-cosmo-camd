C-----------------------------------------------------------------------
       Subroutine AUGTDA
C-----------------------------------------------------------------------
C      Subroutine for Perception of Augmented Atoms

       Character*5 ATYPE(100)
       Common /CTI/NATOM,MATCT(100,100)
       Common /CTA/ATYPE

************************************************************************
*                                                                      *
*     - C # N       =>     " CN "       - N = C = S   =>     " NCS "   *
*                                                                      *
*                                          O                           *
*     - N = O       =>     " NO "         ||          =>     " SO "    *
*                                        - S -                         *
*                                                                      *
*     = N = O       =>     " NaO "         O                           *
*       |                                 ||                           *
*                                        - S -        =>     " SO2 "   *
*       O                                  ||                          *
*      ||                                  O                           *
*     - N = O       =>     " NO2 "                                     *
*                                                                      *
*       O                               - I = O       =>     " IO  "   *
*       ||          =>     " CO "                                      *
*     - C -                                                            *
*                                         O                            *
*       S                                 ||                           *
*       ||          =>     " CS "       - I = O       =>     " IO2 "   *
*     - C -                                                            *
*                                                                      *
************************************************************************

       DO 5000 I=1,NATOM

            IF(ATYPE(I).EQ.'N')    THEN
                 IO = 0
                 INa= 0
                 DO 1000 J=1,NATOM
                      IF(MATCT(I,J).EQ.2)   THEN
                           IF(ATYPE(J).NE.'O')   GO TO 1000
                           ATYPE(J)=' '
                           IO=IO+1
                      END IF
                      IF(MATCT(I,J).EQ.3)   THEN
                           IF(ATYPE(J).NE.'C')   GO TO 1000
                           ATYPE(J)='CN'
                           ATYPE(I)='  '
                      END IF
                      IF(MATCT(I,J).EQ.4)   INa=1
 1000            CONTINUE
                 IF(IO.EQ.1)   THEN
                      IF(INa.EQ.0)   ATYPE(I)='NO'
                      IF(INa.EQ.1)   ATYPE(I)='NaO'
                 END IF
                 IF(IO.EQ.2)       ATYPE(I)='NO2'
            END IF

            IF(ATYPE(I).EQ.'O')    THEN
                 DO 2000 J=1,NATOM
                      IF(MATCT(I,J).NE.2)  GO TO 2000
                      IF(ATYPE(J).NE.'C')  GO TO 2000
                      ATYPE(J)='CO'
                      ATYPE(I)='  '
 2000            CONTINUE 
            END IF

            IF(ATYPE(I).EQ.'S')    THEN
                 IO=0
                 DO 3000 J=1,NATOM
                      IF(MATCT(I,J).NE.2)   GO TO 3000
                      IF(ATYPE(J).EQ.'O')   THEN
                           ATYPE(J)='  '
                           IO=IO+1
                      END IF
                      IF(ATYPE(J).EQ.'C')   THEN
                           ATYPE(J)='CS'
                           ATYPE(I)='  '
                           DO 3500 K=1,NATOM
                                IF(K.EQ.I)            GO TO 3500
                                IF(MATCT(J,K).NE.2)   GO TO 3500
                                IF(ATYPE(K).NE.'N')   GO TO 3500
                                ATYPE(K)='NCS'
                                ATYPE(J)='   '
 3500                      CONTINUE
                      E