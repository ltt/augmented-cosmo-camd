#!/usr/bin/perl


print"SUBROUTINE CHILD.pl OK \n";

sub child{

	# to allow child optimization: see AUTO_ADAPT.pl

	$sumoperator=$param{PROBA_CROSS}/2+$param{PROBA_MUT};

	#foreach $j ($param{ELITISM}..$param{POP}-1){
	$j=$param{ELITISM};
	
	while($j <= ($param{POP}-1)){
	# fill $molchild[]=$mol[] and $molpingchild[]=$molping[];

		$molchild[$j]="";
		$molpingchild[$j]="";
		
		$operator=int(rand($sumoperator))+1;
		
		$success=0;
		$nbessaimax=1000;
		
		#remark: $poids from mol0 mol1 ... not ordered !


# /////////////////////////////////////////////////////////////////////////////////////////////
# /////////////////////////////////////////////////////////////////////////////////////////////
 		
		#$param{PROBA_CROSS is divided by 2 to take account for both children
		if($param{POP} >= 2 && $operator <= ($param{PROBA_CROSS}/2) && $param{LONGGEN}!=1){
		#crossover
	
			#first parent
			$m1=-1;
			$nbessai=0;
			
			while (($m1 == -1 || $mol[$m1]!~/-/) && $nbessai <= $nbessaimax){ # au moins une liaison !wenigsten eine Verbindung --Yifan
				
				$nbessai++;
				
				$nb2=rand($sumpoids); # from SCALE.pl sum of all poids--Yifan
				$m1=0;
				$sumpar=$poids[0]; # the score in SCALE.pl --Yifan
				
				while ($nb2>$sumpar){
					$m1++;
					$sumpar=$sumpar+$poids[$m1];
				};
			};
			
			if($nbessai < $nbessaimax){
				
				#second parent
				$m2=-1;
				$nbessai2=0;
				
				while (($m2 == -1 || $mol[$m2]!~/-/ || $m2==$m1) && $nbessai2 <= $nbessaimax){
					
					$nbessai2++;
					$nb2=rand($sumpoids);
					$m2=0;
					$sumpar=$poids[0];
					$param
					while ($nb2>$sumpar){
						
						$m2++;
						$sumpar=$sumpar+$poids[$m2];
					};
				};
				
				if($nbessai2 < $nbessaimax && $mol[$m1]=~/-/ && $mol[$m2]=~/-/){
					
					print "\nparent1= $m1 and parent2= $m2    \n";
				
                    # split parent 1	
					#dvt
					#$mol[$m1]="1*1-2*2_2*3-3*6_2*4-4*5_3*7-5*8_5*9-6*10";
					#$molping[$m1]="24 1 8 125 10 3";
					#$mol[$m1]="1*1-2*1_2*1-3*6_3*3-4*6_1*1-5*1";
					#$molping[$m1]=" 2 2 1 1  6 ";

					$fgt=$mol[$m1];
					$fgtping=$molping[$m1];
					@getfgt=split('_',$fgt);
					
					$break=int(rand(@getfgt));
					
					#dvt
					#$break=1;
					
					#print "\nmol$m1 $fgt / $fgtping\n";
					#print "break $break : $getfgt[$break]\n";
					
					&separate;		
					#@child1 new numbering scheme for 1st fragment
					#@child2 new numbering scheme for 2nd fragment
					#$childping1 and $childping2 no of legos
					
					@cross1="";
					@cross1b="";
					
					if($param{SCAFFOLD} ne '0' && $childping2=~/ 0 /){ # decided by the phase, which build the 0 generation
						@cross1=@child2;
						@cross1b=@child1;
						$crossping1=$childping2;
						$crossping1b=$childping1;

						#$op_where[$j]="$getfgt4[$m1fgt2-1]";#from &separate
					}
					else{	
						@cross1=@child1;
						@cross1b=@child2; 
						$crossping1=$childping1;
						$crossping1b=$childping2;

						#$op_where[$j]="$getfgt4[$m1fgt1-1]";#from &separate
					};	

				# split parent 2	
				
					#dvt
					#$mol[$m2]="1*5-2*5_1*3-3*1_2*3-4*6_4*3-5*1";
					#$molping[$m2]=" 3 3  6  1 5";

					$fgt=$mol[$m2];
                    $fgtping=$molping[$m2];	
					@getfgt=split('_',$fgt);
					$break=int(rand(@getfgt));
					#print "\nmol$m2 $fgt / $fgtping\n";
					#print "break $break : $getfgt[$break]\n";

					#dvt
					#$break=3;

					&separate;

					@cross2="";
					@cross2b="";
					
					if($param{SCAFFOLD} ne '0' && $childping2=~/ 0 /){
						@cross2=@child2;
						@cross2b=@child1;
						$crossping2=$childping2;
						$crossping2b=$childping1;

						#$op_where[$j]=$op_where[$j]."_$getfgt4[$m1fgt1-1]"; #from &separate
					}
					else{
						@cross2=@child1;
						@cross2b=@child2;
						$crossping2=$childping1;
						$crossping2b=$childping2;

						#$op_where[$j]=$op_where[$j]."_$getfgt4[$m1fgt2-1]"; #from &separate
					};	
##########################################################################
#    --Yifan 
#   03.11.2017
##########################################################################

					
					## First child
				# combine @cross1 and @cross2b (2 and 1b are not used so far)
					$longtab=@cross1;
					
                    if($longtab==1){
						$molpingchild[$j]=$crossping1;
					}
					elsif($longtab > 1){	
						
						$molpingchild[$j]=$crossping1;
						
						foreach $k (1..@cross1-1){
						
							if($molchild[$j] eq ""){
							
								$molchild[$j]=$cross1[$k];
							}
							else{
								$molchild[$j]=$molchild[$j]."_".$cross1[$k];
							};	
						};	
					};
					#print "partial: molchild: $molchild[$j] / $molpingchild[$j]\n";
					
					@getfgt=split(' ',$molpingchild[$j]); # crossping1 --Yifan
					
					$nbfgt=@getfgt;
				
					# connect 1 from cross1 to 1 from cross2b
					@getfgt2=split('\*',$cross2b[0]); 
					
					$p1=$getfgt2[0]+$nbfgt; # number begin at 1, so musst add cross1
					
					if($molchild[$j] eq ""){
						$molchild[$j]=$cross1[$k]."-".$p1."*$getfgt2[1]"; # where comes $k from??--Yifan
					}
					else{	
						$molchild[$j]=$molchild[$j]."_".$cross1[$k]."-".$p1."*$getfgt2[1]"; 
					};
					$molpingchild[$j]=$molpingchild[$j]." ".$crossping2b;
					
					
					foreach $k (1..@cross2b-1){
						@getfgt2=split('-',$cross2b[$k]);
						@getfgt3=split('\*',$getfgt2[0]);
						@getfgt5=split('\*',$getfgt2[1]);
						$p1=$getfgt3[0]+$nbfgt;
						$p2=$getfgt5[0]+$nbfgt;
						$molchild[$j]=$molchild[$j]."_".$p1."*$getfgt3[1]"."-".$p2."*$getfgt5[1]";
					};
					$molpingchild[$j]=~s/  / /g;
					$scorepopchild[$j]="not calculated";
					print "molchild[$j]: $molchild[$j] / $molpingchild[$j] (score=$scorepopchild[$j])\n";

					$op_which[$j]="crossover";
					
					if($scorepop[$m1] >= $scorepop[$m2]){ # origin score of parent
						$op_scorep[$j]=$scorepop[$m1];
					}
					else{
                        $op_scorep[$j]=$scorepop[$m2];
					};

				## second child
				#combine @cross2 and @cross1b
					
					# next child
					$j++;
					$molchild[$j]="";
					$molpingchild[$j]="";

					$longtab=@cross2;
					if($longtab==1){
						$molpingchild[$j]=$crossping2;
					}
					elsif($longtab > 1){
						$molpingchild[$j]=$crossping2;
						foreach $k (1..@cross2-1){
							if($molchild[$j] eq ""){
								$molchild[$j]=$cross2[$k];
							}
							else{
								$molchild[$j]=$molchild[$j]."_".$cross2[$k];
							};
						};
					};
					#print "partial: molchild: $molchild[$j] / $molpingchild[$j]\n";
					
					@getfgt=split(' ',$molpingchild[$j]);
					$nbfgt=@getfgt;

					# connect 1 from cross2 to 1 from cross1b
					@getfgt2=split('\*',$cross1b[0]);
					$p1=$getfgt2[0]+$nbfgt;
					if($molchild[$j] eq ""){
						$molchild[$j]=$cross2[$k]."-".$p1."*$getfgt2[1]";
					}
					else{
						$molchild[$j]=$molchild[$j]."_".$cross2[$k]."-".$p1."*$getfgt2[1]";
					};	
					$molpingchild[$j]=$molpingchild[$j]." ".$crossping1b;

					foreach $k (1..@cross1b-1){
						@getfgt2=split('-',$cross1b[$k]);
						@getfgt3=split('\*',$getfgt2[0]);
						@getfgt5=split('\*',$getfgt2[1]);
						$p1=$getfgt3[0]+$nbfgt;
						$p2=$getfgt5[0]+$nbfgt;
						$molchild[$j]=$molchild[$j]."_".$p1."*$getfgt3[1]"."-".$p2."*$getfgt5[1]";
					};
					$molpingchild[$j]=~s/  / /g;
					$scorepopchild[$j]="not calculated";

					print "molchild[$j]: $molchild[$j] / $molpingchild[$j] (score=$scorepopchild[$j])\n";

					$success=1;

					$op_which[$j]="crossover";
					if($scorepop[$m1] >= $scorepop[$m2]){
						$op_scorep[$j]=$scorepop[$m1];
					}
					else{
						$op_scorep[$j]=$scorepop[$m2];
					};
				}
				else{
					print "Select MUTATION: crossover failed in parent2 selection\n";
				};	
			}
			else{
				print "Select MUTATION: crossover failed in parent1 selection\n";
			};		
		};   # end crossover --Yifan

# /////////////////////////////////////////////////////////////////////////////////////////////
# /////////////////////////////////////////////////////////////////////////////////////////////
 	
		$successm=0;
		if($operator > $param{PROBA_CROSS}/2 || $success==0){
			
			if($operator <= $param{PROBA_CROSS}/2){ # do not think it is useful--Yifan
				print "Select MUTATION: crossover not available Parameter POP = $param{POP} (must be >= 2)\n" if($param{POP} < 2);
				print "Select MUTATION: crossover not available Parameter LONGGEN = $param{LONGGEN} (must be 0 or > 1)\n" if($param{LONGGEN}==1);
			};
			
		#mutation
				
			#first parent	
			$nb2=rand($sumpoids);
			$m1=0;
			$sumpar=$poids[0];
			while ($nb2>$sumpar){
				$m1++;
				$sumpar=$sumpar+$poids[$m1];
			};
			#print "parent1= $m1\n";
			$fgt=$mol[$m1];
			$fgtping=$molping[$m1];
			@getsize=split(' ',$fgtping);
			$sizefgt=@getsize;

			#$probai can be modulated by autoadapt
			$typemutation=$proba1+$proba2+$proba3+$proba4;
			$quelmut=int(rand($typemutation))+1;
			if($quelmut <= $proba1){
				$nb2=1;
			}	
			elsif($quelmut <= ($proba1+$proba2)){
				$nb2=2;
			}
			elsif($quelmut <= ($proba1+$proba2+$proba3)){
				$nb2=3;
			}
			else{
				$nb2=4;
			};	
			
			if($nb2==2 && $param{LONGGEN}==$sizefgt && $param{LONGGEN} > 0){
				$nb2=3;
			};	
			
			#dvt
			#$nb2=2;
			
			#$nb2=1 suppress one fgt/part
			#$nb2=2 add one fgt
			#$nb2=3 replace one fgt
			#$nb2=4 either permutation of 2 substituents or move one substituent on another place
			#      or scramble around fgt with more than one <POINT>

####################################

			if($nb2==1){ #suppress one fragment, keep unchanged if nbfgt == 1
				print "\nsuppress fragment(s) into mol$m1 $fgt / $fgtping (score=$scorepop[$m1])\n";
				
				@getfgt=split('_',$fgt);
				$longtabsup=@getfgt;

				if($optimischild){  # in autpadapt.pl is 0--Yifan

					#try all possibilities
					$bestscore=0;
					$bestmolpingchild="";
					$bestmolchild="";
					$besti=0;
					
					foreach $bri (0..$longtabsup-1){
						
                        $break=$bri;

						&separate;

						@mut1="";
						@mut2="";
						$mutping1="";
						$mutping2="";
						@mut1=@child1;
						$mutping1=$childping1;
						@mut2=@child2;
						$mutping2=$childping2;

						foreach $brj (1..2){
							$molchild[$j]="";
							$molpingchild[$j]="";
						
							if($brj==1){
								@mut="";
								@mut=@mut1;
								$mutping=$childping1;

								#$op_where[$j]="$getfgt4[$m1fgt1-1]"; # from &separate
							}
							else{
								@mut="";
								@mut=@mut2;
								$mutping=$childping2;
								
								#$op_where[$j]="$getfgt4[$m1fgt2-1]"; # from &separate
							};

							if($param{SCAFFOLD} eq '0' || ($param{SCAFFOLD} ne '0' && $mutping=~/ 0 /)){
								$longtab=@mut;
								if($longtab==1){
									$molpingchild[$j]=$mutping;
									$molchild[$j]=$mut[0];
								}
								else{
									$molpingchild[$j]=$mutping;
									foreach $k (1..@mut-1){
										if($molchild[$j] eq ""){
											$molchild[$j]=$mut[$k];
										}		
										else{
											$molchild[$j]=$molchild[$j]."_".$mut[$k];
										};
									};	
								};		
								$molpingchild[$j]=~s/  / /g;

								#Build, Optimis and Score this child
								print "test $molchild[$j] / $molpingchild[$j]\n";
								&bos($j,"child");
								if(-e "mol.sdf" && !-z "mol.sdf"){
									$besti++;
									if($bestscore < $score[$ranking[1]]){
										$bestscore=$score[$ranking[1]];
										$bestmolchild=$molchild[$j];
										$bestmolpingchild=$molpingchild[$j];
									};
								};	
							};	
						};#foreach $brj	

					};#foreach $bri
				};

				if($optimischild && $bestmolchild ne ""){
					$successm=1;
					$molchild[$j]=$bestmolchild;
					$molpingchild[$j]=$bestmolpingchild;
					$scorepopchild[$j]=$bestscore;
					print "Child $j ($besti tests): select $molchild[$j] / $molpingchild[$j] (score=$scorepopchild[$j])\n";
				}
				elsif($optimischild==0 || $bestmolchild eq ""){ # in our case --Yifan
					# random
					
					$molchild[$j]="";	
					$molpingchild[$j]="";

					@getfgt=split('_',$fgt); 
					$break=int(rand(@getfgt));
					&separate;
					print "keep the more complex => child1: @child1 / $childping1 ; child2: @child2 / $childping2\n";
					$longtab=@child1;
					$longtab2=@child2;
					if(($param{SCAFFOLD} eq '0' && $longtab >= $longtab2) || ($param{SCAFFOLD} ne '0' && $childping1=~/ 0 /)){
						@mut="";
						@mut=@child1; #keep first part
						$mutping=$childping1;

						 #$op_where[$j]="$getfgt4[$m1fgt1-1]"; # from &separate
					}
					else{
						@mut="";
						@mut=@child2; #keep second part
						$mutping=$childping2;

						#$op_where[$j]="$getfgt4[$m1fgt2-1]"; # from &separate
					};

					$longtab=@mut;
					
					if($longtab==1){
						$molpingchild[$j]=$mutping;
						$molchild[$j]=$mut[0];
					}
					else{
						$molpingchild[$j]=$mutping;
						foreach $k (1..@mut-1){
							if($molchild[$j] eq ""){
								$molchild[$j]=$mut[$k];
							}
							else{
								$molchild[$j]=$molchild[$j]."_".$mut[$k];
							};
						};

					};	
					$molpingchild[$j]=~s/  / /g;
					$scorepopchild[$j]="not calculated";
							
					if($molchild[$j] eq ""){
						print "\nWarning! suppress one fragment in mol$m1 failed !\n";
					}
					else{
						$successm=1;
						print "molchild[$j] (random): select $molchild[$j] / $molpingchild[$j] (score=$scorepopchild[$j])\n";
					};
				};	
			
				#STUDIES on operator efficiency
				$op_which[$j]="suppress";
				$op_scorep[$j]=$scorepop[$m1];
				
				if($successm){
					$op_where[$j]="";
					@brique=split(' ',$fgtping);
					$testbrique=" ".$molpingchild[$j]." ";
					
					foreach $briquei (0..@brique-1){
						# which lego has been removed
						if($testbrique!~/ $brique[$briquei] /){
							$op_where[$j]=$op_where[$j]." ".$brique[$briquei];
						};
					};
				};

####################################
			}
			elsif($nb2==2){ #add one fragment as in initialize pop in MAIN. if no free anchor: keep unchanged

				$molchild[$j]="";
				$molpingchild[$j]="";

				#try all possibilities
				$bestscore=0;
				$bestmolpingchild="";
				$bestmolchild="";
				$besti++;

				#preliminary test
				$tmpa=&getfreeanchor($fgt,$fgtping); #null if no free anchor !

				if($tmpa eq ""){
					print "\nadd one fragment into mol$m1 $fgt / $fgtping failed: no free anchor\n";
				}	
				else{	
					print "\nadd one fragment into mol$m1 $fgt / $fgtping (score=$scorepop[$m1])\n";
					@getfgt=split(' ',$fgtping);
					$newfgt=@getfgt;
					$newfgt++;

					if($optimischild){

						############################
						#drive the selection toward fullfilling cg of the current best solution or (the fingerref if it exists)
						#use the difference
						if($evaluate_finger){

						#sdfparent $m1
						$sdfparent="g".$generation."_mol".($m1+1).".sdf";
						foreach $k (0..@memo_gi-1){
							 if($memo_gen[$k]==$generation && $memo_gi[$k]==$m1){
						 		$tmpsdf=$memo_sdf[$k];
								open(OUY,">$sdfparent");
									print OUY "$tmpsdf";
								close(OUY);
							};
						};
						chop($tmpfinger=`$leaexe/similarities_GC.pl tanimoto occ $sdfelite $sdfparent`);
						#print "sim:\n $tmpfinger\n";
						@tmpfinger2=split('\n',$tmpfinger);
						$tmpfinger=$tmpfinger2[3];
						unlink "$sdfparent";
						@tmpfingerdiff=split(' ',$tmpfinger);
						$fingerdiff="";
						foreach $k (0..@tmpfingerdiff-1){
							$k2=$k+1;
							$fingerdiff=$fingerdiff." ".$k2." " if($tmpfingerdiff[$k] > 0);
						};
						#print "diff:\n $fingerdiff\n";
						@tmpfingerdiff=split(' ',$fingerdiff);
						$nbdiff=@tmpfingerdiff;

						}#if($evaluate_finger){
						else{ 
						# use the content of the best itself
							chop($tmpfinger=`$leaexe/fingerprint_GC.pl bit $sdfelite`);
							@tmpfingerdiff=split(' ',$tmpfinger);
							$nbdiff=@tmpfingerdiff;
						};

						############################
						
						if($nbdiff!=0){

							#get all elligible legos
							@getlistkani="";
							$glki=0;

							#dvt
							if($flag_privilege==1){
								print "Add: subset created from privileged legos\n";
								@getcell=split(' ',$privileged_lego);
								foreach $kani (0..@getcell-1){
									$getlistkani[$glki]=$getcell[$kani];
									$glki++;
								};
							}
							else{
							foreach $kani (1..@lego-1){
								@getlegogc=split(' ',$legogc[$kani]);
								$nblegogc=@getlegogc;
								$nbvu=0;
								foreach $k (0..@tmpfingerdiff-1){
									$nbvu++ if($legogc[$kani]=~/ $tmpfingerdiff[$k] /);
								};
								#here the fgt has to be big to bring most of the expected molecule
								#$pcvu=($nbvu/$nbdiff)*100;
								# here most of the atoms of the fgt must belong to the expected molecule
								#print "nblegogc=$nblegogc\n";
								if($nblegogc!=0){
									# correction relative to X points
									#$nblegogc=$nblegogc-$typelego[$kani] if(($nblegogc-$typelego[$kani]) > 0); 
									$pcvu=($nbvu/$nblegogc)*100;
								}
								else{
									$pcvu=rand(100);
								};	
								
								if($pcvu >= $pcvumax){
									#print "$kani bring $pcvu: $lego[$kani] $legono[$kani]\n";
									$getlistkani[$glki]=$kani;
									$glki++;
								};
							};
							};#if($flag_privilege)

							# get random lego from the sub-selected set
							$nbgetlistkani=@getlistkani;
							if($nbgetlistkani > 0 && $getlistkani[0] ne ""){ 
							$nbtestadd=1;
							foreach $glki (0..@getlistkani-1){
								if($nbtestadd <= $nbtestmutationmax){
									$randkani=int(rand($nbgetlistkani));
									$kani=$getlistkani[$randkani];
									print "select $kani from sub-selection ($nbgetlistkani hits with >= $pcvumax \% or from privileged legos)\n";
									$nbtestadd++;
									@getpointchild=split(' ',$points[$kani]);
									$brilower=0;
									$briupper=@getpointchild-1;

									if($optimisposl==0){
										$brilower=int(rand(@getpointchild));
										$briupper=$brilower;
									};

									foreach $bri ($brilower..$briupper){
										$noanchor=$getpointchild[$bri];
										$tmpmol=$kani;
										$tmpb=$newfgt."*".$noanchor;
										print "try and score lego no $tmpmol at anchor position $noanchor (from anchors $points[$kani])-> fgt number $newfgt in molchild$j\n";

										#select the anchor site list and try all possibilities
										if($optimisposp==1){
											$listtmpa=&getfreeanchor($fgt,$fgtping,"list");
										}
										else{# get the $tmpa generated above
											$listtmpa=$tmpa;
										};

										print "try list: $listtmpa\n";

										@getlistfreeanchor=split(' ',$listtmpa);
										foreach $brj (0..@getlistfreeanchor-1){
											if($newfgt==2){ #tmpa contains all possibilities
												#old
												#if($newfgt==2){ #dont use tmpa !
												#$molchild[$j]=$fgt."-".$tmpb;
												
												$molchild[$j]=$getlistfreeanchor[$brj]."-".$tmpb;
												$molpingchild[$j]=$fgtping." $tmpmol";
											}	
											else{	
												#old
												#$molchild[$j]=$fgt."_".$tmpa."-".$tmpb;
												
												$molchild[$j]=$fgt."_".$getlistfreeanchor[$brj]."-".$tmpb;
												$molpingchild[$j]=$fgtping." $tmpmol";
											};

											#Build, Optimis and Score this child
											print "$molchild[$j] / $molpingchild[$j]\n";
											&bos($j,"child");
											if(-e "mol.sdf" && !-z "mol.sdf"){
												$besti++;
												if($bestscore < $score[$ranking[1]]){
													$bestscore=$score[$ranking[1]];
													$bestmolchild=$molchild[$j];
													$bestmolpingchild=$molpingchild[$j];
												};
											};

										};#foreach $brj	
									};#foreach $bri
								};#$nbtestadd <= $nbtestmutationmax
							};#foreach $glki
							};
						};#if($nbdiff!=0)
					};#$optimischild
					
					if($optimischild && $bestmolchild ne ""){
						$successm=1;
						$molchild[$j]=$bestmolchild;
						$molpingchild[$j]=$bestmolpingchild;
						$scorepopchild[$j]=$bestscore;
						print "Child $j ($besti tests): select $molchild[$j] / $molpingchild[$j] (score=$scorepopchild[$j])\n";
					}
					elsif($optimischild==0 || $bestmolchild eq ""){
						# random
						
						$molchild[$j]="";
						$molpingchild[$j]="";

						print "no lego bring >= $pcvumax \%: try random selection\n" if($bestmolchild eq "" && $optimischild==1 && $nbdiff!=0);
						print "difference in GC = 0 -> try random selection\n" if($nbdiff==0 && $optimischild==1);

						$tmpmol=&getlego(any);
						$tmpmol=~s/(.*)\*(.*)/$1/;
						$tmpb=$newfgt."*".$2;
						print "select lego $tmpmol at anchor position $2 (from anchors $points[$tmpmol])-> fgt number $newfgt in molchild[$j]\n";
						 
						if($newfgt==2){
							$molchild[$j]=$fgt."-".$tmpb;
							$molpingchild[$j]=$fgtping." $tmpmol";
						}
						else{
							$molchild[$j]=$fgt."_".$tmpa."-".$tmpb;
							$molpingchild[$j]=$fgtping." $tmpmol";
						};
						$scorepopchild[$j]="not calculated";

						if($molchild[$j] eq ""){
							print "\nWarning! add one fragment into mol$m1 $fgt / $fgtping failed\n";
						}
						else{
							$successm=1;
							print "molchild[$j] (random): select $molchild[$j] / $molpingchild[$j] (score=$scorepopchild[$j])\n";
						};

					};

				}; #if($tmpa eq "")

				#STUDIES on operator efficiency
				$op_which[$j]="add";
				$op_scorep[$j]=$scorepop[$m1];
				if($successm){
					$op_where[$j]="";
					@brique=split(' ',$molpingchild[$j]);
					$testbrique=" ".$fgtping." ";
					foreach $briquei (0..@brique-1){
						# which lego has been added:
						if($testbrique!~/ $brique[$briquei] /){
							$op_where[$j]=$op_where[$j]." ".$brique[$briquei];
						};
					};
				};	


####################################				
			}
			elsif($nb2==3){ #replace one fragment
				print "\nreplace one fragment into mol$m1 $fgt / $fgtping (score=$scorepop[$m1])\n";
			
				$molchild[$j]="";
				$molpingchild[$j]="";

				#try all possibilities
				$bestscore=0;
				$bestmolchild="";
				$bestmolpingchild="";
				$besti=0;
			
				if($optimischild){

					@getfgt=split(' ',$fgtping);
					$longtabreplace=@getfgt;
					$brmlower=0;

					if($optimisposp==0){
						$brmlower=int(rand(@getfgt));
						$longtabreplace=$brmlower+1;
					};

					foreach $brm ($brmlower..$longtabreplace-1){

						$molchild[$j]="";
						$molpingchild[$j]="";

						$selectfgt=$brm; #position $selectfgt and fgt no ($selectfgt+1)
						@getfgt=split(' ',$fgtping);
						$selectnolego=$getfgt[$selectfgt];#lego no $selectnolego
						$selectfgtinmol=$selectfgt+1; #fragment number in mol[]

						if($param{SCAFFOLD} ne '0' && $selectnolego==0){
							print "replace this fragment into mol$m1 $fgt / $fgtping failed, scaffold lego 0 cannot be replaced !\n";
						}	
						else{

							#$op_where[$j]="$selectnolego";

							#constraint = number of occupied anchors
							$whichfgtbis=" ".$fgt." ";
							$whichfgtbis=~s/-/ /g;
							$whichfgtbis=~s/_/ /g;
							$car=$whichfgtbis;
							$selectfgt2=$selectfgt+1;
							$nocc=$car=~s/ $selectfgt2\*/ $selectfgt2\*/g;
							$constraint=$nocc; # constraint on anchors
					
							print "replace fgt no $selectfgtinmol (lego $selectnolego, $typelego[$selectnolego] anchors and $constraint substituted) with new fgt of the minimal number of $constraint anchors\n";

						############################
						#drive the selection toward fullfilling cg of the current best solution or (fingerref if exists)
						#use the difference
						if($evaluate_finger){
						
						#sdfparent $m1
						$sdfparent="g".$generation."_mol".($m1+1).".sdf";
						foreach $k (0..@memo_gi-1){
							if($memo_gen[$k]==$generation && $memo_gi[$k]==$m1){
								$tmpsdf=$memo_sdf[$k];
								open(OUY,">$sdfparent");
									print OUY "$tmpsdf";
								close(OUY);
							};
						};	
						chop($tmpfinger=`$leaexe/similarities_GC.pl tanimoto occ $sdfelite $sdfparent`);
						@tmpfinger2=split('\n',$tmpfinger);
						$tmpfinger=$tmpfinger2[3];
						unlink "$sdfparent";
						@tmpfingerdiff=split(' ',$tmpfinger);
						$fingerdiff="";
						foreach $k (0..@tmpfingerdiff-1){
							$k2=$k+1;
							$fingerdiff=$fingerdiff." ".$k2." " if($tmpfingerdiff[$k] > 0);
						};
						#print "diff:\n $fingerdiff\n";
						@tmpfingerdiff=split(' ',$fingerdiff);
						$nbdiff=@tmpfingerdiff;

						}#if($evaluate_finger){
						else{
						# use the content of the best itself
							chop($tmpfinger=`$leaexe/fingerprint_GC.pl bit $sdfelite`);
							@tmpfingerdiff=split(' ',$tmpfinger);
							$nbdiff=@tmpfingerdiff;
						};

						############################
						
						if($nbdiff!=0){	
							
							#get all elligible legos
							@getlistkani="";
							$glki=0;

							#dvt
							if($flag_privilege==1){
								print "Replace: subset created from privileged legos\n";
								@getcell=split(' ',$privileged_lego);
								foreach $kani (0..@getcell-1){
									if($typelego[$getcell[$kani]] >= $constraint){
										$getlistkani[$glki]=$getcell[$kani];
										$glki++;
									};	
								};
							}
							else{
							$flag_around=rand(100);
							if($flag_around < $pc_around){ #create a list around the lego to replace
								print "Subset created around the lego to replace\n";
								foreach $kani (1..25){
									if($lego[$selectnolego] eq $lego[$selectnolego-$kani] && ($selectnolego-$kani > 0) && $typelego[$selectnolego-$kani] >= $constraint){
										$getlistkani[$glki]=$selectnolego-$kani;
										$glki++;
									};	
								};
								#print "avant $selectnolego: @getlistkani\n";
								$bornearound=50-$glki;
								foreach $kani (1..$bornearound){
									if($lego[$selectnolego] eq $lego[$selectnolego+$kani] && ($selectnolego+$kani <= $legoi) && $typelego[$selectnolego+$kani] >= $constraint){
										$getlistkani[$glki]=$selectnolego+$kani;
										$glki++;
									};	
								};	
								#print "apres $selectnolego: @getlistkani\n";
							}
							else{
							foreach $kani (1..@lego-1){
								if($typelego[$kani] >= $constraint){ 
									@getlegogc=split(' ',$legogc[$kani]);
									$nblegogc=@getlegogc;
									$nbvu=0;
									foreach $k (0..@tmpfingerdiff-1){
										$nbvu++ if($legogc[$kani]=~/ $tmpfingerdiff[$k] /);
									};
									#here the fgt has to be big to bring most of the expected molecule
									#$pcvu=($nbvu/$nbdiff)*100;
									#here most of the atoms of the fgt must belong to the expected molecule
									if($nblegogc!=0){
										# correction relative to X points
										#$nblegogc=$nblegogc-$typelego[$kani] if(($nblegogc-$typelego[$kani]) > 0); 
										$pcvu=($nbvu/$nblegogc)*100;
									}
									else{
										$pcvu=rand(100);
									};	
									
									if($pcvu >= $pcvumax){
										#print "$kani bring $pcvu: $lego[$kani] $legono[$kani]\n";
										$getlistkani[$glki]=$kani;
										$glki++;
									};
								};
							};
							};#if($flag_around < )
							};#if($flag_privilege){

							# get random lego from the sub-selected set
							$nbgetlistkani=@getlistkani; 
							if($nbgetlistkani > 0 && $getlistkani[0] ne ""){
							$nbtestreplace=1;
							foreach $glki (0..@getlistkani-1){
									if($nbtestreplace <= $nbtestmutationmax){
										$randkani=int(rand($nbgetlistkani));
										$kani=$getlistkani[$randkani];
										print "select $kani from sub-selection ($nbgetlistkani hits with >= $pcvumax \% or privileged legos or around the substituted lego)\n";

										$nbtestreplace++;

										print "try and score lego no $kani ($typelego[$kani] anchors: $points[$kani])\n";
										@getfgt=split(' ',$fgtping);
										$getfgt[$selectfgt]=$kani;
										$molpingchild[$j]=join(' ',@getfgt);

										#$typelego[$kani] combinations are tested only ! 	
										@listtest="";
										$lii=0;
										@listpoint=split(' ',$points[$kani]);
										foreach $bre (0..@listpoint-1){
											$listtest[$lii]=" $bre ";
											$nblist=1;
											while($nblist < $constraint){
												$m=int(rand(@listpoint));
												if($listtest[$lii]!~/ $m /){
													$listtest[$lii]=$listtest[$lii]." $m ";
													$nblist++;
												};
											};
											$lii++;
										};	
										print "$lii combinations of $constraint anchors are generated\n";
										$lii=$lii-1;
										$fincombin=49;
										$fincombin=$lii if($lii < $fincombin);
										foreach $bri (0..$fincombin){
											@getlisttest=split(' ',$listtest[$bri]);
											#print "test $listtest[$bri]\n";
											$igetlisttest=0;

										@getfgt=split('_',$fgt);
										foreach $k (0..@getfgt-1){
											@getfgt2=split('-',$getfgt[$k]);
											foreach $l (0..@getfgt2-1){
												@getfgt3=split('\*',$getfgt2[$l]);
												if($getfgt3[0] == $selectfgtinmol){
													$getfgt3[1]=$listpoint[$getlisttest[$igetlisttest]];
													$igetlisttest++;
												};
												$getfgt2[$l]=$getfgt3[0]."*".$getfgt3[1];
											};
											$getfgt[$k]=join('-',@getfgt2);
										};

											$molchild[$j]=join('_',@getfgt);

											#Build, Optimis and Score this child
											print "$molchild[$j] / $molpingchild[$j]\n";
											&bos($j,"child");
											if(-e "mol.sdf" && !-z "mol.sdf"){
												$besti++;
												if($bestscore < $score[$ranking[1]]){
													$bestscore=$score[$ranking[1]];
													$bestmolchild=$molchild[$j];
													$bestmolpingchild=$molpingchild[$j];
												};		
											};	

										};#foreach $bri
									};#if($nbtestreplace <= $nbtestmutationmax
								};#foreach $glki	
							};	
						};#if($nbdiff!=0)
						};# if($param{SCAFFOLD} ne '0' && $selectnolego==0)
					}; #foreach $brm	
				};#if($optimischild)

				if($optimischild && $bestmolchild ne ""){
					$successm=1;
					$molchild[$j]=$bestmolchild;
					$molpingchild[$j]=$bestmolpingchild;
					$scorepopchild[$j]=$bestscore;
					print "Child $j ($besti tests): select $molchild[$j] / $molpingchild[$j] (score=$scorepopchild[$j])\n";
				}
				elsif($optimischild==0 || $bestmolchild eq ""){
					# random
				
					$molchild[$j]="";
					$molpingchild[$j]="";

					print "no lego bring >= $pcvumax \%: try random selection\n" if($bestmolchild eq "" && $optimischild==1 && $nbdiff!=0);
					print "difference in GC = 0 -> try random selection\n" if($nbdiff==0 && $optimischild==1);

					@getfgt=split(' ',$fgtping);
					$longtab=@getfgt;
					$selectfgt=int(rand(@getfgt)); #position $selectfgt and fgt no ($selectfgt+1)
					$selectnolego=$getfgt[$selectfgt];
					$selectfgtinmol=$selectfgt+1; #fragment number in mol[]
					
					if($param{SCAFFOLD} ne '0'){
						if($longtab > 1){
							while($selectnolego==0){
								$selectfgt=int(rand(@getfgt));
								$selectnolego=$getfgt[$selectfgt];
								$selectfgtinmol=$selectfgt+1; #fragment number in mol[]
							};
						}
						else{
							print "replace one fragment into mol$m1 $fgt / $fgtping failed, scaffold lego 0 cannot be replaced !\n";
						};
					};
					if($selectnolego != 0){
						$whichfgtbis=" ".$fgt." ";
						$whichfgtbis=~s/-/ /g;
						$whichfgtbis=~s/_/ /g;
						
						$car=$whichfgtbis;
						
						$selectfgt2=$selectfgt+1;
						
						$nocc=$car=~s/ $selectfgt2\*/ $selectfgt2\*/g;
						$constraint=$nocc;

						print "replace fgt no $selectfgtinmol (lego $selectnolego, $typelego[$selectnolego] anchors and $constraint substituted) with new fgt of the minimal number of $constraint anchors\n";

						$tmpmol=&getlego($constraint);
						$tmpmol=~s/(.*)\*(.*)/$1/;
						if($tmpmol ne ""){
							print "select new lego $tmpmol ($typelego[$tmpmol] anchors: $points[$tmpmol])\n";
							@getfgt=split(' ',$fgtping);
							$getfgt[$selectfgt]=$tmpmol;
							$molpingchild[$j]=join(' ',@getfgt);
				
							@listpoint=split(' ',$points[$tmpmol]);
							@getfgt=split('_',$fgt);	
							
							foreach $k (0..@getfgt-1){
								
								@getfgt2=split('-',$getfgt[$k]);
								
								foreach $l (0..@getfgt2-1){
									@getfgt3=split('\*',$getfgt2[$l]);
									
									if($getfgt3[0] == $selectfgtinmol){
										
										$vu=-1;
										
										foreach $m (0..@listpoint-1){
											
											if($vu==-1 && $listpoint[$m] ne ""){
												$vu=$listpoint[$m];
												$listpoint[$m]="";
											};	
										};
										$getfgt3[1]=$vu;
										print "warning inconsistencies with anchors ?\n" if($vu==-1);
									};
									$getfgt2[$l]=$getfgt3[0]."*".$getfgt3[1];
								};
								$getfgt[$k]=join('-',@getfgt2);
							};
							$molchild[$j]=join('_',@getfgt);
							$scorepopchild[$j]="not calculated";

						};#if($tmpmol ne "")

					};#if($selectnolego != 0){

					if($molchild[$j] eq ""){
							print "\nWarning! replace one fragment into mol$m1 $fgt / $fgtping failed\n";
					}
					else{
						$successm=1;
						print "molchild[$j] (random): select $molchild[$j] / $molpingchild[$j] (score=$scorepopchild[$j])\n";
					};	

				};

				#STUDIES on operator efficiency
				$op_which[$j]="replace";
				$op_scorep[$j]=$scorepop[$m1];
				if($successm){
					$op_where[$j]="";
					@brique=split(' ',$molpingchild[$j]);
					$testbrique=" ".$fgtping." ";
					foreach $briquei (0..@brique-1){
					# which lego has been added:
						if($testbrique!~/ $brique[$briquei] /){
							$op_where[$j]=$op_where[$j]." ".$brique[$briquei];
						};
					};	
				};


####################################				
			}	
			elsif($nb2==4){ #permutation of one or several fragment
				print "\npermutation of fragments into mol$m1 $fgt / $fgtping (score=$scorepop[$m1])\n";

				#try all possibilities
				$bestscore=0;
				$bestmolpingchild="";
				$bestmolchild="";
				$besti=0;

				foreach $permuti (1..$nbtestpermut){ # in autoadapt, is 1 because the 

					$molchild[$j]="";
					$molpingchild[$j]="";

					@getfgt=split(' ',$fgtping);
					
					$longtab=@getfgt;
					$selectfgt=int(rand(@getfgt)); #position $selectfgt and fgt no ($selectfgt+1)
					$selectnolego=$getfgt[$selectfgt];
					$constraint=$typelego[$selectnolego];
					$selectfgt++; # no of fgt in mol$i

					print "change the position of fgt no $selectfgt = lego $selectnolego (anchors= $points[$selectnolego])\n";
				
					if($constraint==1 && $longtab > 1){
                        #print "find another fgt with only one substituant\n";	
					
						# try to find another fgt with only one substituant
						$whichfgtbis=" ".$fgt." ";
                        $whichfgtbis=~s/-/ /g;
						$whichfgtbis=~s/_/ /g;
					
						$forceend=0;
						$vu=-1;
                        
                        while($forceend < 10000 && $vu==-1){
                        
							$selectfgt2=int(rand(@getfgt));
							$selectfgt2++;
							$car=$whichfgtbis;
							$nocc=$car=~s/ $selectfgt2\*/ $selectfgt2\*/g;
							
							if($nocc==1 && $selectfgt2 != $selectfgt){
								$vu=$selectfgt2-1; # $vu = indice
							};	
						};
						#dvt
						#$vu=-1;
						if($vu != -1){
							@listpoint=split(' ',$points[$selectnolego]); #constraint==1 then $listpoint[0] is the anchor no
							$selectnolego2=$getfgt[$vu];
							$selectfgt2=$vu+1;
							
							@getfgt=split('_',$fgt);
							
							foreach $k (0..@getfgt-1){
								@getfgt2=split('-',$getfgt[$k]);
								
								foreach $l (0..@getfgt2-1){
									@getfgt3=split('\*',$getfgt2[$l]);
									$anchor2=$getfgt3[1] if($getfgt3[0]== $selectfgt2);
								};
							};	
							print "permutation with terminal fgt no $selectfgt2 anchor $anchor2\n";	
							
							foreach $k (0..@getfgt-1){
							
								@getfgt2=split('-',$getfgt[$k]);
								foreach $l (0..@getfgt2-1){
								
									@getfgt3=split('\*',$getfgt2[$l]);
									
									if($getfgt3[0]== $selectfgt){
										$getfgt3[0]=$selectfgt2;
										$getfgt3[1]=$anchor2;
									}	
									elsif($getfgt3[0]==$selectfgt2){
										$getfgt3[0]=$selectfgt;
										$getfgt3[1]=$listpoint[0];
									};
									$getfgt2[$l]=$getfgt3[0]."*".$getfgt3[1];
								};
								$getfgt[$k]=join('-',@getfgt2);
							};	
							
							$molchild[$j]=join('_',@getfgt);
							$molpingchild[$j]=$fgtping;

							#Build, Optimis and Score this child
							print "test $molchild[$j] / $molpingchild[$j]\n";
							
							&bos($j,"child");
							
							if(-e "mol.sdf" && !-z "mol.sdf"){
								$besti++;
								
								if($bestscore < $score[$ranking[1]]){
								
									$bestscore=$score[$ranking[1]];
									
									$bestmolchild=$molchild[$j];
									
									$bestmolpingchild=$molpingchild[$j];
								};
							};

							#$op_where[$j]=$selectnolego."_".$selectnolego2;
						}
						else{
							#failed to find another fgt with only one substituant thus search for a new anchor in mol$i where to bond $selectnolego
						
							print "Cannot find fgt with only one substituant: search a new anchor point\n";
							$tmpa=&getfreeanchor($fgt,$fgtping);
							$tmpa=~s/(.*)\*(.*)/$1/;
							$tmpanchor=$2;
							if($tmpa eq "" || $tmpa == $selectfgt){ #null or only 1 fgt
								print "no free anchor, moving of fgt failed ($tmpa)\n";
							}
							else{	
								print "free anchor in mol$m1 $tmpa (anchor $tmpanchor)\n";
								@listpoint=split(' ',$points[$selectnolego]);#$listpoint[0] is the anchor no
								@getfgt=split('_',$fgt);
								foreach $k (0..@getfgt-1){
									$supp=0;
									@getfgt2=split('-',$getfgt[$k]);
									foreach $l (0..@getfgt2-1){
										@getfgt3=split('\*',$getfgt2[$l]);
										if($getfgt3[0]== $selectfgt){
											$supp=1;
										};	
									};
									$getfgt[$k]="" if($supp);
								};
								$molchild[$j]=join('_',@getfgt);
								$molchild[$j]=~s/^_//;
								if($molchild[$j] eq ""){
									$molchild[$j]=$tmpa."*".$tmpanchor."-".$selectfgt."*".$listpoint[0];
								}
								else{	
									$molchild[$j]=$molchild[$j]."_".$tmpa."*".$tmpanchor."-".$selectfgt."*".$listpoint[0];
								};
								$molpingchild[$j]=$fgtping;	

								#Build, Optimis and Score this child
								print "test $molchild[$j] / $molpingchild[$j]\n";
								&bos($j,"child");
								if(-e "mol.sdf" && !-z "mol.sdf"){
									$besti++;
									if($bestscore < $score[$ranking[1]]){
										$bestscore=$score[$ranking[1]];
										$bestmolchild=$molchild[$j];
										$bestmolpingchild=$molpingchild[$j];
									};
								};

								#$op_where[$j]=$selectnolego;
							};
						};	
					}#if($constraint==1 && $longtab > 1){
					else{# scramble substitution pattern
						print "scramble around fragment $selectfgt\n";
						@listpoint=split(' ',$points[$selectnolego]);
						@getfgt=split('_',$fgt);
						foreach $k (0..@getfgt-1){
							@getfgt2=split('-',$getfgt[$k]);
							foreach $l (0..@getfgt2-1){
								@getfgt3=split('\*',$getfgt2[$l]);
								if($getfgt3[0] == $selectfgt){
									$vu=-1;
									while($vu==-1){
										$nb3=int(rand($constraint));
										if($listpoint[$nb3] ne ""){
											$vu=$listpoint[$nb3];
											$listpoint[$nb3]="";
										};	
									};
									$getfgt3[1]=$vu;
									#print "select randomly anchor $vu\n";
									print "warning inconsistencies with anchors ?\n" if($vu==-1);	
								};
								$getfgt2[$l]=$getfgt3[0]."*".$getfgt3[1];
							};
							$getfgt[$k]=join('-',@getfgt2);
						};
						$molchild[$j]=join('_',@getfgt);
						$molpingchild[$j]=$fgtping;

						#Build, Optimis and Score this child
						print "test $molchild[$j] / $molpingchild[$j]\n";
						&bos($j,"child");
						if(-e "mol.sdf" && !-z "mol.sdf"){
							$besti++;
							if($bestscore < $score[$ranking[1]]){
								$bestscore=$score[$ranking[1]];
								$bestmolchild=$molchild[$j];
								$bestmolpingchild=$molpingchild[$j];
								unlink("mol.sdf");
							};
						};

						#$op_where[$j]=$selectnolego;
					};

					$op_which[$j]="permutation";
					$op_scorep[$j]=$scorepop[$m1];
					
				};#foreach $permuti

				if($bestmolchild ne ""){
					$successm=1;
					$molchild[$j]=$bestmolchild;
					$molpingchild[$j]=$bestmolpingchild;
					$scorepopchild[$j]=$bestscore;
					print "Child $j ($besti tests): select $molchild[$j] / $molpingchild[$j] (score=$scorepopchild[$j])\n" if($optimischild);
					print "Child $j (random): select $molchild[$j] / $molpingchild[$j] (score=$scorepopchild[$j])\n" if($optimischild==0);

				}
				elsif($bestmolchild eq ""){
					print "\nWarning! permutation in mol$m1 failed !\n";
				};


			};#end of $nb=4	

		};#end of proba_mutation

		@getsize=split(' ',$molpingchild[$j]);
		$sizefgt=@getsize;
		
		if($param{LONGGEN} > 0){
			$j++ if(($success || $successm) && $sizefgt > $param{LONGGEN});
		}
		else{
			$j++ if($success || $successm);
		};	
	};
	
};

##############################################################################################################

sub separate {

	print "$fgt / $fgtping (break at $break)\n"; # random number--Yifan
	
	@getfgt=split('_',$fgt);
	@getfgt4=split(' ',$fgtping);
	
	@getfgt2=split('-',$getfgt[$break]); #get both fragments
	@getfgt3=split('\*',$getfgt2[0]);#get 1st fragment number $getfgt3[0]
	@getfgt5=split('\*',$getfgt2[1]);#get 2nd fragment number $getfgt5[0]	
			
	$m1fgt1=$getfgt3[0];
	$m1fgt2=$getfgt5[0];
					
	@part1=""; #keep fragment number
	@part2=""; #discard fragment number
	
	$part1[0]=$m1fgt1;
	$part2[0]=$m1fgt2;
					
	@child1="";
	$child1[0]="1*".$getfgt3[1];
	$childping1=" ".$getfgt4[$m1fgt1-1];
	
	@child2="";
	$child2[0]="1*".$getfgt5[1];
	$childping2=" ".$getfgt4[$m1fgt2-1];
		
	#split bounds
	$part1i=1;
	$part2i=1;
	$nbfgt=@getfgt4;

	@vugetfgt="";
	$allvu=0;
	$countvu=0;
	
	while($allvu < $nbfgt && $countvu <100){ # why 100 --Yifan
	
        foreach $j2 (0..@getfgt-1){
            
            if($j2 != $break && $vugetfgt[$j2] eq ""){
                
                @getfgt2=split('-',$getfgt[$j2]);
                @getfgt3=split('\*',$getfgt2[0]);
                @getfgt5=split('\*',$getfgt2[1]);
                
                if($getfgt3[0]==$m1fgt1){
                    $part1[$part1i]=$getfgt5[0];
                    $p=$part1i+1;
                    $child1[$part1i]="1*"."$getfgt3[1]"."-"."$p"."*"."$getfgt5[1]";
                    $childping1=$childping1." $getfgt4[$getfgt5[0]-1]";
                    $part1i++;
                    $vugetfgt[$j2]=1;
                }
                elsif($getfgt5[0]==$m1fgt1){
                    $part1[$part1i]=$getfgt3[0];
                    $p=$part1i+1;
                    $child1[$part1i]="1*"."$getfgt5[1]"."-"."$p"."*"."$getfgt3[1]";
                    $childping1=$childping1." $getfgt4[$getfgt3[0]-1]";
                    $part1i++;
                    $vugetfgt[$j2]=1;
                }
                elsif($getfgt3[0]==$m1fgt2){
                    $part2[$part2i]=$getfgt5[0];
                    $p=$part2i+1;
                    $child2[$part2i]="1*"."$getfgt3[1]"."-"."$p"."*"."$getfgt5[1]";
                    $childping2=$childping2." $getfgt4[$getfgt5[0]-1]";
                    $part2i++;
                    $vugetfgt[$j2]=1;
                }
                elsif($getfgt5[0]==$m1fgt2){
                    $part2[$part2i]=$getfgt3[0];
                    $p=$part2i+1;
                    $child2[$part2i]="1*"."$getfgt5[1]"."-"."$p"."*"."$getfgt3[1]";
                    $childping2=$childping2." $getfgt4[$getfgt3[0]-1]";
                    $part2i++;
                    $vugetfgt[$j2]=1;
                }
                else{ #neither $m1fgt1 nor $m1fgt2 but necessary another $part1 or $part2
                    
                    $vu=0;
                    foreach $k (0..@part1-1){
                    
                        if($part1[$k]==$getfgt3[0]){
                            $part1[$part1i]=$getfgt5[0];
                            $p=$part1i+1;
                            $k2=$k+1;
                            $child1[$part1i]="$k2"."*"."$getfgt3[1]"."-"."$p"."*"."$getfgt5[1]";
                            $childping1=$childping1." $getfgt4[$getfgt5[0]-1]";
                            $part1i++;
                            $vugetfgt[$j2]=1;
                            $vu=1;
                        }
                        elsif($part1[$k]==$getfgt5[0]){
                            $part1[$part1i]=$getfgt3[0];
                            $p=$part1i+1;
                            $k2=$k+1;
                            $child1[$part1i]="$k2"."*"."$getfgt5[1]"."-"."$p"."*"."$getfgt3[1]";
                            $childping1=$childping1." $getfgt4[$getfgt3[0]-1]";
                            $part1i++;
                            $vugetfgt[$j2]=1;
                            $vu=1;
                        };
                    };	
                    
                    if($vu==0){
                        foreach $k (0..@part2-1){
                            if($part2[$k]==$getfgt3[0]){
                                $part2[$part2i]=$getfgt5[0];
                                $p=$part2i+1;
                                $k2=$k+1;
                                $child2[$part2i]="$k2"."*"."$getfgt3[1]"."-"."$p"."*"."$getfgt5[1]";
                                $childping2=$childping2." $getfgt4[$getfgt5[0]-1]";
                                $part2i++;
                                $vugetfgt[$j2]=1;
                                $vu=1;
                            }
                            elsif($part2[$k]==$getfgt5[0]){
                                $part2[$part2i]=$getfgt3[0];
                                $p=$part2i+1;
                                $k2=$k+1;
                                $child2[$part2i]="$k2"."*"."$getfgt5[1]"."-"."$p"."*"."$getfgt3[1]";
                                $childping2=$childping2." $getfgt4[$getfgt3[0]-1]";
                                $part2i++;
                                $vu=1;	
                                $vugetfgt[$j2]=1;
                            };
                        };
                    };
                };	
                $allvu=@part2+@part1;
            };		
            $countvu++;
        }; # for seperate with "_" --Yifan
	};
	
	$childping1=$childping1." ";
	$childping2=$childping2." ";

	$allvu=@part2+@part1;
	print "warning did not correctly separate fgts\n" if($allvu < $nbfgt);
#	print "separation: vu= $allvu fgts / total = $nbfgt fgts => @part1 / @part2\n";
#	print "separation: new numbering: @child1 / @child2\n";
#	print "separation: $childping1 / $childping2\n";

};

########################################################################################################
 
