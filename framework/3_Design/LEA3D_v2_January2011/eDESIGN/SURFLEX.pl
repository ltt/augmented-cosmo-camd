#!/usr/bin/perl


$leaexe=$ENV{LEA3D};
die "\nCheck environment variable (setenv LEA3D) \n\n" if (!-e "$leaexe/MAIN");

### VARIABLES
$rms=5; # recherche dans centre du site actif

#lance FLEXX et met les resultats dans DOCK.out

$list_mol2='';
$prot='';
$flexxexe='';

# Coordonnées d'un point dans le site actif
$refx='';
$refy='';
$refz='';

local($flexxexe,$prot,$protomol,$refx,$refy,$refz,$rms,$list_mol2)=@ARGV;

if ($flexxexe eq '' || $list_mol2 eq '' || $prot eq '' || $protomol eq "" || $refx eq '' || $refy eq '' || $refz eq ''){
	die " \n
	usage: SURFLEX.pl <surflex executable> <protein.mol2> <protomol> <refx> <refy> <refz> <$rms> <list_mol2>\n";
};

	unlink "dbsurflex.out-results.mol2" if(-e "dbsurflex.out-results.mol2");
	unlink "dbsurflex.out" if(-e "dbsurflex.out");
	print  "SURFLEX:\n";

	system("$flexxexe dock_list $list_mol2 $protomol $prot dbsurflex.out 2> surflex_log ");
	#chop($tmpsurflex=`$flexxexe dock_list $list_mol2 $protomol $prot dbsurflex.out`);
	
	chop($mol2test=`$leaexe/cut_mol2.pl dbsurflex.out-results.mol2 1`);
	#print "$mol2test\n";

#*************************************************************************
# extrait les resultats

@list='';
@name='';
$i=0;
$no=0;

open(DOC,"<dbsurflex.out");
while(<DOC>){
	@getnomb=split(' ',$_);
	if($_=~/atoms/){
		$mol=$getnomb[0];
		$mol=~s/\://;
		$no=0;
	};	
	if($_=~/dbsurflex.out-/ && $getnomb[1] ne ""){
		@getnomb=split(' ',$_);
		$nomb=$getnomb[0];
		$nomb=~s/\[dbsurflex.out-//;
		$nomb=~s/\://;
		$score=$getnomb[1];
		$pen=$getnomb[3];
		$polar=$getnomb[5];
		#print "sol $nomb\n";
		$nomb++;
		$molecule="$mol"."_$no.mol2";
		rename "dbsurflex.out-results_$nomb.mol2", $molecule;
		$list[$i]="$molecule $score 0.0 0.0 $pen $polar";
		$name[$i]=$mol;
		#print "$list[$i]\n";
		$i++;
		$no++;
	};
};	
close(DOC);

%tabmm=(
	'C',12,
	'O',16,
	'N',14,
	'S',32,
	'P',31,
	'Br',80,
	'Cl',35,
	'I',127,
	'F',19,
	'H',1,
);

# contains different rms solutions for each molecule/conformer
open(KEE,">surflex.tmp"); 
printf KEE "#nom score rms_1er dist_ref penality polar_score\n";
@result='';
$diff="";
foreach $i (0..@list-1){
	@get=split(' ',$list[$i]);
	if (-e "$get[0]" && $get[1] > 0.0){
	
		&readmol2("$get[0]");
		
		$rgyrmm=0;
		$rgx=0;
		$rgy=0;
		$rgz=0;
		foreach $rgyrk (1..$strrgyrno){
			@sepstr = split('\.',$stratomtype[$rgyrk]);
			$rgyrmm=$rgyrmm+$tabmm{$sepstr[0]};
			$rgx=$rgx+$strx[$rgyrk]*$tabmm{$sepstr[0]};
			$rgy=$rgy+$stry[$rgyrk]*$tabmm{$sepstr[0]};
			$rgz=$rgz+$strz[$rgyrk]*$tabmm{$sepstr[0]};
		};
		if($rgyrmm != 0){
			$rgx=$rgx/$rgyrmm;
			$rgy=$rgy/$rgyrmm;
			$rgz=$rgz/$rgyrmm;
		};

		#take coordinates of the first one to calculate rms_first
		if($i==0 || $name[$i] ne $name[$i-1]){
			@strx0=@strx;
			@stry0=@stry;
			@strz0=@strz;
			$rmsfirst=0.0;
		}
		else{
			$rmsfirst=0.0;
			foreach $rgyrk (1..$strrgyrno){
				$rmsfirst=$rmsfirst+(($strx[$rgyrk]-$strx0[$rgyrk])**2+($stry[$rgyrk]-$stry0[$rgyrk])**2+($strz[$rgyrk]-$strz0[$rgyrk])**2);	
			};	
			$rmsfirst=sqrt($rmsfirst);
		};	
		$rmsfirst=$rmsfirst/$istratom;
		
#		print "La masse moleculaire $rgyrmm\n";
#		print "Coordonnees du centre de masse ($rgx ; $rgy ; $rgz)\n";

		if($refx ne "-" && $refy ne "-" && $refz ne "-"){
#			calcul de la distance entre le centre de masse de 2 molecules
			$distance=sqrt(($rgx-$refx)**2+($rgy-$refy)**2+($rgz-$refz)**2);
		}
		else{
		        $distance="-";
			#print "Distance / REF =  $distance\n";
                };

		$rmsfirst=sprintf"%3.2f",$rmsfirst;
		$test=int($rmsfirst);
		$distance=sprintf"%3.2f",$distance;

		if($i==0 || $name[$i] ne $name[$i-1]){
			$diff=" 0 ";
                	$result[$i]="$get[0] $get[1] $rmsfirst $distance $get[4] $get[5]";
			print KEE "$get[0] $get[1] $rmsfirst $distance $get[4] $get[5]\n";
		}
		elsif($diff!~/ $test /){
			$diff=$diff." $test ";
			$result[$i]="$get[0] $get[1] $rmsfirst $distance $get[4] $get[5]";
			print KEE "$get[0] $get[1] $rmsfirst $distance $get[4] $get[5]\n";
		};	
	}
	else{
		$result[$i]="$get[0] 0.0 0.0 - 0.0 0.0";
		printf KEE "$get[0] 0.0 0.0 - 0.0 0.0\n";
	};
	
};
close(KEE);

# all with the correct rms < dist_ref AND IF PENALITY ~ 0.00 ie < -1
# Surflex version 1.25 (july 2008): penality not included in score

$surflexi=0;
$limitpenalty=-2;
open(KEE,">surflex.out");
printf KEE "#nom score rms_1er dist_ref penality polar_score\n";
foreach $i (0..@result-1){
	@get=split(' ',$result[$i]);

	if($get[3] <= $rms && $get[0] ne "" && $get[4] > $limitpenalty){
		$surflexi++;
		print KEE "$result[$i]\n";
	};	
};	
close(KEE);

if($surflexi==0){
        print "No remaining solutions: penality (self-clashing and protein interpenetration) <= $limitpenalty\nSurflex version 1.25 (july 2008): penality not included in score !\n";
};
	

###############################################
###############################################


sub readmol2{
	local($chemin)=@_;

	open(KEN,"<$chemin");

	$flagatom=0;
	$flagbond=0;
	$istratom=1;
	$istrbond=1;
	$fn=0;
		while (<KEN>){
			@getstr = split(' ',$_);

			if ($fn){
				#$name=$getstr[0];
#print"$name\n";
			};

			if ($getstr[0] eq '@<TRIPOS>SUBSTRUCTURE'){
				$flagbond=0;
			};
			if ($getstr[0] eq ''){
				$flagbond=0;
			};

			if (($flagbond)&&($getstr[0] ne '')){
				@strbond1[$istrbond]=$getstr[1];
				@strbond2[$istrbond]=$getstr[2];
				$getstr[3]="am" if (($stratomtype[$getstr[1]]=~/N.am/) && ($stratomtype[$getstr[2]]=~/C.2/));
				$getstr[3]="am" if (($stratomtype[$getstr[2]]=~/N.am/) && ($stratomtype[$getstr[1]]=~/C.2/));
				@strbondtype[$istrbond]=$getstr[3];
				$istrbond++;
			};

			if ($getstr[0] eq '@<TRIPOS>BOND'){
				$flagatom=0;
				$flagbond=1;
			};

			if (($flagatom )&&($getstr[0] ne '')){
				@strx[$istratom]=	$getstr[2];
				@stry[$istratom]=	$getstr[3];
				@strz[$istratom]=	$getstr[4];
				$getstr[5]="N.am" if ($getstr[5]=~/N.3/);
				$stratomtype[$istratom]=$getstr[5];
				@ma=split('\.',$getstr[5]);
				$masse=$masse+$tabmm{$ma[0]};
				$istratom++;
			};

			if ($getstr[0] eq '@<TRIPOS>ATOM'){
				$flagatom=1;
			};
			$fn=0;
			if ($getstr[0] eq '@<TRIPOS>MOLECULE'){
				$fn=1;
			};

		};

	close(KEN);
	$istrbond=$istrbond-1;
	$istratom=$istratom-1;
	$strrgyrno=$istratom;
#print"$strrgyrno, $istrbond\n";

};


#*************************************************************************
#*************************************************************************


