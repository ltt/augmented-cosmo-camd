#!/usr/bin/perl

print "SUBROUTINE AUTO_ADAPT.pl \n";

sub autoadapt{

###############################################################################

	#Adaptation of the selection pressure
	
		if($generation==0){
		
			$param{SCALEMIN}=2;
			#$param{SCALEMAX}=6;
			$param{SCALEMAX}=6;
			$scalemaxinit=$param{SCALEMAX} if($generation==0); #?? delete if --Yifan
		};

		# Progressive pressure on best molecules: $param{SCALEMAX} is doubled at the end
		# weight for each molecule scale uses @scorepop
		
		$param{SCALEMAX} = $scalemaxinit*(1+(1-($param{GENMAX}-$generation)/$param{GENMAX}));

		print "\nSelection pressure [$param{SCALEMIN} ; $param{SCALEMAX}]\n";

###############################################################################

	#Adaptation of $param{PROBA_MUT} and $param{PROBA_CROSS} and $proba1+$proba2+$proba3+$proba4 

		if($generation==0){
			$param{PROBA_MUT}=70;
			$param{PROBA_CROSS}=30; # In CHILD.pl, the probability is divided by 2 because it creates 2 children
			
			$proba1=25; #suppress
			$proba2=25; #add
			$proba3=25; #replace
			$proba4=25; #permutation

			# to allow child optimization
			$optimischild=0; # no child optimization for first part of the optimization
			
			# suppress : all are tested if $optimischild==1 else only randomly one
			
			# add and replace:
			$pcvumax=80; # lego must bring $pcvumax % of its GC finger (fast step) # not understand --Yifan
			$nbtestmutationmax=1; # number of lego with > $pcvumax % tested in add and replace
		
			# add and replace:
			# allow testing each position on parent $optimisposp=1;
			$optimisposp=1; 

			# add and replace:
			# if unchanged best solution over $everygen generations then choose a lego in privileged list
			$flag_privilege=0; #no use of privileged list => generation 0
			$oldbestscore=0;

			# add
			# allow testing each position on lego
			$optimisposl=1;

			#replace
			# combination of position(s) on lego are pseudo-randomly generated if constraint >= 2 (occupied positions) else each anchor of the lego is tested (constraint=1)
			 
			#replace
			#replace with same library lego
			$pc_around=1;


			# permutation:
			if($optimischild){
				$nbtestpermut=10;
			}
			else{
				$nbtestpermut=1;
			};	


		}
		else{
		#$generation > 0
	
			#check for unchanged best solution over $everygen generations
			$everygen=2;

			#if($scorepop[$ranking[0]]==$oldbestscore && $generation > ($param{GENMAX}/2)){
			if($scorepop[$ranking[0]]==$oldbestscore){
				$flag_privilege=0;
				$samebestscore++;
				if($samebestscore==$everygen){ #just one time: $samebestscore reinitialized
					$samebestscore=0;
					$flag_privilege=1;
					print "Warning! >=$everygen generations with the same best solution (or elite if selected) !\n";
				};
			}
			else{
				$samebestscore=1;
				$oldbestscore=$scorepop[$ranking[0]];
				$flag_privilege=0;
			};


			# replace operator
			if($generation > ($param{GENMAX}*2/3)){ # last half of the generations
				$pc_around=50; # if rand(100) < $pc_around then replace with same library lego
				print "Change the probability of replacing the lego by a close one ($pc_around \%)\n";
			};	

			# to allow child optimization
			if($generation > ($param{GENMAX}*2/3)){ #after first third 
# LF: Keine Child Optimization		$optimischild=1;
					$optimischild=0;
			};
			
		};

		print "Mutation Types probability (percentage): \tsuppress one fgt/part= $proba1\tadd one fgt= $proba2\treplace one fgt= $proba3\tpermutation/move/scramble= $proba4\n";

		if($optimischild){
			print "Child optimization procedure selected\n";
		}
		else{
			print "Child optimization disabled, mutations are randomly applied\n";
		};


###############################################################################

	print "\n";
};
