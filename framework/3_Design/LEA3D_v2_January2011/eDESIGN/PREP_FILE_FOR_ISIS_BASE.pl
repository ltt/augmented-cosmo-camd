#!/usr/bin/perl

	$f='';
	my($f)=@ARGV;

	if($f eq ''){
        	print "usage: prepisisbase <file.sdf>\n";
        	exit;
	};

	$file=$f;
	$file=~s/(.*)\/(.*)/$2/;
	$file="isis_$file";

        $nombre_molecule=0;
        $flagnew=1;
	$blanc=" ";
	$zero=0;

	@tocopy="";

	open(DOC,">$file");
	open(IN,"<$f");
        while(<IN>){

                if($flagnew){
                        $compt=0;
                        $ig=1;
                        $jg=0;
                        
			@strx='';
                        @stry='';
                        @strz='';
                        @atom='';
                        @coval='';
                        @fonc='';
                        @ifonc='';
                        @covfonc='';
                        @bond='';
                        @listb='';
                        @typeb='';
                        @lignebond='';
                        $atomlourd=0;
                        
			$flagnew=0;
                        $nombre_molecule++;
			$vumend=0;
			@tocopy="";
			$li=0;
			$flagcopy=1;
			$flagnom=0;
			$nom="";
                };

                @getstr = split(' ',$_);
                $compt++;

		if($flagnom){
			$nom=join(' ',@getstr);
			$flagnom=0;
		};

                if (($compt > 4) && ($ig <= $istratom)){
                        $strx[$ig]=$getstr[0];
                        $stry[$ig]=$getstr[1];
                        $strz[$ig]=$getstr[2];
                        $atom[$ig]=$getstr[3];
                        $atomlourd ++ if($getstr[3] ne 'H');
                        $ig++;

			#print DOC "$_";
			$toutnul=1;
			if($toutnul){
				$getstr[4]="0";
				$getstr[5]="0";
				$getstr[6]="0";
				$getstr[7]="0";
                                $getstr[8]="0";	
                                $getstr[9]="0";
                                $getstr[10]="0";
                                $getstr[11]="0";
                                $getstr[12]="0";
                                $getstr[13]="0";
                                $getstr[14]="0";
                                $getstr[15]="0";
			};
			
			@getlenght=split('',$getstr[3]);
			if(@getlenght == 1){

				$tocopy[$li]=sprintf"%10s%10s%10s%1s%1s  %2s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s\n",$getstr[0],$getstr[1],$getstr[2],$blanc,$getstr[3],$getstr[4],$getstr[5],$getstr[6],$getstr[7],$getstr[8],$getstr[9],$getstr[10],$getstr[11],$getstr[12],$getstr[13],$getstr[14],$getstr[15];
				$li++;

				#printf DOC "%10s%10s%10s%1s%1s  %2s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s\n",$getstr[0],$getstr[1],$getstr[2],$blanc,$getstr[3],$getstr[4],$getstr[5],$getstr[6],$getstr[7],$getstr[8],$getstr[9],$getstr[10],$getstr[11],$getstr[12],$getstr[13],$getstr[14],$getstr[15];
                	}
			elsif(@getlenght == 2){
				$tocopy[$li]=sprintf"%10s%10s%10s%1s%2s %2s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s\n",$getstr[0],$getstr[1],$getstr[2],$blanc,$getstr[3],$getstr[4],$getstr[5],$getstr[6],$getstr[7],$getstr[8],$getstr[9],$getstr[10],$getstr[11],$getstr[12],$getstr[13],$getstr[14],$getstr[15];
				$li++;

				#printf DOC "%10s%10s%10s%1s%2s %2s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s\n",$getstr[0],$getstr[1],$getstr[2],$blanc,$getstr[3],$getstr[4],$getstr[5],$getstr[6],$getstr[7],$getstr[8],$getstr[9],$getstr[10],$getstr[11],$getstr[12],$getstr[13],$getstr[14],$getstr[15];
			}
			elsif(@getlenght ==3){

				$tocopy[$li]=sprintf"%10s%10s%10s%1s%3s%2s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s\n",$getstr[0],$getstr[1],$getstr[2],$blanc,$getstr[3],$getstr[4],$getstr[5],$getstr[6],$getstr[7],$getstr[8],$getstr[9],$getstr[10],$getstr[11],$getstr[12],$getstr[13],$getstr[14],$getstr[15];
				$li++;

				#printf DOC "%10s%10s%10s%1s%3s%2s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s\n",$getstr[0],$getstr[1],$getstr[2],$blanc,$getstr[3],$getstr[4],$getstr[5],$getstr[6],$getstr[7],$getstr[8],$getstr[9],$getstr[10],$getstr[11],$getstr[12],$getstr[13],$getstr[14],$getstr[15];
			};
			#print DOC "\n";
		};

                if (($compt > 4) && ($ig > $istratom) && ($jg > $istrbond)){
                        $ligne=join(' ',@getstr);

			if($_=~/END/ && $_=~/M /){
				$ligne="M  END";
				$tocopy[$li]="$ligne\n";
				$li++;
			};
			if($_=~/\$\$\$\$/){
				$tocopy[$li]="$_";
				$li++;
			};	
			if($vumend==0 && $getstr[0] eq ""){
				$tocopy[$li]="$_";
				$li++;
			};	
			if($vumend && $getstr[0] ne ""){
				$tocopy[$li]="$_";
				$li++;
				$vumend=0;
			};	
			if($_=~/>/ && $_=~/</){
				$vumend=1;
			};
			if($vumend && $getstr[0] ne ""){
				$tocopy[$li]="$_";
				$li++;
			};	
                };

                if (($compt > 4) && ($ig > $istratom) && ($jg <=$istrbond)){
                        if ($jg == 0){
                                $jg++;
                        }
                        else{
                                @coller=split(' *',$getstr[0]);
                                @coller2=split(' *',$getstr[1]);
                                if(@coller==6 && $getstr[1] ne ""){
                                        $getstr[0]=$coller[0].$coller[1].$coller[2];
                                        $getstr[2]=$getstr[1];
                                        $getstr[1]=$coller[3].$coller[4].$coller[5];
                                }
                                elsif(@coller==6 && $getstr[1] eq ""){
                                        $getstr[0]=$coller[0].$coller[1];
                                        $getstr[1]=$coller[2].$coller[3].$coller[4];
                                        $getstr[2]=$coller[5];
                                }
                                elsif(@coller==5){
                                        if($_=~/^\s/){
                                                $getstr[0]=$coller[0].$coller[1];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[2].$coller[3].$coller[4];
                                        }
                                        else{
                                                $getstr[0]=$coller[0].$coller[1].$coller[2];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[3].$coller[4];
                                        };
                                }
                                elsif(@coller==4){
                                        if($_=~/^\s/){
                                                $getstr[0]=$coller[0];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[1].$coller[2].$coller[3];
                                        }
                                        else{
                                                $getstr[0]=$coller[0].$coller[1].$coller[2];
						$getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[3];
                                        };					
                                }
                                elsif(@coller2==4){
                                        $getstr[1]=$coller2[0].$coller2[1].$coller2[2];
                                        $getstr[2]=$coller2[3];
                                }
                                elsif(@coller==7){
                                        $getstr[0]=$coller[0].$coller[1].$coller[2];
                                        $getstr[1]=$coller[3].$coller[4].$coller[5];
                                        $getstr[2]=$coller[6];
                                };


                                $bond[$getstr[0]]=$bond[$getstr[0]].$blanc.$getstr[1].$blanc.$getstr[2];
                                $listb[$getstr[0]]=$listb[$getstr[0]].$blanc.$getstr[1];
                                $typeb[$getstr[0]]=$typeb[$getstr[0]].$blanc.$getstr[2];

                                $bond[$getstr[1]]=$bond[$getstr[1]].$blanc.$getstr[0].$blanc.$getstr[2];
                                $listb[$getstr[1]]=$listb[$getstr[1]].$blanc.$getstr[0];
                                $typeb[$getstr[1]]=$typeb[$getstr[1]].$blanc.$getstr[2];


                                $fonc[$getstr[0]]=$fonc[$getstr[0]].$blanc.$getstr[2].'-'.$atom[$getstr[1]].$blanc;
                                $ifonc[$getstr[0]]=$ifonc[$getstr[0]].$blanc.$getstr[1].$blanc;
                                $covfonc[$getstr[0]]=$covfonc[$getstr[0]].$blanc.$getstr[2];
                                $coval[$getstr[0]]=$coval[$getstr[0]]+$getstr[2];

                                $fonc[$getstr[1]]=$fonc[$getstr[1]].$blanc.$getstr[2].'-'.$atom[$getstr[0]].$blanc;
                                $ifonc[$getstr[1]]=$ifonc[$getstr[1]].$blanc.$getstr[0].$blanc;
                                $covfonc[$getstr[1]]=$covfonc[$getstr[1]].$blanc.$getstr[2];
                                $coval[$getstr[1]]=$coval[$getstr[1]]+$getstr[2];
                                $lignebond[$jg]=$_;
                                $jg++;

				#printf DOC "%3s%3s%3s  0  0  0  0\n",$getstr[0],$getstr[1],$getstr[2];
				#print DOC "\n";
				$tocopy[$li]=sprintf"%3s%3s%3s  0  0  0  0\n",$getstr[0],$getstr[1],$getstr[2];
				$li++;
				
				$flagcopy=0 if($getstr[0] > $istratom);
				
				$flagcopy=0 if($getstr[1] > $istratom);

				$flagcopy=0 if($getstr[2] == 0);

                        };
                };

                if ($compt == 4){
                        $istratom=$getstr[0];
                        $istrbond=$getstr[1];

                        @coller=split(' *',$istratom);
                        if(@coller>3 && @coller==6){
                                $istratom=$coller[0].$coller[1].$coller[2];
                                $istrbond=$coller[3].$coller[4].$coller[5];
                        }
                        elsif(@coller>3 && @coller==5){
                                if($_=~/^\s/){
                                        $istratom=$coller[0].$coller[1];
                                        $istrbond=$coller[2].$coller[3].$coller[4];
                                }
                                else{
                                        $istratom=$coller[0].$coller[1].$coller[2];
                                        $istrbond=$coller[3].$coller[4];
                                };
                        };
			
			#printf DOC "%3s%3s  0  0  0  0  0  0  0  0999 V2000\n",$istratom,$istrbond;
			#print DOC "\n";
			$tocopy[$li]=sprintf"%3s%3s  0  0  0  0  0  0  0  0999 V2000\n",$istratom,$istrbond;
			$li++;

                        #$compt++;
                };

		if($compt == 2){
                        #print DOC "  -ISIS-  02210509232D\n";
			$tocopy[$li]="  -ISIS-  02210509232D\n";
			$li++;	
			#print DOC "$_";
                }
                elsif($compt == 3){
                        #print DOC "\n";
			$tocopy[$li]="\n";
			$li++;
		}
		elsif($compt == 1){
			#print DOC "\n";
			$tocopy[$li]="\n";
			$li++;
                };
		
		if ($_=~/MDLNUMBER/ || $_=~/ZINC/ || $_=~/NAME/){
			$flagnom=1;
		};

                if ($_=~/\$\$\$\$/){
                        $flagnew=1;
			if($flagcopy){
				foreach $li (0..@tocopy-1){
					print DOC $tocopy[$li];
				};
			}
			else{
				print "discarded: ID $nom \tpossesses a Bond line containing an atom number > $istratom or a bond type == 0\n";
			};
                };


        };
	close(IN);

	if($flagnew==0){
		if($flagcopy){
			foreach $li (0..@tocopy-1){
				print DOC $tocopy[$li];
			};
		}
		else{
			 print "ID $nom \tpossesses a Bond line containing an atom number > $istratom or a bond type == 0\n";
		};
	};

	close(DOC);
