#!/usr/bin/perl

print "WRITE OK \n";

sub writeReadMolInfo{
   # print "$idoc\n";
#     $fileInfooutput = 'molecular_info.txt';
#     
#     @outputFile = split('\.', $inputfile);
#     @outputFile1 = split('\/', @outputFile[0]);
#     
#     $outputFileName = $outputFile1[@outputile1-1];
#     
   # print "$outputFileName\n";
#     open(INFOOUT, ">$fileInfooutput");
    if ($iMOl == 1){
        print INFOOUT ("############\n\n");
        print INFOOUT "\# molecular Information\n\n";
        print INFOOUT ("############\n\n");
    }
   
    print INFOOUT "$ligne[1] $ligne[2]\n";
    print INFOOUT ("group\tnum.\trelevant atoms\n");
    print INFOOUT "-OH\t$ngroupOH\tO-H [ @groupOHanchor ]\n" if($ngroupOH > 0);
    print INFOOUT "-COOH\t$ngroupCOOH\tC-O2-O1 [ @groupCOOHanchor ]\n" if($ngroupCOOH > 0);
    print INFOOUT "-NHH\t$ngroupNHH\tN-H-H [@groupNHHanchor]\n" if($ngroupNHH > 0);
    print INFOOUT "-F\t$ngroupF\tF [@groupFanchor]\n" if($ngroupF > 0);
    print INFOOUT "-Cl\t$ngroupCl\tCl [@groupClanchor]\n" if($ngroupCl > 0);
    print INFOOUT "-Br\t$ngroupBr\tBr [@groupBranchor]\n" if($ngroupBr > 0);
    print INFOOUT "-I\t$ngroupI\tI [@groupIanchor]\n" if($ngroupI > 0);
    
    print INFOOUT "-CO2O1\t$ngroupCO2O1\tC-O2-O1//O1-C [ @groupCO2O1anchor//@groupCO2O1anchor2 ]\n" if($ngroupCO2O1 > 0);
    print INFOOUT "-O\t$ngroupO\tO [ @groupOanchor// @groupOanchor2 ]\n" if($ngroupO > 0);
    print INFOOUT "-NH\t$ngroupNH\tN-H [@groupNHanchor//@groupNHanchor2]\n" if($ngroupNH > 0);

    print INFOOUT "-CO2N1\t$ngroupCO2N1\tC-O2-N1 [ @groupCO2N1anchor ]\n" if($ngroupCO2N1 > 0);
    print INFOOUT "-CO2\t$ngroupCO2\tC-O2 [ @groupCO2anchor]\n" if($ngroupCO2 > 0);
    print INFOOUT "\n\$\$\$\$\n\n\n";

#     close INFOOUT;
};
