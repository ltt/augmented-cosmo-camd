function [ x, y, beta ] = LLE_NRTL_calc( z, x0, n_comp, tau, alpha, nitermax, TOL_mu, TOL_beta, TOL_gbeta, R, T, beta0 )
%Calculation of the equilibrium compositions for a given total composition


%% Initialization
%%
    x=x0;
    beta = beta0;

    % Mole balance for Phase 2
        for i=1:(n_comp-1)
            y(i) = (z(i)-(1-beta)*x(i))/beta;
        end
        
       y(n_comp) = 1-sum( y(1:(n_comp-1)) );

%    
    gamma_x = (NRTL(x, n_comp, alpha, tau,R, T));
    gamma_y = (NRTL(y, n_comp, alpha, tau,R, T));
    K = gamma_x./gamma_y;
    delta_mu = abs(gamma_y.*y - gamma_x.*x);

%     beta_vec=beta;
%     x_vec=x0';
%     y_vec=y';
%     delta_mu_vec = delta_mu';
%     K_vec = K';

%% RR-Algorithm
%%
    nit=0;
    while((nit < nitermax)  && ( max(delta_mu) > TOL_mu )  )
        nit = nit + 1;
      
    
        [beta,x,y] = RRSOLVER(n_comp, z, K, nitermax, TOL_beta, TOL_gbeta);
        if beta < TOL_beta
            x=z;
            y=z;
            %disp('single phase1')
            break
        elseif beta >= (1-TOL_beta)
            x=z;
            y=z;
            %disp('single phase2')
            break
        end
    
        gamma_x = (NRTL(x, n_comp, alpha, tau, R, T));
        gamma_y = (NRTL(y, n_comp, alpha, tau, R, T));
        K = gamma_x./gamma_y;
        delta_mu = abs(gamma_y.*y - gamma_x.*x);
    
%         K_vec=[K_vec; K'];
%         x_vec=[x_vec; x'];
%         y_vec=[y_vec; y'];
%         delta_mu_vec = [delta_mu_vec; delta_mu'];
%         beta_vec=[beta_vec;beta];
    end
%     disp('K,x,y,delta_mu,beta')
%     [K_vec,x_vec, y_vec, delta_mu_vec, beta_vec]


end
