function zF =  find_zF(molStruct,T)

%% Systems, parameters and experimental data
    
    [alpha, tau] = getNRTLPara(molStruct,T);
    zF = [0,0,1];

%% Parameters
%%
    beta = 0.5;
    R=8.314; %kJ/kmol/K

    n_comp = 3;
    nitermax = 10000;
    TOL_mu = 1e-9;
    TOL_beta = 1e-9;
    TOL_gbeta = 1e-9;
    beta0 = beta;
    
    x0 = [1 , 0 , 0];      % Startwert f�r die Zusammensetzung einer Phase %#JSc: For LLE in Water

% 2,3

for i = 0:0.1:1;
    
    results = LLE_NRTL_calc( [i,0,1-i], x0, n_comp, tau, alpha, nitermax, TOL_mu, TOL_beta, TOL_gbeta, R, T, beta0 );
    if sum(results ~= [i,0,1-i]) == 2
        zF = [i,0,1-i];
        break;
    end
end
end