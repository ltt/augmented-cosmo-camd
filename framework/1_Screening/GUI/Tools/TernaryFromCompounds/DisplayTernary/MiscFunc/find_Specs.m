function specs = find_Specs(molStruct)

    specs = [];
    for k = 1:length(molStruct)

        if ~isempty(molStruct(k).specs)
            specs = molStruct(k).specs;
        end
    end
    if isempty(specs);
        disp('Specs could not be found!')
    end
end 