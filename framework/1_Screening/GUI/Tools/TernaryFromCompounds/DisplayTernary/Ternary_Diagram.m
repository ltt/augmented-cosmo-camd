function Ternary_Diagram(molStruct, T, specs, varargin)

addpath([pwd,'/DisplayTernary/Ternary_Diagram/']);
addpath([pwd,'/DisplayTernary/MiscFunc/']);
    
   
for j = 1:length(T)
    main_ternPlot(molStruct,T(j),specs,varargin)
end

end

