function main_ternaryfromcompoundsGUI(molStruct, specs)

%% General
%%

% Set T for ternary plot
T_ternary = specs.T_ternary+273.15; %K


%% Display Ternary Diagram
%%
addpath('DisplayTernary');

for k = 1:length(molStruct)
    % Specify multi-component system: use name in form of COSMObase filename
    % Components 2 and 3 need to have miscibility gap with each other

    Ternary_Diagram(molStruct(k), T_ternary, specs)
    savefig([specs.maindir, '\Ternary_', num2str(k)]);

end

datetime
end
