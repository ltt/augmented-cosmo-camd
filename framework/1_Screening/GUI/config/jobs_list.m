jobs = {};
jobs{end+1} = {'gui_job_mexrbm','TabMexRBM'};
jobs{end+1} = {'gui_job_logp','TabLogP'};
jobs{end+1} = {'gui_job_NRTL','TabNRTL'};
jobs{end+1} = {'gui_job_Ternary','TabTernary'};
jobs{end+1} = {'gui_job_mextraction','TabExtraction'};
jobs{end+1} = {'gui_job_LLE','TabLLE'};
jobs{end+1} = {'gui_job_SigmaProfiles','TabSigmaProfiles'};