function gui_job_mexrbm(guidata)
disp('runScript - MexRBM');

name = guidata.name;
% Choose objective function
objfunc = 'Mex-RBM';

% For solute please use name in form of COSMObase filename
solute = guidata.solute;
solvent = guidata.solvent;

% Choose COSMO-RS parametrization
parametrization = guidata.parametrization;

% Set NRTL Temperature vector
T_start = guidata.jobdata.JobMexRBMTstart; % /C
T_end = guidata.jobdata.JobMexRBMTend; % /C; % /C 
T_interval = guidata.jobdata.JobMexRBMIntervals; % /C; % Number of points in T interval    

% Set pressure of extraction column
pExtr = guidata.jobdata.JobMexRBMPExtr; %/bar

% Set temperature of extraction column or set temperature bounds for
% optimization of extraction column temperature
if guidata.jobdata.JobMexRBMOptimizeTExtr
    Tc_Extr_min = guidata.jobdata.JobMexRBMTcExtrmin; %/C;
    Tc_Extr_max = guidata.jobdata.JobMexRBMTcExtrmax; %/C;
else
    TcExtr = guidata.jobdata.JobMexRBMTcExtr; %/C
end

% Input for Extraction column
xF = str2num(guidata.jobdata.JobMexRBMxF); 
xS = str2num(guidata.jobdata.JobMexRBMxS); 
purity = guidata.jobdata.JobMexRBMPurity;


% Set temperature and pressure for RBM:
TcRBM = guidata.jobdata.JobMexRBMTCRBM; %/C
pRBM = guidata.jobdata.JobMexRBMPRBM;%/bar

% Conformer treatment for solute
nconf_max = guidata.nconf_max; % set nconf_max = Inf for all confomers

maindir = pwd;

%% Save specifications in specs-struct
specs = struct;
specs.Objfunc = objfunc;
specs.Solute = solute;
specs.Solvent = solvent;
specs.COSMOpara = parametrization;
specs.Mode = 'screening'; %or 'optimization'
specs.compounds = solute;
specs.compounds{length(specs.compounds)+1} = solvent;
specs.T_start = T_start;
specs.T_end = T_end;
specs.T_interval = T_interval;
if guidata.jobdata.JobMexRBMOptimizeTExtr
    specs.Tc_Extr_min = Tc_Extr_min; %/C;
    specs.Tc_Extr_max = Tc_Extr_max; %/C;
else
    specs.TcExtr = TcExtr;
end
specs.pExtr = pExtr;
specs.xS = xS;
specs.xF = xF;
specs.purity = purity;
specs.TcRBM = TcRBM;
specs.pRBM = pRBM;
specs.nconf = zeros(1,length(specs.compounds));
specs.nconf(:) = nconf_max;
specs.composition = 0:0.1:1;
specs.paths = guidata.paths;
specs.maindir = maindir;



% Display job
display_text = ['RunScript in ', specs.Mode, ' Mode for ', specs.Objfunc, '\n\n'];
fprintf (display_text);

%% Load struct from Input   
% Working variable for struct is molStruct
% Load a struct as 'molStruct'
temp_struct = load([maindir,'/Input/',name]);
name_of_loaded_struct = fieldnames(temp_struct);
molStruct = temp_struct.(name_of_loaded_struct{1});

% Start parallel computing if desired
if guidata.Parallelization
   pool = parpool(guidata.ParallelCores); 
end

%% Get properties for each species in molStruct
%%
addpath([maindir,'/PropertyPrediction/']);


molStruct = main_cosmoData(molStruct,specs.Objfunc,specs);

% Temporary save
save([maindir,'/Input/',name, '_PP_done'],'molStruct')
rmpath([maindir,'/PropertyPrediction/'])

%% Execute RBM and Mextraction
%%
addpath([maindir,'/ProcessModel/ShortcutModel/']);

molStruct = ScoreMexRBM_tempfix( molStruct, specs );

rmpath([maindir,'/ProcessModel/ShortcutModel/']);


%% Save results and quit
%%

molStruct(1).specs = specs;

save([maindir,'/Output/',name, '_MEX-RBM'],'molStruct')
delete([maindir,'/Input/',name, '_PP_done', '.mat'])

% Stop parallel computing if necessary
if guidata.Parallelization
   delete(pool); 
end

datetime
end


