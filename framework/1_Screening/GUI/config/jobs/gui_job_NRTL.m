function gui_job_NRTL(guidata)
disp('runScript - NRTL');

name = guidata.name;
% Choose objective function
Antoine = guidata.jobdata.JobNRTLAntoineBox;
if Antoine
    objfunc = 'Mex-RBM';
else
    objfunc = 'NRTL';
end

% For solute please use name in form of COSMObase filename
solute = guidata.solute;
solvent = guidata.solvent;

% Choose COSMO-RS parametrization
parametrization = guidata.parametrization;

% Set NRTL Temperature vector
T_start = guidata.jobdata.JobNRTLTstart; % /C
T_end = guidata.jobdata.JobNRTLTend; % /C; % /C 
T_interval = guidata.jobdata.JobNRTLIntervals; % /C; % Number of points in T interval    

% Conformer treatment for solute
nconf_max = guidata.nconf_max; % set nconf_max = Inf for all confomers

maindir = pwd;

%% Save specifications in specs-struct
specs = struct;
specs.Objfunc = objfunc;
specs.Solute = solute;
specs.Solvent = solvent;
specs.COSMOpara = parametrization;
specs.Mode = 'screening'; %or 'optimization'
specs.compounds = solute;
specs.compounds{length(specs.compounds)+1} = solvent;
specs.T_start = T_start;
specs.T_end = T_end;
specs.T_interval = T_interval;
specs.Antoine = Antoine;
specs.nconf = zeros(1,length(specs.compounds));
specs.nconf(:) = nconf_max;
specs.composition = 0:0.1:1;
specs.paths = guidata.paths;
specs.maindir = maindir;


% Display job
display_text = ['RunScript in ', specs.Mode, ' Mode for ', specs.Objfunc, '\n\n'];
fprintf (display_text);

%% Load struct from Input   
% Working variable for struct is molStruct
% Load a struct as 'molStruct'
temp_struct = load([maindir,'/Input/',name]);
name_of_loaded_struct = fieldnames(temp_struct);
molStruct = temp_struct.(name_of_loaded_struct{1});

% Start parallel computing if desired
if guidata.Parallelization
   pool = parpool(guidata.ParallelCores); 
end

%% Get properties for each species in molStruct
%%
addpath([maindir,'/PropertyPrediction/']);

molStruct = main_cosmoData(molStruct,specs.Objfunc,specs);

rmpath([maindir,'/PropertyPrediction/']);

%% Sort for Aspen input
ASPEN = struct('name', '', 'ASPEN_NRTL', '');

for w = 1:length(molStruct)
    ASPEN(w).name = molStruct(w).name;
    
    % NRTL
    AlphaParameter = molStruct(w).AlphaParam;
    TauParameters = molStruct(w).TauParam;

    pairs = nchoosek(1:length(specs.compounds),2);
    ASPEN(w).ASPEN_NRTL = cell(12,length(pairs));

    for k = 1:length(pairs)
        i = pairs(k,1);
        j = pairs(k,2);
        ASPEN(w).ASPEN_NRTL{1,k} = specs.compounds{i};
        ASPEN(w).ASPEN_NRTL{2,k} = specs.compounds{j};
        ASPEN(w).ASPEN_NRTL{3,k} = TauParameters{i, j}(1);
        ASPEN(w).ASPEN_NRTL{4,k} = TauParameters{j, i}(1);
        ASPEN(w).ASPEN_NRTL{5,k} = TauParameters{i, j}(2);
        ASPEN(w).ASPEN_NRTL{6,k} = TauParameters{j, i}(2);
        ASPEN(w).ASPEN_NRTL{7,k} = AlphaParameter{i, j}(1) + 273.15 * AlphaParameter{i, j}(2);
        ASPEN(w).ASPEN_NRTL{8,k} = AlphaParameter{i, j}(2);
        ASPEN(w).ASPEN_NRTL{9,k} = TauParameters{i, j}(3);
        ASPEN(w).ASPEN_NRTL{10,k} = TauParameters{j, i}(3);
        ASPEN(w).ASPEN_NRTL{11,k} = TauParameters{i, j}(4);
        ASPEN(w).ASPEN_NRTL{12,k} = TauParameters{j, i}(4);
    end

    % Antoine
    if Antoine
        AntoineParam = molStruct(w).AntoineParam;
        for i = 1:length(specs.compounds)
            ASPEN(w).ASPEN_Antoine{1,i} = specs.compounds{i};
            ASPEN(w).ASPEN_Antoine{2,i} = 'K';
            ASPEN(w).ASPEN_Antoine{3,i} = 'mbar';
            ASPEN(w).ASPEN_Antoine{4,i} = AntoineParam{i}(1);
            ASPEN(w).ASPEN_Antoine{5,i} = -AntoineParam{i}(2);
            ASPEN(w).ASPEN_Antoine{6,i} = AntoineParam{i}(3);
        end
    end
end

%% Save results and quit
%%

molStruct(1).specs = specs;

save([maindir,'/Output/',name, '_NRTL'],'molStruct')
save([maindir,'/Output/',name, '_ASPEN'],'ASPEN')

% Stop parallel computing if necessary
if guidata.Parallelization
   delete(pool); 
end

datetime
end


