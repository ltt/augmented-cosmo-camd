function gui_job_logp( guidata )
disp('runScript - logP');

name = guidata.name;
% Choose objective function
objfunc = 'logP';

% For solute please use name in form of COSMObase filename
compounds = guidata.solute;
compounds{end+1} = guidata.solvent;

% Choose COSMO-RS parametrization
parametrization = guidata.parametrization;

% Set Temperature of job
T = guidata.jobdata.JobLogPT; %�C]

% Set mixture for LLE calculations
LLEcomp = sprintf('%i %i', guidata.jobdata.JobLogPC1, guidata.jobdata.JobLogPC2);

% Conformer treatment for solute
nconf_max = guidata.nconf_max; % set nconf_max = Inf for all confomers

maindir = pwd;

%% Save specifications in specs-struct
specs = struct;
specs.Objfunc = objfunc;
specs.COSMOpara = parametrization;
specs.Mode = 'screening'; %or 'optimization'
specs.compounds = compounds;
specs.T = T;
specs.LLEcomp = LLEcomp;
specs.nconf = zeros(1,length(compounds));
specs.nconf(:) = nconf_max;
specs.composition = 0:0.1:1;
specs.paths = guidata.paths;
specs.maindir = maindir;


% Display job
display_text = ['RunScript in ', specs.Mode, ' Mode for ', specs.Objfunc, '\n\n'];
fprintf (display_text);

%% Load struct from Input   
% Working variable for struct is molStruct
% Load a struct as 'molStruct'
temp_struct = load([maindir,'/Input/',name]);
name_of_loaded_struct = fieldnames(temp_struct);
molStruct = temp_struct.(name_of_loaded_struct{1});

% Start parallel computing if desired
if guidata.Parallelization
   pool = parpool(guidata.ParallelCores); 
end

%% Get properties for each species in molStruct
%%
addpath([maindir,'/PropertyPrediction/']);

molStruct = main_cosmoData(molStruct,specs.Objfunc,specs);

% Temporary save
save([maindir,'/Input/',name, '_PP_done'],'molStruct')
rmpath([maindir,'/PropertyPrediction/'])

%% Execute logP
%%
addpath([maindir,'/ProcessModel/logP/']);

molStruct = logPScore( molStruct, specs );


%% Save results and quit
%%

molStruct(1).specs = specs;

save([maindir,'/Output/', name, '_logP'],'molStruct')
delete([maindir,'/Input/',name, '_PP_done', '.mat'])

% Stop parallel computing if necessary
if guidata.Parallelization
   delete(pool); 
end

datetime
end

