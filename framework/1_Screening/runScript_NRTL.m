function runScript_NRTL(name)

%% General
%%

% Choose objective function
objfunc = 'NRTL';
%objfunc = 'Mex-RBM'; % choose Mex-RBM if Antoine Parameter are also needed


% For solute please use name in form of COSMObase filename
%compounds{1} = 'phenol'; 
%compounds{1} = 'hydroxymethylfurfural';
compounds{1} = 'dihydro-5-methyl-2(3h)-furanone'; %COSMO-name for GVL
%compounds{1} = '4-oxo-pentanoicacid'; % COSMO-name for LA
%compounds{1} = 'ethanol';
compounds{2} = 'h2o';

compounds{end+1} = 'Solvent X';

% Chosse COSMO-RS parametrization
parametrization = 'TZVP'; % 'TZVP' or 'TZVPD-FINE' or 'COSMOfrag' 

% Set NRTL Temperature vector
T_start = 0; % /C
T_end = 200; % /C 
T_interval = 5; %


% Conformer treatment for solute
nconf_max = 10; 

maindir = pwd;

%% Save specifications in specs-struct
specs = struct;
specs.Objfunc = objfunc;
specs.COSMOpara = parametrization;
specs.Mode = 'screening'; %or 'optimization'
specs.compounds = compounds;
specs.T_start = T_start;
specs.T_end = T_end;
specs.T_interval = T_interval;
specs.composition = 0:0.1:1;
specs.nconf = zeros(1,length(compounds));
specs.nconf(:) = nconf_max;
specs.composition = 0:0.1:1;
% Load paths struct
load('Paths/user.mat')
specs.paths = paths;
specs.maindir = maindir;

% Start parpool for parallel computation:
%pool = parpool;

% Display job
display_text = ['RunScript in ', specs.Mode, ' Mode for ', specs.Objfunc, '\n\n'];
fprintf (display_text);

%% Load struct from Input   
% Working variable for struct is molStruct
% Load a struct as 'molStruct'
temp_struct = load([maindir,'/Input/',name]);
name_of_loaded_struct = fieldnames(temp_struct);
molStruct = temp_struct.(name_of_loaded_struct{1});


%% Get properties for each species in molStruct
%%
addpath([maindir,'/PropertyPrediction/']);

molStruct = main_cosmoData(molStruct,specs.Objfunc,specs);

rmpath([maindir,'/PropertyPrediction/'])


%% Sort for Aspen input
ASPEN = struct('name', '', 'ASPEN_NRTL', '');

for w = 1:length(molStruct)
    ASPEN(w).name = molStruct(w).name;
    
    % NRTL
    AlphaParameter = molStruct(w).AlphaParam;
    TauParameters = molStruct(w).TauParam;

    pairs = nchoosek(1:length(specs.compounds),2);
    ASPEN(w).ASPEN_NRTL = cell(12,length(pairs));

    for k = 1:length(pairs)
        i = pairs(k,1);
        j = pairs(k,2);
        ASPEN(w).ASPEN_NRTL{1,k} = specs.compounds{i};
        ASPEN(w).ASPEN_NRTL{2,k} = specs.compounds{j};
        ASPEN(w).ASPEN_NRTL{3,k} = TauParameters{i, j}(1);
        ASPEN(w).ASPEN_NRTL{4,k} = TauParameters{j, i}(1);
        ASPEN(w).ASPEN_NRTL{5,k} = TauParameters{i, j}(2);
        ASPEN(w).ASPEN_NRTL{6,k} = TauParameters{j, i}(2);
        ASPEN(w).ASPEN_NRTL{7,k} = AlphaParameter{i, j}(1) + 273.15 * AlphaParameter{i, j}(2);
        ASPEN(w).ASPEN_NRTL{8,k} = AlphaParameter{i, j}(2);
        ASPEN(w).ASPEN_NRTL{9,k} = TauParameters{i, j}(3);
        ASPEN(w).ASPEN_NRTL{10,k} = TauParameters{j, i}(3);
        ASPEN(w).ASPEN_NRTL{11,k} = TauParameters{i, j}(4);
        ASPEN(w).ASPEN_NRTL{12,k} = TauParameters{j, i}(4);
    end

    % Antoine
    if strcmp(objfunc, 'Mex-RBM')
        AntoineParam = molStruct(w).AntoineParam;
        for i = 1:length(specs.compounds)
            ASPEN(w).ASPEN_Antoine{1,i} = specs.compounds{i};
            ASPEN(w).ASPEN_Antoine{2,i} = 'K';
            ASPEN(w).ASPEN_Antoine{3,i} = 'mbar';
            ASPEN(w).ASPEN_Antoine{4,i} = AntoineParam{i}(1);
            ASPEN(w).ASPEN_Antoine{5,i} = -AntoineParam{i}(2);
            ASPEN(w).ASPEN_Antoine{6,i} = AntoineParam{i}(3);
        end
    end
end


%% Save results and quit
%%

molStruct(1).specs = specs;

save([maindir,'/Output/',name, '_ASPEN'],'ASPEN')
save([maindir,'/Output/',name],'molStruct')

datetime
end
