function runScript_MexRBM(name)

%% General
%%

% Choose objective function
objfunc = 'Mex-RBM';

% For solute please use name in form of COSMObase filename
%compounds{1} = 'phenol'; 
%compounds{1} = 'hydroxymethylfurfural';
compounds{1} = 'dihydro-5-methyl-2(3h)-furanone'; %COSMO-name for GVL
%compounds{1} = '4-oxo-pentanoicacid'; % COSMO-name for LA
%compounds{1} = 'ethanol';
compounds{2} = 'h2o';

compounds{end+1} = 'Solvent X';

% Chosse COSMO-RS parametrization
parametrization = 'TZVP'; % 'TZVP' or 'TZVPD-FINE' or 'COSMOfrag' 

% Set NRTL Temperature vector
T_start = 0; % /C
T_end = 200; % /C 
T_interval = 11; % Number of points in T interval    

% Specify temperatures for T_sat constraint
T_boil_min = 25; % Celsius or 'no_T_min'
T_boil_max = 'no_T_max'; % Celsius or 'no_T_max'

% Set temperature and pressure of extraction column
pExtr = 1.013; %/bar
% Fixed temperature
optimizeTExtr = 0;
TcExtr = 25; %/C
% Variable temperature = specify temperature bounds
%optimizeTExtr = 1;
%Tc_Extr_min = 20; %/C
%Tc_Extr_max = 80; %/C


% Set temperature and pressure for RBM:
TcRBM = 25; %/C
pRBM = 1;%/bar
  
% Conformer treatment
nconf_max = 10;

maindir = pwd;

%% Save specifications in specs-struct
specs = struct;
specs.Objfunc = objfunc;
specs.COSMOpara = parametrization;
specs.Mode = 'screening'; %or 'optimization'
specs.compounds = compounds; 
specs.T_boil_min = T_boil_min;
specs.T_boil_max = T_boil_max;
specs.T_start = T_start;
specs.T_end = T_end;
specs.T_interval = T_interval;
if optimizeTExtr 
    specs.Tc_Extr_min = Tc_Extr_min; %/C
    specs.Tc_Extr_max = Tc_Extr_max; %/C
else
    specs.TcExtr = TcExtr;
end
specs.pExtr = pExtr;
specs.optimizeTExtr = optimizeTExtr;
specs.TcRBM = TcRBM;
specs.pRBM = pRBM;
specs.nconf = zeros(1,length(compounds));
specs.nconf(:) = nconf_max;
specs.composition = 0:0.1:1;
% Load paths struct
load('Paths/user.mat')
specs.paths = paths;
specs.maindir = maindir;


% Start parpool for parallel computation:
%pool = parpool;

% Display job
display_text = ['RunScript in ', specs.Mode, ' Mode for ', specs.Objfunc, '\n\n'];
fprintf (display_text);

%% Load struct from Input   
% Working variable for struct is molStruct
% Load a struct as 'molStruct'
temp_struct = load([maindir,'/Input/',name]);
name_of_loaded_struct = fieldnames(temp_struct);
molStruct = temp_struct.(name_of_loaded_struct{1});


%% Get properties for each species in molStruct
%%
addpath([maindir,'/PropertyPrediction/']);

molStruct = main_cosmoData(molStruct,specs.Objfunc,specs);

molStruct = main_cosmoData(molStruct,'T_boil_con',specs);

% Temporary save
save([maindir,'/Input/',name, '_PP_done'],'molStruct')
rmpath([maindir,'/PropertyPrediction/'])

%% Execute RBM and Mextraction
%%
addpath([maindir,'/ProcessModel/ShortcutModel/']);
if optimizeTExtr
    molStruct = ScoreMexRBM_tempopt( molStruct, specs );
else
    molStruct = ScoreMexRBM_tempfix( molStruct, specs );
end

rmpath([maindir,'/ProcessModel/ShortcutModel/']);


%% Save results and quit
%%

molStruct(1).specs = specs;

save([maindir,'/Output/',name, '_GVL'],'molStruct')
delete([maindir,'/Input/',name, '_PP_done', '.mat'])

datetime
end
