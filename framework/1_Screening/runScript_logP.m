function runScript_logP(name)

%% General
%%

% Choose objective function
objfunc = 'logP';

% For solute please use name in form of COSMObase filename
compounds{1} = 'phenol'; 
%compounds{1} = 'hydroxymethylfurfural';
%compounds{1} = 'dihydro-5-methyl-2(3h)-furanone'; %COSMO-name for GVL
%compounds{1} = '4-oxo-pentanoicacid'; % COSMO-name for LA
%compounds{1} = 'ethanol';
%compounds{1} = '4-hydroxy-3-methoxybenzaldehyde'; % Vanillin
compounds{2} = 'h2o';

compounds{end+1} = 'Solvent X';

% Chosse COSMO-RS parametrization
parametrization = 'TZVP'; % 'TZVP' or 'TZVPD-FINE' or 'COSMOfrag' 

% Set Temperature of job
T = 82; %�C]

% Set mixture for LLE calculations
LLEcomp = '2 3';

% Conformer treatment for solute
nconf_max = 10; 

maindir = pwd;

%% Save specifications in specs-struct
specs = struct;
specs.Objfunc = objfunc;
specs.COSMOpara = parametrization;
specs.Mode = 'screening'; %or 'optimization'
specs.compounds = compounds;
specs.T = T;
specs.LLEcomp = LLEcomp;
specs.nconf = zeros(1,length(compounds));
specs.nconf(:) = nconf_max;
specs.composition = 0:0.1:1;
% Load paths struct
load('Paths/user.mat')
specs.paths = paths;
specs.maindir = maindir;

% Start parpool for parallel computation:
%pool = parpool;

% Display job
display_text = ['RunScript in ', specs.Mode, ' Mode for ', specs.Objfunc, '\n\n'];
fprintf (display_text);

%% Load struct from Input   
% Working variable for struct is molStruct
% Load a struct as 'molStruct'
temp_struct = load([maindir,'/Input/',name]);
name_of_loaded_struct = fieldnames(temp_struct);
molStruct = temp_struct.(name_of_loaded_struct{1});


%% Get properties for each species in molStruct
%%
addpath([maindir,'/PropertyPrediction/']);

% logP
specs.logP = 'mol-based';
molStruct = main_cosmoData(molStruct,specs.Objfunc,specs);

% T_boil
specs.T_boil_min = 80;
specs.T_boil_max = 200;

molStruct = main_cosmoData(molStruct,'T_boil_con',specs);

% Temporary save
save([maindir,'/Input/',name, '_PP_done'],'molStruct')
rmpath([maindir,'/PropertyPrediction/'])

%% Calculate logP-Score
%%
addpath([maindir,'/ProcessModel/logP/']);

molStruct = logPScore( molStruct, specs );


%% Save results and quit
%%

molStruct(1).specs = specs;

save([maindir,'/Output/',name],'molStruct')
delete([maindir,'/Input/',name, '_PP_done', '.mat'])

datetime
end
