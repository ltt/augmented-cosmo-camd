function molStruct = job_SigmaProfile(molStruct,compounds, specs )
%% Calculate sigma-Profiles and sigma-Moments for solvent 

%% General
%%
% Preallocate fields
for k=1:length(molStruct)
    molStruct(k).ConfNum = [];
    molStruct(k).wconf = [];
    molStruct(k).sigmaProfiles = [];
    molStruct(k).Moments = [];
    molStruct(k).weightedProfile = [];
	molStruct(k).weightedMoments = [];   
    molStruct(k).E_cosmo = [];
    molStruct(k).E_cosmo_weighted = [];
    molStruct(k).dHf = [];
end

% Initialize parfor_progressbar
if strcmp(specs.Mode, 'screening')
    display_text = ['\nCalculating sigma profiles, sigma moments and conformer weights of ', num2str(length(molStruct)), ' solvents\nProgress:\n'];
    fprintf (display_text);
    parfor_progress(length(molStruct));
end

parfor k = 1:length(molStruct)
        
        % Get conformer weights for subsequent weighting
        [ConfNum_wconf, molStruct(k).wconf] = sub_wconf(compounds(k,end), k,specs);
        
        % Get sigma-Profiles and sigma-Moments from COSMOtherm
        [ConfNum_wprfl, molStruct(k).sigmaProfiles, molStruct(k).Moments] = sub_SigmaProfilesMoments(compounds(k,end), k,specs);
        
        % Get E_cosmo
        [molStruct(k).E_cosmo_weighted, molStruct(k).E_cosmo] = sub_Ecosmo(compounds(k,end), molStruct(k).wconf, specs);
        
        % Check if no error occures between weights, moments and profiles
         if ConfNum_wconf ~= ConfNum_wprfl
             error('ERROR: Conformer number in wconf and sigma-profiles differ!');            
         else
             molStruct(k).ConfNum = ConfNum_wprfl;
         end
        
        % Create one weighted Profile
        [molStruct(k).weightedProfile, molStruct(k).weightedMoments] = weightProfileMoments(molStruct(k));
               
        % Get Formula of molecule
        molStruct(k).Formula = sub_Formula(molStruct(k), k, specs);
        
        % Calculate enthalpy of Formation
        molStruct(k) = calculateEnthalpyOfFormation(molStruct(k));
              
        % Update parfor-progressbar
        if strcmp(specs.Mode, 'screening')
            parfor_progress;
        end
end
    
if strcmp(specs.Mode, 'screening')
   fprintf 'Sigma profiles and moments calculated!\n';
end

end

function [ weightedProfile, weightedMoments ] = weightProfileMoments( molStruct)

ConfNum = molStruct.ConfNum;
weightedProfile = zeros(61, 2);
weightedMoments.sigmaM = zeros(1,7);
weightedMoments.Hbacc = zeros(1, 4);
weightedMoments.Hbdon = zeros(1, 4);

% Sigma Profile
    for i = 1 : 61
        weightedProfile(i, 1) = molStruct.sigmaProfiles(i, 1);
        % weight by averaging with conformer weights
        for j = 1 : ConfNum        
            weightedProfile(i, 2) = weightedProfile(i, 2) + molStruct.wconf(j) * molStruct.sigmaProfiles(i, j+1);
        end
    end
    
% Sigma Moments
    for i = 1 : ConfNum
        % weight by averaging with conformer weights
        for j = 1 : 7        
            weightedMoments.sigmaM(j) = weightedMoments.sigmaM(j) + molStruct.wconf(i) * molStruct.Moments.sigmaM(i,j);
        end
        for j = 1 : 4        
            weightedMoments.Hbacc(j) = weightedMoments.Hbacc(j) + molStruct.wconf(i) * molStruct.Moments.Hbacc(i,j);
            weightedMoments.Hbdon(j) = weightedMoments.Hbdon(j) + molStruct.wconf(i) * molStruct.Moments.Hbdon(i,j);
        end
    end
end

function molStruct = calculateEnthalpyOfFormation(molStruct)

warning('Make sure values for E_COSMO for each atom are right!');

% Reference values
atoms_considered = {'C'; 'H'; 'O'; 'N'; 'Cl'; 'Br'; 'I'; 'F'; 'S'};

% Order: C H O N Cl Br I F S
E_COSMO = [ -100145.179538319;  %C  % for number of C < 6 (see below)
            -1546.1358399298;   %H 
            -197440.708662363;  %O
            -143846.039264058   %N
            -1208366.82923224;  %Cl
            -6759210.01867016;  %Br
            -30138.0604705434;  %I
            -262041.173511807;  %F
            -1045574.1007582500 ];  %S %-1045654.1634273
        
% Get molecular atom composition
SMILES = lower(molStruct.SMILES);
if isfield(molStruct, 'Formula') 
    Formula = molStruct.Formula;
else
    error('No molecular formula in a field "Formula" found!');
end

% 1. Read SMILES
atoms_number = zeros(length(atoms_considered),1);
for i=1:length(atoms_considered)
    atoms_number(i) = length(strfind(SMILES,lower(atoms_considered{i})));
end

% 1.1. Correct C and Cl
isC = strfind(atoms_considered, 'C');
isCl = strfind(atoms_considered, 'Cl');
indexCl = find(not(cellfun('isempty', isCl)));
isC{indexCl} = [];
indexC = find(not(cellfun('isempty', isC)));
atoms_number(indexC) = atoms_number(indexC) - atoms_number(indexCl);

% 1.2 Use correct value for C for numberofC > 6
if atoms_number(indexC) >= 6
    E_COSMO(1) = -100140.9821081020;
end

% 2. Add H-Atoms
indexH = strfind(Formula, 'H');
if ~isempty(indexH)
    Formula = Formula(indexH+1:end);  
    if isempty(Formula) || isempty(str2num(Formula(1)))
        numbersofH = 1;
    else
        numbersofH = 1;
        numbers = str2double(regexp(Formula,'[\d.]+','match'));
        numbersofH = numbers(1);
    end
    isH = strfind(atoms_considered, 'H');
    index = find(not(cellfun('isempty', isH)));
    atoms_number(index) = numbersofH;
end

% Calculate Enthalpy of Formation
dHf = molStruct.E_cosmo_weighted * 2625.5; % Conversion Hartree in kJ/mol
for i=1:length(atoms_considered)
    dHf = dHf-atoms_number(i)*E_COSMO(i);    
end
molStruct.dHf = dHf;

end

