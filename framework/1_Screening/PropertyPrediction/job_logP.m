function [ molStruct ] = job_logP( molStruct,compounds,specs )
% Property Prediction procedure for logP-Score
%  LLEs, corresponding activity coefficients and molecular weights are
%  calculated

%% General
%%
% Preallocate fields
n = length(specs.compounds);

for k=1:length(molStruct);
    molStruct(k).MW_solvent = NaN;
    molStruct(k).x_LLE = NaN;
    molStruct(k).gammaI = NaN;
    molStruct(k).gammaII = NaN;
end

% Initialize parfor_progressbar
if strcmp(specs.Mode, 'screening')
    display_text = ['\nCalculating LLE, activity coefficients and molecular weight of ', num2str(length(molStruct)), ' solvents\nProgress:\n'];
    fprintf (display_text);
    parfor_progress(length(molStruct));
end


%% Job procedure
%%
T = specs.T+273.15; %Conversion ot T/C to T/K 
active_mixture = specs.LLEcomp;

% Find value component
LLEcomp = str2num(specs.LLEcomp);

parfor k=1:length(molStruct); 
 
%% LLE calculations
         molStruct(k).x_LLE = sub_LLE(active_mixture, k,compounds(k,:), T, specs);

%% Activity coefficients
	if ~isnan(molStruct(k).x_LLE)
		% Phase I
		compositionI = zeros(1,length(compounds(k,:)));
        compositionI(LLEcomp) = molStruct(k).x_LLE(1:2);
        molStruct(k).gammaI = sub_gamma(compositionI, k, compounds(k,:), T, specs);
        
		% Phase II        
		compositionII = zeros(1,length(compounds(k,:)));
        compositionII(LLEcomp) = molStruct(k).x_LLE(3:4);
		molStruct(k).gammaII = sub_gamma(compositionII, k, compounds(k,:), T, specs);
	end

%% Molecular weights of solvents
    
    compound = compounds{k,n};
    molStruct(k).MW_solvent = sub_molweight(compound, k, specs);

    
    % Update parfor-progressbar
    if strcmp(specs.Mode, 'screening')
        parfor_progress;
    end

end

if strcmp(specs.Mode, 'screening')
   fprintf 'Property prediction for logP-Score completed\n';
end

end

