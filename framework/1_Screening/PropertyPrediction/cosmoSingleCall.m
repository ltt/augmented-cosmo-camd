function [tempFile,cosmoSpecAlgo] = cosmoSingleCall(k, compounds, specs, jobPrint,varargin)
%% Implemented by A. Demuth, bachelor thesis supervised by Jan Scheffczyk @LTT, RWTH Aachen University.
% Function which extracts thermodynamic properties from COSMO-RS

%% Interact with COSMO-RS: Create input file, run COSMOtherm, get results00
% [!]: Important points
%INPUT
% argument "input" used in NRTL2 and gamma

% INPUT
%----------------------------------------------------
% Name          | Description           | Size | Unit   | 
% active_mixture| mixtures: 1-2, ...    | [1x2]| [-]    |
% k             | current no. of solvent| [1x1]| [-]    |
% compounds     | compound names: h20,..| [1x3]| [-]    |
% T             | temperature in Kelvin | [1x1]| [K]    |
% job           | Job Type              | [?x?]| [-]    | 
% confVector    | Conformer vector      | [?x?]| [-]    | 
% input         | input (depends on job)| [?x?]| [-]    |
%----------------------------------------------------
%
% OUTPUT
%------------------------------------------------
% Name      | Description           | Size | Unit   | 
% result    | | | |
% ok          |       |      |        |
%------------------------------------------------
%
%
% MODIFY FOR OTHER COMPUTERS 
% See header.m
%

%% STRUCTURE
% Get paths and set definitions
% Write INPUT files
% Run job

%% Definitions, COSMO-RS specifications

    % Import path definitions from header.m
    [~, tempFile, ~, dbPath, exePath, CTD, CTDIR, LDIR, CFDB] = main_header(specs);
    
    if strcmp(specs.Mode, 'optimization')
            tempFile = [tempFile, num2str(specs.Number), '_', num2str(rand)]; % use molecule number from LEA3D for generation of temp file
    elseif (strcmp(specs.Mode, 'screening') || strcmp(specs.Mode, 'refinement'))
            tempFile = [tempFile, num2str(k), '_', num2str(rand)]; % use current solvent number k for generation of temp file
    end
    % Number of COSMO iterations/ concentrations for .tab file
    cosmoSpecAlgo = 29; % Number of iterations/ concentrations for quick/ slow algo qAl=29 sAl=323 TODO CHECK

%% Create COSMO-RS input file
    % Generate components "printCompound"
    
   for i = 1:length(compounds)-1
        printCompound{i}    = [ 'f = ', compounds{i}, '_c0.cosmo fdir="', dbPath ,'" Comp = ', compounds{i}, ' autoc=' num2str(specs.nconf(i)-1)];
   end
    
   % Location of cosmo-Files: For optimization check if file lies in gate
   % folder, otherwise it must be a database file
   if (strcmp(specs.Mode, 'optimization') || strcmp(specs.Mode, 'refinement')) && (~strcmp(specs.COSMOpara,'COSMOfrag')) && exist([specs.Gate{2}, '/', compounds{length(compounds)} '_c0.cosmo'], 'file')
            printCompound{length(compounds)}    = [ 'f = ' compounds{length(compounds)} '_c0.cosmo fdir="',specs.Gate{2}, '/" Comp = ', compounds{length(compounds)} ,' autoc=', num2str(specs.nconf(length(compounds))-1) ];
   elseif strcmp(specs.COSMOpara, 'COSMOfrag')  && exist([specs.Gate{2}, '/', compounds{length(compounds)}, '.mcos'], 'file')
            printCompound{length(compounds)}    = [ 'f = ', compounds{length(compounds)}, '.mcos fdir="',specs.Gate{2}, '/" Comp = ' compounds{length(compounds)}];
            dbPath = CFDB;
   else
            printCompound{length(compounds)}    = [ 'f = ', compounds{length(compounds)}, '_c0.cosmo fdir="', dbPath ,'" Comp = ', compounds{length(compounds)}, ' autoc=', num2str(specs.nconf(length(compounds))-1) ];
   end
    
    % Close all files
    fclose('all');

    % Create new input file (tempFile)
    fileID=fopen([tempFile,'.inp'],'wt');

    % Write header
    fprintf(fileID,['ctd =', CTD, ' cdir = "',CTDIR,'" ldir ="', LDIR,'" fdir="', dbPath, '" = \n']);
    if ~isempty(varargin) 
        fprintf(fileID,['wtln notempty EHfile ',varargin{1,1}, '\n']); % Includes conformer weight information --> extracted in gamma calculation
    else
        fprintf(fileID,['wtln notempty EHfile ', '\n']);  % wtln: print full conformer name, notempty: Print NA for empty table entries, EHfile: energy file with units in hartree, VPfile: use vapor pressure/property file (exprimental)
    end
   
    fprintf(fileID,'!! generated by COSMOthermX !! \n');

    % Write components "printCompound"
    for i=1:length(compounds)
        fprintf(fileID,[printCompound{i},'\n']);
    end

    % Write jobs "jobPrint" 
    fprintf(fileID,[jobPrint,'\n']);

%% Run job

    % Create output file with COSMOtherm
    if ispc
        status = dos([exePath,' ' tempFile, '.inp"']);
    elseif isunix
        status = system([exePath,' ' tempFile, '.inp']);
    end
    if status == 1 
        disp ('ERROR: COSMOtherm.exe not executed');
    end
    % Close all files
    fclose('all');
end
