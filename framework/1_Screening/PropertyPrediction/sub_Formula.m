function Formula =  sub_Formula(molStruct, k, specs)
% return formula of molecule based on SMILES and COSMOfrag

[~, tempFile, ~, ~, ~, ~, ~, ~, CFDB, LDIRfrag, exeFrag] = main_header(specs);

tempFileMW = [tempFile,'frag_', num2str(k),];
fileID = fopen([tempFileMW,'.inp'],'wt');

fprintf(fileID,['ACTION=7 cfdbdir=', CFDB, ' strdir=.', '\n']);
fprintf(fileID,['licensedir=', LDIRfrag, ' USMILES FULLTAB \n\n']);
fprintf(fileID,['smi:',molStruct.SMILES]);

% Execute COSMOfrag
%chdir('temp');
if ispc
    status = dos([exeFrag,' ' tempFileMW, '.inp"']);
elseif isunix
    status = system([exeFrag,' ' tempFileMW, '.inp']);
end

%% open .out datei and get Formula
fileID = fopen([tempFileMW,'.tab'],'rt');
line = fgetl(fileID);
while ~feof(fileID)
    if ~isempty(strfind(line,'SumForm')) % Find results part and read all results
        break
    end
    line = fgetl(fileID);
end
line = fgetl(fileID);
fclose(fileID);
formatSpec = '%*s %*n %*s %*s %*n %*n %*n %*n %*s %*s %*n %*n %*n %*n %*n %*n %*n %*n %*n %*n %*n %*n %*n %s';
Formula = textscan(line,formatSpec); % Read line
Formula = Formula{1}{1};

end