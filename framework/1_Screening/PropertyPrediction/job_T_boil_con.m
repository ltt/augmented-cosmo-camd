function molStruct = job_T_boil_con(molStruct,compounds,specs)
% Property prediction to check T_boil constraint
%  Calculates T_boil and checks if in allowed temperature range

%% General
%%
% Preallocate fields
for k=1:length(molStruct);
    molStruct(k).T_boil = NaN;
    molStruct(k).T_boil_ok = NaN;
end


% Initialize parfor_progressbar
if strcmp(specs.Mode, 'screening')
    parfor_progress(length(molStruct));
    display_text = ['\nCalculating T_boil for ', num2str(length(molStruct)), ' solvents\nProgress:\n'];
    fprintf(display_text);
end

%% Job procedure
%%
parfor k = 1:length(molStruct)
    molStruct(k).T_boil = sub_T_boil(k, compounds(k,end), specs);
    molStruct(k).T_boil_ok = func_check_T_boil( molStruct(k).T_boil, specs);
    
    % Update parfor-progressbar
    if strcmp(specs.Mode, 'screening')
        parfor_progress;
    end
    
end
end

function [T_boil_ok] = func_check_T_boil(T_boil, specs)
% Checking boiling point temperature of solvent 
% The lower and upper bound of acceptable boiling temperature are given in
% the "specs"-struct, fields T_boil_max and T_boil_min

%% Create bounds for checking T_boil

if strcmp(specs.T_boil_max, 'no_T_max')
    T_max = 1e99;
else
    T_max = specs.T_boil_max;
    if ischar(T_max)
        T_max = str2double(T_max);
    end
    T_max = T_max +273.15;
end

if strcmp(specs.T_boil_min, 'no_T_min')
    T_min = 0;
else
    T_min = specs.T_boil_min;
    if ischar(T_min)
        T_min = str2double(T_min);
    end
    T_min = T_min +273.15;
end

%% Check T_boil of given compound
if(T_boil > T_max)
        T_boil_ok = 0;
elseif(T_boil < T_min)
        T_boil_ok = 0;
else    T_boil_ok = 1;
end

end
