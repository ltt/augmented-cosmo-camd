function molStruct = main_cosmoData(molStruct ,job, specs)
%% Implemented by A. Demuth, bachelor thesis supervised by Jan Scheffczyk @LTT, RWTH Aachen University.
% Call the function like this


    
%% General definitions
%%
    
    % Enumerate specifications in cell
    n = length(specs.compounds); % = Number of components in the system (e.g. 3)
    nX = length(molStruct);     % = Number of solvents X
    compounds = cell(nX,n);
    
    % Enumerate possible systems in "compounds" cell
    for k = 1:nX
        for l = 1:n-1
            compounds{k,l} = specs.compounds{l};
        end
        compounds{k,n} = molStruct(k).name; %Add solvent X to "compounds"        
    end
 
    
%% Properties job procedure for corresponding process model
%%
    % Property prediction for Mex-RBM
    if strcmp(job,'Mex-RBM')
        molStruct = job_Mex_RBM(molStruct,compounds,specs);
        
    % Property prediction for Gmehling-Score    
    elseif strcmp(job,'Gmehling')
        molStruct = job_Gmehling(molStruct,compounds,specs);
        
    % Property prediction for logP-Score  
    elseif strcmp(job,'logP')
        molStruct = job_logP(molStruct,compounds,specs);
    
    % Calc Tboil at 1bar     
    elseif strcmp(job,'T_boil_con')
        molStruct = job_T_boil_con(molStruct,compounds,specs);   
    
    % Calc SigmaProfile     
    elseif strcmp(job,'SigmaProfile')
        molStruct = job_SigmaProfile(molStruct,compounds, specs);   
        
    % Calc NRTL Parameter     
    elseif strcmp(job,'NRTL')
        molStruct = job_NRTL(molStruct,compounds, specs); 
        
    % Calculate reaction kinetics in liquid phase
    elseif strcmp(job,'kinetics_liquid')
        molStruct = job_kinetics_liquid( molStruct,compounds,specs );
     
    % Calculate LLE
    elseif strcmp(job,'LLE')
        molStruct = job_LLE(molStruct,compounds,specs);
    
    % Property prediction for Absorption Distillation
    elseif strcmp(job,'FormicAcid')
        molStruct = job_FormicAcid(molStruct,compounds,specs);
    end
    
end
