function molStruct = job_Absorption(molStruct,compounds,specs)
% Property Prediction procedure for shortcut models Mextraction and RBM
%  NRTL-parameter and Antoine coefficients are calculated

%% General
%%
nComp = length(compounds(1,:)); % number of Compounds
resultsNRTL = cell(0);

warning('off', 'all');

% Initialize parfor_progressbar
if strcmp(specs.Mode, 'screening')
    display_text = ['\nCalculating Antoine coefficients of ', num2str(length(molStruct)), ' solvents\nProgress:\n'];
    fprintf (display_text);
    parfor_progress(length(molStruct));
end

%% Job procedure
%%
T = linspace(specs.T_start+273.15, specs.T_end+273.15, specs.T_interval)'; %Conversion ot T/C to T/K 

%% Calculate the Antoine Parameter for RBM
parfor i = 1:nComp-1    
    antParam{i,1} = sub_Antoine(i,compounds(1,i), T, specs);
end

parfor k = 1:length(molStruct)
    molStruct(k).AntoineParam = antParam;
    molStruct(k).AntoineParam{nComp,1} = sub_Antoine(k,compounds(k,end), T, specs);   
    
    % Update parfor-progressbar
    if strcmp(specs.Mode, 'screening')
        parfor_progress;
    end
    
end

%% Calculate the NRTL Parameter for RBM and MEX

%% Calculate NRTL Parameters for all non Solvents

% Initialize parfor_progressbar
if strcmp(specs.Mode, 'screening')
    display_text = ['\nCalculating NRTL parameters of ', num2str(length(molStruct)), ' solvents\nProgress:\n'];
    fprintf (display_text);
    parfor_progress(length(molStruct));
end

parfor i = 1:nComp-1
    for j = 1:nComp-1
        if i<j
            compVec = [compounds(1,i),compounds(1,j)];
            resultsNRTL{i,j} = sub_NRTL_T(i,compVec, T, specs);
        end
    end
end

%% Calculate NRTL Parameters for all Solvents
parfor k = 1: length(molStruct)

    index = 1;
    
  
    %% Set the NRTL Parameters calculated in the previous step
    
    for i = 1:nComp-1
        for j = 1:nComp-1 
            if i<j && nComp >2 
                molStruct(k).TauParam{i,j}      = resultsNRTL{i,j}.NRTLParam_T(2,:);
                molStruct(k).TauParam{j,i}      = resultsNRTL{i,j}.NRTLParam_T(3,:); 
                molStruct(k).AlphaParam{i,j}    = resultsNRTL{i,j}.NRTLParam_T(1,1:2);
                molStruct(k).AlphaParam{j,i}    = resultsNRTL{i,j}.NRTLParam_T(1,1:2);               
                %molStruct(k).resultsGamma{i,j}  = resultsNRTL{i,j}.resultsGamma;   
                molStruct(k).err_NRTL{i,j}      = resultsNRTL{i,j}.Err;
                
                index = index +1;
            end
        end   
    end
    
    %% Calculate NRTL Parameters for a Solvent 
    
    for i = 1: nComp-1
        
        compVec = [compounds(k,i),compounds(k,nComp)];
        resultsNRTLSol = sub_NRTL_T(k,compVec, T, specs);
        
        molStruct(k).TauParam{i,nComp}      = resultsNRTLSol.NRTLParam_T(2,:);
        molStruct(k).TauParam{nComp,i}      = resultsNRTLSol.NRTLParam_T(3,:);
        molStruct(k).AlphaParam{i,nComp}    = resultsNRTLSol.NRTLParam_T(1,1:2);
        molStruct(k).AlphaParam{nComp,i}    = resultsNRTLSol.NRTLParam_T(1,1:2);
        %molStruct(k).resultsGamma{i,nComp}  = resultsNRTLSol.resultsGamma;
        molStruct(k).err_NRTL{i,nComp}      = resultsNRTLSol.Err;
    end   
    
    for i = 1:nComp
        molStruct(k).TauParam{i,i}      = zeros(1,4);
        molStruct(k).AlphaParam{i,i}    = zeros(1,2);
    end
    
    % Update parfor-progressbar
    if strcmp(specs.Mode, 'screening')
        parfor_progress;
    end
    
end

%% Calculate Henry Coefficients for Absorption column
% Initialize parfor_progressbar
if strcmp(specs.Mode, 'screening')
    display_text = ['\nCalculating Henry Coefficients of ', num2str(length(molStruct)), ' solvents\nProgress:\n'];
    fprintf (display_text);
    parfor_progress(length(molStruct));
end

% Preallocate
HenryCoeff_Data = num2cell(NaN(1,length(molStruct)));
[molStruct.HenryCoeff_Data] = HenryCoeff_Data{:};

parfor k = 1: length(molStruct)
    molStruct(k).HenryCoeff_Data = sub_henry(k ,compounds(k,:), T, specs);   
        
    % Update parfor-progressbar
    if strcmp(specs.Mode, 'screening')
        parfor_progress;
    end
end


%% For mabsorption fit to polynomial
% Preallocate
HenryCoeff = num2cell(NaN(1,length(molStruct)));
[molStruct.HenryCoeff] = HenryCoeff{:};

parfor k = 1: length(molStruct)
    molStruct(k).HenryCoeff = fit_Henry(molStruct(k).HenryCoeff_Data, compounds(k,:), T);       
end


if strcmp(specs.Mode, 'screening')
   fprintf 'Property prediction for Absorption completed\n';
end

end

%% Function Appendix

function HenryCoeff = fit_Henry(HenryCoeff_Data,compounds, T)

% HenryCoeff as used in Aspen Plus and in bar
% ln(H) = a + b/T + c ln(T) + d T + e /T^2

HenryCoeff = cell(length(compounds));

for i=1:length(compounds)
    HenryCOSMO = cell2mat(HenryCoeff_Data(:,i));
    for j=1:length(compounds)
        para0 = [1000, -0.01, 0, 0, 0];

        options = optimset('Display','off','MaxFunEvals',1e5);
        HenryCoeff{j,i} = fminsearch( @(para)Henry_function(para,HenryCOSMO(:,j),T),para0,options);

    end
end

end

function diff = Henry_function(a,Henry_Coeff,T)

% As used in Aspen Plus
% ln(H) = a + b/T + c ln(T) + d T + e /T^2

fitted = a(1) + a(2)./T + a(3)*log(T) + a(4)*T + a(5)./((T).^2);

diff = sum(((log(Henry_Coeff) -fitted)./log(Henry_Coeff)).^2);


end


function dHvap_para = fit_dHvap(dHvap,Tc)

para0 = [5e7, -0.01, 0];

options = optimset('Display','off','MaxFunEvals',1e5);
dHvap_para = fminsearch( @(para)dHvap_DIPPR(para,dHvap,T,Tc),para0,options);

end

function diff = dHvap_DIPPR(c,dHvap,T,Tc)

% dHvap-Polynomial 106

t = T/Tc;

fitted = c(1) * (1-t).^(c(2) + c(3)* t; % + c(4)* t .^ 2+ c(5)* t .^ 3);

diff = sum(((dHvap - fitted)./dHvap).^2);


end

