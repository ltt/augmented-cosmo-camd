function [T_boil,ok] = sub_T_boil(k, compound, specs)
% Calculates the Antoine parameters for one compound 
%
% Input:    compounds 
%           k: unique number k used for identiying files 
%           specs 
%
% Output:   Boiling point
%
 %% Generate jobs "jobPrint"
    jobPrint = 'Pvap= 1013.25  x={ 1 }  tc =0.0 # Automatic Boiling Point Calculation'; 
           
 %% Call COSMOtherm 
    
    [tempFile,~] = cosmoSingleCall( k, compound, specs, jobPrint);
   
 %% ReadOutputFile 
 
    [T_boil,ok] = extractTboil(tempFile);    

 %% Delete temporary files after calculation     

if exist([tempFile, '.inp'])>0
    delete([tempFile, '.inp'])
end
if exist([tempFile, '.tab'])>0
    delete([tempFile, '.tab'])
end
if exist([tempFile, '.out'])>0
    delete([tempFile, '.out'])
end

end

function [T_boil,ok] = extractTboil(tempFile)
% RETURNS [T_boil]
% Check if .out file is empty
s = dir([tempFile,'.tab']);
if s.bytes == 0
    T_boil = NaN([1,3]); % Empty file
    ok = 0;
    disp('ERROR: tab file empty');
else
    ok = 1;
    fileID = fopen([tempFile,'.tab'],'r');
    line = fgetl(fileID);
    i = 0;                     
    while ~numel(strfind(line,'T           PVtot         mu(Liquid)            mu(Gas)          H(Vapori)')) && i < 32000 % Move file pointer to line of interest
         line = fgetl(fileID);
         i = i + 1; % Prevent eternal loop
    end
    line = fgetl(fileID);
    formatSpec = '%f %f %f %f %f %f';
    content = textscan(line,formatSpec); % Read line
    T_boil = content{1};
    fclose(fileID);
end
end
