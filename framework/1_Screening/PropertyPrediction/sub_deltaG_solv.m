function [deltaG_solv] = sub_deltaG_solv(k,compounds, T, specs)
%% Calculates deltaG of solvation for reactants and transition states
%
% Input:    k: unique number k used for identiying files 
%           compounds
%           T: temperature vector 
%           specs 
%           
%
% Output:   deltaG_solv_reactants, deltaG_solv_TS
%%

%% Generate jobs "jobPrint"
    jobPrint = ''; % Initialize jobPrint
    tk = num2str(T);
    reaction_mixture = num2str(specs.reaction_mixture);
    jobPrint = strcat(jobPrint, [ 'henry xh={' reaction_mixture '} tk= ' tk '  GSOLV #Automatic Henry Law coefficient Calculation \n' ]); 


 %% Call COSMOtherm
    
    [tempFile,cosmoSpecAlgo] = cosmoSingleCall( k, compounds, specs,jobPrint); %name of COSMOtherm output file ..., ...
   
 %% ReadOutputFile 
 
    [results,ok] = extract_deltaG_solv(tempFile,cosmoSpecAlgo,specs);
    
 %% Delete temporary files after calculation     
    
    if exist([tempFile, '.inp'])>0
        delete([tempFile, '.inp'])
    end
    if exist([tempFile, '.tab'])>0
        delete([tempFile, '.tab'])
    end
    if exist([tempFile, '.out'])>0
        delete([tempFile, '.out'])
    end
    
%% Create outputs
deltaG_solv = results(:,1);


end   
    
function [results,ok] = extract_deltaG_solv(tempFile,cosmoSpecAlgo,specs)
% RETURNS [deltaG_solv_reactant1, ..., deltaG_solv_TS1,...,
% deltaG_solv_solvent]
% Check if .out file is empty
s = dir([tempFile,'.tab']); %
if s.bytes == 0
    result = NaN([1,(specs.n_reactants+specs.n_TS+1)]); % Empty file
    ok = 0;
    disp('ERROR: tab file empty');
else
    ok = 1;
    fileID = fopen([tempFile,'.tab'],'r'); %r: read
    line = fgetl(fileID); %return next line as string
    o=0;
    while ~numel(strfind(line,'Gsolv')) && o < cosmoSpecAlgo*2 % Move file pointer to line of interest     
         line = fgetl(fileID);
         o = o + 1; % Prevent eternal loop
    end
    
    formatSpec = '%*n %*s %*n %*n %*n %n %*s %*s %*s';
    results=zeros(specs.n_reactants+specs.n_TS+1,1);
    for k=1:(specs.n_reactants+specs.n_TS+1) % Move line by line    
         line = fgetl(fileID);
         result{k} = textscan(line,formatSpec); % Read line
         tempmat = cell2mat(result{k});
         results(k,1) = tempmat(1,1);
    end
    fclose(fileID);
  
end    



end 
