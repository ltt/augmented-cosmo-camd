function [result,ok] = sub_Antoine(k,compound, T, specs)
% Calculates the Antoine parameters for one compound 
%
% Input:    compunds 
%           k: unique number k used for identiying files 
%           T: temperature vector 
%           specs 
%
% Output:   Antoine Parameter
%
 %% Generate jobs "jobPrint"
    jobPrint = ''; % Initialize jobPrint              
    tk1 = num2str(T(1)); % Calculation from T(1)...
    tk2 = num2str(T(end)); %... to T(lengthT)
    
    jobPrint = strcat(jobPrint, ['Pvap  tk=' tk1 ' tk2=' tk2 ' tstep=3  x={ 1 } ignore_charge # Automatic Vapor Pressure Calculation']); 
           
 %% Call COSMOtherm 
    
    [tempFile,~] = cosmoSingleCall( k, compound, specs,jobPrint);
   
 %% ReadOutputFile 
 
    [result,ok] = extractAntoine(tempFile);    

 %% Delete temporary files after calculation     

if exist([tempFile, '.inp'])>0
    delete([tempFile, '.inp'])
end
if exist([tempFile, '.tab'])>0
    delete([tempFile, '.tab'])
end
if exist([tempFile, '.out'])>0
    delete([tempFile, '.out'])
end

end

function [result,ok] = extractAntoine(tempFile)
% RETURNS [xS{1}, xS{2}, xS{3}] = Coefficients Antoine
% Check if .out file is empty
s = dir([tempFile,'.tab']);
if s.bytes == 0
    result = NaN([1,3]); % Empty file
    ok = 0;
    disp('ERROR: tab file empty');
else
    ok = 1;
    fileID = fopen([tempFile,'.tab'],'r');
    line = fgetl(fileID);
    i = 0;
    while ~numel(strfind(line,'Vapor pressure (calc.) fitted to Antoine equation ln(p)')) && i < 32000 % Move file pointer to line of interest
         line = fgetl(fileID);
         i = i + 1; % Prevent eternal loop
    end
    fclose(fileID);
    formatSpec = '%*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %f %*s %*s %f %*s %*s %f';
    xS = textscan(line,formatSpec); % Read line
    result = [xS{1}, xS{2}, xS{3}];
end
end
