function [ConfNum, sigmaProfiles, Moments ] = sub_SigmaProfilesMoments(compounds,k,specs)
% Calculates the distribution of confomers and the Sigma Profiles 
%
% Input:    compounds
%           k: unique number k used for identiying files  
%           specs 
%           
%
% Output:   [number of conformer, sigma profiles, sigma moments]
%

 %% Generate jobs "jobPrint"
    jobPrint = ''; % Initialize jobPrint   
    jobPrint = strcat(jobPrint, ['x_pure = 1 tc=25.0 ctab # Automatic Mixture Calculation', '\n']); 
    adHead = ' sprf smom'; 
    
 %% Call COSMOtherm
    [tempFile,~] = cosmoSingleCall( k, compounds, specs,jobPrint,adHead);
   
 %% ReadOutputFile 
 
    [ConfNum, sigmaProfiles, Moments ] = extractProfilesMoments(tempFile);
    
 %% Delete temporary files after calculation     
    
    if exist([tempFile, '.inp'])>0
    delete([tempFile, '.inp'])
    end
    if exist([tempFile, '.tab'])>0
    delete([tempFile, '.tab'])
    end
    if exist([tempFile, '.out'])>0
    delete([tempFile, '.out'])
    end
    if exist([tempFile, '.prf'])>0
    delete([tempFile, '.prf'])
    end
    if exist([tempFile, '.mom'])>0
    delete([tempFile, '.mom'])
    end

end

function [ConfNum, sigmaProfiles, Moments ] = extractProfilesMoments(tempFile)
%% Get Results

   %% Sigma profiles of conformers
    % Check if .out file is empty
    s = dir([tempFile,'.tab']);
    if s.bytes == 0
        result = NaN; % Empty file
        disp('ERROR: tab file empty');
    else
        fileID = fopen([tempFile,'.prf'],'r');

        % Count number of conformers
        line = fgetl(fileID);
        ConfNum = 0;
        while ~feof(fileID)
             if ~isempty(strfind(line,'# Sigma profile (s [e/A^2], p(s)) for'))
                ConfNum = ConfNum +1;     
             end
             line = fgetl(fileID);
        end
      
        fileID = fopen([tempFile,'.tab'],'r');
        line = fgetl(fileID);
        i = 0;
        while ~numel(strfind(line,'sigma')) && i < 3000 % Move file pointer to line of interest
             line = fgetl(fileID);
             i = i + 1; % Prevent eternal loop
        end
        formatSpec = repmat('%f ', 1, ConfNum+1);      

        for i = 1 : 61
            line = fgetl(fileID); % For sigma profile: information below "sigma"
            content = textscan(line,formatSpec); % Read line
            for j = 0 : ConfNum % Extract respective sigma profiles for all conformers
                sigmaProfiles(i, j+1) = content{1, j+1};
            end        
        end    
    end  
    fclose('all');
    
  %% Sigma moments of conformers
    % Check if .out file is empty
    s = dir([tempFile,'.mom']);
    if s.bytes == 0
        result = NaN; % Empty file
        disp('ERROR: tab file empty');
    else
        fileID = fopen([tempFile,'.mom'],'r');
        line = fgetl(fileID);
        i = 0;

        while ~numel(strfind(line,'Molecule')) && i < 3000 % Move file pointer to line of interest
             line = fgetl(fileID);
             i = i + 1; % Prevent eternal loop
        end
        formatSpec = ['%s ',  repmat('%f ', 1, 22)];

        for i = 1 : ConfNum
            line = fgetl(fileID);
            content = textscan(line,formatSpec); % Read line
            for j = 1 : 7 % Extract respective sigma moment for all conformers
                Moments.sigmaM(i, j) = content{1, j+1};
            end        
            for j = 1:4
                Moments.Hbacc(i, j) = content{1, j+8};
                Moments.Hbdon(i, j) = content{1, j+12};
            end
        end    
    end  
    fclose(fileID);
end


