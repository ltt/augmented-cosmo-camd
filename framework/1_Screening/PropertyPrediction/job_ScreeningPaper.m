function molStruct = job_ScreeningPaper(molStruct,T,compounds,specs)

% Calc LLE 
%    molStruct = job_logP(molStruct,T,compounds,specs);
    
%% Get Mex-RBM

%    molStruct = job_Mex_RBM(molStruct,T,compounds,specs);
    
   
    for k =1:length(molStruct)
        
        ant = molStruct(k).AntoineParam;
        alpha = molStruct(k).AlphaParam{1,3};
        tau12 = molStruct(k).TauParam{1,3};
        tau21 = molStruct(k).TauParam{3,1};
        
        T = 298.15; % in K 
        p = 1000; % in mbar
        
        for i = 1:3
            molStruct(k).T_boil(i) = calc_T_boil(ant{i},p);
            molStruct(k).P_sat(i) = calc_psat(ant{i},T);
            molStruct(k).dHvap(i) = calc_dHvap(ant{i},T);
        end
        
        molStruct(k).P = exp(molStruct(k).gammaII(1)) / exp(molStruct(k).gammaI(1));
        molStruct(k).AlphaDes = calc_Alpha(ant{1},ant{3},alpha,tau12,tau21,T);
        
        
        
        %% New Alpha
        
        p_gvl = calc_psat(ant{1},T);
        p_h2o = calc_psat(ant{2},T);
    
        %% Gamma H2O
        
        gamma_h2o = molStruct(k).resultsGamma{2,3}{1,1}(1,1);

        %% Gamma GVL
        
        gamma_gvl = molStruct(k).resultsGamma{1,3}{1,1}(1,1);
                
        %%
        molStruct(k).AlphaRev = gamma_h2o / gamma_gvl * p_h2o / p_gvl;
                
    end

end


function p_sat = calc_psat(ant,T)

    p_sat = exp(ant(1) - ant(2)/(ant(3) + T) );

end


function T_boil = calc_T_boil(ant,p)

    T0 = 500;
    
    T_boil = fsolve( @(T) antFuncn(ant,p,T) ,T0,optimset('Display','off'));

end

function d = antFuncn(ant,p,T)

    d = exp(ant(1) - ant(2)/(ant(3) + T)) - p;

end

function alphaDes = calc_Alpha(ant1,ant2,alpha,tau12,tau21,T)

    p_gvl = calc_psat(ant1,T);
    p_sol = calc_psat(ant2,T);
    
    temp = calc_gamma(alpha,tau12,tau21,T,1); 
    gamma_sol = exp(temp(2));
    temp = calc_gamma(alpha,tau12,tau21,T,0);
    gamma_gvl = exp(temp(1));
    
    alphaDes = gamma_sol / gamma_gvl * p_sol / p_gvl;
    
end


function gamma = calc_gamma(alpha,tau12,tau21,T,x)

a = alpha(1) + alpha(2) * T;

t12 = tau12(1) + tau12(2) / T + tau12(3) *log(T)  + tau12(4) * T;

t21= tau21(1) + tau21(2) / T + tau21(3) *log(T)  + tau21(4) * T;

x1 = x;
x2 = 1-x;

G12= exp(-a * t12);
G21= exp(-a * t21);

gamma1 = x2^2 *(t21.*(G21./(x1+x2.*G21)).^2+t12*G12./(x2+x1.*G12).^2);
gamma2 = x1.^2.*(t12.*(G12./(x2+x1.*G12)).^2+t21*G21./(x1+x2.*G21).^2);

gamma = [gamma1,gamma2];

end

function dHvap = calc_dHvap(ant,T)

R = 8.314;
dHvap = -ant(2)/((T+ant(3))^2)*R*T^2;

end