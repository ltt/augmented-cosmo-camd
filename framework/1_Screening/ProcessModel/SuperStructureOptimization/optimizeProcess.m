function molStruct = optimizeProcess( molStruct, specs )
%% optimizes the Process in runProcess2 with respect to the process paramters in p

%% Defintions
if strcmp(specs.storage, 'DMF')
	lb = [1,0.5]; % lower bound
	ub = [2,5]; % upper bound
elseif strcmp(specs.storage, 'MeF')
	lb = [50,0.5]; % lower bound
    ub = [200,5]; % upper bound
elseif strcmp(specs.storage, 'DEF')
	lb = [1,0.5]; % lower bound
    ub = [2,5]; % upper bound
end
options = optimoptions('fmincon', 'Algorithm', 'interior-point', 'MaxFunEvals', 1000, 'Display', 'off');


%% Preallocate fields
for k=1:length(molStruct)
    molStruct(k).Topology = [];
    molStruct(k).ProcessStructure = [];
    molStruct(k).Variant = [];
    molStruct(k).BlockResults = [];
    molStruct(k).Exergyloss = [];
end


%% Optimization
parfor k=1:length(molStruct)
    disp(k)
    %% Preallocate
    popt = cell(4,1);
    fmin = zeros(4,1);

    % optimize Structure by examining all variants
    for variant = 1 : 4
        % optimize process parameters for each variant
        % choose initial value
        if (variant == 1 || variant == 3)
            if strcmp(specs.storage, 'DMF')
                p0 = [1,5];
            elseif strcmp(specs.storage, 'MeF')
                p0 = [100,5];
            elseif strcmp(specs.storage, 'DEF')
                p0 = [1,5];
            end
        else
            if strcmp(specs.storage, 'DMF')
                p0 = [1,1];
            elseif strcmp(specs.storage, 'MeF')
                p0 = [100,1];
            elseif strcmp(specs.storage, 'DEF')
                p0 = [1,1];
            end
        end
        func = @(p) runProcess(molStruct(k), variant, p, specs); % objective function
        [pval,fval,exitflag,~] = fmincon(func,p0,[],[],[],[],lb,ub,[],options);
        % check results
        if (exitflag > 0)
            popt{variant} = pval;
            fmin(variant) = fval;
        else
            popt{variant} = NaN;
            fmin = 1e100;
        end
    end

    %% Find optimal flow sheet
    [~, ranks] = sort(fmin);
    optFlowSheet = ranks(1);
    optPara = popt{ranks(1)};

    %% runFlowsheet finally in best variant with optimized parameters
    molStruct_temp(k) = runProcess_opt(molStruct(k), optFlowSheet, optPara, specs);
    
end
    

%% Collect results
for k=1:length(molStruct)
    molStruct(k) = molStruct_temp(k);
    
end


end

