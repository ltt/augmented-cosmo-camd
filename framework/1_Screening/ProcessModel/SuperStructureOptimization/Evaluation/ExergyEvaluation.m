function [ Eloss ] = ExergyEvaluation (molStruct)
%% Calculates the exergy loss [MJ] per kmol of produced CO for the CO process
% 3 contributions: electric power for compression [+], condenser duty [-], and reboiler duty [+] 


%% read the relevant data and calculate contributions

% check for succes
if (~isempty(molStruct.BlockResults.DistColumn1.Results) && ~isempty(molStruct.BlockResults.DistColumn2.Results))

    % compressor duty
    p = molStruct.ProcessStructure(1).specs.p;
    T = molStruct.ProcessStructure(1).specs.T;
    R = 8.314e-3; % universal gas constant [MJ/kmolK]
    n = sum(molStruct.BlockResults.StorageReactor.Inflow(1:2));

    Wcomp = n*R*T*log(p); % formula for isothermal (=compressor with infinite number of stages) compression
    
    % exergy for vaporizing reforming feed
    x_in = molStruct.BlockResults.ReformingReactor.Inflow / sum(molStruct.BlockResults.ReformingReactor.Inflow); % composition
    Tboil = fsolve(@(T) BubblePoint(T,x_in,1e5,molStruct.AntoineParam),500,optimset('Display', 'off')); % boiling point
    dhv_Tboil = 0; % enthalpy of vaporization at BP
    for j = 1 : 7
        dhv_Tboil = dhv_Tboil + x_in(j) * (molStruct.AntoineParam{j}(2)/(Tboil+molStruct.AntoineParam{j}(3))^2 * R*Tboil^2); %TDG Formelsammlung from Clausius Clayperon
    end
    Qv = sum(molStruct.BlockResults.ReformingReactor.Inflow) * dhv_Tboil; % heating duty
    Ev = Qv * (1 - 298.15/Tboil);
        
    % exergies of duties of column 1
    QB = molStruct.BlockResults.DistColumn1.Results.QB; % reboiler duty
    TB = molStruct.BlockResults.DistColumn1.Results.TB; % reboiler temperature
    Eqboil = QB * (1 - 298.15/TB); % Carbot factor

    QC = molStruct.BlockResults.DistColumn1.Results.QC; % condenser duty
    TC = molStruct.BlockResults.DistColumn1.Results.TC; % condenser temperature
    Eqcond = QC * (1 - 298.15/TC); % Carbot factor

    % exergies of duties of column 2
    if isfield(molStruct.BlockResults.DistColumn2.Results, 'QB') % check for heteroazeotropic distillation
        QB = molStruct.BlockResults.DistColumn2.Results.QB; % reboiler duty
        TB = molStruct.BlockResults.DistColumn2.Results.TB; % reboiler temperature
        Eqboil = Eqboil + QB * (1 - 298.15/TB); % Carnot factor

        QC = molStruct.BlockResults.DistColumn2.Results.QC; % condenser duty
        TC = molStruct.BlockResults.DistColumn2.Results.TC; % condenser temperature
        Eqcond = Eqcond + QC * (1 - 298.15/TC); % Carnot factor
    else
        QB1 = molStruct.BlockResults.DistColumn2.Results.QB1; % reboiler1 duty
        TB1 = molStruct.BlockResults.DistColumn2.Results.TB1; % reboiler1 temperature
        QB2 = molStruct.BlockResults.DistColumn2.Results.QB2; % reboiler2 duty
        TB2 = molStruct.BlockResults.DistColumn2.Results.TB2; % reboiler2 temperature
        Eqboil = Eqboil + QB1 * (1 - 298.15/TB1) + QB2 * (1 - 298.15/TB2); % Carnot factor

        QC1 = molStruct.BlockResults.DistColumn2.Results.QC1; % condenser duty
        TC1 = molStruct.BlockResults.DistColumn2.Results.TC1; % condenser temperature
        QC2 = molStruct.BlockResults.DistColumn2.Results.QC2; % condenser duty
        TC2 = molStruct.BlockResults.DistColumn2.Results.TC2; % condenser temperature
        Eqcond = Eqcond + QC1 * (1 - 298.15/TC1) + QC2 * (1 - 298.15/TC2); % Carnot factor
    end

    % sum up
    Eloss = Wcomp + Ev + Eqboil + Eqcond;

    % CO Production
    nCO = molStruct.BlockResults.ReformingReactor.Outflow(3);

    % Exergyloss per CO
    Eloss = Eloss / nCO;
else
    Eloss = [];
end


end


function F = BubblePoint(T,x,P,AntoineParam)

N = length(AntoineParam);

F=-P;

for i=1:N % consider only liquids
   
 F = F + 100 * (x(i)*exp(AntoineParam{i,1}(1)-AntoineParam{i,1}(2)/(T+AntoineParam{i,1}(3)))); 
    
end

end