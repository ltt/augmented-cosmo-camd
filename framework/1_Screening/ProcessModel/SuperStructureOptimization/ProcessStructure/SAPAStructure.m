%% Writes the process structure struct for CO process variant SBPB

% general
N = 7; % total number of species
n0 = [1, 1, 0, 1, 0, 1, 0]; % Inflow for flowsheet [kmol/s]
%n0 = zeros(1,N);

% 1st block: storage reactor
structureStruct(1).type = 'VLLEReactor';
structureStruct(1).name = 'StorageReactor';
structureStruct(1).species = 1:7;
structureStruct(1).inflow = [];
structureStruct(1).specs.T = 298.15;
if strcmp(storage, 'DEF')
    structureStruct(1).specs.p= 1;
elseif strcmp(storage, 'DMF')
    structureStruct(1).specs.p = 1;
elseif strcmp(storage, 'MeF')
    structureStruct(1).specs.p = 100;
end
if strcmp(storage, 'DEF')
    structureStruct(1).specs.deltaGR = 5110; % [J/mol] (Values from Jens 2016)
elseif strcmp(storage, 'DMF')
    structureStruct(1).specs.deltaGR = 9920; % [J/mol] (Values from Jens 2016)
elseif strcmp(storage, 'MeF')
    structureStruct(1).specs.deltaGR = 24900; % [J/mol] (Values from Jens 2016)
end
structureStruct(1).specs.v = [-1, -1, 0, 1, 1, -1, 0];
structureStruct(1).specs.phases = 'VLE';
structureStruct(1).specs.liquids = 4:7;
if strcmp(storage, 'DEF')
    structureStruct(1).specs.xi0 = 0.3;
elseif strcmp(storage, 'DMF')
    structureStruct(1).specs.xi0 = 0.9;
elseif strcmp(storage, 'MeF')
    structureStruct(1).specs.xi0 = 1e-3;
end

% 2nd block: extraction
structureStruct(2).type = 'Mextraction';
structureStruct(2).name = 'ExtrColumn1';
structureStruct(2).species = 4:7;
structureStruct(2).inflow = [1,2];
structureStruct(2).specs.p = 1;
structureStruct(2).specs.t = 25;
structureStruct(2).specs.xS = [0,0,0,1];
structureStruct(2).specs.purity = 0;
structureStruct(2).specs.raffinate = 0;
structureStruct(2).specs.solvent = 3;

% 3rd block: reforming reactor
structureStruct(3).type = 'RStoic';
structureStruct(3).name = 'ReformingReactor';
structureStruct(3).species = 3:7;
structureStruct(3).inflow = [2,1];
structureStruct(3).specs.v = [1, 0, -1, 1, 0];
structureStruct(3).specs.turnover = 1; % full turnover of storage molecule
structureStruct(3).specs.targetcompound = 3;

% 4th block: purification
structureStruct(4).type = 'RBM';
structureStruct(4).name = 'DistColumn1';
structureStruct(4).species = 4:7;
structureStruct(4).inflow = [3,1];
structureStruct(4).specs.p = 1;
structureStruct(4).specs.tf = 0;
structureStruct(4).specs.compsontop = [];

% 6th block: purification
structureStruct(5).type = [];
structureStruct(5).name = 'DistColumn2';
structureStruct(5).species = 4:7;
structureStruct(5).inflow = [];
structureStruct(5).specs.p = 1;
structureStruct(5).specs.tf = 0;

%% Fill remaining fields of the structure struct
SAPATopCheck;
