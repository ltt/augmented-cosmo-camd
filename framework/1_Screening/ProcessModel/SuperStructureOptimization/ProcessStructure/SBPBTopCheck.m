%% Fills missing fields of structureStruct by checking the topology of the mixture

AntoineParam = molStruct(k).AntoineParam;
AlphaParam = molStruct(k).AlphaParam;
TauParam = molStruct(k).TauParam;

%% Definitions

possible = 1; % 0 = process not possible for topology
heteroazeotropic = 0; % indicates if a heteroazeotropic column is needed

%% Initiate RBM to check azeotropic table

system = ['Data/DataRBM.dat']; % Data for mexrbm  
hdl = mexrbm('init',['',system]);

%% Set Antoine parameters
% Change Antoine equation parameters from ln(p)= A-B/(T+C) in mbar (COSMOtherm 

for i = 4 : 7
    k1 = i - 3;
    antc1 = AntoineParam{i}(1) + log(100) ;
    antc2 = -AntoineParam{i}(2);
    antc3 = AntoineParam{i}(3);
    antc4 = 0;
    antc5 = 0;
    antc6 = 0;
    antc7 = 0;
    antc8 = 0;
    antc9 = 9000;
    ok = mexrbm('set_antoine',hdl,antc1,antc2,antc3,antc4,antc5,antc6,antc7,antc8,antc9,k1);
end

    

%% Set NRTL parameters
    
for i = 4 : 6
    for j = (i + 1) : 7
        k1 = i - 3;
        k2 = j - 3;
        alpha = AlphaParam{i, j}(1) + 273.15*AlphaParam{i, j}(2); % mexprops expects alpha for C
        alphat = AlphaParam{i, j}(2) ;
        tau12_1 = TauParam{i, j}(1) ;
        tau12_2 = TauParam{i, j}(2) ;
        tau12_3 = TauParam{i, j}(3) ;
        tau12_4 = TauParam{i, j}(4) ;
        tau21_1 = TauParam{j, i}(1) ;
        tau21_2 = TauParam{j, i}(2) ;
        tau21_3 = TauParam{j, i}(3) ;
        tau21_4 = TauParam{j, i}(4) ;
        ok = mexrbm('set_nrtl_alpha',hdl,alpha,alphat,k1,k2);
        ok = mexrbm('set_nrtl_tau',hdl,tau12_2,tau12_1,tau12_3,tau12_4,k1,k2);
        ok = mexrbm('set_nrtl_tau',hdl,tau21_2,tau21_1,tau21_3,tau21_4,k2,k1);
    end
end
    
%% Calculate results
    
% check azeotropes (at 1 bar)
azeo = mexrbm('azeo',hdl,[],[],[],[],1);
             
a = size(azeo);
if a(2) == 0
    possible = 0;
end
clear a;

% check for homogeneous azeotropes
for j = 1 : length(azeo)
    if (azeo(j).type == 1)
        possible = 0;  % process not possible for homogeneous azeotropes
        break;
    end
end

if possible
    % check for heterogenoeous azeotropes
    for j = 1 : length(azeo)
        if (azeo(j).type == 3)
            % allow binary heterogeneous azeotropes between water(1) and
            % solvent(4)
            if (azeo(j).x(2) == 0 && azeo(j).x(3) == 0)
                heteroazeotropic = 1;
            else
                possible = 0; % process not possible for ternary etc. het. azeotropes
                break;
            end
        end
    end
end

if possible
    % get boiling points and boiling sequence
    for i = 1 : 4
        T_boil(i) = azeo(i).t;
    end    
    [~, sequence] = sort(T_boil);
    % restrict to solvents which are liquid at room temperature
    if (T_boil(4)<25)
        possible = 0;
    end
end

%% Sequence for zeotropic mixtures

if (possible && ~heteroazeotropic)
    structureStruct(3).type = 'RBM'; % third block is normal RBM    
    % check if storage molecule(2) and add. reactant(3) have consecutive
    % boiling points (bp) -> otherwise PB variants are not possible
    bpok = 0;
    % search bp of storage molecule
    for i = 1 : 3
        if ((sequence(i) == 2 && sequence(i + 1) == 3) || (sequence(i) == 3 && sequence(i + 1) == 2))
            bpok = 1;
            index = i;
            break;
        end
    end
    if ~bpok
        possible = 0;
        return;
    end

    if (index == 1) % always remove hk
        % first column distillate
        structureStruct(2).specs.compsontop = 1:4;
        structureStruct(2).specs.compsontop(sequence(4)) = [];
        % second column inflow
        structureStruct(3).inflow = [2, 1];
        % second column distillate
        structureStruct(3).specs.compsontop = 1:4;
        structureStruct(3).specs.compsontop([sequence(4),sequence(3)]) = [];
        % reforming inflow
        structureStruct(4).inflow = [3,1];
    elseif (index == 2) % 1st: remove lk, 2nd: remove hk
        % first column distillate
        structureStruct(2).specs.compsontop = sequence(1);
        % second column inflow
        structureStruct(3).inflow = [2, 2];
        % second column distillate
        structureStruct(3).specs.compsontop = 1:4;
        structureStruct(3).specs.compsontop([sequence(1),sequence(4)]) = [];
        % reforming inflow
        structureStruct(4).inflow = [3,1];
    elseif (index == 3) % always remove lk
        % first column distillate
        structureStruct(2).specs.compsontop = sequence(1);
        % second column inflow
        structureStruct(3).inflow = [2,2];
        % second column distillate
        structureStruct(3).specs.compsontop = sequence(2);
        % reforming inflow
        structureStruct(4).inflow = [3,2];
    end
end

%% Sequence for azeotropic mixtures

if (possible && heteroazeotropic)
    structureStruct(3).type = 'HetRBM'; % third block is heteroazeotropic RBM
    % check if storage molecule (2) and add reactant (3) are either
    % the two lightest or the two heaviest components -> otherwise
    % PB variants are not possible, i.e. the following split must be
    % possible: storage mol + add reactant & water + solvent
    bpok = 0;
    if (T_boil(2) == min(T_boil) && T_boil(3) == min (T_boil([1,3,4])))
        bpok = 1;
        % 1st column: storage and add reactant at top, 2nd column: heteroazetropic, 
        % first column distillate
        structureStruct(2).specs.compsontop = [2,3];
        % second column inflow
        structureStruct(3).inflow = [2, 2];
        % second column bottoms
        structureStruct(3).specs.compinfirst = 1;
        structureStruct(3).specs.compinsecond = 4;
        % reforming inflow
        structureStruct(4).inflow = [2,1];
    elseif (T_boil(3) == min(T_boil) && T_boil(2) == min (T_boil([1,2,4])))
        bpok = 1;
        % like above
        % first column distillate
        structureStruct(2).specs.compsontop = [2,3];
        % second column inflow
        structureStruct(3).inflow = [2, 2];
        % second column bottoms
        structureStruct(3).specs.compinfirst = 1;
        structureStruct(3).specs.compinsecond = 4;
        % reforming inflow
        structureStruct(4).inflow = [2,1];
    elseif (T_boil(2) == max(T_boil) && T_boil(3) == max(T_boil([1,3,4])))
        bpok = 1;
        % 1st column: storage and add reactant at bottom,  2nd column: heteroazetropic, 
        % first column distillate
        structureStruct(2).specs.compsontop = [1,4];
        % second column inflow
        structureStruct(3).inflow = [2, 1];
        % second column bottoms
        structureStruct(3).specs.compinfirst = 1;
        structureStruct(3).specs.compinsecond = 4;
        % reforming inflow
        structureStruct(4).inflow = [2,2];
    elseif (T_boil(3) == max(T_boil) && T_boil(2) == max(T_boil([1,2,4])))
        bpok = 1;
        % like above
        % first column distillate
        structureStruct(2).specs.compsontop = [1,4];
        % second column inflow
        structureStruct(3).inflow = [2, 1];
        % second column bottoms
        structureStruct(3).specs.compinfirst = 1;
        structureStruct(3).specs.compinsecond = 4;
        % reforming inflow
        structureStruct(4).inflow = [2,2];             
    end        
    if ~bpok
        possible = 0;
    end
end
   