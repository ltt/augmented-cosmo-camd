%% Fills missing fields of structureStruct by checking the topology of the mixture

AntoineParam = molStruct(k).AntoineParam;
AlphaParam = molStruct(k).AlphaParam;
TauParam = molStruct(k).TauParam;

%% Definitions

possible = 1; % 0 = process not possible for topology
heteroazeotropic = 0; % indicates if a heteroazeotropic column is needed

%% Initiate RBM to check azeotropic table

system = ['Data/DataRBM.dat']; % Data for mexrbm  
hdl = mexrbm('init',['',system]);

%% Set Antoine parameters
% Change Antoine equation parameters from ln(p)= A-B/(T+C) in mbar (COSMOtherm 

for i = 4 : 7
    k1 = i - 3;
    antc1 = AntoineParam{i}(1) + log(100) ;
    antc2 = -AntoineParam{i}(2);
    antc3 = AntoineParam{i}(3);
    antc4 = 0;
    antc5 = 0;
    antc6 = 0;
    antc7 = 0;
    antc8 = 0;
    antc9 = 9000;
    ok = mexrbm('set_antoine',hdl,antc1,antc2,antc3,antc4,antc5,antc6,antc7,antc8,antc9,k1);
end

    

%% Set NRTL parameters
    
for i = 4 : 6
    for j = (i + 1) : 7
        k1 = i - 3;
        k2 = j - 3;
        alpha = AlphaParam{i, j}(1) + 273.15*AlphaParam{i, j}(2); % mexprops expects alpha for C
        alphat = AlphaParam{i, j}(2) ;
        tau12_1 = TauParam{i, j}(1) ;
        tau12_2 = TauParam{i, j}(2) ;
        tau12_3 = TauParam{i, j}(3) ;
        tau12_4 = TauParam{i, j}(4) ;
        tau21_1 = TauParam{j, i}(1) ;
        tau21_2 = TauParam{j, i}(2) ;
        tau21_3 = TauParam{j, i}(3) ;
        tau21_4 = TauParam{j, i}(4) ;
        ok = mexrbm('set_nrtl_alpha',hdl,alpha,alphat,k1,k2);
        ok = mexrbm('set_nrtl_tau',hdl,tau12_2,tau12_1,tau12_3,tau12_4,k1,k2);
        ok = mexrbm('set_nrtl_tau',hdl,tau21_2,tau21_1,tau21_3,tau21_4,k2,k1);
    end
end
    
%% Calculate results
    
% check azeotropes (at 1 bar)
azeo = mexrbm('azeo',hdl,[],[],[],[],1);
             
a = size(azeo);
if a(2) == 0
    possible = 0;
end
clear a;

% check for homogeneous azeotropes
for j = 1 : length(azeo)
    if (azeo(j).type == 1)
        % allow azeotrope between one component and storage molecule (2) in PA
        % variants
        if (azeo(j).x(3) == 0 && azeo(j).x(4) == 0)
        elseif (azeo(j).x(1) == 0 && azeo(j).x(4) == 0)
        elseif (azeo(j).x(1) == 0 && azeo(j).x(3) == 0)
        % process not possible for all other homogeneous azeotropes
        else
            possible = 0; 
            break;
        end
    end
end

if possible
    % check for heterogenoeous azeotropes
    for j = 1 : length(azeo)
        if (azeo(j).type == 3)
            % allow binary heterogeneous azeotropes between water(1) and
            % solvent(4)
            if (azeo(j).x(2) == 0 && azeo(j).x(3) == 0)
                heteroazeotropic = 1;
            % allow azeotrope one component and storage molecule (2) in PA
            % variants
            elseif (azeo(j).x(3) == 0 && azeo(j).x(4) == 0)
            elseif (azeo(j).x(1) == 0 && azeo(j).x(4) == 0)
            elseif (azeo(j).x(1) == 0 && azeo(j).x(3) == 0)
            else
                possible = 0; % process not possible for ternary etc. het. azeotropes
                break;
            end
        end
    end
end

if possible
    % get boiling points and boiling sequence
    for i = 1 : 4
        T_boil(i) = azeo(i).t;
    end
    % exclude storage molecule in PA variants
    T_boil(2) = inf;
    [~, sequence] = sort(T_boil);
    sequence(end) = [];
    % restrict to solvents which are liquid at room temperature
    if (T_boil(4)<25)
        possible = 0;
    end
end


%% Sequence for zeotropic mixtures

if (possible && ~heteroazeotropic)
    structureStruct(4).type = 'RBM'; % fourth block is normal RBM    
    % sufficient to always remove lk on top
    for j = 1 : 2
        structureStruct(j+2).specs.compsontop = sequence(j);
        if (j>1)
            structureStruct(j+2).inflow = [j+1,2];
        end
    end
end

%% Sequence for azeotropic mixtures

if (possible && heteroazeotropic)
    structureStruct(4).type = 'HetRBM'; % fourth block is heteroazeotropic RBM    
    % check if water (1) and solvent (4) have consecutive
    % boiling points (bp) -> otherwise PA variants are not possible
    bpok = 0;
    % search bp of water
    for i = 1 : 2
        if ((sequence(i) == 1 && sequence(i + 1) == 4) || (sequence(i) == 4 && sequence(i + 1) == 1))
            bpok = 1;
            index = i;
            break;
        end
    end
    if ~bpok
        possible = 0;
        return;
    end
    
    if (index == 1) % remove add reactant (hk), then heteroazeotropic
        % first column distillate
        structureStruct(3).specs.compsontop = [1,4];
        % second column inflow
        structureStruct(4).inflow = [3, 1];
        % second column bottoms
        structureStruct(4).specs.compinfirst = 1;
        structureStruct(4).specs.compinsecond = 4;
    elseif (index == 2) % remove add reactant (lk), then heteroazeotropic
        % first column distillate
        structureStruct(3).specs.compsontop = 3;
        % second column inflow
        structureStruct(4).inflow = [3, 2];
        % second column bottoms
        structureStruct(4).specs.compinfirst = 1;
        structureStruct(4).specs.compinsecond = 4;
    end       
end
   