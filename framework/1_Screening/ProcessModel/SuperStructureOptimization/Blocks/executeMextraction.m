function [n_in, n_out, results] = executeMextraction(n_in, specs, properties)
%% main execution of the RBM model
% Input:    n_in (inflow of only relevant species)
%           specs (struct containing specifications (t [C], p [bar], Solvent composition Xs,
%           purity of target compound, raffinate (index of carrier) solvent (index of solvent))
% Output:   nout (outflow (matrix with two rows, E: row 1, R: row 2))
%           results (results struct (QB [MJ], QC [MJ], TB [K], TC [K], R [-])))

%% Preallocate fields
results = [];

n_out = zeros(2,length(n_in));

%% Check if previous block finished succesful

if (max(n_in) == 0)
    return;
end

%% Definitions

% temperature
t = specs.t; % [C]
Tk = t + 273.15; % [K]

% pressure
p = specs.p; % [bar]

% species considered
NC = length(n_in);

% solvent composition
xS = specs.xS;

% purity
purity = specs.purity;

% raffinate
raffinate = specs.raffinate;

% solvent
solvent = specs.solvent;


%% Thermodynamic Properties

AlphaParam = properties.AlphaParam;
TauParam = properties.TauParam;

% Calculate alpha_ij and tau_ij for given temperature
[tau, alpha] = nrtlParam(TauParam, AlphaParam, Tk);

%% Initiate mex

system = ['Data/DataMex.dat']; % Data for mexrbm  
hpd = mextraction('init',['',system]);

%% Set NRTL Parameters

for i = 1 : (NC-1)
    for j = (i + 1) : NC
        k1 = i - 1;
        k2 = j - 1;
        alpha12 = alpha(i, j);
        tau12 = tau(i, j);
        tau21 = tau(j, i);
        ok = mextraction('set_alpha',hpd,alpha12,k1,k2);
        ok = mextraction('set_tau',hpd,tau12,k1,k2);
        ok = mextraction('set_tau',hpd,tau21,k2,k1);
    end
end

%% Molar balances

% Feed
xF = n_in / sum(n_in) ;

[extrResult,ok] = mextraction('EXMIN',hpd,t,p,xF,xS,purity,raffinate,solvent);
clear mex
if(ok>0)
    n_out(1,:) = extrResult.EN * extrResult.xen; % Extract flow
    n_out(2,:) = extrResult.R1 * extrResult.xr; % Raffinate flow flow
    results.SFmin = extrResult.S; % minimal solvent demand
end

clear mex;

end

function [tau, alpha] = nrtlParam(tauParam, alphaParam, T)
%% Calculates NRTL parameters (tau, alpha) for a given temperature T

    n = length(tauParam) ;

    % NRTL:
    alpha = ones(n);
    tau = zeros(n);
    for i = 1:n
        for j = 1:n
            % tau_ij(T) = a_ij + b_ij / T + c_ij * ln(T) + dij * T
            tau(i,j) = tauParam{i,j}(1)	+ tauParam{i,j}(2)/T + tauParam{i,j}(3)*log(T) + tauParam{i,j}(4)*T;
            % alpha_ij(T) = alpha_ij,0 + alpha_ij,1 * T
            alpha(i,j) = alphaParam{i,j}(1) + alphaParam{i,j}(2)*T;
        end
    end
end