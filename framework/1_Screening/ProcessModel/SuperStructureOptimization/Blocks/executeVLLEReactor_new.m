function [n_in, n_out, results] = executeVLLEReactor(n_in, specs, properties)
%% main execution of the VLLE Reactor model
% Input:    n_in (inflow of only relevant species)
%           specs (struct containing specifications (p [bar], T [K],
%           deltaGR [J/mol], v (stoichiometric coefficients, phases (only
%           ideal gas: V, VLE, VLLE), key compounds of firstliquid,
%           reacting phase (only for VLLE), liquids (index of compounds
%           that should be treated as liquids), xi0 (initial guess for turnover))
% Output:   nout (outlow (matrix with max three rows: 1st: V, 2nd: L1, 3rd: L2))
%           results (results struct (extent of reaction xi))

%% Preallocate fields

results = [];

if strcmp(specs.phases, 'V')
    n_out = zeros(1,length(n_in));
elseif strcmp(specs.phases, 'VLE')
    n_out = zeros(2,length(n_in));        
elseif strcmp(specs.phases, 'VLLE')
    n_out = zeros(3,length(n_in));
end

%% Check if previous block finished succesful

if (max(n_in) == 0)
    return;
end

%% Define Function  to be solved

if strcmp(specs.phases, 'V')
    func = @(xi) IGReactor_mismatch(n_in, specs, xi);
elseif strcmp(specs.phases, 'VLE')
    func = @(xi) VLEReactor_mismatch(n_in, specs, properties, xi);        
elseif strcmp(specs.phases, 'VLLE')
    func = @(xi) VLLEReactor_mismatch(n_in, specs, properties, xi);
end

xi0 = specs.xi0;
func(xi0);

% Solve the equation system with fsolve
options = optimoptions('fsolve', 'MaxFunEvals', 1e6, 'MaxIter', 5e3, 'TolFun', 1e-8, 'Display', 'off');
[xi_final, ~, exitflag, ~] = fsolve(func, xi0, options);

% Check solution and get results
if (exitflag > 0 && xi_final >= 0 && xi_final <= 1)
    results.xi = xi_final;
    if strcmp(specs.phases, 'V')
        n_out = IGReactor(n_in, specs, xi_final);
    elseif strcmp(specs.phases, 'VLE')
        n_out = VLEReactor(n_in, specs, properties, xi_final);        
    elseif strcmp(specs.phases, 'VLLE')
        n_out = VLLEReactor(n_in, specs, properties, xi_final);
    end
end

clear mex;


end

function [ error ] = IGReactor_mismatch( n_in, specs, xi)
%% Solves the equation for an isothermal ideal gas reactor
% output is the error in reaction equilibrium

%% Definitions

% standard conditions
p0 = 1; % [bar]

% pressure
p = specs.p; % [bar]

% temperature
T = specs.T; % [bar]

% universal gas constant
R = 8.314; % [J / (mol * K)]

% species considered
NC = length(n_in);

% stoichiometric coefficients
v = specs.v;

% free ideal gas enthalpy of reaction
deltaGR_ig = specs.deltaGR;

%% Mass balance

nout = n_in + v * xi;

y = nout / sum(nout);

%% Check Reaction equilibria

Kf = exp(-deltaGR_ig / R / T);
Ky = Kf * (p0 / p) ^ sum(v);

error = Ky - prod(y .^ v);

end

function [ nout ] = IGReactor( n_in, specs, xi)
%% Solves the equation for an isothermal ideal gas reactor

%% Definitions

% stoichiometric coefficients
v = specs.v;

%% Mass balance

nout = n_in + v * xi;

end

function [ error ] = VLEReactor_mismatch( n_in, specs, properties, xi)
%% Solves the phase split problem for given turnover xi
% output is the error in reaction equilibrium

%% Definitions

% standard conditions
p0 = 1; % [bar]

% pressure
p = specs.p; % [bar]

% temperature
T = specs.T; % [bar]

% universal gas constant
R = 8.314; % [J / (mol * K)]

% species considered
NC = length(n_in);

% liquids
liquids = specs.liquids;
NL = length(liquids);

% gases
gases = 1:NC;
gases(liquids) = [];
NG = length(gases);

% stoichiometric coefficients
v = specs.v;

% free ideal gas enthalpy of reaction
deltaGR_ig = specs.deltaGR;

%% Thermodynamic Properties

AntoineParam = properties.AntoineParam;
AlphaParam = properties.AlphaParam;
TauParam = properties.TauParam;

%% Initiate mex

system = ['Data/DataVLLE.dat']; % Data for mexprops  
hdl = mexprops('init', ['' system]);

%% Set Antoine parameters
    % Change Antoine equation parameters from ln(p)= A-B/(T+C) in mbar (COSMOtherm) 

    for i = 1 : NC
        k1 = i - 1;
        antc1 = AntoineParam{i}(1) + log(100) ;
        antc2 = -AntoineParam{i}(2);
        antc3 = AntoineParam{i}(3);
        antc4 = 0;
        antc5 = 0;
        antc6 = 0;
        antc7 = 0;
        antc8 = 0;
        antc9 = 9000;
        ok = mexprops('set_antoine',hdl,antc1,antc2,antc3,antc4,antc5,antc6,antc7,antc8,antc9,k1);
    end
    
%% Set NRTL parameters
    
    for i = 1 : (NC - 1)
        for j = (i + 1) : NC
            k1 = i - 1;
            k2 = j - 1;
            alpha = AlphaParam{i, j}(1) + 273.15*AlphaParam{i, j}(2); % mexprops expects alpha for C
            alphat = AlphaParam{i, j}(2) ;
            tau12_1 = TauParam{i, j}(1) ;
            tau12_2 = TauParam{i, j}(2) ;
            tau12_3 = TauParam{i, j}(3) ;
            tau12_4 = TauParam{i, j}(4) ;
            tau21_1 = TauParam{j, i}(1) ;
            tau21_2 = TauParam{j, i}(2) ;
            tau21_3 = TauParam{j, i}(3) ;
            tau21_4 = TauParam{j, i}(4) ;
            ok = mexprops('set_nrtl_alpha',hdl,alpha,alphat,k1,k2);
            ok = mexprops('set_nrtl_tau',hdl,tau12_2,tau12_1,tau12_3,tau12_4,k1,k2);
            ok = mexprops('set_nrtl_tau',hdl,tau21_2,tau21_1,tau21_3,tau21_4,k2,k1);
        end
    end

%% calculate VLE split

% get feed by solving mass balances for given xi
for i = 1 : NC
    nF(i) = n_in(i) + v(i) * xi;
end
zF = nF / sum(nF);

% liquid flow
for i = 1 : NL
    nL(i) = n_in(liquids(i)) + v(liquids(i)) * xi;
end
x = nL / sum(nL);

% gaseous flow
for i = 1 : NG
    nG(i) = n_in(gases(i)) + v(gases(i)) * xi;
end
zG = nG / sum(nG);

vapour = sum(nG) / sum(nF); % vapor fraction
t = T - 273.15; % temperature in C

[x,~,~,~,vapour]=mexprops('tpz_flash',hdl,zF,x,zG,t,p,vapour);
if (isempty(x) || vapour < 0) % flash has failed
    error = 1000;
    return;
end
     
%% Check Reaction equilibria

% Activity coefficients
% get NRTL parameters
[Tau, Alpha] = nrtlParam(TauParam, AlphaParam, T);
gamma = NRTL(x, NC, Alpha, Tau);

% psat calculations
psat = Psat (AntoineParam, T);

% Equilibrium constant for solution in Solvent
Kf = exp(-deltaGR_ig / R / T);
Ka = prod((p0 ./ psat) .^ v) * Kf;

error = Ka - prod(x .^ v) * prod(gamma .^ v);

end

function [ nout ] = VLEReactor( n_in, specs, properties, xi)
%% Solves the phase split problem for given turnover xi

%% Definitions

% pressure
p = specs.p; % [bar]

% temperature
T = specs.T; % [bar]

% species considered
NC = length(n_in);

% liquids
liquids = specs.liquids;
NL = length(liquids);

% gases
gases = 1:NC;
gases(liquids) = [];
NG = length(gases);

% stoichiometric coefficients
v = specs.v;

%% Thermodynamic Properties

AntoineParam = properties.AntoineParam;
AlphaParam = properties.AlphaParam;
TauParam = properties.TauParam;

%% Initiate mex

system = ['Data/DataVLLE.dat']; % Data for mexprops  
hdl=mexprops('init',['',system]);

%% Set Antoine parameters
    % Change Antoine equation parameters from ln(p)= A-B/(T+C) in mbar (COSMOtherm) 

    for i = 1 : NC
        k1 = i - 1;
        antc1 = AntoineParam{i}(1) + log(100) ;
        antc2 = -AntoineParam{i}(2);
        antc3 = AntoineParam{i}(3);
        antc4 = 0;
        antc5 = 0;
        antc6 = 0;
        antc7 = 0;
        antc8 = 0;
        antc9 = 9000;
        ok = mexprops('set_antoine',hdl,antc1,antc2,antc3,antc4,antc5,antc6,antc7,antc8,antc9,k1);
    end
    
%% Set NRTL parameters
    
    for i = 1 : (NC - 1)
        for j = (i + 1) : NC
            k1 = i - 1;
            k2 = j - 1;
            alpha = AlphaParam{i, j}(1) + 273.15*AlphaParam{i, j}(2); % mexprops expects alpha for C
            alphat = AlphaParam{i, j}(2) ;
            tau12_1 = TauParam{i, j}(1) ;
            tau12_2 = TauParam{i, j}(2) ;
            tau12_3 = TauParam{i, j}(3) ;
            tau12_4 = TauParam{i, j}(4) ;
            tau21_1 = TauParam{j, i}(1) ;
            tau21_2 = TauParam{j, i}(2) ;
            tau21_3 = TauParam{j, i}(3) ;
            tau21_4 = TauParam{j, i}(4) ;
            ok = mexprops('set_nrtl_alpha',hdl,alpha,alphat,k1,k2);
            ok = mexprops('set_nrtl_tau',hdl,tau12_2,tau12_1,tau12_3,tau12_4,k1,k2);
            ok = mexprops('set_nrtl_tau',hdl,tau21_2,tau21_1,tau21_3,tau21_4,k2,k1);
        end
    end

%% calculate VLE split

% get feed by solving mass balances for given xi
for i = 1 : NC
    nF(i) = n_in(i) + v(i) * xi;
end
zF = nF / sum(nF);

% liquid flow
for i = 1 : NL
    nL(i) = n_in(liquids(i)) + v(liquids(i)) * xi;
end
x = nL / sum(nL);

% gaseous flow
for i = 1 : NG
    nG(i) = n_in(gases(i)) + v(gases(i)) * xi;
end
zG = nG / sum(nG);

vapour = sum(nG) / sum(nF); % vapor fraction
t = T - 273.15; % temperature in C

[x,y,~,~,vapour]=mexprops('tpz_flash',hdl,zF,x,zG,t,p,vapour);
     
%% Calculated Streams

nVtot = sum(nF) * vapour;
nLtot = sum(nF) * (1 - vapour);

nout(1,:) = nVtot * y;
nout(2,:) = nLtot * x;


end

function [ error ] = VLLEReactor_mismatch( n_in, specs, properties, xi)
%% Solves the phase split problem for given turnover xi
% output is the error in reaction equilibrium

%% Definitions

% standard conditions
p0 = 1; % [bar]

% pressure
p = specs.p; % [bar]

% temperature
T = specs.T; % [bar]

% universal gas constant
R = 8.314; % [J / (mol * K)]

% species considered
NC = length(n_in);

% liquids
liquids = specs.liquids;
NL = length(liquids);

% gases
gases = 1:NC;
gases(liquids) = [];
NG = length(gases);

% key compounds of first liquid
firstliquid = specs.firstliquid;

% stoichiometric coefficients
v = specs.v;

% free ideal gas enthalpy of reaction
deltaGR_ig = specs.deltaGR;

%% Thermodynamic Properties

AntoineParam = properties.AntoineParam;
AlphaParam = properties.AlphaParam;
TauParam = properties.TauParam;

%% Initiate mex

system = ['Data/DataVLLE.dat']; % Data for mexprops   
hdl=mexprops('init',['',system]);

%% Set Antoine parameters
    % Change Antoine equation parameters from ln(p)= A-B/(T+C) in mbar (COSMOtherm) 

    for i = 1 : NC
        k1 = i - 1;
        antc1 = AntoineParam{i}(1) + log(100) ;
        antc2 = -AntoineParam{i}(2);
        antc3 = AntoineParam{i}(3);
        antc4 = 0;
        antc5 = 0;
        antc6 = 0;
        antc7 = 0;
        antc8 = 0;
        antc9 = 9000;
        ok = mexprops('set_antoine',hdl,antc1,antc2,antc3,antc4,antc5,antc6,antc7,antc8,antc9,k1);
    end
    
%% Set NRTL parameters
    
    for i = 1 : (NC - 1)
        for j = (i + 1) : NC
            k1 = i - 1;
            k2 = j - 1;
            alpha = AlphaParam{i, j}(1) + 273.15*AlphaParam{i, j}(2); % mexprops expects alpha for C
            alphat = AlphaParam{i, j}(2) ;
            tau12_1 = TauParam{i, j}(1) ;
            tau12_2 = TauParam{i, j}(2) ;
            tau12_3 = TauParam{i, j}(3) ;
            tau12_4 = TauParam{i, j}(4) ;
            tau21_1 = TauParam{j, i}(1) ;
            tau21_2 = TauParam{j, i}(2) ;
            tau21_3 = TauParam{j, i}(3) ;
            tau21_4 = TauParam{j, i}(4) ;
            ok = mexprops('set_nrtl_alpha',hdl,alpha,alphat,k1,k2);
            ok = mexprops('set_nrtl_tau',hdl,tau12_2,tau12_1,tau12_3,tau12_4,k1,k2);
            ok = mexprops('set_nrtl_tau',hdl,tau21_2,tau21_1,tau21_3,tau21_4,k2,k1);
        end
    end

%% For given xi calculate VLE flash
%% calculate VLE split

% get feed by solving mass balances for given xi
for i = 1 : NC
    nF(i) = n_in(i) + v(i) * xi;
end
zF = nF / sum(nF);

% liquid flow
for i = 1 : NL
    nL(i) = n_in(liquids(i)) + v(liquids(i)) * xi;
end
x = nL / sum(nL);

% gaseous flow
for i = 1 : NG
    nG(i) = n_in(gases(i)) + v(gases(i)) * xi;
end
zG = nG / sum(nG);

vapour = sum(nG) / sum(nF); % vapor fraction
t = T - 273.15; % temperature in C

[x,y,tvle,pvle,vapour]=mexprops('tpz_flash',hdl,zF,x,zG,t,p,vapour);
if (isempty(x) || vapour < 0) % flash has failed
    error = 66e66;
    return;
end  
    
%% calculate LLE split for non gaseous components

% Initial guess from spec
z0 = zeros(1,NC);
for i = 1 : length(firstliquid)
    z0(firstliquid(i)) = nL(firstliquid(i))/sum(nL(firstliquid));
end
    
% get NRTL parameters
[Tau, Alpha] = nrtlParam(TauParam, AlphaParam, T);

% Solve LLE phase split problem
[xI, xII] = calcPhaseSplit(Alpha, Tau, T, x, z0, NC);

if sum(xI == xII) == 5
    error = 66e66;
    return;
end

%% calculate VLLE split
t = T - 273.15; % temperature in C

% check phase split results
[x,yvlle,tvlle,pvlle,vapour,xI,xII,phi,~] = mexprops('tpz_vllflash',hdl,zF,[],y,t,p,vapour,[],[],xI,xII,[]);
if (isempty(x) || phi < 0 || vapour < 0 || xI(4) == 0) % flash has failed or no LLE
    error = 66e66;
    return;
end
    

%% Check Reaction equilibria

% Activity coefficient
if xI(4) > xII(4)
    xeq = xI;
else
    xeq = xII;
end
gamma = NRTL(xeq, NC, Alpha, Tau);

% psat calculations
psat = Psat (AntoineParam, T);

% Equilibrium constant for solution in Solvent
% Kf = exp(-deltaGR_ig / R / T);
% Ka = prod((p0 ./ psat) .^ v) * Kf;
% 
% error = Ka - prod(xeq .^ v) * prod(gamma .^ v);

error = deltaGR_ig + R*T*sum(log(xeq.*gamma.*psat).*v);
    
end

function [ nout ] = VLLEReactor( n_in, specs, properties, xi)
%% Solves the phase split problem for given turnover xi

%% Definitions

% pressure
p = specs.p; % [bar]

% temperature
T = specs.T; % [bar]

% species considered
NC = length(n_in);

% liquids
liquids = specs.liquids;
NL = length(liquids);

% gases
gases = 1:NC;
gases(liquids) = [];
NG = length(gases);

% key compounds of first liquid
firstliquid = specs.firstliquid;

% stoichiometric coefficients
v = specs.v;

%% Thermodynamic Properties

AntoineParam = properties.AntoineParam;
AlphaParam = properties.AlphaParam;
TauParam = properties.TauParam;

%% Initiate mex

system = ['Data/DataVLLE.dat']; % Data for mexprops 
hdl=mexprops('init',['',system]);

%% Set Antoine parameters
    % Change Antoine equation parameters from ln(p)= A-B/(T+C) in mbar (COSMOtherm) 

    for i = 1 : NC
        k1 = i - 1;
        antc1 = AntoineParam{i}(1) + log(100) ;
        antc2 = -AntoineParam{i}(2);
        antc3 = AntoineParam{i}(3);
        antc4 = 0;
        antc5 = 0;
        antc6 = 0;
        antc7 = 0;
        antc8 = 0;
        antc9 = 9000;
        ok = mexprops('set_antoine',hdl,antc1,antc2,antc3,antc4,antc5,antc6,antc7,antc8,antc9,k1);
    end
    
%% Set NRTL parameters
    
    for i = 1 : (NC - 1)
        for j = (i + 1) : NC
            k1 = i - 1;
            k2 = j - 1;
            alpha = AlphaParam{i, j}(1) + 273.15*AlphaParam{i, j}(2); % mexprops expects alpha for C
            alphat = AlphaParam{i, j}(2) ;
            tau12_1 = TauParam{i, j}(1) ;
            tau12_2 = TauParam{i, j}(2) ;
            tau12_3 = TauParam{i, j}(3) ;
            tau12_4 = TauParam{i, j}(4) ;
            tau21_1 = TauParam{j, i}(1) ;
            tau21_2 = TauParam{j, i}(2) ;
            tau21_3 = TauParam{j, i}(3) ;
            tau21_4 = TauParam{j, i}(4) ;
            ok = mexprops('set_nrtl_alpha',hdl,alpha,alphat,k1,k2);
            ok = mexprops('set_nrtl_tau',hdl,tau12_2,tau12_1,tau12_3,tau12_4,k1,k2);
            ok = mexprops('set_nrtl_tau',hdl,tau21_2,tau21_1,tau21_3,tau21_4,k2,k1);
        end
    end

%% For given xi calculate VLE flash
%% calculate VLE split

% get feed by solving mass balances for given xi
for i = 1 : NC
    nF(i) = n_in(i) + v(i) * xi;
end
zF = nF / sum(nF);

% liquid flow
for i = 1 : NL
    nL(i) = n_in(liquids(i)) + v(liquids(i)) * xi;
end
x = nL / sum(nL);

% gaseous flow
for i = 1 : NG
    nG(i) = n_in(gases(i)) + v(gases(i)) * xi;
end
zG = nG / sum(nG);

vapour = sum(nG) / sum(nF); % vapor fraction
t = T - 273.15; % temperature in C

[x,y,~,~,vapour]=mexprops('tpz_flash',hdl,zF,x,zG,t,p,vapour);
if (isempty(x) || vapour < 0) % flash has failed
    nout = zeros(3,5);
    return;
end   
    
%%% calculate LLE split for non gaseous components

% Initial guess from spec
z0 = zeros(1,NC);
for i = 1 : length(firstliquid)
    z0(firstliquid(i)) = nL(firstliquid(i))/sum(nL(firstliquid));
end
    
% get NRTL parameters
[Tau, Alpha] = nrtlParam(TauParam, AlphaParam, T);

% Solve LLE phase split problem
[xI, xII] = calcPhaseSplit(Alpha, Tau, T, x, z0, NC);

%% calculate VLLE split
t = T - 273.15; % temperature in C

% check phase split results
[~,y,~,~,vapour,xI,xII,phi,~] = mexprops('tpz_vllflash',hdl,zF,[],y,t,p,vapour,[],[],xI,xII,[]);
%% Calculate Streams
if isempty(y)
    nout = zeros(3,5);
    return
end

nVtot = sum(nF) * vapour;
nLtot = sum(nF) * (1 - vapour);
nItot = nLtot * (1 - phi);
nIItot = nLtot * phi;

nout(1,:) = nVtot * y;
nout(2,:) = nItot * xI;
nout(3,:) = nIItot * xII;
end

function [tau, alpha] = nrtlParam(tauParam, alphaParam, T)
    %% Implemented by A. Demuth, bachelor thesis supervised by Jan Scheffczyk @LTT, RWTH Aachen University.	
    %% Calculates NRTL parameters (tau, alpha) for a given temperature T and given parametrizations (see below)
    % INPUT:    TauParam and AlphaParam are cell arrays with dimensions
    %           nxn. Each entry contains a [1x4] (TauParam) or [1x2]
    %           (AlphaParam) double, so that the parametrizations are given
    %           by: % tau_ij(T) = a_ij + b_ij / T + c_ij * ln(T) + dij * T
    %           and % alpha_ij(T) = alpha_ij,0 + alpha_ij,1 * T.
    %-------------------------------------------------------------------
    % Name      | Description           | Size                  | Unit | 
    % T         | temperature in Kelvin | [1x1]                 | [K]  |
    % tauParam  | alpha param. matrix   | [nxn] w. [1x4] each   | [-]  |
    % alphaParam| tau param. matrix     | [nxn] w. [1x4] each   | [-]  |    
    %-------------------------------------------------------------------
    %
    % OUTPUT:   Tau and alpha are matrices with dimensions nxn. 
    %------------------------------------------------
    % Name  | Description           | Size  | Unit  | 
    % alpha | alpha matrix          | [nxn] | [-]   |
    % tau   | tau matrix            | [nxn] | [-]   |
    %------------------------------------------------

    n = length(tauParam) ;

    % NRTL:
    alpha = ones(n);
    tau = zeros(n);
    for i = 1:n
        for j = 1:n
            % tau_ij(T) = a_ij + b_ij / T + c_ij * ln(T) + dij * T
            tau(i,j) = tauParam{i,j}(1)	+ tauParam{i,j}(2)/T + tauParam{i,j}(3)*log(T) + tauParam{i,j}(4)*T;
            % alpha_ij(T) = alpha_ij,0 + alpha_ij,1 * T
            alpha(i,j) = alphaParam{i,j}(1) + alphaParam{i,j}(2)*T;
        end
    end
end

function [gamma]=NRTL(x, n_comp, alpha, tau,~,~)
%% NRTL model activity coefficient calculation

%%

% G
    G=eye(n_comp,n_comp);
    for i=1:n_comp
        for j=1:n_comp
            if i~=j
                G(i,j) = exp(-alpha(i,j)*tau(i,j));
            end
        end
    end

%%

%B(i)
    for i=1:n_comp
        B(i)=0;
        for j=1:n_comp
            B(i) = B(i) + tau(j,i)*G(j,i)*x(j);
        end
    end

%A(i)
    for i=1:n_comp
        A(i)=0;
        for l=1:n_comp
            A(i) = A(i) + G(l,i)*x(l);
        end
    end


%% gamma(i)
%%
    for i=1:n_comp
        summe_lngamma(i)=0;
        for j=1:n_comp
            summe_lngamma(i) = summe_lngamma(i) + x(j)*G(i,j)/ A(j)*(tau(i,j)-B(j)/A(j));
        end
    end

    for i=1:n_comp
        lngamma(i) = B(i)/A(i)+summe_lngamma(i);
        gamma(i) = exp(lngamma(i));
    end

end

function [ psat ] = Psat( Antoine, T )
%% Psat calculation from Antoine Parameters

n = length(Antoine);

for i = 1 : n
    psat(i) = 1e-3 * exp(Antoine{i}(1) - Antoine{i}(2)/(T + Antoine{i}(3)));
end

end

function [xI, xII] =  calcPhaseSplit(Alpha, Tau, T, zF, x0, NC)
                                    
    beta = 0.5;
    R=8.314; %kJ/kmol/K

    nitermax = 10000;
    TOL_mu = 1e-9;
    TOL_beta = 1e-9;
    TOL_gbeta = 1e-9;
    beta0 = beta;
        
    [ x_calc(1,:), y_calc(1,:), ~ ] = LLE_NRTL_calc( zF(1,:), x0, NC, Tau, Alpha, nitermax, TOL_mu, TOL_beta, TOL_gbeta, R, T, beta0 );  
      
    xI = x_calc(1,:);
    xII = y_calc(1,:);
   
end

function [ x, y, beta ] = LLE_NRTL_calc( z, x0, n_comp, tau, alpha, nitermax, TOL_mu, TOL_beta, TOL_gbeta, R, T, beta0 )
%Calculation of the equilibrium compositions for a given total composition


%% Initialization
%%
    x=x0;
    beta = beta0;

    % Mole balance for Phase 2
        for i=1:(n_comp-1)
            y(i) = (z(i)-(1-beta)*x(i))/beta;
        end
        
       y(n_comp) = 1-sum( y(1:(n_comp-1)) );

%    
    gamma_x = (NRTL(x, n_comp, alpha, tau,R, T));
    gamma_y = (NRTL(y, n_comp, alpha, tau,R, T));
    K = gamma_x./gamma_y;
    delta_mu = abs(gamma_y.*y - gamma_x.*x);

%     beta_vec=beta;
%     x_vec=x0';
%     y_vec=y';
%     delta_mu_vec = delta_mu';
%     K_vec = K';

%% RR-Algorithm
%%
    nit=0;
    while((nit < nitermax)  && ( max(delta_mu) > TOL_mu )  )
        nit = nit + 1;
      
    
        [beta,x,y] = RRSOLVER(n_comp, z, K, nitermax, TOL_beta, TOL_gbeta);
        if beta < TOL_beta
            x=z;
            y=z;
%             disp('single phase1')
            break
        elseif beta >= (1-TOL_beta)
            x=z;
            y=z;
%             disp('single phase2')
            break
        end
    
        gamma_x = (NRTL(x, n_comp, alpha, tau, R, T));
        gamma_y = (NRTL(y, n_comp, alpha, tau, R, T));
        K = gamma_x./gamma_y;
        delta_mu = abs(gamma_y.*y - gamma_x.*x);
    
%         K_vec=[K_vec; K'];
%         x_vec=[x_vec; x'];
%         y_vec=[y_vec; y'];
%         delta_mu_vec = [delta_mu_vec; delta_mu'];
%         beta_vec=[beta_vec;beta];
    end
%     disp('K,x,y,delta_mu,beta')
%     [K_vec,x_vec, y_vec, delta_mu_vec, beta_vec]


end

function [beta, x, y] = RRSOLVER(nc, z, K, ni, TOL_beta, TOL_gbeta)
% Attention:    beta=n''/n_tot
%               K(i) = x(i)''/x(i)' = gamma(i)'/gamma(i)''
%               betafunction: sum(i, x(i)''-x(i)') = sum(i, z(i)*(K(i)-1) /
%               (1-beta+beta*K(i)))            
%               

    % nc: Number of components
    % z : feed mole fractions
    % K : k-factors

    beta = 0.5;
    beta_max = 1;
    beta_min = 0;
%     beta_max = 1/(1-min(K));
%     beta_min = -1/(max(K)-1);

    delta_beta = 1;
    delta_g = 1;

    zaehler = 0;
    
    for zaehler = 0:ni
        zaehler = zaehler +1;
        g = 0;
        g_alt = g;
        for i = 1:nc
            g = g + z(i).*(K(i)-1)./(1-beta+beta.*K(i));
        end     
%         for i=1:nc
%             g = g + z(i)./(beta+K(i)*(1-beta));
%         end
%         g = g-1;
%------------        
        
        

        if g < 0
            beta_max = beta;
        else beta_min = beta;
        end

%         if beta_max > 1
%            beta_max = 1; 
%         end

        g_strich = 0;

        for i = 1:nc
            g_strich = g_strich - z(i).*(K(i)-1)^2./(beta.*K(i)-beta+1)^2;
        end
%         for i=1:nc
%             g_strich = g_strich - z(i)*(1-K(i))./((beta+K(i)*(1-beta)).^2);
%         end
%------------  

        beta_neu = beta - g/g_strich;

        
% normal RR %%%%%%%%%%%%%%%%%%%        
        if beta_neu >= beta_min && beta_neu <= beta_max && ni > 1
           delta_beta = abs(beta-beta_neu);
           beta = beta_neu;           
        else beta_neu = (beta_min + beta_max)/2;
             beta = beta_neu;
        end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% SLOPPY RR for ni = 1 %%%%%%%%%%%                                     
%         if beta_neu >= beta_min && beta <= beta_max && ni == 1  
%             l = (1-beta)*z./(1-beta+beta*K);          
%             v = (beta*K.*z)./(1-beta+beta*K);         
%             beta_neu = sum(v);                        
%             delta_beta = abs(beta-beta_neu);          
%             beta = beta_neu;                          
%         else beta_neu = (beta_min + beta_max)/2;      
%              delta_beta = abs(beta-beta_neu);         
%              beta = beta_neu;                         
%         end                                           
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      

        delta_g = abs(g_alt - g);

        
        if delta_beta <= TOL_beta && delta_g <= TOL_gbeta
            break
        end
    end
    
    if ni > 1
        for i=1:nc
            l(i) = (1-beta)*z(i)./(1-beta+beta*K(i));  
            v(i) = (beta*K(i).*z(i))./(1-beta+beta*K(i));
            x(i) = l(i)/(1-beta);
            y(i) = v(i)/beta;   
        end
        
%         for i=1:nc-1
%             l(i) = (1-beta)*z(i)./(1-beta+beta*K(i));  
%             v(i) = (beta*K(i).*z(i))./(1-beta+beta*K(i));
%             x(i) = l(i)/(1-beta);
%             y(i) = v(i)/beta;   
%         end
%         x(nc)=1-sum( x(1:(nc-1)) );
%         y(nc)=1-sum( y(1:(nc-1)) );
    end
end
