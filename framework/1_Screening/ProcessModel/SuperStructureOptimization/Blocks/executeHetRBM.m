function [n_in, n_out, results] = executeHetRBM(n_in, specs, properties)
%% main execution of the RBM model for heteroazeotropic distillation
% Input:    n_in (inflow of only relevant species)
%           specs (struct containing specifications (p [bar], compinfirst 
%           (those components which are drawn off in the first column), compinsecond (analogously), tf (Feed
%           temperature [C]))
% Output:   nout (outflow (matrix with two rows, column1: row 1, column2: row 2))
%           results (results struct (QB [MJ], QC [MJ], TB [K], TC [K], R [-])))

%% Preallocate fields
results = [];

n_out = zeros(2,length(n_in));

%% Check if previous block finished succesful

if (max(n_in) == 0)
    return;
end

%% Definitions

% pressure
p = specs.p; % [bar]

% species considered
NC = length(n_in);

% components drawn off at 1st column
compinfirst = specs.compinfirst;

% components drawn off at the bottom
compinsecond = specs.compinsecond;

%% Thermodynamic Properties

AntoineParam = properties.AntoineParam;
AlphaParam = properties.AlphaParam;
TauParam = properties.TauParam;


%% Initiate mex

system = ['Data/DataRBM.dat']; % Data for mexrbm  
hdl = mexrbm('init',['',system]);

%% Set Antoine parameters
    % Change Antoine equation parameters from ln(p)= A-B/(T+C) in mbar (COSMOtherm 

for i = 1 : NC
    antc1 = AntoineParam{i}(1) + log(100) ;
    antc2 = -AntoineParam{i}(2);
    antc3 = AntoineParam{i}(3);
    antc4 = 0;
    antc5 = 0;
    antc6 = 0;
    antc7 = 0;
    antc8 = 0;
    antc9 = 9000;
    ok = mexrbm('set_antoine',hdl,antc1,antc2,antc3,antc4,antc5,antc6,antc7,antc8,antc9,i);
end

    

    %% Set NRTL parameters
    
for i = 1 : (NC-1)
    for j = (i + 1) : NC
        alpha = AlphaParam{i, j}(1) + 273.15*AlphaParam{i, j}(2); % mexprops expects alpha for C
        alphat = AlphaParam{i, j}(2) ;
        tau12_1 = TauParam{i, j}(1) ;
        tau12_2 = TauParam{i, j}(2) ;
        tau12_3 = TauParam{i, j}(3) ;
        tau12_4 = TauParam{i, j}(4) ;
        tau21_1 = TauParam{j, i}(1) ;
        tau21_2 = TauParam{j, i}(2) ;
        tau21_3 = TauParam{j, i}(3) ;
        tau21_4 = TauParam{j, i}(4) ;
        ok = mexrbm('set_nrtl_alpha',hdl,alpha,alphat,i,j);
        ok = mexrbm('set_nrtl_tau',hdl,tau12_2,tau12_1,tau12_3,tau12_4,i,j);
        ok = mexrbm('set_nrtl_tau',hdl,tau21_2,tau21_1,tau21_3,tau21_4,j,i);
    end
end

%% Molar balances
    
% check with azeotropic table
azeo = mexrbm('azeo',hdl,[],[],[],[],p);
             
a = size(azeo);
if a(2) == 0
    clear mex
    return;
end
clear a;

% find boiling point and composition of azeotrope
for i = 1 : length(azeo)
    if (azeo(i).type == 3)
        xAz = azeo(i).x;
        xI = azeo(i).xI;
        xII = azeo(i).xII;
        tF = azeo(i).t;
        break;
    end
end

% Define phases for feed
if (xI(compinfirst) > xII(compinfirst))
    xF1 = xI;
    xF2 = xII;
else
    xF1 = xII;
    xF2 = xI;
end

% molefractions
f1.z = xF1;
d1.z = xAz; % distillate is azeotrope
b1.z = zeros(1,NC);
b1.z(compinfirst) = 1; % pure
    
f2.z = xF2;
d2.z = xAz; % distillate is azeotrope
b2.z = zeros(1,NC);
b2.z(compinsecond) = 1; % pure

% molar flows
b1.flow = n_in(compinfirst);
d1.flow = (b1.z(compinfirst) - f1.z(compinfirst)) / (f1.z(compinfirst) - d1.z(compinfirst)) * b1.flow; % "Gesetz der abgewandten Hebelarme"
f1.flow = b1.flow + d1.flow;
    
b2.flow = n_in(compinsecond);
d2.flow = (b2.z(compinsecond) - f2.z(compinsecond)) / (f2.z(compinsecond) - d2.z(compinsecond)) * b2.flow; % "Gesetz der abgewandten Hebelarme"
f2.flow = b2.flow + d2.flow;


%% Call mex file for column 1

%Input: Last entry is vapor fraction of destillate
%Output: (1)qB [J/kmol], (2) qD [J/kmol], (3) TB [K], (4) TD [K], (5)convergence, (6)reflux per Feed flow
mexresult1 = mexrbm('CALC_RBM',hdl,p,azeo,b1.z,d1.z,(d1.flow/f1.flow),tF,0); 
    

%% Call mex file for column 2

%Input: Last entry is vapor fraction of destillate
%Output: (1)qB [J/kmol], (2) qD [J/kmol], (3) TB [K], (4) TD [K], (5)convergence, (6)reflux per Feed flow
mexresult2 = mexrbm('CALC_RBM',hdl,p,azeo,b2.z,d2.z,(d2.flow/f2.flow),tF,0); 
    
%% write results
if (mexresult1(5) == 1 && mexresult2(5) == 1)
    n_out(1,:) = b1.flow * b1.z;
    results.QB1 = mexresult1(1) * f1.flow / 1000000; % [MJ] (flows are assumed to be in [kmol]
    results.QC1 = mexresult1(2) * f1.flow / 1000000; % [MJ] (flows are assumed to be in [kmol]
    results.TB1 = mexresult1(3);
    results.TC1 = mexresult1(4);
    results.R1 = mexresult1(6) * f1.flow / d1.flow;
    n_out(2,:) = b2.flow * b2.z;
    results.QB2 = mexresult2(1) * f2.flow / 1000000; % [MJ] (flows are assumed to be in [kmol]
    results.QC2 = mexresult2(2) * f2.flow / 1000000; % [MJ] (flows are assumed to be in [kmol]
    results.TB2 = mexresult2(3);
    results.TC2 = mexresult2(4);
    results.R2 = mexresult2(6) * f2.flow / d2.flow;
end

clear mex;

end