function [n_in, n_out, results] = executeRStoic(n_in, specs, properties)
%% main execution of the stoichiometric Reactor model
% Input:    n_in (inflow of only relevant species)
%           specs (struct containing specifications (v (stoichiometric coefficients), turnover (of target
%           compound (with index))
% Output:   nout (outlow (row vector))
%           results (results struct (empty))

%% Preallocate fields

results = [];

n_out = zeros(1,length(n_in));


%% Check if previous block finished succesful

if (max(n_in) == 0)
    return;
end

%% Definitions

targetcompound = specs.targetcompound;

turnover = specs.turnover; % [-]

v = specs.v;

% extent of reaction
xi = - (turnover * n_in(targetcompound)) / v(targetcompound);

%% Solve mass balance

n_out = n_in + v * xi;