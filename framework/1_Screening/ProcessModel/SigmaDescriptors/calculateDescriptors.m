function [ molStruct ] = calculateDescriptors( molStruct, specs )
% use descriptors from Zhou et al. 2015 or from Zhou et al. 2014

%% Number of segments
sigmasegments = specs.sigmasegments;
nsigma = length(sigmasegments);

%% Preallocate fields

for i=1:length(molStruct)
    molStruct(i).sigmaDescriptors = zeros(1, nsigma);
end


%% integrate SCD

parfor k=1:length(molStruct)

    areas = zeros(1,60);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Constant Interpolation

    %areas = weightedProfile(2:end, 2) * 0.1;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Linear Interpolation

    for i = 1 : 60
       areas(i) = 0.5 * (molStruct(k).weightedProfile(i, 2) + molStruct(k).weightedProfile(i+1, 2)) * 0.1; %e/nm2
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %% divide Integral into contributions
    for j = 1:nsigma
        molStruct(k).sigmaDescriptors(j) = sum(areas(sigmasegments(j,1):sigmasegments(j,2)));
    end
    
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


end

end


