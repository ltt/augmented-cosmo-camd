function dHvap_para = fit_DIPPER(AntPara,Tc)

T  = 250:700;

dHvap = ClausiusClapeyron(AntPara,T);
para0 = [5e7, -0.01, 0];

options = optimset('Display','off','MaxFunEvals',1e5);
dHvap_para = fminsearch( @(para)DIPPER_function(para,dHvap,T,Tc),para0,options);

end

function dHvap = ClausiusClapeyron(AntPara,T)

    R = 8.314;
    dHvap = AntPara(2)./(AntPara(3) + T).^2 .* R .* T.^2 * 1000;

end


function d = DIPPER_function(c,dHvap,T,Tc)

t = T/Tc;

temp = c(1) * (1-t).^(c(2) + c(3)* t); %+ c(4)* t .^ 2+ c(5)* t .^ 3);

d = sum(((dHvap -temp)./dHvap).^2);


end