function molStruct = main_absorption(molStruct, specs)
%% Prepare properties and handle results for all solvents
       
%% Preallocate fields for output of mabsorption
molStruct_temp = molStruct;

for i=1:length(molStruct)
    molStruct(i).SFmin = NaN;
    molStruct(i).xN = NaN;
    molStruct(i).resultsAbs =NaN ;
    
    molStruct_temp(i).AntoineParam = molStruct_temp(i).AntoineParam([1 2 3 7]);
    molStruct_temp(i).AlphaParam = molStruct_temp(i).AlphaParam([1 2 3 7],[1 2 3 7]);
    molStruct_temp(i).TauParam = molStruct_temp(i).TauParam([1 2 3 7],[1 2 3 7]);
end    
        
%% Set specifications for shortcuts
specs.pressure = 60; %/bar -> Rectisol
specs.xF = [ 0.4 0.37 0.23 0];  % h2 co2 co h2o DMF DMA X
specs.xS = [ 0 0 0 1];
specs.tF = 30; % Rectisol,  CRP: 29.98
specs.tS = -50; % Rectisol, CRP: -30
specs.purity = 0;

specs.iSolute = 2;
specs.iSolvent = 4;

%% Calculate absorption column
parfor i=1:length(molStruct)
    
    % Display info
    if strcmp(specs.Mode, 'screening')
        disp(['Compound No. ', num2str(i), '\', num2str(length(molStruct_temp)), ': ', molStruct_temp(i).name]);
    end
    
    [result, ok] = calc_absorption(molStruct_temp(i), specs);

    % Save results
    molStruct(i).SFmin = result.Smin;
    molStruct(i).xN = result.xN;
    molStruct(i).resultsAbs = result;           

end
      
end
