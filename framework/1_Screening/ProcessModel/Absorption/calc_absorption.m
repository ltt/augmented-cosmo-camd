function [result, ok] = calc_absorption(molStruct, specs)
%% Calculate absorption column for one given solvent

%% Specifications from specs-struct
pressure = specs.pressure;
xF = specs.xF;
xS = specs.xS;
tF = specs.tF;
tS = specs.tS;
purity = specs.purity;

iSolute = specs.iSolute;
iSolvent = specs.iSolvent;


%% Properties handle

% Write dummy.dat
%write_dummy

% Initialize mabsorption with dummy.dat
hdl=mabsorption('init','dummy.dat'); % absorption column

%% set parameters
NC = length(molStruct.AntoineParam);

%% Antoine-Parameter
AntoineParam = molStruct.AntoineParam;

i = iSolvent;
%for i = 1 : NC
    antc1 = AntoineParam{i}(1) + log(100) ;
    antc2 = -AntoineParam{i}(2);
    antc3 = AntoineParam{i}(3);
    antc4 = 0;
    antc5 = 0;
    antc6 = 0;
    antc7 = 0;
    antc8 = 0;
    antc9 = 9000;
    ok = mabsorption('set_antoine',hdl,antc1,antc2,antc3,antc4,antc5,antc6,antc7,antc8,antc9,i);
%end


%% NRTL-Parameter
AlphaParam = molStruct.AlphaParam;
TauParam = molStruct.TauParam;		

 for i = 1 : (NC-1)
    for j = (i + 1) : NC
        alpha = AlphaParam{i, j}(1) + 273.15*AlphaParam{i, j}(2); % mexprops expects alpha for C
        alphat = AlphaParam{i, j}(2) ;
        tau12_1 = TauParam{i, j}(1) ;
        tau12_2 = TauParam{i, j}(2) ;
        tau12_3 = TauParam{i, j}(3) ;
        tau12_4 = TauParam{i, j}(4) ;
        tau21_1 = TauParam{j, i}(1) ;
        tau21_2 = TauParam{j, i}(2) ;
        tau21_3 = TauParam{j, i}(3) ;
        tau21_4 = TauParam{j, i}(4) ;
        ok = mabsorption('set_nrtl_alpha',hdl,alpha,alphat,i,j);
        ok = mabsorption('set_nrtl_tau',hdl,tau12_2,tau12_1,tau12_3,tau12_4,i,j);
        ok = mabsorption('set_nrtl_tau',hdl,tau21_2,tau21_1,tau21_3,tau21_4,j,i);
    end
 end

%% Verdampfungsenthalpie
Tc = 1e+04;
dHvap_para = fit_DIPPER(AntoineParam{end},Tc);
dHvap_para(4:7) = [0 0 1 2000];
ok = mabsorption('set_hvl',hdl,dHvap_para(1),dHvap_para(2),dHvap_para(3),dHvap_para(4),dHvap_para(5),dHvap_para(6),dHvap_para(7),iSolvent) ;


%% Wärmekapazitäten cpig
NASA = molStruct.NASA * 8314;
NASA(6:11) = [0 1 2000 0 0 0];
ok = mabsorption('set_cpig',hdl,NASA(1),NASA(2),NASA(3),NASA(4),NASA(5),NASA(6),NASA(7),NASA(8),NASA(9),NASA(10),NASA(11),iSolvent) ;


%% calculate ABSORPTION

[ok,result] = mabsorption('MABSORPTION',hdl, pressure, xF , xS , tF , tS, purity, iSolute , iSolvent);

clear mex

end
