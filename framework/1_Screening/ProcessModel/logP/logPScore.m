function [ molStruct ] = logPScore( molStruct, specs, valuecomp)
% Score the molStruct molecules using the mass-based logP Score
% System is defined in LLEcomp

MW_water = 18.02;
LLEcomp = str2num(specs.LLEcomp);
compounds = [1 2 3];
compounds(LLEcomp) = 0;
%valuecomp = find(compounds);


%% Display Score calculation
if strcmp(specs.Mode, 'screening')
    if strcmp(specs.logP, 'mol-based')
        display (['Score being calculated: logP mole-based']);
    else
        display (['Score being calculated: logP mass-based']);
    end
end

%% log P:
for k=1:length(molStruct)
    x_LLE = molStruct(k).x_LLE;
    if ~isnan(x_LLE(1))
        gamma_I = molStruct(k).gammaI;
        gamma_II = molStruct(k).gammaII;
        
        % Either mass-based or mol-based
        if strcmp(specs.logP, 'mol-based')
            
            if x_LLE(3) >= x_LLE(1) % x_LLE(3) is aqueous phase
                P = exp(gamma_II(valuecomp)-gamma_I(valuecomp));

            else  % x_LLE(1) is aqueous phase
                P = exp(gamma_I(valuecomp)-gamma_II(valuecomp));
            end
            
        else
            
            MW_solvent = molStruct(k).MW;
            
            if x_LLE(3) >= x_LLE(1) % x_LLE(3) is aqueous phase
                %calc MW_aq, 
                MW_aq = x_LLE(3)*MW_water + x_LLE(4) * MW_solvent;

                %calc MW_org
                MW_org = x_LLE(1)*MW_water + x_LLE(2) * MW_solvent;

                P = exp(gamma_II(valuecomp)-gamma_I(valuecomp))*MW_aq/MW_org;

            else                    % x_LLE(1) is aqueous phase
                %calc MW_aq, 
                MW_aq = x_LLE(1)*MW_water + x_LLE(2) * MW_solvent;

                %calc MW_org
                MW_org = x_LLE(3)*MW_water + x_LLE(4) * MW_solvent;

                P = exp(gamma_I(valuecomp)-gamma_II(valuecomp))*MW_aq/MW_org;
            end
        end
        molStruct(k).logP = log10(P);
    else
        molStruct(k).logP = 0;
    end
end

end

