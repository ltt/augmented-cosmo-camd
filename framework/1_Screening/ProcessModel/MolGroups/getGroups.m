function molStruct = getGroups(molStruct)

%% Get molecular groups for all molecules based on the SMILES Code

% Preallocate
for k=1:length(molStruct);
    molStruct(k).Oxygens = NaN;
    molStruct(k).Nitrogens = NaN;
    molStruct(k).Carbons = NaN;
    molStruct(k).Halogens = NaN;
    molStruct(k).CDoubleBonds = NaN;
    molStruct(k).CTripleBonds = NaN;
    molStruct(k).AromRings = NaN;
    molStruct(k).Ring = NaN;
end

parfor i=1:length(molStruct)
        
        % Count oxygen atoms
        tempSMILES = lower(molStruct(i).SMILES);
        noO = length(strfind(tempSMILES,'o'));
        if isempty(noO)
            molStruct(i).Oxygens = 0;
        else
            molStruct(i).Oxygens = noO;
        end
    
        noN = length(strfind(tempSMILES,'n'));
        if isempty(noO)
            molStruct(i).Nitrogens = 0;
        else
            molStruct(i).Nitrogens = noN;
        end
 

        noC = length(strfind(tempSMILES,'c'));
        noCl = length(strfind(tempSMILES,'cl'));
        noC = noC-noCl;
        if isempty(noO)
            molStruct(i).Carbons = 0;
        else
            molStruct(i).Carbons = noC;
        end

        noCl = length(strfind(tempSMILES,'cl'));
        noBr = length(strfind(tempSMILES,'br'));
        noF = length(strfind(tempSMILES,'f'));
        noI = length(strfind(tempSMILES,'i'));
        noHal = noCl+noBr+noF+noI;
        if isempty(noO)
            molStruct(i).Halogens = 0;
        else
            molStruct(i).Halogens = noHal;
        end

        % Count double bonds
        NoDouble = length(strfind(molStruct(i).SMILES,'=C'));
        NoDoubleO = length(strfind(molStruct(i).SMILES,'O=C'));
        NoDoubleN = length(strfind(molStruct(i).SMILES,'N=C'));
        NoDouble = NoDouble - NoDoubleO -NoDoubleN;
        if isempty(NoDouble)
            molStruct(i).CDoubleBonds = 0;
        else
            molStruct(i).CDoubleBonds = NoDouble;
        end

        % Count triple bonds carbon
        NoTriple = length(strfind(molStruct(i).SMILES,'#C'));
        NoTripleN = length(strfind(molStruct(i).SMILES,'N#C'));
        NoTriple = NoTriple - NoTripleN;
        if isempty(NoTriple)
            molStruct(i).CTripleBonds = 0;
        else
            molStruct(i).CTripleBonds = NoTriple;
        end

        % Count aromatic rings    
        if ~isempty(strfind(molStruct(i).SMILES,'c4')) || ~isempty(strfind(molStruct(i).SMILES,'n4'))
            molStruct(i).AromRings = 4;
        elseif ~isempty(strfind(molStruct(i).SMILES,'c3')) || ~isempty(strfind(molStruct(i).SMILES,'n3'))
            molStruct(i).AromRings = 3;
        elseif ~isempty(strfind(molStruct(i).SMILES,'c2')) || ~isempty(strfind(molStruct(i).SMILES,'n2'))
            molStruct(i).AromRings = 2;
        elseif ~isempty(strfind(molStruct(i).SMILES,'c1')) || ~isempty(strfind(molStruct(i).SMILES,'n1')) || ~isempty(strfind(molStruct(i).SMILES,'o1'))
            molStruct(i).AromRings = 1;
        else
            molStruct(i).AromRings = 0;
        end
        
        
        % Count rings in general       
        if ~isempty(strfind(tempSMILES,'c4')) || ~isempty(strfind(molStruct(i).SMILES,'n4'))
            molStruct(i).Ring = 4;
        elseif ~isempty(strfind(tempSMILES,'c3')) || ~isempty(strfind(molStruct(i).SMILES,'n3'))
            molStruct(i).Ring = 3;
        elseif ~isempty(strfind(tempSMILES,'c2')) || ~isempty(strfind(molStruct(i).SMILES,'n2'))
            molStruct(i).Ring = 2;
        elseif ~isempty(strfind(tempSMILES,'c1')) || ~isempty(strfind(molStruct(i).SMILES,'n1')) || ~isempty(strfind(molStruct(i).SMILES,'o1'))
            molStruct(i).Ring = 1;
        else
            molStruct(i).Ring = 0;
        end

        
end
  

end