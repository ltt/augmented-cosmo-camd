function molStruct = get_kinetics_l(molStruct,specs)
%compute unimolecular reaction rates in liquid phase 



gas_rates = (specs.gasphase_rate);

for k=1:length(molStruct)
 
 
 Gsolv_reactants = (molStruct(k).deltaG_solv_reactants);
 %Gsolv_reactants = sum(Gsolv_reactants);
 
 Gsolv_TS = (molStruct(k).deltaG_solv_TS);
 
 %Unit conversion
 Gsolv_reactants = Gsolv_reactants.*4.184; %change units to kJ/mol
 Gsolv_TS = Gsolv_TS.*4.184; %kJ/mol
 
 
 
 %get reaction temperature in K
 T_reaction = specs.T_reaction; %K

 reaction_rate_constants = zeros(7,1);
 
 
 %% update reaction rates
 %Reaction 1 NrN 
 for n=1:8
 liquid_rates(n) = gas_rates(n).*exp(-(Gsolv_TS(n)-Gsolv_reactants(1)-Gsolv_reactants(2))/0.008314/T_reaction);
 reaction_rate_constants(1)=reaction_rate_constants(1)+liquid_rates(n);
 end
 
 %Reaction 2 NrB 
 for n=9:15
 liquid_rates(n) = gas_rates(n).*exp(-(Gsolv_TS(n)-Gsolv_reactants(1)-Gsolv_reactants(4))/0.008314/T_reaction);
 reaction_rate_constants(2)=reaction_rate_constants(2)+liquid_rates(n);
 end
 
 %Reaction 3 VrV 
 for n=16:20
 liquid_rates(n) = gas_rates(n).*exp(-(Gsolv_TS(n)-Gsolv_reactants(5)-Gsolv_reactants(6))/0.008314/T_reaction);
 reaction_rate_constants(3)=reaction_rate_constants(3)+liquid_rates(n);
 end
 
 %Reaction 4 VrB
 for n=21:25
 liquid_rates(n) = gas_rates(n).*exp(-(Gsolv_TS(n)-Gsolv_reactants(5)-Gsolv_reactants(4))/0.008314/T_reaction);
 reaction_rate_constants(4)=reaction_rate_constants(4)+liquid_rates(n);
 end
 
 %Reaction 5 BrB
 for n=26:28
 liquid_rates(n) = gas_rates(n).*exp(-(Gsolv_TS(n)-Gsolv_reactants(3)-Gsolv_reactants(4))/0.008314/T_reaction);
 reaction_rate_constants(5)=reaction_rate_constants(5)+liquid_rates(n);
 end
 
 %Reaction 6 BrN
 for n=29:34
 liquid_rates(n) = gas_rates(n).*exp(-(Gsolv_TS(n)-Gsolv_reactants(3)-Gsolv_reactants(2))/0.008314/T_reaction);
 reaction_rate_constants(6)=reaction_rate_constants(6)+liquid_rates(n);
 end
 
 %Reaction 7 BrV
 for n=35:38
 liquid_rates(n) = gas_rates(n).*exp(-(Gsolv_TS(n)-Gsolv_reactants(3)-Gsolv_reactants(6))/0.008314/T_reaction);
 reaction_rate_constants(7)=reaction_rate_constants(7)+liquid_rates(n);
 end
 
%write liquid rates to molStruct
molStruct(k).single_rate_constants = liquid_rates;

%compute overall rate constants for reactions
molStruct(k).rate_constants = reaction_rate_constants;

end



end