function results =  calcPhaseSplit(molStruct,T)

    beta = 0.5;
    R=8.314; %kJ/kmol/K

    n_comp = 3;
    nitermax = 10000;
    TOL_mu = 1e-9;
    TOL_beta = 1e-9;
    TOL_gbeta = 1e-9;
    beta0 = beta;
    
    
	zF = [0 , 0.7 , 0.3]; % Startwerte f�r die Gesamtzusammensetzung
	x0 = [0 , 1 , 0];      % Startwert f�r die Zusammensetzung einer Phase %#JSc: For LLE in Water systems -> Use Water phase = 0.99 pure as intial guess
    
    alpha = zeros(3);
    tau = zeros(3);
    
    for i = 1:3
        for j = 1:3
            if i ~= j
                alpha(i,j) = calcAlphaFromPar(T,molStruct.AlphaParam{i,j});
            else
                alpha(i,j) = 1;
            end
        end
    end
    
    for i = 1:3
        for j = 1:3
            if i ~= j
                tau(i,j) = calcTauFromPar(T,molStruct.TauParam{i,j});
            else
                tau(i,j) = 0;
            end
        end
    end
        
    [ x_calc(1,:), y_calc(1,:), ~ ] = LLE_NRTL_calc( zF(1,:), x0, n_comp, tau, alpha, nitermax, TOL_mu, TOL_beta, TOL_gbeta, R, T, beta0 );  
      
    if y_calc(1) > x_calc(1)
        results = x_calc;
    else
        results = y_calc;
    end
   
end

function tau= calcTauFromPar(T,NRTLpar)


tau = NRTLpar(2)./T + NRTLpar(3).*log(T)  + NRTLpar(4) .* T + NRTLpar(1);

end

function alpha = calcAlphaFromPar(T,NRTLPar)

alpha = NRTLPar(1) +NRTLPar(2) .*T;

end