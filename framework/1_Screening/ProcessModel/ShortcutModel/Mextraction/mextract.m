function [result,ok] = mextract(tau, alpha, tc , pExtr, molStruct, xF, purity)
    %% Implemented by A. Demuth, bachelor thesis supervised by Jan Scheffczyk @LTT, RWTH Aachen University    
    % Initialize mextraction:
    system = [pwd, '/ProcessModel/ShortcutModel/Mextraction/dummy.dat']; % Folder for temporary files  
    %system = ['Mextraction/dummy.dat'];
    hpd = mextraction('init',system);
   
     for i = 1:3
        for j=1:3
            if i<j
                ok = mextraction('set_alpha', hpd, alpha(i,j), i-1, j-1) ;
                ok = mextraction('set_tau', hpd, tau(i,j), i-1, j-1) ;
                ok = mextraction('set_tau', hpd, tau(j,i), j-1,i-1) ;
            end
        end
    end

	%% Call extraction shortcut by C. Redepenning
    pressure = pExtr;
    temperature = tc;
    xLLE = calcPhaseSplit(molStruct,tc+273.15);
    xS = [0.0, xLLE(2),  xLLE(3)];
    [extrResult,ok] = mextraction('EXMIN',hpd,temperature,pressure,xF,xS,purity,1,2);
    clear mex
    if(ok>0)     
        result{1} = extrResult.S; % S = (S/F)_min because F=1mol/s
        result{2} = extrResult.xen; % Final composition in extract (stage N)
        result{3} = extrResult; % Complete Mextraction ouput in struct
    else 
        result = {NaN, NaN, NaN, NaN, NaN};
    end
end