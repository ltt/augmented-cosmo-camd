function [reflux, f, b, d,Qreb] = rbm(antoineParam, alphaParam, tauParam, xExtr, specs)
%% Implemented by A. Demuth, bachelor thesis supervised by Jan Scheffczyk @LTT, RWTH Aachen University.	
    %% Initiate mex
    %system = [pwd, '/ProcessModel/ShortcutModel/RBM/dummy.dat']; % Folder for temporary files 
    system = ['dummy.dat'];
    hdl=mexrbm('init',['',system]); 
    
    % Initialize solution vector
    reflux = NaN;
    Qreb = NaN;
    f.z = NaN;
    f.menge = NaN;
    b.z = NaN;
    b.menge = NaN;
    d.z = NaN;
    d.menge = NaN;
    
    %% Set Antoine parameters
    % Change Antoine equation parameters from ln(p)= A-B/(T+C) in mbar (COSMOtherm 
    % spec) to ln(p)= A+B/(T+C) in Pa
    % HMF and water parameters are the same for every calculation
    
    c=[1,2,3];
    for i=1:length(c)
        for j=1:9
            switch j
                case 1
                    antc(j)=antoineParam{i,1}(j)+log(100);
                case 2
                    antc(j)=-antoineParam{i,1}(j);
                case 3
                    antc(j)=antoineParam{i,1}(j);
                case 9
                    antc(j)=9000;
                otherwise
                    antc(j)=0;
            end
        end
            ok=mexrbm('set_antoine',hdl,antc(1),antc(2),antc(3),antc(4),antc(5),antc(6),antc(7),antc(8),antc(9),c(1));
    end
    
    %% Set NRTL parameters
    % Sets the NRTL parameters alpha and tau for the temperature 273,15K
    for i=1:length(c)
        for j=1:length(c)
            if i~=j
                alpha=alphaParam{i,j}(1)+273.15*alphaParam{i,j}(2);
                alphat=alphaParam{i,j}(2);
                for k=1:4
                    tau12(k)=tauParam{i,j}(k);
                    tau21(k)=tauParam{j,i}(k);
                end
                ok = mexrbm('set_nrtl_alpha',hdl,alpha,alphat,i,j) ;
                ok = mexrbm('set_nrtl_tau',hdl,tau12(2),tau12(1),tau12(3),tau12(4),i,j) ;
                ok = mexrbm('set_nrtl_tau',hdl,tau21(2),tau21(1),tau21(3),tau21(4),j,i) ;
            end
        end
    end
    
    %% Calculate results
    % Azeo
    p = specs.pRBM;
    azeo = mexrbm('azeo',hdl,[],[],[],[],p);
    azeoCell = {azeo.x};
    
    for i=3:6
        if length(azeoCell)>i && azeoCell{i+1}(1)>0 
            reflux='azeotrope with hk';
            return;
        end
    end
 
    a = size(azeo);
    if a(2) == 0
        reflux = 'ERROR';
        clear mex
        return;
    end
    clear a;
    
    % Check if HMF is hk
    if azeo(1,1).t < azeo(1,3).t %azeo (1,1) = solute, % azeo(1,3) = solvent
        reflux = NaN;
        disp('Solvent is heavy key => not feasible');
        return;
    end
    
    % Molar balances, specification: Sharp split with hk (HMF) pure in bottom
    f.menge = 1 ;
    f.z = xExtr ; % xExtr = [ x_HMF,Extr, x_H2O,Extr, x_Solv,Extr ]
    %     disp(sum(f.z));
    f.vfrac = 0 ;
    
    d.menge = f.menge*(1-xExtr(1)) ;
    d.z = [0, xExtr(2)/(1-xExtr(1)), xExtr(3)/(1-xExtr(1))] ;
    %     disp(sum(d.z));
    d.vfrac = 0 ;

    b.menge = f.menge*xExtr(1) ;
    b.z = [1 0 0] ;
    %     disp(sum(b.z));
    b.vfrac = 0 ;

    D_result = mexrbm('CALC_RBM',hdl,p,azeo,b.z,d.z,d.menge/f.menge,specs.TcRBM,0) ;
    reflux = D_result(6);
    Qreb = D_result(1);


    clear mex;
end