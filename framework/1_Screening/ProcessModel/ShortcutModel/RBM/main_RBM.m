function molStruct = main_RBM(molStruct, specs)
 %% Implemented by A. Demuth, bachelor thesis supervised by Jan Scheffczyk @LTT, RWTH Aachen University.	
 %% Load structures with parameters, call mextraction and rbm shortcuts
    
% Set tempFolder
    tempFolder = [pwd, '/ProcessModel/ShortcutModel/RBM/temp/']; % Folder for temporary files  

% Import COSMOdata screening results
%load([tempFolder, name, '.mat'], 'molStruct');

    % Preallocate fields for output of RBM
    for i=1:length(molStruct)
        molStruct(i).Rmin = NaN; 
        molStruct(i).RBM = NaN;
        molStruct(i).Qreb = NaN;
    end    

    % Calculate Results of RBM
    for i=1:length(molStruct)

        % Find alpha and tau parametrization for NRTL
        antoineParam    = molStruct(i).AntoineParam;
        alphaParam      = molStruct(i).AlphaParam;
        tauParam        = molStruct(i).TauParam;
%         dHvapPar        = molStruct(i).dHvapPar;

        % Display info
        if strcmp(specs.Mode, 'screening')
            disp(['Compound No. ', num2str(i), '\', num2str(length(molStruct)), ': ', molStruct(i).name]);
        end
        if ~isempty(molStruct(i).SFmin) && ~isnan(molStruct(i).SFmin)
    %         if isnumeric(molStruct(i).SFmin)            
            [reflux, f, b, d,Qreb] = rbm(antoineParam, alphaParam, tauParam, molStruct(i).xExtr, specs);
            molStruct(i).Rmin = reflux;
            molStruct(i).RBM = [f.menge, d.menge, d.z];
            molStruct(i).Qreb = [Qreb,Qreb*molStruct(i).extrResult.EN];
            clear mex;
        end
    end   
% Save
%save([tempFolder, name, '.mat'], 'molStruct');

end