function runScript_ExergyLoss(name)

%% General
%%

% Choose objective function
objfunc = 'FormicAcid';

% components
%components = {'h2', 'co2', 'co', 'h2o', 'methylformate', 'methanol', 'Solvent_X'};
components = {'co2', 'h2', 'co', 'h2o', 'dimethylformamide', 'dimethylamine', 'Solvent_X'};
components = {'methane', 'co2', 'h2', 'co', 'h2o', 'dimethylformamide', 'dimethylamine', 'Solvent_X'};

% Chosse COSMO-RS parametrization
parametrization = 'TZVP'; % 'TZVP' or 'TZVPD-FINE' or 'COSMOfrag' 

% Set NRTL Temperature vector
T_start = 25; % /C
T_end = 200; % /C
T_interval = 11; % Number of points in T interval

% Set storage molecule for Superstructre optimization
if strcmp(components{6}, 'methylformate')
    storage = 'MeF';
elseif strcmp(components{6}, 'dimethylformamide')
    storage = 'DMF';
end

% Specify temperatures for T_sat constraint
T_boil_min = 25; % Celsius or 'no_T_min'
T_boil_max = 'no_T_max'; % Celsius or 'no_T_max'

% Conformer treatment
nconf_max = 10; 

maindir = pwd;

%% Save specifications in specs-struct
specs = struct;
specs.Objfunc = objfunc;
specs.COSMOpara = parametrization;
specs.Mode = 'screening'; %or 'optimization'
specs.components = components;
specs.compounds = components;
specs.T_boil_min = T_boil_min;
specs.T_boil_max = T_boil_max;
specs.T_start = T_start;
specs.T_end = T_end;
specs.T_interval = T_interval;
specs.nconf = zeros(1,length(components));
specs.nconf(:) = nconf_max;
specs.composition = 0:0.1:1;
specs.storage = storage;

% Load paths struct
load('Paths/user.mat')
specs.paths = paths;
specs.maindir = maindir;


% Start parpool for parallel computation:
%pool = parpool;

% Display job
display_text = ['RunScript in ', specs.Mode, ' Mode for ', specs.Objfunc, '\n\n'];
fprintf (display_text);

%% Load struct from Input   
% Working variable for struct is molStruct
% Load a struct as 'molStruct'
temp_struct = load([maindir,'/Input/',name]);
name_of_loaded_struct = fieldnames(temp_struct);
molStruct = temp_struct.(name_of_loaded_struct{1});


%% Get properties for each species in molStruct
%%
addpath([maindir,'/PropertyPrediction/']);
molStruct = main_cosmoData(molStruct,specs.Objfunc,specs);

molStruct = main_cosmoData(molStruct,'T_boil_con',specs);

%% Temporary save
save([maindir,'/Input/',name, '_PP_done'],'molStruct')
rmpath([maindir,'/PropertyPrediction/'])


%% Execute SuperStructureOptimization
%%
%chdir([maindir,'/ProcessModel/SuperStructureOptimization']);
%molStruct = optimizeProcess( molStruct, specs );
%chdir(maindir);



%% Save results and quit
%%

molStruct(1).specs = specs;

save([maindir,'/Output/',name, '_', storage],'molStruct')
delete([maindir,'/Input/',name, '_PP_done', '.mat'])

datetime
end
