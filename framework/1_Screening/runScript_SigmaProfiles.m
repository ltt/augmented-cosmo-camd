function runScript_SigmaProfiles(name)

%% General
%%

% Choose objective function
objfunc = 'SigmaProfile';

% For solute please use name in form of COSMObase filename
compounds{1} = 'Solvent X';

% Choose COSMO-RS parametrization
parametrization = 'TZVP'; % 'TZVP' or 'TZVPD-FINE' or 'COSMOfrag' 

% Conformer treatment for solute
nconf_max = 10; 

% Number and distribution of segments for sigma-Descriptors (Zhou 2015: 8, Zhou 2014: 12)
sigmasegments = [[1,12];[13,20];[21,25];[26,30];[31,35];[36,40];[41,48];[49,60]];
%sigmasegments = [[1,5];[6,10];[11,15];[16,20];[21,25];[26,30];[31,35];[36,40];[41,45];[46,50];[51,55];[56,60]];

maindir = pwd;

%% Save specifications in specs-struct
specs = struct;
specs.Objfunc = objfunc;
specs.COSMOpara = parametrization;
specs.Mode = 'screening'; %or 'optimization'
specs.compounds = compounds;
specs.nconf = zeros(1,length(compounds));
specs.nconf(:) = nconf_max;
specs.sigmasegments = sigmasegments;
% Load paths struct
load('Paths/user.mat')
specs.paths = paths;
specs.maindir = maindir;

% Start parpool for parallel computation:
%pool = parpool;

% Display job
display_text = ['RunScript in ', specs.Mode, ' Mode for ', specs.Objfunc, '\n\n'];
fprintf (display_text);

%% Load struct from Input   
% Working variable for struct is molStruct
% Load a struct as 'molStruct'
temp_struct = load([maindir,'/Input/',name]);
name_of_loaded_struct = fieldnames(temp_struct);
molStruct = temp_struct.(name_of_loaded_struct{1});


%% Get properties for each species in molStruct
%%
addpath([maindir,'/PropertyPrediction/']);

molStruct = main_cosmoData(molStruct,specs.Objfunc,specs);

rmpath([maindir,'/PropertyPrediction/'])


%% Get sigma-Descriptors (as in Zhou 2015)
%%
addpath([maindir,'/ProcessModel/SigmaDescriptors/']);

molStruct = calculateDescriptors(molStruct,specs);

rmpath([maindir,'/ProcessModel/SigmaDescriptors/'])


%% Save results and quit
%%

molStruct(1).specs = specs;

save([maindir,'/Output/',name],'molStruct')

datetime
end
