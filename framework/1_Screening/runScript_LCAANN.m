function runScript_LCAANN(name)

%% General
%%

% Choose objective function
objfunc = 'LCA ANN';

% For solute please use name in form of COSMObase filename
compounds{1} = 'Solvent X';

% Choose COSMO-RS parametrization
parametrization = 'TZVP'; % 'TZVP' or 'TZVPD-FINE' or 'COSMOfrag' 

% Conformer treatment for solute
nconf_max = 10; 

% Number and distribution of segments for sigma-Descriptors (Zhou 2015: 8, Zhou 2014: 12)
sigmasegments = [[1,12];[13,20];[21,25];[26,30];[31,35];[36,40];[41,48];[49,60]];
%sigmasegments = [[1,5];[6,10];[11,15];[16,20];[21,25];[26,30];[31,35];[36,40];[41,45];[46,50];[51,55];[56,60]];

maindir = pwd;

%% Save specifications in specs-struct
specs = struct; 
specs.Objfunc = objfunc;
specs.COSMOpara = parametrization;
specs.Mode = 'screening'; %or 'optimization'
specs.compounds = compounds;
specs.nconf = zeros(1,length(compounds));
specs.nconf(:) = nconf_max;
specs.composition = 0:0.1:1;
specs.sigmasegments = sigmasegments;
% Load paths struct
load('Paths/user.mat')
specs.paths = paths;
specs.maindir = maindir;

% Start parpool for parallel computation:
%pool = parpool;

% Display job
display_text = ['RunScript in ', specs.Mode, ' Mode for ', specs.Objfunc, '\n\n'];
fprintf (display_text);

%% Load struct from Input   
% Working variable for struct is molStruct
% Load a struct as 'molStruct'
temp_struct = load([maindir,'/Input/',name]);
name_of_loaded_struct = fieldnames(temp_struct);
molStruct = temp_struct.(name_of_loaded_struct{1});


%% Get properties for each species in molStruct
%%
addpath([maindir,'/PropertyPrediction/']);

%% 1. Sigma Profiles and Moments and currently dHf
molStruct = main_cosmoData(molStruct,'SigmaProfile',specs);

%Get sigma-Descriptors (as in Zhou 2015)
addpath([maindir,'/ProcessModel/SigmaDescriptors/']);
molStruct = calculateDescriptors(molStruct,specs);
rmpath([maindir,'/ProcessModel/SigmaDescriptors/']);

%% 2. T boil
specs.T_boil_min = 25;
specs.T_boil_max = 'no_T_max';

molStruct = main_cosmoData(molStruct,'T_boil_con',specs);


%% 3. x_LLE water
tempspecs = specs;
tempspecs.compounds = {'h2o', 'SolventX'};
tempspecs.LLEcomp = '1 2';
tempspecs.T = 25;
tempspecs.nconf = zeros(1,length(tempspecs.compounds));
tempspecs.nconf(:) = nconf_max;

molStruct = main_cosmoData(molStruct,'LLE',tempspecs);
[molStruct.x_LLE_water] = molStruct.x_LLE;

%% 4. log P(OW)
tempspecs = specs;
tempspecs.compounds = {'h2o', '1-octanol', 'SolventX'};
tempspecs.LLEcomp = '1 2';
tempspecs.T = 25;
tempspecs.logP = 'mol-based';
tempspecs.nconf = zeros(1,length(tempspecs.compounds));
tempspecs.nconf(:) = nconf_max;

% Call LLE and logP scripts
molStruct = main_cosmoData(molStruct,'logP',tempspecs);
addpath([maindir,'/ProcessModel/logP/']);
%molStruct = logPScore(molStruct,tempspecs);
rmpath([maindir,'/ProcessModel/logP/']);

%% 5. NRTL and Antoine
tempspecs = specs;
% Set NRTL Temperature vector
tempspecs.T_start = 0; % /C
tempspecs.T_end = 200; % /C
tempspecs.T_interval = 11;  % Number of points in T interval 
tempspecs.compounds = {'dihydro-5-methyl-2(3h)-furanone', 'h2o', 'SolventX'};
tempspecs.nconf = zeros(1,length(tempspecs.compounds));
tempspecs.nconf(:) = nconf_max;

molStruct = main_cosmoData(molStruct,'Mex-RBM',tempspecs);

%% 6. Hvap
molStruct = ClausiusClapeyron(molStruct,298.15);

%% 7. pS (saturation pressure at 25�C)
molStruct = SaturationPressure(molStruct,298.15);

%% 8. Groups
addpath([maindir,'/ProcessModel/MolGroups/']);
molStruct = getGroups(molStruct);
rmpath([maindir,'/ProcessModel/MolGroups/']);

%% 9. Process Model

% Extraction column
specs.pExtr = 1.013; %/bar
specs.TcExtr = 25; %/C
specs.optimizeTExtr = 0;

% Distillation column
specs.TcRBM = 25;
specs.pRBM = 1; %/bar

addpath([maindir,'/ProcessModel/ShortcutModel/']);
molStruct = ScoreMexRBM_tempfix(molStruct,specs);
rmpath([maindir,'/ProcessModel/ShortcutModel/']);


%% Save results and quit
%%

molStruct(1).specs = specs;

save([maindir,'/Output/',name, '_full'],'molStruct')

for k=1:length(molStruct)
    if ~isnan(molStruct(k).x_LLE_water)
        molStruct(k).x_LLE_aq_water=molStruct(k).x_LLE_water(3);
        molStruct(k).x_LLE_aq_solvent=molStruct(k).x_LLE_water(4);
        molStruct(k).x_LLE_org_water=molStruct(k).x_LLE_water(1);
        molStruct(k).x_LLE_org_solvent=molStruct(k).x_LLE_water(2);
    end
    if ~isnan(molStruct(k).Qreb)
        molStruct(k).Qreb=molStruct(k).Qreb(2);
    else
        molStruct(k).Qreb = [];
    end
    molStruct(k).sigmaM = molStruct(k).weightedMoments.sigmaM;
    molStruct(k).Hbacc = molStruct(k).weightedMoments.Hbacc;
    molStruct(k).Hbdon = molStruct(k).weightedMoments.Hbdon;
end

% Save just the descriptorsj
molStruct = rmfield(molStruct, {'specs', 'ConfNum', 'wconf', 'sigmaProfiles', 'Moments' ...
    'E_cosmo', 'T_boil_ok', 'x_LLE', 'MW_solvent', 'gammaI', 'gammaII', 'AntoineParam', 'TauParam', 'AlphaParam', ...
    'err_NRTL', 'SFmin', 'xExtr', 'extrResult', 'Rmin', 'RBM', 'x_LLE_water', 'weightedMoments'});

save([maindir,'/Output/',name, '_descriptors'],'molStruct')

datetime
end

function molStruct = ClausiusClapeyron(molStruct,T)

for i=1:length(molStruct)
    
    AntoineParam = molStruct(i).AntoineParam{3};
    R = 8.314;
    molStruct(i).Hvap = AntoineParam(2)./(AntoineParam(3) + T).^2 .* R .* T.^2 * 1000;
    
end

end


function molStruct = SaturationPressure(molStruct,T)

for i=1:length(molStruct) % consider only liquids
   
    AntoineParam = molStruct(i).AntoineParam{3};
    molStruct(i).pS = exp(AntoineParam(1)-AntoineParam(2)/(T+AntoineParam(3))); 
    
end

end
