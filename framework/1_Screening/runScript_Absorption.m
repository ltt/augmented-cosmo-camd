function runScript_Absorption(name)

%% General
%%

% Choose objective function
objfunc = 'Absorption';

% For solute please use name in form of COSMObase filename
%compounds = {'methane', 'h2', 'co2', 'co', 'h2o', 'dimethylformamide', 'dimethylamine', 'Solvent_X'};
compounds = {'h2', 'co', 'co2', 'Solvent_X'};


% Chosse COSMO-RS parametrization
parametrization = 'TZVP'; % 'TZVP' or 'TZVPD-FINE' or 'COSMOfrag' 

% Specify temperatures for T_sat constraint
T_boil_min = 25; % Celsius or 'no_T_min'
T_boil_max = 'no_T_max'; % Celsius or 'no_T_max'

% Set NRTL Temperature vector
T_start = -50; % /C
T_end = 100; % /C 
T_interval = 11; % Number of points in T interval    

% Set temperature and pressure of extraction column
pExtr = 1.013; %/bar
% Fixed temperature
optimizeTExtr = 0;
TcExtr = 25; %/C
% Variable temperature = specify temperature bounds
%optimizeTExtr = 1;
%Tc_Extr_min = 20; %/C
%Tc_Extr_max = 80; %/C

% Set temperature and pressure for RBM:
TcRBM = 25; %/C
pRBM = 1;%/bar

% % Set temperature and pressure for absorption column:
% T_abs = 25; %/C
% p_abs = 1; %/bar
% feed_gas = [1  1  0 ]; % /mol/s
% feed_solvent = 1; % /mol/s
% T_abs_feed = 25; %/C
% T_abs_solv = 25; %/C
% iSolute = 3;
% iSolvent = 7;
% 
% % Reactor temperature
% T_reactor = 25; %/�C
% p_reactor = 1;  % /bar
%   
% deltaGR = 9920; % [J/mol] (Values from Jens 2016)
% xi0 = 0.9;
% v = [-1, -1, 0, 1, 1, -1, 0];
% phases = 'VLLE';
% firstliquid = 1; %[1,3];
% reactingphase = 1; % 1: extraction solvent, 2: catalyst solvent
% liquids = 4:7;
% type = 'VLLEReactor';
% %name = 'StorageReactor';
% species = 1:7;
% 
% 
% % 2nd block: reforming reactor
% type = 'RStoic';
% %name = 'ReformingReactor';
% species = 3:7;
% inflow = [1,3]; %[1,3]: extraction solvent, [1,2]: catalyst solvent
% v = [1, 0, -1, 1, 0];
% turnover = 1; % full turnover of storage molecule
% targetcompound = 3;
% 
% % 3rd block: purification
% type = 'RBM';
% %name = 'DistColumn1';
% species = 4:7;
% inflow = [2,1];
% p = 1;
% tf = 0;
% compsontop = [];
% 
% % 4th block: purification
% type = [];
% %name = 'DistColumn2';
% species = 4:7;
% inflow = [];
% p = 1;
% tf = 25;

% Conformer treatment
nconf_max = 10;

maindir = pwd;

%% Save specifications in specs-struct
specs = struct;
specs.Objfunc = objfunc;
specs.COSMOpara = parametrization;
specs.Mode = 'screening'; %or 'optimization'
specs.compounds = compounds; 
specs.T_boil_min = T_boil_min;
specs.T_boil_max = T_boil_max;
specs.T_start = T_start;
specs.T_end = T_end;
specs.T_interval = T_interval;
if optimizeTExtr 
    specs.Tc_Extr_min = Tc_Extr_min; %/C
    specs.Tc_Extr_max = Tc_Extr_max; %/C
else
    specs.TcExtr = TcExtr;
end
% specs.pExtr = pExtr;
% specs.optimizeTExtr = optimizeTExtr;
% specs.TcRBM = TcRBM;
% specs.pRBM = pRBM;
% specs.T_abs = T_abs;
specs.nconf = zeros(1,length(compounds));
specs.nconf(:) = nconf_max;
specs.composition = 0:0.1:1;
% specs.feed = feed;
% Load paths struct
load('Paths/user.mat')
specs.paths = paths;
specs.maindir = maindir;


% Start parpool for parallel computation:
%pool = parpool;

% Display job
display_text = ['RunScript in ', specs.Mode, ' Mode for ', specs.Objfunc, '\n\n'];
fprintf (display_text);

%% Load struct from Input   
% Working variable for struct is molStruct
% Load a struct as 'molStruct'
temp_struct = load([maindir,'/Input/',name]);
name_of_loaded_struct = fieldnames(temp_struct);
molStruct = temp_struct.(name_of_loaded_struct{1});


%% Get properties for each species in molStruct
%%
addpath([maindir,'/PropertyPrediction/']);

molStruct = main_cosmoData(molStruct,'T_boil_con',specs);

molStruct = main_cosmoData(molStruct,specs.Objfunc,specs);

% Temporary save
save([maindir,'/Input/',name, '_PP_done'],'molStruct')
rmpath([maindir,'/PropertyPrediction/'])

%% Execute RBM and Mextraction
%%
% addpath([maindir,'/ProcessModel/ShortcutModel/']);
% if optimizeTExtr
%     molStruct = ScoreMexRBM_tempopt( molStruct, specs );
% else
%     molStruct = ScoreMexRBM_tempfix( molStruct, specs );
% end
% 
% rmpath([maindir,'/ProcessModel/ShortcutModel/']);


%% Save results and quit
%%

molStruct(1).specs = specs;

save([maindir,'/Output/',name, '_GVL'],'molStruct')
delete([maindir,'/Input/',name, '_PP_done', '.mat'])

datetime
end
