function runScript_kinetics(name)

%% General
%%

% Choose objective function
objfunc = 'kinetics_liquid';

% Specify reactants, transition states
%
reactants{1} = 'NIPAM-R';
reactants{2} = 'NIPAM';

reactants{3} = 'BIS-R';
reactants{4} = 'BIS';

reactants{5} = 'VCL-R';
reactants{6} = 'VCL';

%Reaction 1
    TS{1}= 'Nr+N_TS';
    TS{2}= 'Nr+N_TS2';
    TS{3}= 'Nr+N_TS3';
    TS{4}= 'Nr+N_TS4';
    TS{5}= 'Nr+N_TST';
    TS{6}= 'Nr+N_TST2';
    TS{7}= 'Nr+N_TST3';
    TS{8}= 'Nr+N_TST4';


%Reaction 2
    TS{9}= 'Nr+B_TS';
    TS{10}= 'Nr+B_TS2';
    TS{11}= 'Nr+B_TS3';
    TS{12}= 'Nr+B_TS4';
    TS{13}= 'Nr+B_TST';
    TS{14}= 'Nr+B_TST2';
    TS{15}= 'Nr+B_TST4';


%Reaction 3
    TS{16}= 'Vr+V_TS';
    TS{17}= 'Vr+V_TS2';
    TS{18}= 'Vr+V_TST';
    TS{19}= 'Vr+V_TST2';
    TS{20}= 'Vr+V_TST3';

%Reaction 4
    TS{21}= 'Vr+B_TS2';
    TS{22}= 'Vr+B_TS3';
    TS{23}= 'Vr+B_TS4';
    TS{24}= 'Vr+B_TST';
    TS{25}= 'Vr+B_TST2';

%Reaction 5
    TS{26}= 'Br+B_TS';
    TS{27}= 'Br+B_TS2';
    TS{28}= 'Br+B_TST';

% %Reaction 6
% TS{29}= 'Br+N_TS2';
% TS{30}= 'Br+N_TS4';
% %TS{31}= 'Br+N_TST';
% TS{31}= 'Br+N_TST2';
% TS{32}= 'Br+N_TST3';
% TS{33}= 'Br+N_TST4';
% TS{34}= 'Br+N_TST5';
%
% %Reaction 7
% TS{35}= 'Br+V_TS2';
% TS{36}= 'Br+V_TS3';
% TS{37}= 'Br+V_TS4';
% TS{38}= 'Br+V_TST';

%Reaction 6
    TS{29}= 'Br+N_TS2';
    TS{30}= 'Br+N_TS4';
    TS{31}= 'Br+N_TST';
    TS{32}= 'Br+N_TST2';
    TS{33}= 'Br+N_TST3';
%TS{34}= 'Br+N_TST4';
    TS{34}= 'Br+N_TST5';

%Reaction 7
    TS{35}= 'Br+V_TS2';
    TS{36}= 'Br+V_TS3';
    TS{37}= 'Br+V_TS4';
    TS{38}= 'Br+V_TST';

% create compounds matrix

for k=1:(length(reactants))
    components{k} = reactants{k};
end

for k=(length(reactants)+1):(length(reactants)+length(TS))
    components{k} = TS{(k-length(reactants))};
end

components{end+1} = 'Solvent X'; %all Solvents

% Kinetic parameters
gasphase_rate = [7.30E+03;
7.21E+01;
4.59E+01;
2.08E+02;
3.16E+03;
5.40E+02;
5.85E+02;
1.85E+03;
4.01E+02;
8.27E+03;
6.53E+02;
1.32E+02;
2.89E-01;
1.00E+03;
1.95E+01;
1.68E+00;
6.89E-02;
7.25E-02;
1.79E+00;
6.44E+00;
4.61E+01;
2.08E+01;
4.90E+02;
1.51E+03;
9.23E+01;
3.23E+02;
9.16E-01;
7.35E+01;
1.05E+02;
1.32E+03;
4.79E-01;
3.68E+00;
2.42E+00;
1.26E+01;
1.02E+00;
3.98E+00;
3.28E+00;
3.34E+01
];

% Reaction temperature in K
T_reaction_value = 343;

% reaction mixture composition
mole_frac_reactants = zeros(1,length(reactants)); %fraction of reactants in mixture; 0 for infinite dilution
mole_frac_TS = zeros(1,length(TS)); %fraction of molecules in transition state (zero)
mole_frac_solvent = 1; %fraction of solvent in mixture
reaction_mixture_composition = [mole_frac_reactants mole_frac_TS mole_frac_solvent];

% Conformer treatment
nconf_max = 10; s


%% Save parameters in specs-struct

% Define struct carrying specifications
specs = struct;
specs.Objfunc = objfunc;
specs.components = components;
specs.COSMOpara = parametrization;
specs.Mode = 'screening';

% Write solvent specific compounds
specs.compounds = components;

% Write objective function specific specifications
specs.reactants = reactants;
specs.transition_states = TS;
specs.gasphase_rate = gasphase_rate;
specs.T_reaction = T_reaction_value;
specs.n_reactants = length(reactants);
specs.n_TS = length(TS);
specs.reaction_mixture = reaction_mixture_composition;

% Constraints
specs.T_boil_min = T_boil_min;
specs.T_boil_max = T_boil_max;

% Conformer treatment for solute
specs.nconf = zeros(1,length(compounds));
specs.nconf(:) = nconf_max;

% Load paths struct
load('Paths/user.mat')
specs.paths = paths;
maindir = pwd;
specs.maindir = maindir;

% Start parpool for parallel computation:
%pool = parpool;

% Display job
display_text = ['RunScript in ', specs.Mode, ' Mode for ', specs.Objfunc, '\n\n'];
fprintf (display_text);

%% Load struct from Input   
% Working variable for struct is molStruct
% Load a struct as 'molStruct'
temp_struct = load([maindir,'/Input/',name]);
name_of_loaded_struct = fieldnames(temp_struct);
molStruct = temp_struct.(name_of_loaded_struct{1});


%% Get properties for each species in molStruct
%%
addpath([maindir,'/PropertyPrediction/']);

molStruct = main_cosmoData(molStruct,specs.Objfunc,specs);

% Temporary save
save([maindir,'/Input/',name, '_PP_done'],'molStruct')
rmpath([maindir,'/PropertyPrediction/'])

%% Calculate rate constants
%%
addpath([maindir,'/ProcessModel/Kinetics/']);

molStruct = get_kinetics_l( molStruct, specs );


%% Save results and quit
%%

molStruct(1).specs = specs;

save([maindir,'/Output/',name],'molStruct')
delete([maindir,'/Input/',name, '_PP_done', '.mat'])

datetime
end
