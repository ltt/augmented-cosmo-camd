%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%                   COSMO-CAMD Solvent Screening
%
%% Developed at Chair of Technical Thermodynamics, RWTH Aachen University
% 
%% August, 2017
%
% For information and questions please contact Jan Scheffczyk (jan.scheffczyk@ltt.rwth-aachen.de)
% or Lorenz Fleitmann (lorenz.fleitmann@ltt.rwth-aachen.de)
%% Please run this script to start the Graphical User Interface.
%
%
%% If parallel computing is desired, start parpool here
%
%pool = parpool(4);
%
%% Execute GUI for COSMO-CAMD Screening
addpath('GUI');
GUI;

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%