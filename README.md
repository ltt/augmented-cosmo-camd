# Augmented COSMO-CAMD Framework

## Project description
This project provides the source code for the augmented COSMO-CAMD framework, a warm-started genetic algorithm for CAMD, 
along with its application to two case studies. 
It is associated with the publication [Fine-Tuning a Genetic Algorithm for Computer-Aided Molecular Design: 
A Screening-Guided Warm Start](https://scholar.google.de/) (see the Referencing section below for more details).
 
The framework consists of three phases: Screening, Fragmentation and Design. 
The schematic description of the framework is presented as follows : 

![](framework.png)

## Project structure
The entire project is structured as follows:
```
Augmented COSMO-CAMD framework
|-- documentation
|-- framework
    |-- 1_Screening*
    |-- 2_Fragmentation
    |-- 3_Design*
|-- case_studies**
    |-- case_study_01
        |-- cold vs direct warm start
            |-- case 1_cold start: [1 2 3 4 5 6]
            |-- case 2_direct warm start: [1 2 3 4 5 6]
        |-- fine_tuned warm start
            |-- case 1_T40-without-H: [1 2 3 4 5 6]
            |-- case 2_T40-without-X: [1 2 3 4 5 6]
            |-- case 3_T40-with-H: [1 2 3 4 5 6]
            |-- case 4_T40-with-X: [1 2 3 4 5 6]
            |-- case 5_T50-without-H: [1 2 3 4 5 6]
            |-- case 6_T50-without-X: [1 2 3 4 5 6]
            |-- case 7_T50-with-H: [1 2 3 4 5 6]
            |-- case 8_T50-with-X: [1 2 3 4 5 6]
            |-- case 9_T500-without-H: [1 2 3 4 5 6]
            |-- case 10_T500-without-X: [1 2 3 4 5 6]
            |-- case 11_T500-with-H: [1 2 3 4 5 6]
            |-- case 12_T500-with-X: [1 2 3 4 5 6]
    |-- case_study_02
        |-- case 1_benchmark: [1 2 3 4 5 6]
        |-- case 2_augmented: [1 2 3 4 5 6]
|-- README.md
|-- LICENSE.txt
|-- .gitignore
```
*Note that a successful execution of the Screening and Design phases requires MATLAB and COSMO-related commercial software.
  
**Note that the folders case_study_01 and case_study_02 contain the results of a total of 96 cases (about 10 GB). For brevity, their full contents are provided on Zenodo at https://doi.org/10.5281/zenodo.14841013.



## License
This project is licensed under the GNU General Public License (v3.0), for more information please refer to the [LICENSE](LICENSE.txt) file.


## Documentation and Support

The augmeneted COSMO-CAMD framwork is a further development of the COSMO-CAMD framwork that is based on various programming laguages 
and programs. The three phases can also be conducted independently. 

| Program           | Screening | Fragmentation | Design |
| ----------------- | :-------: | :-----------: | :----: |
| Matlab            |     x     |       -       |   -    |
| BIOVIA COSMOsuite |     x     |       -       |   -    |
| LEA3D/Perl        |     -     |       x       |   x    |


External proprietary programs: 
- Matlab (min version R2018b) (for Screening, Design part)
- BIOVIA COSMOsuite (former COSMOlogic), i.e. COSMOconf, Turbomole, COSMOtherm, COSMOfrag

Open source tool:
- LEA3D
- Parallel Forkmanager (see on CPAN) 
- Process model (black-box model)


## Usage
```
### Download the code

git clone --depth 1 git@git-ce.rwth-aachen.de:ltt/augmented-cosmo-camd.git

### The three phases are conducted separately. 

## Screening phase
# Please check the default setups of GUI, e.g., paths, case study parameters
cd framework/1_Screening/
load matlab

run runGUI.m 
# or
run run_anyScript.m 

## Fragmentation phase
# Please check the manual in the documentation folder
cd framework/2_Fragmentation/

run ./splitGroup.pl example_input.sdf


## Design phase 
# Please check LEA_run_options.pl in the workspace directory and the MATLAB struct user.mat 
  in workspace/Paths for setting the path variables to the external programs correctly.

cd framework/3_Design/
copy summary.out into the folder workspace_01/
copy collection.sdf into the folder LEA3D_v2_January2011/eDESIGN/LEGO/

run lea job.in > logfile


```
## Referencing
Wenn referencing the augmented COSMO-CAMD project in an academic context, please include the proper citations:

[[1] Fine-Tuning a Genetic Algorithm for Computer-Aided Molecular Design: A Screening-Guided Warm Start](https://scholar.google.de/)
(Note: Update coming soon!)

```bibtex
@article{wang202Xtbd,
  title={Fine-Tuning a Genetic Algorithm for Computer-Aided Molecular Design: A Screening-Guided Warm Start},
  author={Wang, Yifan and Fleitmann, Lorenz and Raßpe-Lange, Lukas and von der Assen, Niklas and Bardow, André and Leonhard, Kai},
  booktitle={tbd},
  volume={tbd},
  pages={tbd},
  year={tbd},
  publisher={tbd}
}
```
The current work builds upon two previous studies:
###### Screening phase
[[2] Massive, automated solvent screening for minimum energy demand in hybrid extraction–distillation using COSMO-RS](https://www.sciencedirect.com/science/article/pii/S0263876216303148)


```bibtex
@article{scheffczyk2016massive,
  title={Massive, automated solvent screening for minimum energy demand in hybrid extraction--distillation using COSMO-RS},
  author={Scheffczyk, Jan and Redepenning, Christian and Jens, Christian M and Winter, Benedikt and Leonhard, Kai and Marquardt, Wolfgang and Bardow, Andr{\'e}},
  journal={Chemical Engineering Research and Design},
  volume={115},
  pages={433--442},
  year={2016},
  publisher={Elsevier}
}
```

###### Design phase
[[3] COSMO-CAMD: A framework for optimization-based computer-aided molecular design using COSMO-RS](https://www.sciencedirect.com/science/article/pii/S0009250916302846)

```bibtex
@article{scheffczyk2017cosmo,
  title={COSMO-CAMD: A framework for optimization-based computer-aided molecular design using COSMO-RS},
  author={Scheffczyk, Jan and Fleitmann, Lorenz and Schwarz, Annett and Lampe, Matthias and Bardow, Andr{\'e} and Leonhard, Kai},
  journal={Chemical Engineering Science},
  volume={159},
  pages={84--92},
  year={2017},
  publisher={Elsevier}
}
```

## Other development and applications of the COSMO-CAMD framwork

Papers: 
- [COSMO-CAMPD](https://pubs.rsc.org/en/content/articlelanding/2018/me/c7me00125h): A framework for integrated design of molecules and process based on COSMO-RS
- [COSMO-CAMPED](https://onlinelibrary.wiley.com/doi/full/10.1002/cite.202200144): Solvent Design for an Extraction Distillation Considering Molecular, Process, Equipment, and Economic Optimization
- [COSMO-susCAMPD](https://www.sciencedirect.com/science/article/pii/S0009250921004280): Sustainable solvents from combining computer-aided molecular and process design with predictive life cycle assessment
- [Rx-COSMO-CAMD](https://pubs.acs.org/doi/10.1021/acs.iecr.9b03232): Computer-Aided Molecular Design of Reaction Solvents Based on Predictive Kinetics from Quantum Chemistry
- [Rx-COSMO-CAMPD](https://onlinelibrary.wiley.com/doi/full/10.1002/cite.202000112): Enhancing Reactions by Integrated Computer-Aided Design of Solvents and Processes based on Quantum Chemistry
- [CAT-COSMO-CAMPD](https://www.sciencedirect.com/science/article/pii/S0098135421002167?via%3Dihub): Integrated in silico design of catalysts and processes based on quantum chemistry



## Support
For models and methods developed in our GitLab repository, you are welcome to initiate discussions, report issues or bugs, and contribute to ongoing projects. Should you have any further questions or need assistance, please feel free to 
reach out via email at [yifan.wang@ltt.rwth-aachen.de](yifan.wang@ltt.rwth-aachen.de).


